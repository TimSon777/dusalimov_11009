﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        public static void Paint(bool[,] field)
        {
            Console.SetCursorPosition(0, 0);
            for (int y = 0; y < field.GetLength(1); y++)
            {
                for (int x = 0; x < field.GetLength(0); x++)
                {
                    var symbol = field[x, y] ? '#' : ' ';
                    Console.Write(symbol);
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            var random = new Random();
            var field = new bool[25, 25];
            int x = field.GetLength(1);
            int y = field.GetLength(0);
            for (int i = 0; i < y; i++)
                for (int j = 0; j < x; j++)
                    field[j, i] = random.Next(2) == 1 ? true : false;
            while (true)
            {
                Paint(field);
                Thread.Sleep(500);
                field = Game.NextStep(field);
            }
        }
    }
}
