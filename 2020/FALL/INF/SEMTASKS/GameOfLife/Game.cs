﻿using System;
using System.Globalization;

namespace GameOfLife
{
    public class Game
    {
        public static int ConsiderOfNeighborsTrue(int x, int y, bool[,] field)
        {
            // I don't realize some tests, 
            // but I do random value in each cell of field in file Program.Cs 
            int yCount = field.GetLength(1); // height of field
            int xCount = field.GetLength(0); // width of field
            int countOfLifeNeighbor = 0;
            for (int yNew = y - 1; yNew <= y + 1; yNew++) // check, which cells around regular cell have true value
            {
                if (yNew < 0 || yNew >= yCount)
                    continue;
                for (int xNew = x - 1; xNew <= x + 1; xNew++)
                {
                    if (xNew < 0 || xNew >= xCount)
                        continue;
                    if (yNew != y && xNew != x && field[xNew, yNew] == true)
                        countOfLifeNeighbor++; // consider this 
                }
            }
            return countOfLifeNeighbor;
        }

        public static bool[,] NextStep(bool[,] field)
        {
            int yCount = field.GetLength(1);
            int xCount = field.GetLength(0);
            bool[,] newField = new bool[yCount, xCount];
            int countOfLifeNeighbor;
            for (int y = 0; y < yCount; y++) // pass by each cell of field
            {
                for (int x = 0; x < xCount; x++)
                {
                    countOfLifeNeighbor = 0;
                    countOfLifeNeighbor = ConsiderOfNeighborsTrue(x, y, field);
                    if (countOfLifeNeighbor == 2)
                        newField[x, y] = field[x, y];
                    else if (countOfLifeNeighbor == 3)
                        newField[x, y] = true;
                    else
                        newField[x, y] = false;
                }
            }
            return newField;
        }
    }
}