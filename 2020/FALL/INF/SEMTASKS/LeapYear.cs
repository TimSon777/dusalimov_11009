﻿using System;

namespace LeapYear
{
    class Program
    {
        static int LY (int year)
        {
            return (year / 4) - (year / 100) + (year / 400); //Method considers count of leap years 
        }
        static void Main(string[] args)
        {
            int a, b;
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(LY(b) - LY(a - 1)); //Output count of leap years within the borders
        }
        
    }
}
