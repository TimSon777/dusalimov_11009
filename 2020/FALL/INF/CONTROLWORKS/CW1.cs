﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите координаты треугольника: x1, y1, x2, y2, x3, y3");
            int x1 = int.Parse(Console.ReadLine());
            int y1 = int.Parse(Console.ReadLine());
            int x2 = int.Parse(Console.ReadLine());
            int y2 = int.Parse(Console.ReadLine());
            int x3 = int.Parse(Console.ReadLine());
            int y3 = int.Parse(Console.ReadLine());
            Console.WriteLine(Triangle(x1, y1, x2, y2, x3, y3));

            Console.WriteLine("Введите десятичное число n");
            int n = int.Parse(Console.ReadLine());
            int countOfOne = BinDigit(n);
            Console.WriteLine(countOfOne);

            Console.WriteLine("Введите координаты центра: x0, y0");
            double x0 = double.Parse(Console.ReadLine());
            double y0 = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите радиус");
            double r = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите количество точек");
            int countN = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите координаты точек");
            double x;
            double y;
            double xMin = 0;
            double yMin = 0;
            double length = 1.7976931348623157E+308; ;
            for (int i = 0; i < n; i++)
            {
                x = double.Parse(Console.ReadLine());
                y = double.Parse(Console.ReadLine());
                double value = Math.Sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
                if (value < length && value - r > 0)
                {
                    xMin = x;
                    yMin = y;
                    length = Math.Sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
                }
            }
            Console.WriteLine("(" + xMin + "; " + yMin + ")");

            Console.WriteLine("Введите x");
            double xConst = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите e");
            double e = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите k");
            int k = int.Parse(Console.ReadLine());
            Console.WriteLine(Summ(xConst, e, k));

        }
        public static double Triangle(int x1 , int y1, int x2, int y2, int x3, int y3)
        {
            return 0.5 * Math.Abs((x1 - x2) * (y3 - y2) - (x3 - x2) * (y1 - y2));
        }

        public static int BinDigit (int n)
        {
            int count = 0;
            while (n > 0)
            {
                if (n % 2 != 0)
                    count++;
                n = n / 2; 
            }
            return count;
        }

        public static double Summ(double x, double e, int k)
        {
            double power = 1;
            int factorial = 1;
            double res = 0;
            for (int i = 1; i < 2 * k + 1; i++)
            {
                power = power * x *x;
                factorial = factorial * i;
                res += power / factorial; //не успел эту прогу дописать, не успел учесть e
            }
            return res;
        }
    }
}
