﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var (listOne, listTwo) = InputDataFourProgram();
            var listResult = FindIntersectionOfTwoLists(listOne, listTwo);
            foreach (var element in listResult)
                Console.Write(element + " ");
            Console.Write("– пересечение множеств");

            var (number, denominator) = InputDataThreeProgram();
            int[] wholePartOfNumber = GetWholePartOfNumber(number, denominator);
            int wholePartOfNumberInt = number / denominator;
            int[] fractionalPartOfNumber = GetFractionalPartOfNumber(number, denominator, wholePartOfNumberInt);

        }

        static List<int> FindIntersectionOfTwoLists(List<int> listOne, List<int> listTwo)
        {
            List<int> listResult = new List<int>();
            foreach (var elementOne in listOne)
                foreach (var elementTwo in listTwo)
                    if (elementOne == elementTwo)
                        listResult.Add(elementTwo);
            return listResult;
        }

        static (List<int>, List<int>) InputDataFourProgram()
        {
            var listOne = new List<int> { 5, 3, 6, 7, 2, 10, 23, 345 };
            var listTwo = new List<int> { 5, 7, 6, 0, 2, 10, 43, 34, 56, 78 };
            return (listOne, listTwo);
        }

        static (int, int) InputDataThreeProgram()
        {
            int number = 100;
            int denominator = 3;
            return (number, denominator);
        }

        static int[] GetWholePartOfNumber(int number, int denominator)
        {
            int result = number / denominator;
            List<int> WholePartOfNumberList = new List<int>();
            string resultStr = result.ToString();
            for (int i = 0; i < resultStr.Length; i++)
                WholePartOfNumberList.Add(resultStr[i]);
            return WholePartOfNumberList.ToArray();
        }

        static int[] GetFractionalPartOfNumber(int number, int denominator, int wholePartOfNumber)
        {
            double denominatorDouble = denominator;
            string minPartStr = (number - wholePartOfNumber * denominator).ToString();
            //нужно реализовать цикл while, постоянно запоминая полученную цифру и добавляя 0 к остатку, слишком много операций, не успел
            return new int[1] { 1 };
        }

        //[TestClass] 
        //   public class Tests
        //    {
        // [TestMethod]
        //  public void OrdinaryCase(List<int> listResult)
        //    { 
        //  Assert.AreEqual(expectedResult, result);
        //}

        // [TestMethod]
        //public void WhenSecondListHaveZirolElementCase(List<int> listResult); не успел

    }
}
