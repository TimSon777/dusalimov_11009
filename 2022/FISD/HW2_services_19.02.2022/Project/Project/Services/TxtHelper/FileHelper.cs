﻿namespace Project.Services.TxtHelper;

public class FileHelper : IFileHelper
{
    private readonly IWebHostEnvironment _webHostEnvironment;

    public FileHelper(IWebHostEnvironment webHostEnvironment)
        => _webHostEnvironment = webHostEnvironment;
    
    public async Task<string> FindContentFileAsync(string relativePathToFile)
    {
        var path = _webHostEnvironment.ContentRootPath + relativePathToFile;
        return File.Exists(path) 
            ? await File.ReadAllTextAsync(path) 
            : null!;
    }

    public async Task<bool> TryOverwriteAsync(string relativePathToFile, string content)
    {
        var path = _webHostEnvironment.ContentRootPath + relativePathToFile;
        if (!File.Exists(path)) return false;
        await File.WriteAllTextAsync(path, content);
        return true;
    }
}