﻿namespace Project.Services.MessageSender;

public interface IMessageSender
{ 
    Task SendAsync(HttpResponse response);
}