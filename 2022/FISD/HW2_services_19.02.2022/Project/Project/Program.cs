using Project.Infrastructure;
using Project.Middleware.FilesHandler;
using Project.Middleware.MessageSender;
using Project.Middleware.StaticFiles;
using Project.Middleware.WelcomePage;
using Project.Services.MessageSender;
using Project.Services.TxtHelper;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;

services
    .AddTransient<IFileHelper, FileHelper>()
    .AddTransient<MessageSenderDuringWorkingHours>()
    .AddTransient<MessageSenderDuringNonworkingHours>()
    .AddTransient<IMessageSender>(provider =>
        DateTime.Now.TimeOfDay.Between(
            TimeSpan.FromHours(6),
            TimeSpan.FromHours(18))
            ? provider.GetRequiredService<MessageSenderDuringWorkingHours>()
            : provider.GetRequiredService<MessageSenderDuringNonworkingHours>());

var app = builder.Build();

app
    .UseMyStaticFiles()
    .UseMyWelcomePage("wwwroot/html/index.html")
    .UseMessageSender("/GetMessage")
    .UseFileHandler("files");

app.Run();