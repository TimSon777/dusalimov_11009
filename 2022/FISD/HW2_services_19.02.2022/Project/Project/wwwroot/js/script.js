﻿function initHttp() {
    return window.XMLHttpRequest 
        ? new XMLHttpRequest() 
        : new ActiveXObject("Microsoft.XMLHTTP")
}

function makeAjaxRequest(method, url, selector) {
    let http = initHttp()

    http.onreadystatechange = () => {
        if (http.readyState === 4 && http.status === 200)
            selector(http.responseText)
    }

    http.open(method, 'https://localhost:7002/' + url, true)
    http.send()    
}

function displayFiles() {
    let http = initHttp()
    
    http.onreadystatechange = () => {
        if (http.readyState === 4 && http.status === 200) {
            appendFiles(JSON.parse(http.responseText))
        }
    }
    
    http.open('GET', 'https://localhost:7002/FilesHandler/GetFiles', true)
    http.send()
}

function submitOrAlert() {
    let form = document.getElementById('form-add-file')
    let inputFile = document.getElementById('input-file')
    form.onsubmit = () => {
        if(inputFile.files.length === 0) {
            alert('Выберите файл')
            return false
        }
    }
    
    return true
}

function getExtension(filename) {
    let file = filename.split(".");
    if(file.length === 1 || (file.length === 2 && (file[0] === "" && filename[1] === "")))
        return null;
    
    return file.pop();
}

document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
        displayFiles()
        submitOrAlert()
        if (document.URL === "/")
            getMessageAndAlert()
    }
}