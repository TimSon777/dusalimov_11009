﻿function initHttp() {
    return window.XMLHttpRequest
        ? new XMLHttpRequest()
        : new ActiveXObject("Microsoft.XMLHTTP")
}

function getMessageAndAlert() {
    let http = initHttp()

    http.onreadystatechange = () => {
        if (http.readyState === 4 && http.status === 200)
            alert(http.responseText)
    }

    http.open('GET', 'https://localhost:7002/GetMessage', true)
    http.send()
}

document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
        getMessageAndAlert()
    }
}