﻿namespace Project.Middleware.FilesHandler;

public static class FilesHandlerExtensions
{
    public static IApplicationBuilder UseFileHandler(this IApplicationBuilder app, string pathToDirectory)
        => app.UseMiddleware<FilesHandler>(pathToDirectory);
}