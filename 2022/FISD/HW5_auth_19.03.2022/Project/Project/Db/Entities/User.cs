﻿using Project.SharedKernel;

namespace Project.Db.Entities;

public class User : BaseEntity
{
    public string UserName { get; set; }

    public string HashedPassword { get; set; }

    public byte[] Salt { get; set; }

    public ICollection<UserRole> Roles { get; set; } = new List<UserRole>();
}