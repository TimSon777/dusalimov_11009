﻿using Project.SharedKernel;

namespace Project.Db.Entities;

public class UserRole : BaseEntity
{
    public string Name { get; set; }
    public string Description { get; set; }
}