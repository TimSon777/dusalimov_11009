﻿using Microsoft.EntityFrameworkCore;
using Project.Db.Entities;

namespace Project.Db;

public sealed class AppDbContext : DbContext
{
    public DbSet<User> Users => Set<User>();
    public DbSet<UserRole> UserRoles => Set<UserRole>();
    
    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    { }
}