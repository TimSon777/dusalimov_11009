﻿using System.ComponentModel.DataAnnotations;

namespace Project.SharedKernel;

public class PasswordUserName
{
    [Required]
    public string UserName { get; set; }
    
    [Required]
    public string Password { get; set; }
}