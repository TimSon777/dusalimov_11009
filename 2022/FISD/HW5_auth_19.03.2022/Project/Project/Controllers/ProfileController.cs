﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers;

public class ProfileController : Controller
{
    [Authorize]
    public IActionResult Index()
    {
        var user = HttpContext.User;

        var userName = user.Claims.First(claim => claim.Type == ClaimTypes.Name);
        
        return View(model:userName.Value);
    }
}