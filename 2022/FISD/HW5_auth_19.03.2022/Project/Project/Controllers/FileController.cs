﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project.Services.FileHelper;
using Project.ViewModels;

namespace Project.Controllers;

public class FileController : Controller
{
    private readonly IFileHelper _fileHelper;
    private readonly string _pathToDirectory;
    private readonly string _relativePathToDirectory;
    
    public FileController(IHostEnvironment environment, IConfiguration configuration, IFileHelper fileHelper)
    {
        _fileHelper = fileHelper;
        _relativePathToDirectory = configuration.GetSection("PathToDirectoryFiles").Value ?? throw new SystemException();
        _pathToDirectory = Path.Combine(environment.ContentRootPath, _relativePathToDirectory);
    }
    
    [Authorize]
    public IActionResult Index()
    {
        var files = Directory.GetFiles(_pathToDirectory).Select(Path.GetFileName);
        return View(files);
    }

    [HttpGet]
    [Authorize(Policy = "AdvancedUserAccess")]
    public async Task<IActionResult> OpenFile(string fileName)
    {
        var content = await _fileHelper.FindContentFileAsync(Path.Combine(_relativePathToDirectory, fileName));

        if (content is null)
        {
            return BadRequest();
        }

        var file = new FileVm
        {
            Name = fileName,
            Content = content
        };

        return View(file);
    }

    [HttpPost]
    [Authorize(Policy = "AdvancedUserAccess")]
    public async Task<IActionResult> Save(FileVm model)
    {
        var isOk = await _fileHelper.TryOverwriteAsync(Path.Combine(_relativePathToDirectory, model.Name), model.Content);
        
        if (!isOk)
        {
            return BadRequest();
        }

        return RedirectToAction("OpenFile", "File", new { fileName = model.Name });
    }
}