﻿namespace Project.Services.FileHelper;

public class FileHelper : IFileHelper
{
    private readonly IHostEnvironment _hostEnvironment;

    public FileHelper(IHostEnvironment hostEnvironment)
        => _hostEnvironment = hostEnvironment;

    public async Task<string?> FindContentFileAsync(string relativePathToFile)
    {
        var path = _hostEnvironment.ContentRootPath + "/" + relativePathToFile;
        return File.Exists(path)
            ? await File.ReadAllTextAsync(path)
            : null!;
    }

    public async Task<bool> TryOverwriteAsync(string relativePathToFile, string content)
    {
        var path = _hostEnvironment.ContentRootPath + "/" + relativePathToFile;
        if (!File.Exists(path)) return false;
        await File.WriteAllTextAsync(path, content);
        return true;
    }
}