using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using Project.Db;
using Project.Db.Entities;
using Project.Services.FileHelper;
using Project.Services.Hashing;
using Project.Services.RandomBytes;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;

services.AddTransient<IHashing, Hashing>();
services.AddTransient<IRandomBytes, RandomBytes>();
services.AddTransient<IFileHelper, FileHelper>();

services.AddDbContext<AppDbContext>(options => 
    options.UseNpgsql(configuration.GetConnectionString("FilesManagerDb") ?? throw new SystemException()));

services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options => options.LoginPath = "/Account/SignIn");

services.AddAuthorization(options =>
{
    options.AddPolicy("DefaultUserAccess", policy => policy.RequireRole("DefaultUser", "AdvancedUser"));
    options.AddPolicy("AdvancedUserAccess", policy => policy.RequireRole("AdvancedUser"));
});

services.AddControllersWithViews();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

using var scope = app.Services.CreateScope();
var scopedProvider = scope.ServiceProvider;
try
{
    var appDbContext = scopedProvider.GetRequiredService<AppDbContext>();
    if (!await appDbContext.UserRoles.AnyAsync())
    {
        appDbContext.UserRoles.AddRange(new List<UserRole>
        {
            new()
            {
                Name = "DefaultUser",
                Description = "DefaultUser"
            },
            new()
            {
                Name = "AdvancedUser",
                Description = "AdvancedUser"
            }
        });

        await appDbContext.SaveChangesAsync();
    }
}
catch (Exception ex)
{
    app.Logger.LogError(ex, "An error occurred seeding the DB.");
}


app.Run();