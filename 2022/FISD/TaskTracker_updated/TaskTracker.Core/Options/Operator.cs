﻿namespace TaskTracker.Core.Options;

public enum Operator
{
    Eq,
    Ls,
    Gt
}