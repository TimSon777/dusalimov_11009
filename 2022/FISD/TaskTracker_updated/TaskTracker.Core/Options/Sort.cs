﻿namespace TaskTracker.Core.Options;

public enum Sort
{
    Asc,
    Desc
}