﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Options;

namespace TaskTracker.Core.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Retrieves entity from the database
        /// </summary>
        /// <param name="id">Entity's id</param>
        /// <returns></returns>
        Task<T?> FindByIdAsync(int id);

        /// <summary>
        /// Retrieves all entities from the database
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAndFilterByIdAsync(Operator op, Sort sort, int value);

        /// <summary>
        /// Adds entity to the database
        /// </summary>
        /// <param name="entity">Entity to add</param>
        Task<T> AddAsync(T entity);

        /// <summary>
        /// Updates entity in the database
        /// </summary>
        /// <param name="entity">Entity to update</param>
        Task<T> UpdateAsync(T entity);

        /// <summary>
        /// Marks entity as removed in the database
        /// </summary>
        /// <param name="id">Entity's id</param>
        Task<bool> TryDeleteAsync(int id);
    }
}
