﻿using TaskTracker.Core.Enums;

namespace TaskTracker.Core.Entities
{
    public class TaskItem : BaseEntity
    {
        public string Name { get; set; } = "";
        public string? Description { get; set; }
        private DateTime? _startDate;        
        public DateTime? StartDate => _startDate;
        private DateTime? _completionDate;        
        public DateTime? CompletionDate => _completionDate;        
        public TaskItemStatus Status { get; set; }        
        public int Priority { get; set; }
        public int? ProjectId { get; set; }

        public Project? Project { get; set; }
        public List<Feature> Features { get; set; }

        /// <summary>
        /// Sets the start date for the task, given that the passed value is valid
        /// </summary>
        /// <param name="startDate">Start date to set</param>
        /// <exception cref="ArgumentOutOfRangeException">If the date is not valid</exception>
        public void SetStartDate(DateTime? startDate)
        {
            if (startDate is null)
            {
                _startDate = null;
            }
            else if (_completionDate is null || startDate < _completionDate)
            {
                _startDate = startDate;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(startDate));
            }
        }

        /// <summary>
        /// Sets the completion date for the task, given that the passed value is valid
        /// </summary>
        /// <param name="completionDate">Completion date to set</param>
        /// <exception cref="ArgumentOutOfRangeException">If the date is not valid</exception>
        public void SetCompletionDate(DateTime? completionDate)
        {
            if (completionDate is null)
            {
                _completionDate = null;
            }
            else if (_startDate is null || completionDate > _startDate)
            {
                _completionDate = completionDate;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(completionDate));
            }
        }
    }
}
