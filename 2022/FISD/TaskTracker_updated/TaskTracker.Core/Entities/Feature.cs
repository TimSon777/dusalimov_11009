﻿namespace TaskTracker.Core.Entities;

public class Feature : BaseEntity
{
    public string Name { get; set; }
    public TaskItem TaskItem { get; set; }
}