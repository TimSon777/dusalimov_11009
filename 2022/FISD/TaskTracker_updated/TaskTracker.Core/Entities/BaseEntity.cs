﻿namespace TaskTracker.Core.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; } = DateTime.Now;        
        public bool IsDeleted { get; set; }
    }
}
