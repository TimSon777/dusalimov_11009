﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Options;

namespace TaskTracker.Infrastructure.Repositories
{
    public class TaskItemRepository : Repository<TaskItem>, IRepository<TaskItem>
    {
        private readonly DbContext _context;
        private readonly DbSet<TaskItem> _set;

        public TaskItemRepository(ApplicationDbContext context) 
            : base(context)
        {
            _context = context;
            _set = context.Set<TaskItem>();
        }

        public async Task<TaskItem?> FindByIdAsync(int id)
        {
            var r = await _set.FirstOrDefaultAsync(x => x.Id == id);
            return r?.IsDeleted == true ? null! : r;
        }

        public async Task<IEnumerable<TaskItem>> GetAllAndFilterByIdAsync(Operator op, Sort sort, int value) 
            => await base.GetAllAndFilterByIdAsync(_set.Include(c => c.Project), op, sort, value);

        public async Task<TaskItem> AddAsync(TaskItem entity)
        {
            var entry = await _set.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entry.Entity;
        }

        public async Task<TaskItem> UpdateAsync(TaskItem entity)
        {
            var entry = _set.Update(entity);
            await _context.SaveChangesAsync();
            return entry.Entity;
        }

        public async Task<bool> TryDeleteAsync(int id)
        {
            var entity = await FindByIdAsync(id);
            return await base.TryDeleteAsync(entity!);
        }
    }
}
