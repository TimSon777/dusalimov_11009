﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Options;

namespace TaskTracker.Infrastructure.Repositories
{
    public class ProjectRepository : Repository<Project>, IRepository<Project>
    {
        private readonly DbContext _context;
        private readonly DbSet<Project> _set;

        public ProjectRepository(ApplicationDbContext context) 
            : base(context)
        {
            _context = context;
            _set = context.Set<Project>();
        }

        public async Task<Project?> FindByIdAsync(int id)
        {
            var r = await _set.FirstOrDefaultAsync(x => x.Id == id);
            return r?.IsDeleted == true ? null! : r;
        }

        public async Task<IEnumerable<Project>> GetAllAndFilterByIdAsync(Operator op, Sort sort, int value) 
            => await base.GetAllAndFilterByIdAsync(_set, op, sort, value);

        public async Task<Project> AddAsync(Project entity)
        {
            var entry = await _set.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entry.Entity;
        }

        public async Task<Project> UpdateAsync(Project entity)
        {
            var entry = _set.Update(entity);
            await _context.SaveChangesAsync();
            return entry.Entity;
        }

        public async Task<bool> TryDeleteAsync(int id)
        {
            var entity = await FindByIdAsync(id);
            return await base.TryDeleteAsync(entity!);
        }
    }
}
