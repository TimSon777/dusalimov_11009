﻿using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;
using TaskTracker.Core.Options;

namespace TaskTracker.Infrastructure.Repositories;

public class FeatureRepository : Repository<Feature>, IRepository<Feature>
{
    private readonly DbContext _context;
    private readonly DbSet<Feature> _set;

    public FeatureRepository(ApplicationDbContext context) 
        : base(context)
    {
        _context = context;
        _set = context.Set<Feature>();
    }

    public async Task<Feature?> FindByIdAsync(int id)
    {
        var r = await _set
            .Include(x => x.TaskItem)
            .FirstOrDefaultAsync(x => x.Id == id);

        return r?.IsDeleted == true ? null! : r;
    }

    public async Task<IEnumerable<Feature>> GetAllAndFilterByIdAsync(Operator op, Sort sort, int value) 
        => await base.GetAllAndFilterByIdAsync(_set.Include(c => c.TaskItem), op, sort, value);

    public async Task<Feature> AddAsync(Feature entity)
    {
        var entry = await _set.AddAsync(entity);
        await _context.SaveChangesAsync();
        return entry.Entity;
    }

    public async Task<Feature> UpdateAsync(Feature entity)
    {
        var entry = _set.Update(entity);
        await _context.SaveChangesAsync();
        return entry.Entity;
    }

    public async Task<bool> TryDeleteAsync(int id)
    {
        var entity = await FindByIdAsync(id);
        return await base.TryDeleteAsync(entity!);
    }
}