﻿using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Entities;
using TaskTracker.Core.Options;

namespace TaskTracker.Infrastructure.Repositories;

public class Repository<T> 
    where T : BaseEntity
{
    private readonly DbContext _context;

    protected Repository(DbContext context)
    {
        _context = context;
    }
    
    protected Task<IEnumerable<T>> GetAllAndFilterByIdAsync(IQueryable<T> queryable, Operator op, Sort sort, int value)
    {
        var withoutDeleted = queryable.Where(x => !x.IsDeleted);
        var filterCollection = op switch
        {
            Operator.Eq => withoutDeleted.Where(x => x.Id == value),
            Operator.Gt => withoutDeleted.Where(x => x.Id > value),
            _ =>           withoutDeleted.Where(x => x.Id < value)
        };
        
        var r = sort switch
        {
            Sort.Desc => filterCollection.OrderByDescending(x => x.Id),
            _ => filterCollection.OrderBy(x => x.Id)
        };

        return Task.FromResult(r.AsEnumerable());
    }

    
    protected async Task<bool> TryDeleteAsync(BaseEntity? baseEntity)
    {
        if (baseEntity is null || baseEntity.IsDeleted)
        {
            return false;
        }
        
        baseEntity.IsDeleted = true;
        await _context.SaveChangesAsync();
        return true;
    }
}