﻿using TaskTracker.Core.Interfaces;

namespace TaskTracker.Infrastructure.Services;

public class MailSender : IMailSender
{
    public Task SendNotificationAsync(string html, string title, string email)
    {
        Console.WriteLine($"Title: {title}\nHtml:{html}\n Send to {email}");
        return Task.FromResult(0);
    }
}