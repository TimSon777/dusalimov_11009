﻿namespace TaskTracker.Infrastructure.Models;

public class AdminSettings
{
    public string Email { get; set; }
}