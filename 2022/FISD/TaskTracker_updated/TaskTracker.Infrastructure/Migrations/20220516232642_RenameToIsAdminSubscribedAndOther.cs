﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskTracker.Infrastructure.Migrations
{
    public partial class RenameToIsAdminSubscribedAndOther : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsAdminSubscriber",
                table: "Project",
                newName: "IsAdminSubscribed");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsAdminSubscribed",
                table: "Project",
                newName: "IsAdminSubscriber");
        }
    }
}
