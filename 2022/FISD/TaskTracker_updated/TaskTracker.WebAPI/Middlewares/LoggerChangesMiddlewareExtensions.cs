﻿namespace TaskTracker.WebAPI.Middlewares;

public static class LoggerChangesMiddlewareExtensions
{
    public static IApplicationBuilder UseChangesLogger(this IApplicationBuilder app)
    {
        return app.UseMiddleware<LoggerChangesMiddleware>();
    }
}