﻿using Microsoft.Extensions.Options;
using TaskTracker.Infrastructure.Models;

namespace TaskTracker.WebAPI.Middlewares;

public class LoggerChangesMiddleware
{
    public readonly RequestDelegate Next;
    private readonly string _contentRootPath;
    
    public LoggerChangesMiddleware(RequestDelegate next, IHostEnvironment hostEnvironment)
    {
        Next = next;
        _contentRootPath = hostEnvironment.ContentRootPath;
    }

    public async Task InvokeAsync(HttpContext ctx, IOptions<LoggerSettings> loggerSettingsOptions)
    {
        var httpMethod = ctx.Request.Method;
        var loggerSettings = loggerSettingsOptions.Value;

        if (httpMethod is not ("POST" or "PUT" or "DELETE" or "PATCH"))
        {
            await Next(ctx);
            return;
        }

        var pathToFile = _contentRootPath + loggerSettings.RelativePathToFile;

        await File.AppendAllTextAsync(pathToFile, $"{DateTime.Now}: {httpMethod} - {ctx.Request.Path.Value}\n");

        await Next(ctx);
    }
}