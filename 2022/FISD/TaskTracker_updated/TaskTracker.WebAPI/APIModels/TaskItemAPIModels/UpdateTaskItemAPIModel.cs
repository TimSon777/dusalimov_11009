﻿using TaskTracker.Core.Entities;

namespace TaskTracker.WebAPI.APIModels.TaskItemAPIModels
{
    public class UpdateTaskItemApiModel
    {        
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public int? Priority { get; set; }

        public TaskItem MapToTaskItem(TaskItem taskItem)
        {
            taskItem.Name = Name ?? taskItem.Name;
            taskItem.Description = Description ?? taskItem.Description;
            taskItem.Priority = Priority ?? taskItem.Priority;

            if (CompletionDate is not null)
            {
                taskItem.SetCompletionDate(CompletionDate);
            }

            if (StartDate is not null)
            {
                taskItem.SetStartDate(StartDate);
            }
            
            return taskItem;
        }
    }
}
