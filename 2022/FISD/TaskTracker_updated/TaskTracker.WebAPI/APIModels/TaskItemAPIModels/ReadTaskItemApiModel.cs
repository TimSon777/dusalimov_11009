﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Enums;

namespace TaskTracker.WebAPI.APIModels.TaskItemAPIModels
{
    public class ReadTaskItemApiModel : BaseApiModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public int? Priority { get; set; }
        public TaskItemStatus Status { get; set; }

        public static ReadTaskItemApiModel MapFromTaskItem(TaskItem taskItem)
        {
            return new ReadTaskItemApiModel
            {
                Description = taskItem.Description,
                StartDate = taskItem.StartDate,
                CompletionDate = taskItem.CompletionDate,
                Status = taskItem.Status,
                Priority = taskItem.Priority,
                CreatedAt = taskItem.CreatedAt,
                Id = taskItem.Id,
                Name = taskItem.Name
            };
        }
    }    
}
