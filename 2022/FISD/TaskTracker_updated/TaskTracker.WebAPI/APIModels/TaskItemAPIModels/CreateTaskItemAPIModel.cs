﻿using TaskTracker.Core.Entities;

namespace TaskTracker.WebAPI.APIModels.TaskItemAPIModels
{
    public class CreateTaskItemApiModel
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public int Priority { get; set; }

        public TaskItem MapToTaskItem(Project project)
        {
            var taskItem = new TaskItem
            {   
                Name = Name,
                Description = Description,
                Priority = Priority,
                Project = project
            };
            
            taskItem.SetCompletionDate(CompletionDate);
            taskItem.SetStartDate(StartDate);
            
            return taskItem;
        }
    }
}
