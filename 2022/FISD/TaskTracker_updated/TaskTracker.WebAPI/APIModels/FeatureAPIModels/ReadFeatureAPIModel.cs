﻿using TaskTracker.Core.Entities;
using TaskTracker.WebAPI.APIModels.TaskItemAPIModels;

namespace TaskTracker.WebAPI.APIModels.FeatureAPIModels;

public class ReadFeatureApiModel : BaseApiModel
{
    public string Name { get; set; } = "";
    public ReadTaskItemApiModel TaskItem { get; set; } = null!;

    public static ReadFeatureApiModel FromFeature(Feature feature)
    {
        return new ReadFeatureApiModel
        {
            Id = feature.Id,
            Name = feature.Name,
            TaskItem = ReadTaskItemApiModel.MapFromTaskItem(feature.TaskItem),
            CreatedAt = feature.CreatedAt,
        };
    }
}