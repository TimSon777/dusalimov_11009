﻿using TaskTracker.Core.Entities;

namespace TaskTracker.WebAPI.APIModels.FeatureAPIModels;

public class UpdateFeatureApiModel
{
    public int Id { get; set; }
    public string? Name { get; set; }

    public void MapToFeature(Feature feature)
    {
        if (Name is not null)
        {
            feature.Name = Name;
        }
    }
}