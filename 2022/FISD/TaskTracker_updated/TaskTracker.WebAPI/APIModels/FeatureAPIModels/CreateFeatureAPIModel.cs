﻿using TaskTracker.Core.Entities;

namespace TaskTracker.WebAPI.APIModels.FeatureAPIModels;

public class CreateFeatureApiModel
{
    public string Name { get; set; }

    public int TaskItemId { get; set; }
    
    public Feature ToFeature(TaskItem taskItem)
    {
        return new Feature
        {
            Name = Name,
            TaskItem = taskItem
        };
    }
}