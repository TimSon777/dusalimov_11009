﻿namespace TaskTracker.WebAPI.APIModels;

public class BaseApiModel
{
    public int Id { get; set; }
    public DateTime CreatedAt { get; set; }        
}