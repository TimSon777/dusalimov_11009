﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Enums;

namespace TaskTracker.WebAPI.APIModels.ProjectAPIModels
{
    public class CreateProjectApiModel
    {
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public int Priority { get; set; }
        public bool IsAdminSubscriber { get; set; }

        public Project ToProject()
        {
            var proj =  new Project
            {
                Description = Description,
                Name = Name,
                Priority = Priority,
                IsAdminSubscribed = IsAdminSubscriber
            };
            
            proj.SetCompletionDate(CompletionDate);
            proj.SetStartDate(StartDate);
            
            return proj;
        }
    }
}
