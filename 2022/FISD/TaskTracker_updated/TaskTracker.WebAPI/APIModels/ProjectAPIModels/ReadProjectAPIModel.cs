﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Enums;
using TaskTracker.WebAPI.APIModels.TaskItemAPIModels;

namespace TaskTracker.WebAPI.APIModels.ProjectAPIModels
{
    public class ReadProjectApiModel : BaseApiModel
    {        
        public List<ReadTaskItemApiModel>? Tasks { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public int Priority { get; set; }
        public bool IsAdminSubscribed { get; set; }
        public ProjectStatus Status { get; set; }
        
        public static ReadProjectApiModel FromProject(Project project)
        {
            return new ReadProjectApiModel
            {
                Id = project.Id,
                Description = project.Description,
                StartDate = project.StartDate,
                CompletionDate = project.CompletionDate,
                Status = project.Status,
                Priority = project.Priority,
                CreatedAt = project.CreatedAt,
                
                Tasks = project.TasksItems
                    .Select(ReadTaskItemApiModel.MapFromTaskItem)
                    .ToList(),
                
                Name = project.Name,
                IsAdminSubscribed = project.IsAdminSubscribed
            };
        }
    }               
}
