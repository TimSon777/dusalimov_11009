﻿using TaskTracker.Core.Entities;

namespace TaskTracker.WebAPI.APIModels.ProjectAPIModels
{
    public class UpdateProjectApiModel
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public int? Priority { get; set; }
        public bool? IsAdminSubscribed { get; set; }

        public void MapToProject(Project project)
        {
            project.Description = Description ?? project.Description;
            project.Name = Name ?? project.Name;
            project.Priority = Priority ?? project.Priority;
            project.IsAdminSubscribed = IsAdminSubscribed ?? project.IsAdminSubscribed;

            if (CompletionDate is not null)
            {
                project.SetCompletionDate(CompletionDate);
            }

            if (StartDate is not null)
            {
                project.SetStartDate(StartDate);
            }
        }
    }
}
