﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.Core.Options;
using TaskTracker.WebAPI.APIModels.FeatureAPIModels;
using TaskTracker.WebAPI.APIModels.ProjectAPIModels;
using TaskTracker.WebAPI.APIModels.TaskItemAPIModels;

namespace TaskTracker.WebAPI.Controllers;

[Route("api/tasks")]
[ApiController]
public class TaskItemController : ControllerBase
{
    private readonly IRepository<Project> _projectRepository;
    private readonly IRepository<TaskItem> _taskItemRepository;

    public TaskItemController(IRepository<Project> projectRepository, IRepository<TaskItem> taskItemRepository)
    {
        _projectRepository = projectRepository;
        _taskItemRepository = taskItemRepository;
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<ReadTaskItemApiModel>> Read(int id)
    {
        var task = await _taskItemRepository.FindByIdAsync(id);

        if (task is null)
        {
            return NotFound();
        }

        return Ok(ReadTaskItemApiModel.MapFromTaskItem(task));
    }

    [HttpGet("{op}/{value:int}/{sort}")]
    public async Task<ActionResult<IEnumerable<ReadTaskItemApiModel>>> GetList(Operator op, Sort sort, int value)
    {
        var features = await _taskItemRepository.GetAllAndFilterByIdAsync(op, sort, value);
        var result = features.Select(ReadTaskItemApiModel.MapFromTaskItem);
        return Ok(result);
    }

    [HttpPost]
    public async Task<ActionResult> Create(CreateTaskItemApiModel model)
    {
        var project = await _projectRepository.FindByIdAsync(model.ProjectId);

        if (project is null)
        {
            return BadRequest();
        }

        var taskItem = model.MapToTaskItem(project);
        var createdTaskItem = await _taskItemRepository.AddAsync(taskItem);
        var result = ReadTaskItemApiModel.MapFromTaskItem(createdTaskItem);
        return Ok(result);
    }

    [HttpPut]
    public async Task<ActionResult> Update(UpdateTaskItemApiModel model)
    {
        var taskItem = await _taskItemRepository.FindByIdAsync(model.Id);
        
        if (taskItem is null)
        {
            return NotFound();
        }

        model.MapToTaskItem(taskItem);
        var updatedFeature = await _taskItemRepository.UpdateAsync(taskItem);
        var result = ReadTaskItemApiModel.MapFromTaskItem(updatedFeature);
        return Ok(result);
    }

    [HttpDelete("{id:int}")]
    public async Task<IActionResult> Delete(int id)
    {
        var isOk = await _taskItemRepository.TryDeleteAsync(id);
        return isOk
            ? NoContent()
            : NotFound();
    }
}