﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.Core.Options;
using TaskTracker.WebAPI.APIModels.FeatureAPIModels;

namespace TaskTracker.WebAPI.Controllers;

[Route("api/features")]
[ApiController]
public class FeatureController : ControllerBase
{
    private readonly IRepository<Feature> _featureRepository;
    private readonly IRepository<TaskItem> _taskItemRepository;

    public FeatureController(IRepository<Feature> featureRepository, IRepository<TaskItem> taskItemRepository)
    {
        _featureRepository = featureRepository;
        _taskItemRepository = taskItemRepository;
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<ReadFeatureApiModel>> Read(int id)
    {
        var feature = await _featureRepository.FindByIdAsync(id);

        if (feature is null)
        {
            return NotFound();
        }

        return Ok(ReadFeatureApiModel.FromFeature(feature));
    }

    [HttpGet("{op}/{value:int}/{sort}")]
    public async Task<ActionResult<IEnumerable<ReadFeatureApiModel>>> GetList(Operator op, Sort sort, int value)
    {
        var features = await _featureRepository.GetAllAndFilterByIdAsync(op, sort, value);
        var result = features.Select(ReadFeatureApiModel.FromFeature);
        return Ok(result);
    }

    [HttpPost]
    public async Task<ActionResult> Create(CreateFeatureApiModel model)
    {
        var task = await _taskItemRepository.FindByIdAsync(model.TaskItemId);

        if (task is null)
        {
            return BadRequest();
        }

        var feature = model.ToFeature(task);
        var createdFeature = await _featureRepository.AddAsync(feature);
        var result = ReadFeatureApiModel.FromFeature(createdFeature);
        return Ok(result);
    }

    [HttpPut]
    public async Task<ActionResult> Update(UpdateFeatureApiModel model)
    {
        var feature = await _featureRepository.FindByIdAsync(model.Id);
        if (feature is null)
        {
            return NotFound();
        }

        model.MapToFeature(feature);
        var updatedFeature = await _featureRepository.UpdateAsync(feature);
        var result = ReadFeatureApiModel.FromFeature(updatedFeature);
        return Ok(result);
    }

    [HttpDelete("{id:int}")]
    public async Task<IActionResult> Delete(int id)
    {
        var isOk = await _featureRepository.TryDeleteAsync(id);
        return isOk
            ? NoContent()
            : NotFound();
    }
}