﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TaskTracker.Core.Options;
using TaskTracker.Infrastructure.Models;
using TaskTracker.WebAPI.APIModels.FeatureAPIModels;
using TaskTracker.WebAPI.APIModels.ProjectAPIModels;

namespace TaskTracker.WebAPI.Controllers;

[Route("api/projects")]
[ApiController]
public class ProjectController : ControllerBase
{
    private readonly IRepository<Project> _projectRepository;
    private readonly AdminSettings _adminSettings;
    private readonly IMailSender _mailSender;

    public ProjectController(IRepository<Project> projectRepository, IOptions<AdminSettings> adminSettingsOptions, IMailSender mailSender)
    {
        _adminSettings = adminSettingsOptions.Value;
        _projectRepository = projectRepository;
        _mailSender = mailSender;
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<ReadFeatureApiModel>> Read(int id)
    {
        var project = await _projectRepository.FindByIdAsync(id);

        if (project is null)
        {
            return NotFound();
        }

        return Ok(ReadProjectApiModel.FromProject(project));
    }

    [HttpGet("{op}/{value:int}/{sort}")]
    public async Task<ActionResult<IEnumerable<ReadFeatureApiModel>>> GetList(Operator op, Sort sort, int value)
    {
        var projects = await _projectRepository.GetAllAndFilterByIdAsync(op, sort, value);
        var result = projects.Select(ReadProjectApiModel.FromProject);
        return Ok(result);
    }

    [HttpPost]
    public async Task<ActionResult> Create(CreateProjectApiModel model)
    {
        var project = model.ToProject();
        var createdProject = await _projectRepository.AddAsync(project);
        var result = ReadProjectApiModel.FromProject(createdProject);
        await NotifyAdminAsync(createdProject, $"Project {createdProject.Name} was created", "CREATE");
        return Ok(result);
    }

    [HttpPut]
    public async Task<ActionResult> Update(UpdateProjectApiModel model)
    {
        var project = await _projectRepository.FindByIdAsync(model.Id);
        
        if (project is null)
        {
            return NotFound();
        }

        model.MapToProject(project);
        var updatedProject = await _projectRepository.UpdateAsync(project);
        var result = ReadProjectApiModel.FromProject(updatedProject);
        await NotifyAdminAsync(updatedProject, $"Project {updatedProject.Name} was updated", "UPDATE");
        return Ok(result);
    }

    [HttpDelete("{id:int}")]
    public async Task<IActionResult> DeleteFeature(int id)
    {
        var project = await _projectRepository.FindByIdAsync(id);
        
        if (project is null)
        {
            return NotFound();
        }

        await NotifyAdminAsync(project, $"Project {project.Name} was deleted", "DELETE");
        await _projectRepository.TryDeleteAsync(id);
        return NoContent();
    }

    private async Task NotifyAdminAsync(Project project, string html, string title)
    {
        if (project.IsAdminSubscribed)
        {
            await _mailSender.SendNotificationAsync(html, title, _adminSettings.Email);
        }
    }
}