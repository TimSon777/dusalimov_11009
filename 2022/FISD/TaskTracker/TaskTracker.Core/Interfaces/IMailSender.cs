﻿namespace TaskTracker.Core.Interfaces;

public interface IMailSender
{
    Task SendNotificationAsync(string html, string title);
}