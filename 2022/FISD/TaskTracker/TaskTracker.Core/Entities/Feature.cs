﻿namespace TaskTracker.Core.Entities;

public class Feature : BaseEntity
{
    public int Id { get; set; }
    public string Name { get; set; }
    public TaskItem TaskItem { get; set; }
}