﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;
using TaskTracker.WebAPI.APIModels;
using TaskTracker.WebAPI.APIModels.ProjectAPIModels;
using TaskTracker.WebAPI.APIModels.TaskItemAPIModels;
using Microsoft.AspNetCore.Mvc;

namespace TaskTracker.WebAPI.Controllers
{
    [Route("api/projects")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IMailSender _mailSender;

        public ProjectController(IProjectRepository projectRepository, IMailSender mailSender)
        {
            _projectRepository = projectRepository;
            _mailSender = mailSender;
        }        

        [HttpGet("{projectId:int}")]
        public ActionResult<ProjectAPIModel> GetById(int projectId)
        {
            try
            {
                var project = _projectRepository.GetById(projectId);
                if (project == null)
                    return NotFound();

                var result = ProjectAPIModel.FromProject(project);                

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<ListProjectAPIModel>> GetAll()
        {
            try
            {
                var projects = _projectRepository.GetAll();

                var result =
                    projects
                        .Select(p => ListProjectAPIModel.FromProject(p))
                        .ToList();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        [HttpPost]
        public async Task<ActionResult<CreateProjectAPIModel>> CreateProject([FromBody]CreateProjectAPIModel model)
        {
            try
            {
                var project = new Project(model.Name)
                {
                    Description = model.Description,
                    Priority = model.Priority,
                    IsAdminSubscriber = model.IsAdminSubscriber
                };

                project.SetStartDate(model.StartDate);
                project.SetCompletionDate(model.CompletionDate);

                _projectRepository.Add(project);

                await NotifyAdminIfSubscribedAsync(project, "Project created", "Create");
                return CreatedAtAction(nameof(GetById), new { projectId = project.Id }, model);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        [HttpPut("{projectId:int}")]
        public async Task<ActionResult<ProjectAPIModel>> UpdateProject(int projectId, [FromBody]UpdateProjectAPIModel model)
        {
            try
            {
                var project = _projectRepository.GetById(projectId);
                if (project == null)
                    return NotFound();

                project.Name = model.Name;
                project.Description = model.Description;
                project.SetStartDate(model.StartDate);
                project.SetCompletionDate(model.CompletionDate);
                project.SetStatus(model.Status);
                project.Priority = model.Priority;
                project.IsAdminSubscriber = model.IsAdminSubscriber;

                _projectRepository.Update(project);
                await NotifyAdminIfSubscribedAsync(project, "Project updated", "Update");
                return Ok(ProjectAPIModel.FromProject(project));
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        [HttpDelete("{projectId:int}")]
        public async Task<IActionResult> DeleteProject(int projectId)
        {
            try
            {
                var project = _projectRepository.GetById(projectId);
                if (project == null)
                    return NotFound();

                _projectRepository.Delete(projectId);
                await NotifyAdminIfSubscribedAsync(project, "Project deleted", "Delete");
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        private async Task NotifyAdminIfSubscribedAsync(Project project, string html, string title)
        {
            if (project.IsAdminSubscriber)
            {
                await _mailSender.SendNotificationAsync(html, title);
            }
        }
    }
}
