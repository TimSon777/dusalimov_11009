﻿using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;
using TaskTracker.WebAPI.APIModels;
using TaskTracker.WebAPI.APIModels.ProjectAPIModels;
using TaskTracker.WebAPI.APIModels.TaskItemAPIModels;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.WebAPI.APIModels.FeatureAPIModels;

namespace TaskTracker.WebAPI.Controllers
{
    [Route("api/features")]
    [ApiController]
    public class FeatureController : ControllerBase
    {
        private readonly IRepository<Feature> _featureRepository;
        private readonly ITaskItemRepository _taskItemRepository;
        
        public FeatureController(IRepository<Feature> featureRepository, ITaskItemRepository taskItemRepository)
        {
            _featureRepository = featureRepository;
            _taskItemRepository = taskItemRepository;
        }

        [HttpGet("{taskItemId:int}/features/{featureId:int}")]
        public ActionResult<ReadFeatureAPIModel> GetById(int featureId, int taskItemId)
        {
            try
            {
                var task = _taskItemRepository.GetById(taskItemId);
                if (task is null)
                {
                    return NotFound();
                }

                var feature = task.Features.FirstOrDefault(c => c.Id == featureId);

                if (feature is null)
                {
                    return NotFound();
                }
                
                return Ok(new ReadFeatureAPIModel(feature));
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        [HttpGet("{taskId:int}/features")]
        public ActionResult<IEnumerable<ReadFeatureAPIModel>> GetAll(int taskId)
        {
            try
            {
                var task = _taskItemRepository.GetById(taskId);
                if (task == null)
                    return NotFound();

                var result =
                    task.Features
                        .Select(t => new ReadFeatureAPIModel(t))
                        .ToList();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        [HttpPost("{taskId:int}/features")]
        public ActionResult CreateFeature(int taskId, [FromBody] CreateFeatureAPIModel model)
        {
            try
            {
                var task = _taskItemRepository.GetById(taskId);
                if (task == null)
                    return BadRequest();

                var feature = new Feature
                {
                    Name = model.Name,
                    IsDeleted = false,
                    TaskItem = task
                };
                
                _featureRepository.Add(feature);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        [HttpPut("{taskItemId:int}/feature/{featureId:int}")]
        public ActionResult UpdateFeature(int taskItemId, int featureId, [FromBody] UpdateFeatureAPIModel model)
        {
            try
            {
                var taskItem = _taskItemRepository.GetById(taskItemId);
                if (taskItem == null)
                    return NotFound();

                var feature = taskItem.Features.FirstOrDefault(t => t.Id == featureId);
                if (feature == null)
                    return NotFound();

                feature.Name = model.Name;

                _featureRepository.Update(feature);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }

        [HttpDelete("{taskItemId:int}/feature/{featureId:int}")]
        public IActionResult DeleteFeature(int featureId, int taskItemId)
        {
            try
            {
                var feature = _featureRepository.GetById(featureId);

                if (feature is null || feature.TaskItem.Id != taskItemId)
                {
                    return NotFound();
                }

                _featureRepository.Delete(feature.Id);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorAPIModel(ex.Message));
            }
        }
    }
}
