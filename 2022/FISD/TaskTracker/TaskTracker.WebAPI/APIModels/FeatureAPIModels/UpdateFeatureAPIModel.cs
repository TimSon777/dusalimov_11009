﻿namespace TaskTracker.WebAPI.APIModels.FeatureAPIModels;

public class UpdateFeatureAPIModel
{
    public string Name { get; set; }
}