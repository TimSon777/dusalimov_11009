﻿namespace TaskTracker.WebAPI.APIModels.FeatureAPIModels;

public class CreateFeatureAPIModel
{
    public string Name { get; set; }
}