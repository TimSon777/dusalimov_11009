﻿using TaskTracker.Core.Entities;
using TaskTracker.WebAPI.APIModels.TaskItemAPIModels;

namespace TaskTracker.WebAPI.APIModels.FeatureAPIModels;

public class ReadFeatureAPIModel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public TaskItemAPIModel TaskItem { get; set; }

    public ReadFeatureAPIModel(Feature feature)
    {
        Id = feature.Id;
        Name = feature.Name;
        TaskItem = TaskItemAPIModel.FromTaskItem(feature.TaskItem);
    }
}