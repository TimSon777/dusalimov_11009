﻿using Microsoft.Extensions.Options;
using TaskTracker.Core.Interfaces;
using TaskTracker.WebAPI.Models;

namespace TaskTracker.Infrastructure.Services;

public class MailSender : IMailSender
{
    private readonly AdminSettings _adminSettings;
    public MailSender(IOptions<AdminSettings> adminSettingsOptions)
    {
        _adminSettings = adminSettingsOptions.Value;
    }
    public Task SendNotificationAsync(string html, string title)
    {
        Console.WriteLine($"Title: {title}\nHtml:{html}\n Send to {_adminSettings.Email}");
        return Task.FromResult(0);
    }
}