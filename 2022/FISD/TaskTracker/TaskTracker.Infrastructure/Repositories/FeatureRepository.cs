﻿using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Entities;
using TaskTracker.Core.Interfaces;

namespace TaskTracker.Infrastructure.Repositories;

public class FeatureRepository : IRepository<Feature>
{
    private readonly ApplicationDbContext _context;

    public FeatureRepository(ApplicationDbContext context)
    {
        _context = context;
    }
    
    public Feature? GetById(int id)
    {
        return _context.Features
            .Include(c => c.TaskItem)
            .FirstOrDefault(f => f.Id == id);
    }

    public IEnumerable<Feature> GetAll()
    {
        return _context.Features;
    }

    public void Add(Feature entity)
    {
        _context.Features.Add(entity);
        _context.SaveChanges();
    }

    public void Update(Feature entity)
    {
        _context.Features.Update(entity);
        _context.SaveChanges();
    }

    public void Delete(int id)
    {
        var project = _context
            .Features
            .FirstOrDefault(p => p.Id == id);

        if (project is null || project.IsDeleted)
        {
            throw new ArgumentException("Feature cannot be found");
        }
        
        project.IsDeleted = true;
        _context.SaveChanges();
    }
}