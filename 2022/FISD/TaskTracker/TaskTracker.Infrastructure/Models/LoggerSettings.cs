﻿namespace TaskTracker.Infrastructure.Models;

public class LoggerSettings
{
    public string RelativePathToFile { get; set; }
}