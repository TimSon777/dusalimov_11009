﻿function displayFiles() {
    $.ajax({
        method: "GET",
        url: 'https://localhost:7002/FilesHandler/GetFiles',
        success: (response) => {
            appendFiles(response)
        }
    })
}

function submitOrAlert() {
    let form = document.getElementById('form-add-file')
    let inputFile = document.getElementById('input-file')
    form.onsubmit = () => {
        if(inputFile.files.length === 0) {
            alert('Выберите файл')
            return false
        }
    }

    return true
}

function appendFiles(files) {
    for (let i = 0; i < files.length; i++) {
        let extension = findExtension(files[i])
        let isTxt = extension && extension === "txt"
        let buttonOpenTxtFile = isTxt ?
            `<li>
                    <button class="show-txt-file" value="${files[i]}">Открыть</button>
                </li>` : ""
        document.getElementById('files').innerHTML += `
            <li>
                <ul class="file">
                    <li>
                        <a href="/FilesHandler/Download?fileName=${files[i]}" download="${files[i]}">${files[i]}</a>
                    </li>
                    <li>
                        <form method="post" action="/FilesHandler/Delete">
                            <input type="hidden" value="${files[i]}" name="path">
                            <input type="submit" value="Удалить">
                        </form>
                    </li>
                    ${buttonOpenTxtFile}
                </ul>
            </li>
        `
    }

    let buttonsShowTxt = document.querySelectorAll(".show-txt-file");
    for (let i = 0; i < buttonsShowTxt.length; i++) {
        buttonsShowTxt[i].onclick = function () {
            let fileName = this.getAttribute("value")
            $.ajax({
                method: 'GET',
                url: `/FilesHandler/ShowTxt?fileName=${fileName}`,
                success: (response) => {
                    $("#txt-file").html(response)
                    $("#file-name-for-save").attr("value", fileName)
                }
            })
        }
    }
}

$(document).ready(function () {
    displayFiles()
    submitOrAlert()
})