﻿function findExtension(filename) {
    let file = filename.split(".");
    if(file.length === 1 || (file.length === 2 && (file[0] === "" && filename[1] === "")))
        return null;
    
    return file.pop();
}

function getMessageAndAlert() {
    $.ajax({
        method: 'GET',
        url: '/GetMessage',
        success: (response) => {
            alert(response)
        }
    })
}

function addNavigation() {
    $.each($('.nav-links'), (index, link) => {
        $(link).click((e) => {
            e.preventDefault()
            $.ajax({
                url: `${$(link).href}`,
                method: 'GET',
                success: (response) => {
                    $('#content').html(response)
                }
            })
        })
    })
}

$(document).ready(function () {
    if (document.URL === "/")
        getMessageAndAlert()
    addNavigation()
})
