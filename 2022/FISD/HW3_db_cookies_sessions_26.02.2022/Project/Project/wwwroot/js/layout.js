﻿function insertInto(id, url) {
    $.ajax({
        url: url,
        method: 'GET',
        success: (response) => {
            $(`#${id}`).html(response)
        }
    })
}

function insertLayout() {
    insertInto("header", "/html/layout/header.html")
}

$(document).ready(function () {
    insertLayout()
})
