﻿namespace Project.Services.Hashing;

public interface IHashing
{
    string Hash(string password, byte[] salt);
}