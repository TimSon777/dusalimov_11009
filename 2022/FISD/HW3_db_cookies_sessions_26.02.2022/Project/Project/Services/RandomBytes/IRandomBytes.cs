﻿namespace Project.Services.RandomBytes;

public interface IRandomBytes
{
    byte[] Generate(int length);
}