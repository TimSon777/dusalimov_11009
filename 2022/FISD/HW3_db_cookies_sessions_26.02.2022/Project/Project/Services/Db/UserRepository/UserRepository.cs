﻿using Microsoft.EntityFrameworkCore;

namespace Project.Services.Db.UserRepository;

public class UserRepository : Repository<User>, IUserRepository
{
    public UserRepository(ApplicationDbContext dbContext) 
        : base(dbContext)
    { }
    
    public new async Task<User> AddAndSaveAsync(User entity) 
        => await base.AddAndSaveAsync(entity);

    public new async Task<User?> FindByIdAsync(int id) 
        => await base.FindByIdAsync(id);

    public async Task<User?> FindByLoginAsync(string login) 
        => await DbSet.FirstOrDefaultAsync(user => user.Login == login);
}