﻿namespace Project.Services.Db.UserRepository;

public interface IUserRepository : IRepository<User>
{
    Task<User?> FindByLoginAsync(string login);
}