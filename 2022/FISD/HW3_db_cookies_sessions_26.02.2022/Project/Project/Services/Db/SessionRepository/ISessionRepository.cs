﻿namespace Project.Services.Db.SessionRepository;

public interface ISessionRepository : IRepository<Session>
{
    Task<Session?> FindSessionByTokenAsync(string token);
}