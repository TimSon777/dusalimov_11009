﻿namespace Project.Services.Db;

public interface IRepository<TEntity>
{
    Task<TEntity> AddAndSaveAsync(TEntity entity);
    Task<TEntity?> FindByIdAsync(int id);
}