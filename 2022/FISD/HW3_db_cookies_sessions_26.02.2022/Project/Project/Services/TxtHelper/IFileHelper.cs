﻿namespace Project.Services.TxtHelper;

public interface IFileHelper
{
    Task<string> FindContentFileAsync(string relativePathToFile);
    Task<bool> TryOverwriteAsync(string relativePathToFile, string content);
}