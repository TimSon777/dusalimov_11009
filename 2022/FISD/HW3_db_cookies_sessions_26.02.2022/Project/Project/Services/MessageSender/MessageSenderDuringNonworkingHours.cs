﻿namespace Project.Services.MessageSender;

public class MessageSenderDuringNonworkingHours : IMessageSender
{
    public async Task SendAsync(HttpResponse response)
    {
        await response.WriteAsync("Добрый день! В настоящий момент сайт работает в урезанном режиме!");
    }
}