﻿namespace Project.Extensions;

// ReSharper disable once InconsistentNaming
public static class IEnumerableExtensions
{
    public static string Join(this IEnumerable<byte> source, string param = "X2") 
        => string.Join("", source.Select(e => e.ToString(param)));
}