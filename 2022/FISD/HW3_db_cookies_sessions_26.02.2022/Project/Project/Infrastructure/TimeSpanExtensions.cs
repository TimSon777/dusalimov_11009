﻿namespace Project.Infrastructure;

public static class TimeSpanExtensions
{
    public static bool Between(this TimeSpan middle, TimeSpan left, TimeSpan right) 
        => left <= middle && middle <= right;
}