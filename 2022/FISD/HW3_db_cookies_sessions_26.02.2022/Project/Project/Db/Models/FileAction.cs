﻿namespace Project.Db.Models;

public enum FileAction : byte
{
    Created = 1,
    Changed = 2,
    Removed = 3
}