﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Db.Models;

public class FileInformation : CommonEntity
{
    [Required]
    [Column(TypeName = "varchar(100)")]
    public string Title { get; set; }

    [Required]
    [Column( TypeName = "varchar(10)")]
    public string Extension { get; set; }

    [Required]
    public ICollection<FileHistory> FileHistories { get; set; } = new List<FileHistory>();
}