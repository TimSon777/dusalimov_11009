﻿using System.ComponentModel.DataAnnotations;

namespace Project.Db.Models;

public class FileHistory : CommonEntity
{
    [Required]
    public FileInformation FileInfo { get; set; }
    
    [Required]
    public FileAction FileAction { get; set; }
}