﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Db.Models;

public class User : CommonEntity
{
    [Required]
    [Column(TypeName = "varchar(100)")]
    public string HashedPassword { get; set; }
    
    [Required]
    [Column(TypeName = "varchar(20)")]
    public string Login { get; set; }
    
    [Required]
    public byte[] Salt { get; set; }

    public ICollection<FileHistory> FileHistories { get; set; }
}