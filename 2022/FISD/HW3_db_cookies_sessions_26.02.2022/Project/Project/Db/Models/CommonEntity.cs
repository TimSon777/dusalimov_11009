﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Db.Models;

public abstract class CommonEntity
{
    public int Id { get; set; }

    [Required]
    [Column(TypeName = "timestamp")]
    public DateTime Publication { get; set; } = DateTime.Now;
}