﻿using Microsoft.EntityFrameworkCore;

namespace Project.Db;

public sealed class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<User> Users => Set<User>();

    public DbSet<FileInformation> FileInfos  => Set<FileInformation>();

    public DbSet<FileHistory> FileHistories  => Set<FileHistory>();
    
    public DbSet<Session> Sessions => Set<Session>();
}