using Microsoft.EntityFrameworkCore;
using Project.Infrastructure;
using Project.Middleware.FilesHandler;
using Project.Middleware.MessageSender;
using Project.Middleware.Registration;
using Project.Middleware.StaticFiles;
using Project.Middleware.WelcomePage;
using Project.Services.Db.SessionRepository;
using Project.Services.Db.UserRepository;
using Project.Services.Hashing;
using Project.Services.MessageSender;
using Project.Services.RandomBytes;
using Project.Services.TxtHelper;
using Project.Middleware.Authentication;
using Project.Middleware.Authorization;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;

services
    .AddScoped<ISessionRepository, SessionRepository>()
    .AddScoped<IUserRepository, UserRepository>()
    .AddTransient<IRandomBytes, RandomBytes>()
    .AddTransient<IHashing, Hashing>()
    .AddTransient<IFileHelper, FileHelper>()
    .AddTransient<MessageSenderDuringWorkingHours>()
    .AddTransient<MessageSenderDuringNonworkingHours>()
    .AddTransient<IMessageSender>(provider =>
        DateTime.Now.TimeOfDay.Between(
            TimeSpan.FromHours(6),
            TimeSpan.FromHours(18))
            ? provider.GetRequiredService<MessageSenderDuringWorkingHours>()
            : provider.GetRequiredService<MessageSenderDuringNonworkingHours>())
   .AddDbContext<ApplicationDbContext>(options => 
        options
            .UseNpgsql(configuration.GetConnectionString("FilesManagerDb")!)
            .UseAllCheckConstraints())
    .AddDistributedMemoryCache()
    .AddSession(options => options.IdleTimeout = TimeSpan.FromDays(365));

var app = builder.Build();

app
    .UseMyStaticFiles()
    .UseMyWelcomePage("wwwroot/html/welcome.html")
    .UseMessageSender("/GetMessage")
    .UseSession()
    .UseRegistration("/register",
        "login",
        "password",
        context => context.Response.Redirect("/html/error.html"),
        "Пользователь уже существует",
        "html/autho.html")
    .UseMyAuthentication("/autho", "login", "password", "remember",
        context => context.Response.Redirect("/html/error.html"), 
        "fu")
    .UseMyAuthorization()
    .UseFileHandler("files");
app.Run();

