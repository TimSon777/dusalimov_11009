﻿using Project.Services.Db.UserRepository;
using Project.Services.Hashing;
using Project.Services.RandomBytes;

namespace Project.Middleware.Registration;

public class Registration
{
    private readonly string _url;
    private readonly string _loginName;
    private readonly string _passwordName;
    private readonly IHashing _hashing;
    private readonly IRandomBytes _randomBytes;
    private readonly Action<HttpContext> _selectorWhenError;
    private readonly string _messageWhenUserAlreadyExists;
    private readonly string _redirectToAuthorization;
    private readonly RequestDelegate _next;
    
    public Registration(RequestDelegate next, 
        string url, 
        string loginName, 
        string passwordName,
        IHashing hashing,
        IRandomBytes randomBytes,
        Action<HttpContext> selectorWhenError,
        string messageWhenUserAlreadyExists,
        string redirectToAuthorization)
    {
        _next = next;
        _url = url;
        _loginName = loginName;
        _passwordName = passwordName;
        _hashing = hashing;
        _randomBytes = randomBytes;
        _selectorWhenError = selectorWhenError;
        _messageWhenUserAlreadyExists = messageWhenUserAlreadyExists;
        _redirectToAuthorization = redirectToAuthorization;
    }

    public async Task InvokeAsync(HttpContext context, IUserRepository userRepository)
    {
        var request = context.Request;
        var response = context.Response;
        
        if (request.Path != _url)
        {
            await _next.Invoke(context);
            return;
        }

        if (request.Method != "POST")
        {
            _selectorWhenError(context);
            return;
        }

        var login = request.Form[_loginName].ToString();
        var password = request.Form[_passwordName].ToString();
        
        if (login == "" || password == "")
        {
            _selectorWhenError(context);
            return;
        }

        var user = await userRepository.FindByLoginAsync(login);
        if (user is not null)
        {
            response.StatusCode = 409;
            await response.WriteAsync(_messageWhenUserAlreadyExists);
        }

        var salt = _randomBytes.Generate(20);
        var hashedPassword = _hashing.Hash(password, salt);
        
        var newUser = new User
        {
            HashedPassword = hashedPassword,
            Login = login,
            Salt = salt
        };
        
        await userRepository.AddAndSaveAsync(newUser);
        response.Redirect(_redirectToAuthorization);
    }
}