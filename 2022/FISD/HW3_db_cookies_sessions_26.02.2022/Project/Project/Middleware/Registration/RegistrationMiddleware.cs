﻿namespace Project.Middleware.Registration;

public static class RegistrationMiddleware
{
    public static IApplicationBuilder UseRegistration(this IApplicationBuilder app, 
        string url, 
        string loginName, 
        string passwordName, 
        Action<HttpContext> selectorWhenError, 
        string messageWhenUserAlreadyExists,
        string redirectToAuthorization)
        => app.UseMiddleware<Registration>(url, 
            loginName, 
            passwordName, 
            selectorWhenError, 
            messageWhenUserAlreadyExists, 
            redirectToAuthorization);
}