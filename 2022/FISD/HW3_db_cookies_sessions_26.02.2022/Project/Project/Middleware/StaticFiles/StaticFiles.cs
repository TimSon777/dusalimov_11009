﻿namespace Project.Middleware.StaticFiles;

public class StaticFiles : Middleware
{
    private readonly string _pathToStaticDirectory;
    private readonly IHostEnvironment _environment;

    private readonly Dictionary<string, string> _mimeTypes = new()
    {
        {"html", "text/html"},
        {"css", "text/css"},
        {"js", "application/javascript"}
    };

    public StaticFiles(RequestDelegate next, 
        string pathToStaticDirectory, 
        IHostEnvironment environment,
        Dictionary<string, string>? additionalMimeTypes = null)
        : base(next)
    {
        _pathToStaticDirectory = pathToStaticDirectory;
        _environment = environment;

        if (additionalMimeTypes is not null)
        {
            _mimeTypes = _mimeTypes
                .Union(additionalMimeTypes)
                .ToDictionary(x => x.Key, x => x.Value);
        }
    }

    public override async Task InvokeAsync(HttpContext context)
    {
        var requestPath = context.Request.Path.ToString();
        var normalRequestPath = requestPath.Last() == '/' ? requestPath[..^1] : requestPath;
        var pathToFile = _environment.ContentRootPath + "/" + _pathToStaticDirectory + normalRequestPath;

        if (File.Exists(pathToFile))
        {
            var extension = FindExtension(normalRequestPath);
            
            if (_mimeTypes.ContainsKey(extension))
                context.Response.Headers.ContentType = _mimeTypes[extension];

            await context.Response.SendFileAsync(pathToFile);
            return;
        }

        await Next.Invoke(context);
    }

    private static string FindExtension(string pathRequest)
        => pathRequest.Split('/').Last().Split('.')[1];
}