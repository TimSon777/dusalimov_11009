﻿namespace Project.Middleware.StaticFiles;

public static class StaticFilesExtensions
{
    public static IApplicationBuilder UseMyStaticFiles(this IApplicationBuilder app, string pathToDirectory = "wwwroot")
        => app.UseMiddleware<StaticFiles>(pathToDirectory);
}