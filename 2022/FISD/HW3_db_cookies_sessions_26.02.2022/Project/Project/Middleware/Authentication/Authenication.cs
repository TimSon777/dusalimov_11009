﻿using Project.Extensions;
using Project.Services.Db.SessionRepository;
using Project.Services.Db.UserRepository;
using Project.Services.Hashing;
using Project.Services.RandomBytes;

namespace Project.Middleware.Authentication;

public class Authentication
{
    private readonly RequestDelegate _next;
    private readonly string _url;
    private readonly string _loginName;
    private readonly string _passwordName;
    private readonly string _rememberedName;
    private readonly Action<HttpContext> _selectorWhenError;
    private readonly IHashing _hashing;
    private readonly string _messageWhenUserNotExistsOrPasswordWrong;
    private readonly IRandomBytes _randomBytes;

    public Authentication(RequestDelegate next, 
        string url, string loginName, string passwordName, string rememberedName,
        Action<HttpContext> selectorWhenError, 
        IHashing hashing,
        string messageWhenUserNotExistsOrPasswordWrong,
        IRandomBytes randomBytes)
    {
        _next = next;
        _url = url;
        _loginName = loginName;
        _passwordName = passwordName;
        _rememberedName = rememberedName;
        _selectorWhenError = selectorWhenError;
        _hashing = hashing;
        _messageWhenUserNotExistsOrPasswordWrong = messageWhenUserNotExistsOrPasswordWrong;
        _randomBytes = randomBytes;
    }
    
    public async Task InvokeAsync(HttpContext context, IUserRepository userRepository, ISessionRepository sessionRepository)
    {
        var request = context.Request;
        var response = context.Response;

        if (request.Path != _url)
        {
            await _next.Invoke(context);
            return;
        }
        
        if (request.Method != "POST")
        {
            _selectorWhenError(context);
            return;
        }
        
        var login = request.Form[_loginName].ToString();
        var password = request.Form[_passwordName].ToString();
        var isRemember = request.Form[_rememberedName] == "on";

        var user = await userRepository.FindByLoginAsync(login);
        
        if (user is null || _hashing.Hash(password, user.Salt) != user.HashedPassword)
        {
            response.StatusCode = 400;
            await response.WriteAsync(_messageWhenUserNotExistsOrPasswordWrong);
            return;
        }

        if (isRemember)
        {
            var token = _randomBytes.Generate(40).Join();
            response.Cookies.Append("token", token);
            await sessionRepository.AddAndSaveAsync(new Session
            {
                Token = token,
                User = user
            });
        }
        else
        {
            context.Session.SetString("login", user.Login); 
        }
        
        response.Redirect("html/index.html");
    }
}