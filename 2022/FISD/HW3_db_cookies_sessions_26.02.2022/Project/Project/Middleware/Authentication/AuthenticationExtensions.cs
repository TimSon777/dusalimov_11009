﻿namespace Project.Middleware.Authentication;

public static class AuthenticationExtensions
{
    public static IApplicationBuilder UseMyAuthentication(this IApplicationBuilder app, 
        string url, string loginName, string passwordName, 
        string rememberedName, 
        Action<HttpContext> selectorWhenError, string messageWhenUserNotExistsOrPasswordWrong)
        => app.UseMiddleware<Authentication>(url, loginName, passwordName, rememberedName, selectorWhenError, messageWhenUserNotExistsOrPasswordWrong);
}