﻿using Microsoft.EntityFrameworkCore;
using Project.Services.TxtHelper;

namespace Project.Middleware.FilesHandler;

public class FilesHandler
{
    private readonly string _pathToDirectory;
    private readonly RequestDelegate _next;
    private readonly string _relativePathToDirectory;
    private readonly IFileHelper _fileHelper;
    private User? CurrentUser { get; set; }

    public FilesHandler(RequestDelegate next,
        string relativePathToDirectory,
        IFileHelper fileHelper,
        IHostEnvironment environment)
    {
        _pathToDirectory = environment.ContentRootPath + "/" + relativePathToDirectory + "/";
        _next = next;
        _relativePathToDirectory = relativePathToDirectory;
        _fileHelper = fileHelper;
    }

    private static readonly Action<HttpContext> RedirectWhenOk
        = context => context.Response.Redirect("/html/fileHandler.html");

    private static readonly Action<HttpContext> RedirectWhenError
        = context => context.Response.Redirect("/html/error.html");

    public async Task InvokeAsync(HttpContext context, ApplicationDbContext appDbContext)
    {
        var request = context.Request;
        var response = context.Response;
        CurrentUser = context.Items["user"] as User;

        switch (request.Path.Value)
        {
            case "/FilesHandler/GetFiles":
            {
                await OnGetAsync(context);
                return;
            }
            case "/FilesHandler/Delete":
            {
                await OnDeleteAsync(context, request, appDbContext);
                return;
            }
            case "/FilesHandler/Add":
            {
                await OnAddAsync(context, request, appDbContext);
                return;
            }
            case "/FilesHandler/Download":
            {
                await OnDownloadAsync(request, response);
                return;
            }
            case "/FilesHandler/ShowTxt":
            {
                await OnShowTxtAsync(context);
                return;
            }
            case "/FilesHandler/Save":
            {
                await OnSaveAsync(context, appDbContext);
                return;
            }
        }

        await _next.Invoke(context);
    }

    private async Task OnSaveAsync(HttpContext context, ApplicationDbContext applicationDbContext)
    {
        if (CurrentUser is null)
        {
            RedirectWhenError(context);
            return;
        }

        var request = context.Request;
        var fileName = request.Form["fileName"];

        if (Path.GetExtension(fileName) != ".txt")
        {
            RedirectWhenError(context);
            return;
        }

        var content = request.Form["content"];

        var isOk = await _fileHelper
            .TryOverwriteAsync(string.Join("/", _relativePathToDirectory, fileName), content);

        if (!isOk)
            RedirectWhenError(context);
        else
        {
            var file = applicationDbContext.FileInfos.Include(f => f.FileHistories)
                .FirstOrDefault(f => f.Title == fileName.ToString());
            file!.FileHistories.Add(new FileHistory
            {
                FileAction = FileAction.Changed
            });
            await applicationDbContext.SaveChangesAsync();
            RedirectWhenOk(context);
        }
    }

    private async Task OnShowTxtAsync(HttpContext context)
    {
        var request = context.Request;
        var response = context.Response;
        var fileName = request.Query["fileName"];

        if (Path.GetExtension(fileName) != ".txt")
        {
            RedirectWhenError(context);
            return;
        }

        var content = await _fileHelper.FindContentFileAsync(string.Join("/", _relativePathToDirectory, fileName));
        await response.WriteAsync(content);
    }

    private async Task OnDownloadAsync(HttpRequest request, HttpResponse response)
    {
        var fileName = request.Query["fileName"];
        var pathToFile = _pathToDirectory + request.Query["fileName"];

        if (File.Exists(pathToFile))
            await response.SendFileAsync(_relativePathToDirectory + "/" + fileName);
    }

    private async Task OnAddAsync(HttpContext context, HttpRequest request, ApplicationDbContext applicationDbContext)
    {
        if (CurrentUser is null)
        {
            RedirectWhenError(context);
            return;
        }

        if (!IsPostOrRedirect(context, request)) return;

        var form = await request.ReadFormAsync();

        if (form.Files.Count == 0)
        {
            RedirectWhenError(context);
            return;
        }

        foreach (var file in request.Form.Files)
        {
            var pathToFile = _pathToDirectory + file.FileName;

            await using var stream = File.Create(pathToFile);
            await file.CopyToAsync(stream);

            var entity = applicationDbContext.FileInfos.Add(new FileInformation
            {
                Extension = file.FileName.Split('.').Last(),
                Title = file.FileName
            });

            entity.Entity.FileHistories.Add(
                new FileHistory
                {
                    FileAction = FileAction.Created,
                });
            await applicationDbContext.SaveChangesAsync();
        }

        RedirectWhenOk(context);
    }

    private async Task OnDeleteAsync(HttpContext context, HttpRequest request,
        ApplicationDbContext applicationDbContext)
    {
        if (CurrentUser is null)
        {
            RedirectWhenError(context);
            return;
        }

        if (!IsPostOrRedirect(context, request)) return;

        var form = await request.ReadFormAsync();
        var path = form["path"];

        if (string.IsNullOrEmpty(path))
        {
            RedirectWhenError(context);
            return;
        }

        var pathToFile = _pathToDirectory + path;

        if (File.Exists(pathToFile))
        {
            File.Delete(pathToFile);
            var file = await applicationDbContext.FileInfos.Include(a => a.FileHistories)
                .FirstOrDefaultAsync(file => file.Title == path.ToString());
            
            file!.FileHistories.Add(new FileHistory
            {
                FileAction = FileAction.Removed,
            });
            await applicationDbContext.SaveChangesAsync();

            
            RedirectWhenOk(context);
            return;
        }

        RedirectWhenError(context);
    }

    private async Task OnGetAsync(HttpContext context)
    {
        var files = Directory.EnumerateFiles(_pathToDirectory).Select(Path.GetFileName);
        await context.Response.WriteAsJsonAsync(files);
    }

    private static bool IsPostOrRedirect(HttpContext context, HttpRequest request)
    {
        if (request.Method == "POST") return true;
        RedirectWhenError(context);
        return false;
    }
}