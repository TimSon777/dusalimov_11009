﻿namespace Project.Middleware.MessageSender;

public static class MessageSenderMiddlewareExtensions
{
    public static IApplicationBuilder UseMessageSender(this IApplicationBuilder app, string url)
        => app.UseMiddleware<MessageSenderMiddleware>(url);
}