﻿using Project.Services.MessageSender;

namespace Project.Middleware.MessageSender;

public class MessageSenderMiddleware : Middleware
{
    private readonly string _url;
    private readonly IMessageSender _messageSender;

    public MessageSenderMiddleware(RequestDelegate next, string url, IMessageSender messageSender) : base(next)
    {
        _url = url;
        _messageSender = messageSender;
    }

    public override async Task InvokeAsync(HttpContext context)
    {
        if (context.Request.Path == _url)
            await _messageSender.SendAsync(context.Response);
        else
            await Next.Invoke(context);
    }
}