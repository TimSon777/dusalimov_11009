﻿namespace Project.Middleware;

public abstract class Middleware
{
    protected readonly RequestDelegate Next;
    protected Middleware(RequestDelegate next) => Next = next;
    public abstract Task InvokeAsync(HttpContext context);
}