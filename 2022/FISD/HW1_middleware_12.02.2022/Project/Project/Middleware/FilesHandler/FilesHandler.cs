﻿namespace Project.Middleware.FilesHandler;

public class FilesHandler : Middleware
{
    private readonly string _pathToDirectory;
    private readonly string _relativePathToDirectory;
    
    public FilesHandler(RequestDelegate next, string relativePathToDirectory) : base(next)
    {
        _pathToDirectory = Settings.CurrentDirectory + relativePathToDirectory + "/";
        _relativePathToDirectory = relativePathToDirectory;
    }

    private static readonly Action<HttpContext> RedirectWhenOk 
        = context => context.Response.Redirect("/html/fileHandler.html");
    
    private static readonly Action<HttpContext> RedirectWhenError 
        = context => context.Response.Redirect("/html/error.html");
    
    public override async Task InvokeAsync(HttpContext context)
    {
        var request = context.Request;
        var response = context.Response;
        
        switch (request.Path.Value)
        {
            case "/FilesHandler/GetFiles":
            {
                await OnGetAsync(context);
                return;
            }
            case "/FilesHandler/Delete":
            {
                await OnDeleteAsync(context, request);
                return;
            }
            case "/FilesHandler/Add":
            {
                await OnAddAsync(context, request);
                return;
            }
            case "/FilesHandler/Download":
            {
                await OnDownloadAsync(request, response);
                return;
            }
        }
        
        await Next.Invoke(context);
    }

    private async Task OnDownloadAsync(HttpRequest request, HttpResponse response)
    {
        var fileName = request.Query["fileName"];
        var pathToFile = _pathToDirectory + request.Query["fileName"];
        
        if (File.Exists(pathToFile))
            await response.SendFileAsync(_relativePathToDirectory + "/" + fileName);
    }

    private async Task OnAddAsync(HttpContext context, HttpRequest request)
    {
        if (!IsPostOrRedirect(context, request)) return;
        
        var form = await request.ReadFormAsync();
        
        if (form.Files.Count == 0)
        {
            RedirectWhenError(context);
            return;
        }
                
        foreach (var file in request.Form.Files)
        {
            var pathToFile = _pathToDirectory + file.FileName;
            await using var stream = File.Create(pathToFile);
            await file.CopyToAsync(stream);
        }

        RedirectWhenOk(context);
    }

    private async Task OnDeleteAsync(HttpContext context, HttpRequest request)
    {
        if (!IsPostOrRedirect(context, request)) return;
        
        var form = await request.ReadFormAsync();
        var path = form["path"];
                
        if (string.IsNullOrEmpty(path))
        {
            RedirectWhenError(context);
            return;
        }

        var pathToFile = _pathToDirectory + path;
        
        if (File.Exists(pathToFile))
        {
            File.Delete(pathToFile);
            RedirectWhenOk(context);
            return;
        }
        
        RedirectWhenError(context);
    }

    private async Task OnGetAsync(HttpContext context)
    {
        var files = Directory.EnumerateFiles(_pathToDirectory).Select(Path.GetFileName);
        await context.Response.WriteAsJsonAsync(files);
    }
    
    private static bool IsPostOrRedirect(HttpContext context, HttpRequest request)
    {
        if (request.Method == "POST") return true;
        RedirectWhenError(context);
        return false;
    }
}