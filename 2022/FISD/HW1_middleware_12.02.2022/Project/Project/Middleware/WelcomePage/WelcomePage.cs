﻿namespace Project.Middleware.WelcomePage;

public class WelcomePage : Middleware
{
    private readonly string _pathToFile;
    private readonly string _url;

    public WelcomePage(RequestDelegate next, string pathToFile, string url) : base(next)
    {
        _pathToFile = pathToFile;
        _url = url;
    }

    public override async Task InvokeAsync(HttpContext context)
    {
        if (context.Request.Path == _url)
        {
            context.Response.Headers.ContentType = "text/html";
            await context.Response.SendFileAsync(_pathToFile);
            return;
        }
        
        await Next.Invoke(context);
    }
}