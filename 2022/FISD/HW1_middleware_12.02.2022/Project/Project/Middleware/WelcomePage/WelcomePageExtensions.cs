﻿namespace Project.Middleware.WelcomePage;

public static class WelcomePageExtensions
{
    public static IApplicationBuilder UseMyWelcomePage(this IApplicationBuilder app, string namePage, string url = "/") 
        => app.UseMiddleware<WelcomePage>(namePage, url);
}