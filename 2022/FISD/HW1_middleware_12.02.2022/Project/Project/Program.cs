using Project.Middleware.FilesHandler;
using Project.Middleware.StaticFiles;
using Project.Middleware.WelcomePage;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app
    .UseMyStaticFiles()
    .UseMyWelcomePage("wwwroot/html/index.html")
    .UseFileHandler("files");

app.Run();

