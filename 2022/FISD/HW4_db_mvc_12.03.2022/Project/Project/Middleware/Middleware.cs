﻿namespace Project.Middleware;

public class Middleware
{
    protected readonly RequestDelegate Next;
    protected Middleware(RequestDelegate next) => Next = next;
}