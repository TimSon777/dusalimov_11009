﻿using Project.Services.Db.SessionRepository;
using Project.Services.Db.UserRepository;

namespace Project.Middleware.Authorization;

public class Authorization : Middleware
{
    public Authorization(RequestDelegate next) 
        : base(next) 
    { }

    private static async Task TryGetUserFromSessionAndAddToItems(HttpContext context, IUserRepository userRepository)
    {
        var login = context.Session.GetString("login");
        
        if (login is not null)
        {
            var user = await userRepository.FindByLoginAsync(login);
            
            if (user is not null)
            {
                context.Items.Add("user", user);
            }
        }
    }
    
    public async Task InvokeAsync(HttpContext context,
        IUserRepository userRepository,
        ISessionRepository sessionRepository)
    {
        var token = context.Request.Cookies["token"];
        
        if (token is null)
        {
            await TryGetUserFromSessionAndAddToItems(context, userRepository);
        }
        else
        {
            var session = await sessionRepository.FindSessionByTokenAsync(token);
            
            if (session is not null)
            {
                context.Items.Add("user", session.User);
            }
            else
            {
                await TryGetUserFromSessionAndAddToItems(context, userRepository);
            }
        }

        await Next.Invoke(context);
    }
}