﻿namespace Project.Middleware.Authorization;

public static class AuthorizationExtensions
{
    public static IApplicationBuilder UseMyAuthorization(this IApplicationBuilder app)
        => app.UseMiddleware<Authorization>();
}