using Microsoft.EntityFrameworkCore;
using Project.Middleware.Authorization;
using Project.Services.Db.SessionRepository;
using Project.Services.Db.UserRepository;
using Project.Services.FileHelper;
using Project.Services.Hashing;
using Project.Services.RandomBytes;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;

services.AddControllersWithViews();

services.AddDbContext<AppDbContext>(options =>
        options.UseNpgsql(configuration.GetConnectionString("FilesManagerDb"))
            .UseAllCheckConstraints())
    .AddTransient<IUserRepository, UserRepository>()
    .AddTransient<ISessionRepository, SessionRepository>();

services.AddTransient<IHashing, Hashing>()
    .AddTransient<IRandomer, Randomer>()
    .AddTransient<IFileHelper, FileHelper>();

services.AddDistributedMemoryCache()
    .AddSession();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error")
        .UseHsts();
}

app.UseHttpsRedirection()
    .UseStaticFiles()
    .UseRouting()
    .UseSession()
    .UseMyAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();