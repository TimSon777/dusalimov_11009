﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project.Db.Models;

public class Session : CommonEntity
{
    [Required]
    public User User { get; set; }

    [Required]
    [Column(TypeName = "varchar(100)")]
    public string Token { get; set; }

    public Session(User user, string token)
    {
        User = user;
        Token = token;
    }
    
    public Session()
    { }
}