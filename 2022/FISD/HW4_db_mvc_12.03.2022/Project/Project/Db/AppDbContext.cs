﻿using Microsoft.EntityFrameworkCore;

namespace Project.Db;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) 
        : base(options)
    { }
    
    public DbSet<User> Users => Set<User>();

    public DbSet<FileInformation> FileInfos => Set<FileInformation>();

    public DbSet<FileHistory> FileHistories => Set<FileHistory>();
    
    public DbSet<Session> Sessions => Set<Session>();
}