﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeTypes;
using Project.Extensions;
using Project.Filters;
using Project.Services.FileHelper;

namespace Project.Controllers;

public class FileController : Controller
{
    private readonly IFileHelper _fileHelper;
    private readonly AppDbContext _dbContext;
    private readonly string _pathToDirectory;

    public FileController(IHostEnvironment environment, IFileHelper fileHelper, AppDbContext dbContext)
    {
        _fileHelper = fileHelper;
        _dbContext = dbContext;
        _pathToDirectory = string.Join("/", environment.ContentRootPath, "files") + "/";
    }

    [HttpGet]
    public IActionResult Index()
    {
        var files = Directory.EnumerateFiles(_pathToDirectory).Select(Path.GetFileName);
        return View(files);
    }

    [HttpGet]
    public async Task<IActionResult> ShowTxtFile(string fileName)
    {
        var ext = Path.GetExtension(fileName);

        if (ext != ".txt")
        {
            return BadRequest($"Вы не можете открыть не файл с расширением {ext}");
        }

        var content = await _fileHelper.FindContentFileAsync("files/" + fileName);

        if (content is null)
        {
            return new NotFoundObjectResult($"{fileName} не найден");
        }

        return new JsonResult(content);
    }

    [HttpPost]
    [AuthorizationFilter("/File/", true)]
    public async Task<IActionResult> SaveFileAsync(string fileName, string content)
    {
        var isOk = await _fileHelper.TryOverwriteAsync("files/" + fileName, content);

        if (!isOk)
        {
            return BadRequest("Что-то пошло не так");
        }
        
        var file = _dbContext.FileInfos.Include(f => f.FileHistories)
            .FirstOrDefault(f => f.Title == fileName);
        
        file!.FileHistories.Add(new FileHistory
        {
            FileAction = FileAction.Changed
        });
        
        await _dbContext.SaveChangesAsync();
        

        return Redirect("/File");
    }

    [HttpGet]
    public IActionResult DownloadFile(string fileName)
    {
        var path = Path.Combine(_pathToDirectory, fileName);

        if (!System.IO.File.Exists(path)) return BadRequest();

        var extension = Path.GetExtension(fileName);
        var mimeType = MimeTypeMap.GetMimeType(extension);
        return new PhysicalFileResult(path, mimeType);
    }

    [HttpPost]
    [AuthorizationFilter("/File/", true)]
    public async Task<IActionResult> DeleteFileAsync(string fileName)
    {
        var path = _pathToDirectory + fileName;

        if (!System.IO.File.Exists(path)) return RedirectToAction("Index");
        
        var fileInfo = _dbContext.FileInfos.FirstOrDefault(fi => fi.Title == fileName);
        var user = HttpContext.FindUser();
        var fileHistory = new FileHistory
        {
            FileAction = FileAction.Removed,
            FileInfo = fileInfo!,
        };
            
        user!.FileHistories.Add(fileHistory);

        await _dbContext.AddAsync(fileHistory);
        await _dbContext.SaveChangesAsync();
        System.IO.File.Delete(path);
        return RedirectToAction("Index");
    }

    [HttpPost]
    [AuthorizationFilter("/File/", true)]
    public async Task<IActionResult> AddFileAsync(IFormFile file)
    {
        var pathToFile = _pathToDirectory + file.FileName;
        await using var stream = System.IO.File.Create(pathToFile);
        await file.CopyToAsync(stream);
        
        var fileInfo = new FileInformation
        {
            Title = file.FileName,
            Extension = Path.GetExtension(file.FileName),
        };

        await _dbContext.AddAsync(fileInfo);
        
        var fileHistory = new FileHistory
        {
            FileAction = FileAction.Created,
            FileInfo = fileInfo
        };

        await _dbContext.AddAsync(fileHistory);

        var user = HttpContext.FindUser();
        user!.FileHistories.Add(fileHistory);

        await _dbContext.SaveChangesAsync();
        return RedirectToAction("Index");
    }
}