﻿using Microsoft.AspNetCore.Mvc;
using Project.Filters;
using Project.Infrastructure.Constants;
using Project.Services.Db.SessionRepository;
using Project.Services.Db.UserRepository;
using Project.Services.Hashing;
using Project.Services.RandomBytes;
using Project.SharedKernel.Extensions;

namespace Project.Controllers;

public class AccountController : Controller
{
    private readonly IUserRepository _userRepository;
    private readonly ISessionRepository _sessionRepository;
    private readonly IRandomer _randomer;
    private readonly IHashing _hashing;

    public AccountController(IUserRepository userRepository, 
        ISessionRepository sessionRepository, 
        IRandomer randomer,
        IHashing hashing)
    {
        _userRepository = userRepository;
        _sessionRepository = sessionRepository;
        _randomer = randomer;
        _hashing = hashing;
    }

    [HttpGet]
    [ActionName("SignIn")]
    [AuthorizationFilter(PageConstants.Main)]
    public IActionResult OnGetSignIn()
    {
        return View();
    }
    
    [HttpPost]
    [ActionName("SignIn")]
    [AuthorizationFilter(PageConstants.Main)]
    public async Task<IActionResult> OnPostSignIn(string login, string password, bool isRemember)
    {
        var user = await _userRepository.FindByLoginAsync(login);
        
        if (user is null || _hashing.Hash(password, user.Salt) != user.HashedPassword)
        {
            return BadRequest();
        }

        if (isRemember)
        {
            var token = _randomer.Generate(40).Join();
            Response.Cookies.Append("token", token);
            await _sessionRepository.AddAndSaveAsync(new Session(user, token));
        }
        else
        {
            HttpContext.Session.SetString("login", user.Login); 
        }
        
        return Redirect($"{PageConstants.UserAccount}/{user.Login}");
    }

    [HttpGet]
    [ActionName("SignUp")]
    [AuthorizationFilter(PageConstants.Main)]
    public IActionResult OnGetSignUp()
    {
        return View();
    }

    [HttpPost]
    [ActionName("SignUp")]
    [AuthorizationFilter(PageConstants.Main)]
    public async Task<IActionResult> OnPostSignUp(string login, string password)
    {
        var isUserExists = await _userRepository.FindByLoginAsync(login) is not null;
        
        if (isUserExists)
        {
            return new ConflictResult();
        }

        var salt = _randomer.Generate(20);
        var hashedPassword = _hashing.Hash(password, salt);
        
        var user = new User
        {
            HashedPassword = hashedPassword,
            Login = login,
            Salt = salt
        };
        
        await _userRepository.AddAndSaveAsync(user);
        return Redirect($"{PageConstants.UserAccount}/{user.Login}");
    }

    [Route("/Account/Index/{login?}")]
    [AuthorizationFilter(PageConstants.Authorization, true)]
    public async Task<IActionResult> Index(string login)
    {
        var user = await _userRepository.FindByLoginAsync(login);
        
        if (user is null)
        {
            return NotFound();
        }
        
        return View(user);
    }
}