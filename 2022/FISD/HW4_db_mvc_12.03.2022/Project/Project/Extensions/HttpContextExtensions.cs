﻿namespace Project.Extensions;

public static class HttpContextExtensions
{
    public static User? FindUser(this HttpContext context)
    {
        if (context.Items.ContainsKey("user"))
        {
            return context.Items["user"] as User;
        }

        return null;
    }
}