﻿using Microsoft.AspNetCore.Mvc.Filters;
using Project.Extensions;

namespace Project.Filters;

[AttributeUsage(AttributeTargets.Method)]
public class AuthorizationFilterAttribute : Attribute, IAuthorizationFilter
{
    private readonly string _redirectPage;
    private readonly bool _isPageForAuthorized;

    public AuthorizationFilterAttribute(string redirectPage, bool isPageForAuthorized = false)
    {
        _redirectPage = redirectPage;
        _isPageForAuthorized = isPageForAuthorized;
    }

    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var httpContext = context.HttpContext;
        var isAuthorized = httpContext.FindUser() is not null;

        if (isAuthorized && _isPageForAuthorized || !isAuthorized && !_isPageForAuthorized)
        {
            return;
        }
        
        httpContext.Response.Redirect(_redirectPage);
    }
}