function showFiles() {
    $('.btn-show-txt').click(function () {
        const fileName = $(this).val()
        $.ajax({
            method: 'GET',
            url: `/File/ShowTxtFile?fileName=${fileName}`,
            success: (response) => {
                $('#txt-file').html(response)
                $('#inp-file-name').val(fileName)
            },
            error: (response) => {
                $('#txt-file-validation').html(response)
            }
        })
    })
}

$(document).ready(() => {
    showFiles()
})