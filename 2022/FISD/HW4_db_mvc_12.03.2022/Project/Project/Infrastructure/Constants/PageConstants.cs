﻿namespace Project.Infrastructure.Constants;

public static class PageConstants
{
    public const string Main = "/Home/Index";
    public const string UserAccount = "/Account/Index";
    public const string Authorization = "/Account/SignIn";
}