﻿using Microsoft.EntityFrameworkCore;

namespace Project.Services.Db;

public abstract class Repository<TEntity>
    where TEntity : CommonEntity
{
    private readonly AppDbContext _dbContext;
    protected readonly DbSet<TEntity> DbSet;

    protected Repository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
        DbSet = dbContext.Set<TEntity>();
    }

    protected async Task<TEntity?> FindByIdAsync(int id)
        => await DbSet.FirstOrDefaultAsync(entity => entity.Id == id);

    protected async Task<TEntity> AddAndSaveAsync(TEntity entity)
    {
        var result = await DbSet.AddAsync(entity);
        await _dbContext.SaveChangesAsync();
        return result.Entity;
    }
}