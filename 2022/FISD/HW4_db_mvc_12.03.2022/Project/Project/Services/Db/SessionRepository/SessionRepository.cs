﻿using Microsoft.EntityFrameworkCore;

namespace Project.Services.Db.SessionRepository;

public class SessionRepository : Repository<Session>, ISessionRepository
{
    public SessionRepository(AppDbContext dbContext) : base(dbContext)
    { }

    public async Task<Session?> FindSessionByTokenAsync(string token)
        => await DbSet
            .Include(session => session.User)
            .ThenInclude(user => user.FileHistories)
            .FirstOrDefaultAsync(session => session.Token == token);

    public new async Task<Session> AddAndSaveAsync(Session entity)
        => await base.AddAndSaveAsync(entity);

    public new async Task<Session?> FindByIdAsync(int id)
        => await base.FindByIdAsync(id);
}