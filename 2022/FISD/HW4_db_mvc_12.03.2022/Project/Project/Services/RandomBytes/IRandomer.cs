﻿namespace Project.Services.RandomBytes;

public interface IRandomer
{
    byte[] Generate(int length);
}