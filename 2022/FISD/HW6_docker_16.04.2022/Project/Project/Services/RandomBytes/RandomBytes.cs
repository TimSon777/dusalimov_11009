﻿using System.Security.Cryptography;

namespace Project.Services.RandomBytes;

public class RandomBytes : IRandomBytes
{
    public byte[] Generate(int length)
    {
        if (length < 0)
        {
            throw new ArgumentException("The length cannot be negative", nameof(length));
        }

        var salt = new byte[length];
        
        using var random = RandomNumberGenerator.Create();
        random.GetNonZeroBytes(salt);

        return salt;
    }
}