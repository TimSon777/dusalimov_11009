﻿namespace Project.Services.FileHelper;

public interface IFileHelper
{
    Task<string?> FindContentFileAsync(string relativePathToFile);
    Task<bool> TryOverwriteAsync(string relativePathToFile, string content);
}