﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Project.Services.Hashing;

public class Hashing : IHashing
{
    public string Hash(string password, byte[] salt)
    {
        var bytes = KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 100000,
            numBytesRequested: 256 / 8);
        
        return string.Join("", bytes.Select(@byte => @byte.ToString("X2")));
    }
}