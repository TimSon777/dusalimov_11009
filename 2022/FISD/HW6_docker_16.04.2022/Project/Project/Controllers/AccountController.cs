﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Project.Db;
using Project.Db.Entities;
using Project.Services.Hashing;
using Project.Services.RandomBytes;
using Project.ViewModels;

namespace Project.Controllers;

public class AccountController : Controller
{
    private readonly IHashing _hashing;
    private readonly IRandomBytes _randomBytes;
    private readonly AppDbContext _appDbContext;

    public AccountController(IHashing hashing, IRandomBytes randomBytes, AppDbContext appDbContext)
    {
        _hashing = hashing;
        _randomBytes = randomBytes;
        _appDbContext = appDbContext;
    }
    
    [HttpGet]
    public IActionResult SignIn() => View();

    [HttpGet]
    public IActionResult SignUp() => View();

    [HttpPost]
    public async Task<IActionResult> SignIn(SignInVm model)
    {
        if (!ModelState.IsValid)
        {
            return View();
        }

        var user = await _appDbContext.Users
            .Include(a => a.Roles)
            .FirstOrDefaultAsync(a => a.UserName == model.UserName);
        
        if (user is null || user.HashedPassword != _hashing.Hash(model.Password, user.Salt))
        {
            ModelState.AddModelError("", "Неправильный логин и (или) пароль");
            return View();
        }

        var claims = user.Roles.Select(a => new Claim(ClaimTypes.Role, a.Name)).ToList();
        claims.Add(new Claim(ClaimTypes.Name, user.UserName));
        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme); 
        var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
            
        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal);

        return Redirect("/");
    }

    [HttpPost]
    public async Task<IActionResult> SignUp(SignUpVm model)
    {
        if (!ModelState.IsValid)
        {
            return View();
        }

        var user = _appDbContext.Users.FirstOrDefault(a => a.UserName == model.UserName);
        if (user is not null)
        {
            ModelState.AddModelError(nameof(model.UserName), "Логин занят");
            return View();
        }
        
        var salt = _randomBytes.Generate(20);
        var hashedPassword = _hashing.Hash(model.Password, salt);
        
        var newUser = new User
        {
            UserName = model.UserName,
            HashedPassword = hashedPassword,
            Salt = salt
        };

        var roleName = model.IsAdvancedUser ? "AdvancedUser" : "DefaultUser";
        var role = await _appDbContext.UserRoles.FirstAsync(r => r.Name == roleName);
        newUser.Roles.Add(role);

        await _appDbContext.AddAsync(newUser);
        await _appDbContext.SaveChangesAsync();
        
        return RedirectToAction("SignIn");
    }
}