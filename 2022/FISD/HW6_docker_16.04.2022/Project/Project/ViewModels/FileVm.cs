﻿namespace Project.ViewModels;

public class FileVm
{
    public string Name { get; set; }
    public string Content { get; set; }
}