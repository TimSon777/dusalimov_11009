﻿using Project.SharedKernel;

namespace Project.ViewModels;

public class SignUpVm : PasswordUserName
{
    public bool IsAdvancedUser { get; set; }
}