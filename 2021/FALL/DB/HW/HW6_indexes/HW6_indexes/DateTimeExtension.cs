﻿using System;

namespace HW6_indexes
{
    public static class DateTimeExtension
    {
        private static readonly Random Rnd = new();
        public static DateTime GetBetween(this DateTime date1, DateTime date2)
        {
            var range = Math.Abs((date1 - date2).Days);
            return date1.AddDays(Rnd.Next(range));
        }
    }
}