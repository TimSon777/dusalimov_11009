﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Npgsql;

namespace HW6_indexes
{
    public static class Program
    {
        private static readonly string CurrentDirectory = DirectoryExtension.FindDirectoryNet5();
        private static readonly string PathToData = $"{CurrentDirectory}/data/";
        
        private const string ConnectionString = 
            "Host=localhost;Username=postgres;Password=postgres;Database=Point_Rating_System";
        
        private static readonly string[] FirstNamesMales = File.ReadAllLines($"{PathToData}first_name_male.txt");
        private static readonly string[] FirstNamesFemales = File.ReadAllLines($"{PathToData}first_name_female.txt");
        private static readonly string[] SecondNameMales = File.ReadAllLines($"{PathToData}second_name_male.txt");
        private static readonly string[] SecondNameFemales = File.ReadAllLines($"{PathToData}second_name_female.txt");
        private static readonly string[] PatronymicMales = File.ReadAllLines($"{PathToData}patronymic_male.txt");
        private static readonly string[] PatronymicFemales = File.ReadAllLines($"{PathToData}patronymic_female.txt");

        private static readonly char[] Digits = "0123456789".ToCharArray();
        
        static void Main(string[] args)
        {
            for (var i = 0; i < 10; i++)
            {
                FillPersonWithRandomData(200000, 
                    new DateTime(2000, 01, 01), 
                    new DateTime(2004, 12, 31));
            }
        }

        private static string Get(IReadOnlyList<string> words)
        {
            return words[Rnd.Next(0, words.Count - 1)];
        }

        private static readonly Random Rnd = new();

        public static string GenerateValue(IEnumerable<int> lengthsGeneratedNumbers, 
            IEnumerable<(DateTime, DateTime)> dates, 
            params string[][] data)
        {
            var strBuilder = new StringBuilder("(");
            
            foreach (var e in data)
            {
                strBuilder.Append( $"'{Get(e)}',");
            }

            foreach (var (dateMin, dateMax) in dates)
            {
                strBuilder.Append($"'{dateMin.GetBetween(dateMax):yyyy-MM-dd}',");
            }
            
            foreach (var length in lengthsGeneratedNumbers)
            {
                strBuilder.Append($"'{Digits.GetRandomWord(length)}',");
            }

            return $"{strBuilder.ToString()[..(strBuilder.Length - 1)]}),";
        }

        private static void FillPersonWithRandomData(int count, DateTime dateMin, DateTime dateMax)
        {
            var insert =
                $"INSERT INTO person (first_name, second_name, patronymic, birthday_date, passport_ser, passport_no) VALUES";

            var strBuilder = new StringBuilder(insert);
            for (var i = 0; i < count / 2; i++)
            {
                var male = GenerateValue(new[] {10, 10},
                    new []{(dateMin, dateMax)},
                    FirstNamesMales,
                    SecondNameMales,
                    PatronymicMales);
                
                var female = GenerateValue(new[] {10, 10},
                    new []{(dateMin, dateMax)},
                    FirstNamesFemales,
                    SecondNameFemales,
                    PatronymicFemales);
                strBuilder.Append(male);
                strBuilder.Append(female);
            }

            var query = strBuilder
                .ToString()
                [..(strBuilder.Length - 1)];
            using var connection = new NpgsqlConnection(ConnectionString);
            connection.Open();
            var cmd = new NpgsqlCommand(query, connection);
            cmd.ExecuteNonQuery();
        }
    }
}