﻿using System;
using System.IO;

namespace HW6_indexes
{
    public static class DirectoryExtension
    {
        public static string FindDirectoryNet5()
        {
            var directoryInfo = Directory.GetParent(Environment.CurrentDirectory)?.Parent;
            return directoryInfo?.Parent?.ToString();
        }
    }
}