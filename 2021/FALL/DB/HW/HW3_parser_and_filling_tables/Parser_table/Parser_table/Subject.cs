﻿using System.Collections.Generic;

namespace Parser_table
{
    public record Subject(string Title)
    {
        public static readonly Subject[] Subjects1Semester = {
            new("Алгебра и геометрия"),
            new("Информатика и программирование"),
            new("Математический анализ"),
            new("Дискретная математика"),
            new("Иностранный язык"),
            new("Иностранный язык в профессиональной сфере"),
            new("История России")
        };

        public static readonly Subject[] Subjects2Semester = {
            new("Алгебра и геометрия"),
            new("Алгоритмы и структуры данных"),
            new("Дискретная математика"),
            new("Информатика и программирование"),
            new("Математический анализ"),
            new("Иностранный язык"),
            new("Иностранный язык в профессиональной сфере"),
            new("Русский язык и культура речи"),
        };
        
        public static Subject[] AllSubjects = {
            new("Информатика и программирование"),
            new("Математический анализ"),
            new("Алгебра и геометрия"),
            new("Дискретная математика"),
            new("Иностранный язык"),
            new("Иностранный язык в профессиональной сфере"),
            new("История России"),
            new("Алгоритмы и структуры данных"),
            new("Русский язык и культура речи"),
        };
        
        public static readonly Dictionary<Subject, int> TeacherSubjectToSubject = new()
        {
            {AllSubjects[0], 1},
            {AllSubjects[1], 2},
            {AllSubjects[2], 3},
            {AllSubjects[3], 4},
            {AllSubjects[4], 5},
            {AllSubjects[5], 6},
            {AllSubjects[6], 7},
            {AllSubjects[7], 8},
            {AllSubjects[8], 9}
        };

        public override string ToString()
        {
            return $"'{Title}'";
        }
    }
    
    public record Subjects(Subject[] Subjects1, int Semester)
    {
        public static readonly Subjects[] SubjectsSemester = {
            new(Subject.Subjects1Semester, 1),
            new(Subject.Subjects2Semester, 2)
        };
    }
}