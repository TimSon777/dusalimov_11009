﻿using System;
using System.Collections.Generic;

namespace Parser_table
{
    public record Person(FullName FullName,
        DateTime BirthdayDate,
        Passport Passport)
    {
        public override string ToString()
            => $"{FullName}, '{BirthdayDate:yyyy-MM-dd}', {Passport}";
        
        public static readonly List<Person> Teachers = new()
        {
            new Person(
                new FullName(
                    "Липачев",
                    "Евгений",
                    "Константинович"),
                new DateTime(1800, 6, 6),
                Passport.GetRandomPassport()),
            new Person(
                new FullName(
                    "Скворцова",
                    "Галия",
                    "Шакировна"),
                new DateTime(1000, 2, 3),
                Passport.GetRandomPassport()),
            new Person(
                new FullName(
                    "Ильин",
                    "Сергей",
                    "Николаевич"),
                new DateTime(2000, 12, 23),
                Passport.GetRandomPassport()),
            new Person(
                new FullName(
                    "Калимуллин",
                    "Искандер",
                    "Шагитович"),
                new DateTime(200, 2, 23),
                Passport.GetRandomPassport()),
            new Person(
                new FullName(
                    "Низамиев",
                    "Рустам",
                    "Асхатович"),
                new DateTime(200, 2, 23),
                Passport.GetRandomPassport()),
            new Person(
                new FullName(
                    "Ибрагимов",
                    "Руслан",
                    "Рустамович"),
                new DateTime(2006, 2, 23),
                Passport.GetRandomPassport()),
            new Person(
                new FullName(
                    "Салимов",
                    "Рустем",
                    "Фаридович"),
                new DateTime(1, 1, 1),
                Passport.GetRandomPassport()),
            new Person(
                new FullName(
                    "Гараева",
                    "Виктория",
                    "Юрьевна"),
                new DateTime(1444, 12, 31),
                Passport.GetRandomPassport()),
        };
    }
}