﻿namespace Parser_table
{
    public record Student(Person Person,
        AcademicPerformance[] AcademicPerformance);
}