﻿namespace Parser_table
{
    public record FullName(string FirstName, string SecondName, string Patronymic)
    {
        public static FullName GetFullName(string[] words)
        {
            return words.Length switch
            {
                3 => new FullName(words[0], words[1], words[2]),
                2 => new FullName(words[0], words[1], null),
                _ => new FullName($"{words[0]}-{words[1]}", $"{words[2]}-{words[3]}", null),
            };
        }

        public override string ToString()
        {
            return Patronymic == null 
                ? $"'{FirstName}', '{SecondName}', null" 
                : $"'{FirstName}', '{SecondName}', '{Patronymic}'";
        }
    }
}