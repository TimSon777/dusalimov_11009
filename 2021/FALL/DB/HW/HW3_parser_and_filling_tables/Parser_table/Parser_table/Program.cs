﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Parser_table
{
    public static class Program
    {
        public static readonly HashSet<FullName> FullNames = new();
        public static readonly Dictionary<FullName, int> Students = new();
        
        public static void Main()
        {
            var lines = File.ReadAllLines($"{ProgramService.GetRootProjectNet5()}\\tables\\results_20-21.csv");
            var students = new List<Student>();
            var dateStart = new DateTime(2000, 1, 1);
            var dateEnd = new DateTime(2003, 12, 12);
            var personIdToGroup = new List<int>[13];
            
            for (var i = 0; i < personIdToGroup.Length; i++)
            {
                personIdToGroup[i] = new List<int>();
            }
            
            var studentId = 1;
            foreach (var line in lines)
            {
                var partsLine = line.Split(';', StringSplitOptions.RemoveEmptyEntries);
                if (ProgramService.IsUselessLine(partsLine)) continue;

                var numberGroup = int.Parse(partsLine[2].Split('-')[1]);
                personIdToGroup[numberGroup - 1].Add(studentId);
                
                var fullName = FullName.GetFullName(partsLine[1].Split(new []{' ', '-'}, StringSplitOptions.RemoveEmptyEntries));
                
                FullNames.Add(fullName);
                Students.Add(fullName, studentId);
                
                var birthdayDate = dateStart.GetBetween(dateEnd);
                var passport = Passport.GetRandomPassport();

                var academicPerformance = new []
                {
                    AcademicPerformance.GetAcademicPerformanceStudent(1, partsLine[3..9]),
                    AcademicPerformance.GetAcademicPerformanceStudent(2, partsLine[11..18])
                };
                
                students.Add(new Student(
                    new Person(
                        fullName,
                        birthdayDate, 
                        passport),
                    academicPerformance
                ));

                studentId++;
            }
            
            WriterToFile.WriteSubjectsToFile();
            WriterToFile.WritePeopleToFile(students.Select(a => a.Person).ToList(), "person_student");
            WriterToFile.WritePeopleToFile(Person.Teachers, "person_teacher");
            WriterToFile.WriteStudyGroupToFile();
            WriterToFile.WriteStudentToFile(personIdToGroup);
            WriterToFile.WriteMarksToFile(students);
            
            ParserOtherTables.ParseTableAdditionalPointsDivision(1);
            ParserOtherTables.ParseTableAdditionalPointsDivision(2);
            ParserOtherTables.ParseDormitoryPoints();
            ParserOtherTables.UpdateWishesDormitory();
        }
    }
}