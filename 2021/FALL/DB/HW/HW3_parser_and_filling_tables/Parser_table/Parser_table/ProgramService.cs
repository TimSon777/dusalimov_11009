﻿using System;
using System.IO;

namespace Parser_table
{
    public static class ProgramService
    {
        public static bool IsUselessLine(string[] partsLine)
            => partsLine.Length is 2 or 1
               || partsLine[0] == "группа";
        
        public static string GetRootProjectNet5()
        {
            var net5Directory = Environment.CurrentDirectory;
            var directoryInfo = Directory.GetParent(net5Directory)?.Parent;
            
            if (directoryInfo is {Parent: {Parent: { }}}) 
                return directoryInfo.Parent.Parent.FullName;
            
            throw new DirectoryNotFoundException();
        }
    }
}