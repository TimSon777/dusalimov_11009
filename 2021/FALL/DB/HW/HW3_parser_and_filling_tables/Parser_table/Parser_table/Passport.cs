﻿using System;

namespace Parser_table
{
    public record Passport(string Number, string Series)
    {
        private static readonly char[] Digits = "1234567890".ToCharArray();
        private static readonly Random Rnd = new();
        
        public static Passport GetRandomPassport()
        {
            var number = Digits.GetRandomWord(Rnd.Next(4, 10));
            var series = Digits.GetRandomWord(Rnd.Next(4, 10));
            return new Passport(number, series);
        }

        public override string ToString()
        {
            return $"'{Series}', '{Number}'";
        }
    }
}