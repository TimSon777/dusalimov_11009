﻿using System.Linq;

namespace Parser_table
{
    public record AcademicPerformance(Subject[] Subjects1, int[] Marks)
    {
        public static AcademicPerformance GetAcademicPerformanceStudent(
            int semester, 
            string[] marksNotParsed)
        {
            var marks = marksNotParsed.Select(int.Parse).ToArray();
            return new AcademicPerformance(
                Subjects.SubjectsSemester[semester - 1].Subjects1, 
                marks);
        }
    }
}