﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Parser_table
{
    public static class ParserOtherTables
    {
        public static void ParseTableAdditionalPointsDivision(int semester)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            using var sw = WriterToFile.GetStreamWriter($"additional_points_{semester}");
            sw.WriteLine($"INSERT INTO additional_points (student_id, points_division, semester)");
            sw.WriteLine($"VALUES");
            var lines = File.ReadAllLines($"{ProgramService.GetRootProjectNet5()}\\tables\\additional_points_{semester}_semester.csv", Encoding.UTF8);
            foreach (var line in lines)
            {
                var partsLine = line.Split(';', ' ');
                var fullName = FullName.GetFullName(partsLine[..^1]);
                if (!Program.FullNames.Contains(fullName)) continue;
                var points = double.Parse(partsLine.Last(), new CultureInfo("ru"));
                sw.WriteLine($"\t({Program.Students[fullName]}, {Math.Round(points, 1)}, {semester}),");
            }
        }

        public static void UpdateWishesDormitory()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            using var sw = WriterToFile.GetStreamWriter($"wishes");
            var lines = File.ReadAllLines($"{ProgramService.GetRootProjectNet5()}\\tables\\wishes.csv", Encoding.UTF8);
            foreach (var line in lines)
            {
                var parts = line.Split(';', StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 1) continue;
                var name = parts[0].Split();
                var fullName = FullName.GetFullName(name);
                if (!Program.FullNames.Contains(fullName)) continue;
                
                sw.WriteLine($"UPDATE student");
                var dormitory = parts[1].Split('-');
                var dormitoryId = dormitory[0] == "ДУ" ? 1 : int.Parse(dormitory[1]) == 3 ? 2 : 3;
                sw.WriteLine($"SET wishes_dormitory_id = {dormitoryId}");
                sw.WriteLine($"WHERE student_id = {Program.Students[fullName]};");
                sw.WriteLine();
            }
        }
        
        public static void ParseDormitoryPoints()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            using var sw = WriterToFile.GetStreamWriter($"dormitory");
            var lines = File.ReadAllLines($"{ProgramService.GetRootProjectNet5()}\\tables\\dormitory.csv", Encoding.UTF8);
            foreach (var line in lines)
            {
                var parts = line.Split(';', StringSplitOptions.RemoveEmptyEntries);
                var fullName = FullName.GetFullName(parts[0].Split(' ', '-'));
                if (!Program.FullNames.Contains(fullName)) continue;
                if (parts.Length == 2) continue;
                var points = Math.Round(double.Parse(parts.Last(), new CultureInfo("ru")), 2);
                for (var i = 1; i <= 2; i++)
                {
                    sw.WriteLine($"UPDATE additional_points");
                    sw.WriteLine($"SET points_dormitory = {Math.Round(points / 2, 2)}");
                    sw.WriteLine($"WHERE student_id = {Program.Students[fullName]} AND semester = '{i}';");
                    sw.WriteLine();
                }
            }
        }
    }
}