﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Parser_table
{
    public static class WriterToFile
    {
        public static StreamWriter GetStreamWriter(string nameFile)
            => new($"{ProgramService.GetRootProjectNet5()}\\requests\\{nameFile}.txt");
        
        public static void WriteMarksToFile(List<Student> students)
        {
            using var sw = GetStreamWriter("mark");
            var studentId = 1;
            foreach (var (_, academicPerformances) in students)
            {
                for (var k = 0; k < academicPerformances.Length; k++)
                {
                    for (var j = 0; j < academicPerformances[k].Marks.Length; j++)
                    {
                        sw.WriteLine($"UPDATE student_subject_information");
                        sw.WriteLine($"SET mark = {academicPerformances[k].Marks[j]}");
                        var subject = academicPerformances[k].Subjects1[j];
                        sw.WriteLine(
                            $"WHERE {studentId} = student_id AND teacher_subject_id = {Subject.TeacherSubjectToSubject[subject]} AND semester = '{k + 1}';");
                        sw.WriteLine();
                    }
                }

                studentId++;
            }
        }

        public static void WriteStudentToFile(List<int>[] personIdToGroup)
        {
            using var sw = GetStreamWriter("student");
            sw.WriteLine($"INSERT INTO student (group_id, person_id)");
            sw.WriteLine($"VALUES");

            for (var i = 0; i < personIdToGroup.Length; i++)
            {
                foreach (var personId in personIdToGroup[i])
                {
                    sw.WriteLine($"\t({i + 1}, {personId}),");
                }
            }
        }

        public static void WriteStudyGroupToFile()
        {
            using var sw = GetStreamWriter("study_group");
            sw.WriteLine($"INSERT INTO study_group (group_number, year_creation)");
            sw.WriteLine($"VALUES");

            for (var i = 1; i <= 13; i++)
            {
                sw.WriteLine(i < 10 ? $"\t('0{i}', '2020')," : $"\t('{i}', '2020'),");
            }
        }

        public static void WritePeopleToFile(IReadOnlyList<Person> people, string nameDirectory)
        {
            using var sw = GetStreamWriter(nameDirectory);
            sw.WriteLine($"INSERT INTO person (first_name, second_name, patronymic, birthday_date, passport_ser, passport_no)");
            sw.WriteLine($"VALUES");
            foreach (var t in people)
                sw.WriteLine($"\t({t}),");
        }
        
        public static void WriteSubjectsToFile()
        {
            using var sw = GetStreamWriter("subject");
            var subjects = Subject.AllSubjects.ToList();
            
            sw.WriteLine($"INSERT INTO subject (subject_name)");
            sw.WriteLine($"VALUES");

            for (var i = 0; i < subjects.Count - 1; i++)
            {
                sw.WriteLine($"\t({subjects[i]}),");
            }

            sw.Write($"\t({subjects.Last()});");
        }
    }
}