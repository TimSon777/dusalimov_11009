﻿using System.Net;
using System.Threading.Tasks;

namespace HW11_web_socket
{
    public static class Distributor
    {
        public static async Task DistributeAsync(HttpListenerContext ctx)
        {
            await ApplicationFile.SendResponseAsync(ctx);
        }
    }
}