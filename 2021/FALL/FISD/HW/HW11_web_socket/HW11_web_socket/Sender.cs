﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HW11_web_socket
{
    public static class Sender
    {
        public static async Task SendMessageAsync(Dictionary<WebSocket, string> users, WebSocket webSocket)
        {
            var buffer = new byte[1024];
            var receiveResult = await webSocket.ReceiveAsync(buffer, CancellationToken.None);
            var length = receiveResult.Count;
            var result = Encoding.UTF8.GetString(buffer, 0, length);
            
            if (receiveResult.MessageType == WebSocketMessageType.Close)
            {
                users.Remove(webSocket);
                await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
            }
            else
            {
                if (users.ContainsKey(webSocket)) result = users[webSocket] + ": " + result;
                else result = AddUserAndGetMessage(users, webSocket, result);
            }

            var output = Encoding.UTF8.GetBytes(result);
            foreach (var socket in users.Keys)
            {
                await socket.SendAsync(new ArraySegment<byte>(output, 0, output.Length),
                    WebSocketMessageType.Text,
                    true,
                    CancellationToken.None);
            }
            
            await Task.Delay(1000);
        }
        
        private static string AddUserAndGetMessage(IDictionary<WebSocket, string> users, WebSocket webSocket, string result)
        {
            var split = result.Split("!", 2);
            users.Add(webSocket, split[0]);
            return split.Length == 1 ? "Что-то пошло не так!" : split[0] + " " + split[1];
        }
    }
}