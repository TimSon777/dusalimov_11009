﻿using System.Net;
using System.Threading.Tasks;

namespace HW11_web_socket
{
    public static class Server
    {
        public static readonly string PathToCurrentDirectory = Directory.FindDirectoryNet5();
        
        public static async Task Start()
        {
            var listener = new HttpListener
            {
                Prefixes = { "http://localhost:8080/" }
            };
            
            listener.Start();
            await Handler(listener);
        }

        private static async Task Handler(HttpListener listener)
        {
            while (true)
            {
                var ctx = await listener.GetContextAsync();
                if (ctx.Request.IsWebSocketRequest)
                {
                    await Task.Factory.StartNew(() => ApplicationWebSocket.Handle(ctx));
                }
                else
                {
                    await Distributor.DistributeAsync(ctx);
                }
            }
            // ReSharper disable once FunctionNeverReturns
        }
    }
}