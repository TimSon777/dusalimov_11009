﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace HW11_web_socket
{
    public static class ApplicationFile
    {
        public static async Task SendResponseAsync(HttpListenerContext ctx)
        {
            var request = ctx.Request;
            var response = ctx.Response;
            var url = request.Url?.AbsolutePath;
            if (url is null)
            {
                await StreamHelper.SendBytesToSiteAsync(Array.Empty<byte>(), 404, response);
                return;
            }

            if (url == "/") url = "/index.html";
            var path = Server.PathToCurrentDirectory + "/wwwroot" + url;
            
            if (File.Exists(path))
            {
                var bytes = await File.ReadAllBytesAsync(path);
                await StreamHelper.SendBytesToSiteAsync(bytes, 200, response);
            }
            else
            {
                await StreamHelper.SendBytesToSiteAsync(Array.Empty<byte>(), 404, response);
            }
        }
    }
}