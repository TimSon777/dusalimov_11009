﻿using System;

namespace HW11_web_socket
{
    public static class Directory
    {
        public static string FindDirectoryNet5()
        {
            var directoryInfo = System.IO.Directory.GetParent(Environment.CurrentDirectory)?.Parent;
            return directoryInfo?.Parent?.ToString();
        }
    }
}