﻿using System.Net;
using System.Threading.Tasks;

namespace HW11_web_socket
{
    public static class StreamHelper
    {
        public static async Task SendBytesToSiteAsync(byte[] bytes, int statusCode, HttpListenerResponse response)
        {
            response.StatusCode = statusCode;
            var sw = response.OutputStream;
            response.ContentLength64 = bytes.Length;
            await sw.WriteAsync(bytes);
            sw.Close();
        }
    }
}