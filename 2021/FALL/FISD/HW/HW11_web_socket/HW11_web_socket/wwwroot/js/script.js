let webSocket = null
let login = "Аноним"

function report(message) {
    let html = document.createElement('p');
    html.textContent = message
    $('#chat').append(html)
}

function openWebsocket() {
    webSocket = new WebSocket("ws://localhost:8080")
    webSocket.onopen = () => webSocket.send(`${login}!присоединился к чату`)
    webSocket.onmessage = (evt) => report(evt.data)
    window.onunload = function(){
        webSocket.send('пользователь вышел из чата')
        webSocket.close()
        webSocket = null
    }
}

function WebSocketTest() {
    if ("WebSocket" in window) {
        if (webSocket === null) {
            setTimeout(openWebsocket, 1000)
        } 
        else {
            let message = $('#txt').val()
            webSocket.send(message)
        }
    } 
    else {
        alert("WebSocket не поддерживается вашим браузером!")
    }
}

$(document).ready(function () {
    $('#login_sbm').click(function () {
        login = $('#login_inp').val()
        $('.container').load('html/chat.html', function () {
            WebSocketTest()
            $('#send').click(WebSocketTest)
        })
    })
})
