﻿using System.Threading.Tasks;

namespace HW11_web_socket
{
    public static class Program
    {
        public static async Task Main()
        {
            await Server.Start();
        }
    }
}