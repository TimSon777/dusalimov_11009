﻿using System.Collections.Generic;
using System.Net;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace HW11_web_socket
{
    public static class ApplicationWebSocket
    {
        private static readonly Dictionary<WebSocket, string> Users = new();
        
        public static async Task Handle(HttpListenerContext ctx)
        {
            var wsCtx = await ctx.AcceptWebSocketAsync(null!);
            using var webSocket = wsCtx.WebSocket;

            using var cts = new CancellationTokenSource();
            
            while (webSocket.State == WebSocketState.Open)
            {
                await Sender.SendMessageAsync(Users, webSocket);
            }
            
            cts.Cancel();
        }
    }
}