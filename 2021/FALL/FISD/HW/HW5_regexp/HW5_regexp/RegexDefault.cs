﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace HW5_regexp
{
    public class RegexDefault
    {
        public static bool Task01B(string str)
        {
            const string minutes = @"[0-5]\d";
            const string hours = @"((0|1)\d|2[0-3])";
            const string days = @"(0[1-9]|(1|2)\d|30|31)";
            const string months = @"(0[1-9]|10|11|12)";
            
            const string _1237 = @"(03/06/1237 (1[2-9]|2[0-3]):[0-5]\d|03/(0[7-9]|(1|2)\d|30|31) ((0|1)\d|2[0-3]):[0-5]\d)|(0[4-9]|10|11|12)/(0[1-9]|(1|2)\d|30|31)/1237 ((0|1)\d|2[0-3]):[0-5]\d";
            const string _1978 = @"(01/(0[1-9]|(1|2)\d|30|31)/1978 ((0|1)\d|2[0-3]):[0-5]\d|02/(0[1-9]|1\d|2[0-6])/1978 ((0|1)\d|2[0-3]):[0-5]\d|02/27/1978 ((0|1)\d|20|21):([0-2]\d|3[0-5]))";
            const string _1238__1977 = @"(12(38|39|[4-9]\d)|1[3-8]\d\d|19([0-6]\d|7[0-7]))";
            
            var result = $"^({_1237}|{months}/{days}/{_1238__1977} {hours}:{minutes}|{_1978})$";
            return new Regex(result).IsMatch(str);
        }

        public static bool Task01A(string str)
        {
            const string integers = @"([-+]?[1-9]\d*|0)";
            const string fractionalPart = @"[\.,]\d*[1-9]";
            const string fractionalPartWithZeros = @"[\.,]\d*";
            const string period = @"\(\d*[1-9]\d*\)";
            const string numberStartingZero = @"[-+]?0[.,](\d*[1-9](\(\d*[1-9]\d*\))?|\d*\(\d*[1-9]\d*\))";
            
            return new Regex($"^({integers}(({fractionalPart}({period})?)?|{fractionalPartWithZeros}{period})|{numberStartingZero})$").IsMatch(str);
        }

        public static IEnumerable<int> Task02C(IReadOnlyList<string> arrays)
        {
            var regex = new Regex(@"^(0+|1+|(01)+0?|(10)+1?)$");
            for (var i = 0; i < arrays.Count; i++)
                if (regex.IsMatch(arrays[i])) yield return i;
        }

        private static IEnumerable<int> RunRandomWithCondition(Regex regex, bool isInverseRegex, int countResultElements)
        {
            var rnd = new Random();
            var i = 0;
            var count = 0;
            while (i != countResultElements)
            {
                var current = rnd.Next();
                count++;
                if (isInverseRegex)
                {
                    if (regex.IsMatch(current.ToString())) continue;
                }
                else
                {
                    if (regex.IsMatch(current.ToString())) continue;
                }
                yield return current;
                i++;
            }
            
            yield return count;
        }
        
        public static IEnumerable<int> Task03C()
        {
            return RunRandomWithCondition(new Regex(@"^([1-9]\d*[02468]|0)$"), 
                false, 10);
        }
        
        public static IEnumerable<int> Task03B()
        {
            return RunRandomWithCondition(new Regex(@"^\d*[02468]{3}\d*$"), 
                true, 10);
        }
        
        public static IEnumerable<int> Task04B()
        {
            return RunRandomWithCondition(new Regex(@"^[02468]{4,5}$"), 
                false, 10);
        }
        
        public static IEnumerable<int> Task05B()
        {
            return RunRandomWithCondition(new Regex(@"^\d*([02468]{2}\d*[02468]{2}|[02468]{3})\d*$"), 
                false, 10);
        }
    }
}