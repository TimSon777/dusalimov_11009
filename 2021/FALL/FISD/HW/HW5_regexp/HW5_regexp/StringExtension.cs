﻿using System;

namespace HW5_regexp
{
    public static class StringExtension
    {
        public static string DeleteFirstOccurrences(this string str, params string[] pattern)
        {
            foreach (var e in pattern)
            {
                var n = str.IndexOf(e, StringComparison.Ordinal);
                if (n != -1)
                    str = str.Remove(n, e.Length);
            }

            return str;
        }
    }
}