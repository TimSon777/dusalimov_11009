﻿using System.Text.RegularExpressions;

namespace HW5_regexp
{
    public static class RegexEntities
    {
        public static bool IsCarNumber(string str)
        {
            var regex = new Regex(@"^[а-яё]\d{3}[а-яё]{2}$");
            return regex.IsMatch(str);
        }

        public static bool IsINoILA(string str)
        {
            var regex = new Regex(@"^\d{3}-\d{3}-\d{3} \d{2}$");
            return regex.IsMatch(str);
        }
        
        //245 2 - блок (всего 4), 4 - этаж (всего 10), 5 - номер комнаты (всего 8)
        public static bool IsNumberSg9Dormitory(string str) 
        {
            var regex = new Regex(@"^[1-4]\d[0-7]$");
            return regex.IsMatch(str);
        }
    }
}