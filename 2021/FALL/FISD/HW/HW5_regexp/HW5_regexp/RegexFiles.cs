﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace HW5_regexp
{
    public record Url(string ParamString, List<Param> Items);
    public record Param(string Name, string Value);
    public class RegexFiles
    {
        public RegexFiles(string address)
        {
            _page = new WebClient().DownloadString(address);
        }
        private readonly string _page;
        
        public Dictionary<string, List<string>> Task06C()
        {
            var regex = new Regex(@"[\w\d]+\.(css|js|jpg|png|bmp|gif|tif|doc|docx|xls|xlsx|pdf|txt|zip|rar|7z|gzip|mp3|wav|midi|aac|mp4|avi|mkv|wmv|flv|mpeg|html|htm|mht|ppt|pptx|mdb|accdb|iso|cdr|torrent|djvu|fb2|epub|mobi)");
            return regex.Matches(_page)
                .Select(c => c.Value)
                .Distinct()
                .Select(x => x.Split("."))
                .GroupBy(x => x[1], x => x[0])
                .ToDictionary(x => x.Key, x => x.ToList());
        }

        public void PrintTask06C()
        {
            foreach (var (key, value) in Task06C())
            {
                Console.WriteLine("EXTENSION: " + key);
                foreach (var e in value)
                {
                    Console.WriteLine("FILENAME: " + e);
                }
            }
        }

        public IEnumerable<Url> Task07C()
        {
            var regex = new Regex(@"\?(\w+=[\d\w]+[&]?)+");
            return regex.Matches(_page)
                .Select(m => new Url(m.Value.DeleteFirstOccurrences("?"),
                    new List<Param>(m.Value.Split(new[] {'?', '&'}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(v => v.Split("="))
                        .Select(g => new Param(g[0], g[1])))));
        }

        public void PrintTask07C()
        {
            foreach (var url in Task07C())
            {
                Console.WriteLine("PARAM STRING: " + url.ParamString);
                foreach (var (name, value) in url.Items)
                {
                    Console.WriteLine("NAME: {0}", name);
                    Console.WriteLine("VALUE: {0}", value);
                }
            }
        }

        public IEnumerable<string> Task07B()
        {
            var regex = new Regex(@"(""|/)/[\w\d]+[^""\?']*");
            return regex
                .Matches(_page)
                .Select(h => h.Value)
                .Distinct()
                .Select(h => h.DeleteFirstOccurrences("\"", "//", "www."));
        }

        public void PrintTask07B()
        {
            foreach (var e in Task07B())
            {
                Console.WriteLine(e);
            }
        }
    }
}