﻿using System.Threading.Tasks;

namespace HW10_async_addition
{
    public static class Program
    {
        public static async Task Main()
        {
            await Task.Run(Server.StartAsync);
        }
    }
}