﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HW10_async_addition.AdditionalClasses;
using HW10_async_addition.Helpers;

namespace HW10_async_addition.Applications
{
    public sealed class ApplicationUniqueUsers : Application
    {
        public ApplicationUniqueUsers(string url) : base(url)
        { }

        public override async Task<Result> DistributeByTasksAsync(HttpListenerRequest request)
        {
            return Url switch
            {
                "get_count" => GetCountUniqueUsers(),
                _ => throw new NotImplementedException()
            };
        }

        public static readonly string PathToUniqueUsers =
            Path.Combine(Distributor.CurrentDirectory, "website", "json/unique_users.json");

        private static readonly object UniqueUsersLock = new();
        private static readonly HashSet<string> _uniqueUsers = JsonHelper.ConvertJson<HashSet<string>>(PathToUniqueUsers);

        public static HashSet<string> UniqueUsers { get { lock (UniqueUsersLock) { return _uniqueUsers; } } }

        private static Result GetCountUniqueUsers()
        {
            var bytes = Encoding.Default.GetBytes(UniqueUsers.Count.ToString());
            return new Result(200, bytes);
        }

        public static void UpdateUniqueUsersOrQuit(HttpListenerRequest request)
        {
            var dataUser = $"{request.UserAgent} {request.RemoteEndPoint?.Address}";
            if (!UniqueUsers.Contains(dataUser)) UniqueUsers.Add(dataUser);
        }

        public static async void ClearHistoryUniqueUsersAsync(int timeInMs)
        {
            while (true)
            {
                await Task.Delay(timeInMs);
                UniqueUsers.Clear();
                var fileStream = File.Open(PathToUniqueUsers, FileMode.Open);
                fileStream.SetLength(0);
                fileStream.Close();
            }
        }
    }
}