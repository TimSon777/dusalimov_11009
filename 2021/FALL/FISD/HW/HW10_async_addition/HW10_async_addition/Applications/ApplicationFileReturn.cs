﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using HW10_async_addition.AdditionalClasses;

namespace HW10_async_addition.Applications
{
    public sealed class ApplicationFileReturn : Application
    {
        public ApplicationFileReturn(string url) : base(url)
        { }
        
        public override async Task<Result> DistributeByTasksAsync(HttpListenerRequest request)
            => await SendResponseToWebsiteAsync(Url);
        
        private static async Task<Result> SendResponseToWebsiteAsync(string path)
        {
            var pathToFile = Path.Combine(Distributor.CurrentDirectory, "website", path);

            if (!File.Exists(pathToFile)) return new Result(404, Array.Empty<byte>());
            
            var bytes = await File.ReadAllBytesAsync(pathToFile);
            return new Result(200, bytes);
        }
    }
}