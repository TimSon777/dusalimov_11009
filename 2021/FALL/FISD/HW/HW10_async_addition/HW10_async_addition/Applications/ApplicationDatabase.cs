﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using HW10_async_addition.AdditionalClasses;
using HW10_async_addition.Helpers;
using Npgsql;

namespace HW10_async_addition.Applications
{
    public sealed class ApplicationDatabase : Application
    {
        public ApplicationDatabase(string url) : base(url)
        { }
        
        public override async Task<Result> DistributeByTasksAsync(HttpListenerRequest request)
        {
            return Url switch
            {
                "add_new_student" => await AddStudentToDatabaseAsync(request),
                "get_students" => await GetResultStudentsAsync(request),
                _ => throw new NotImplementedException()
            };
        }

        private const string ConnectionString = 
            "Host=localhost;Username=postgres;Password=postgres;Database=Point_Rating_System";

        private static async Task<Result> GetResultStudentsAsync(HttpListenerRequest request)
        {
            var data = await StreamHelper.GetDataAsync(request);
            var split = data.Split('_');
            var countStudentForOutput = int.Parse(split[1]);
            var countStudentOutput = int.Parse(split[0]);
            var students = await GetStudentsFromDatabaseAsync(countStudentOutput, countStudentForOutput);
            var jsonInString = JsonSerializer.Serialize(students);
            var bytes = Encoding.UTF8.GetBytes(jsonInString);
            
            return new Result(200, bytes);
        }

        private static async Task<List<Student>> GetStudentsFromDatabaseAsync(int countStudentOutput, int countStudentForOutput)
        {
            var result = new List<Student>();
            await using var connection = new NpgsqlConnection(ConnectionString);
            await connection.OpenAsync();

            const string select = "SELECT first_name, second_name, COALESCE(patronymic, '') patronymic, birthday_date, group_number ";
            const string from = "FROM person NATURAL JOIN student NATURAL JOIN study_group ";
            var offsetLimit = $"OFFSET {countStudentOutput} LIMIT {countStudentForOutput}";
            
            var cmd = new NpgsqlCommand($"{select}{from}{offsetLimit}", connection);

            await using var reader = cmd.ExecuteReader();
            
            while (await reader.ReadAsync())
            {
                var firstName = reader.GetString(reader.GetOrdinal("first_name"));
                var secondName = reader.GetString(reader.GetOrdinal("second_name"));
                var patronymic = reader.GetString(reader.GetOrdinal("patronymic"));
                var birthday = reader.GetDateTime(reader.GetOrdinal("birthday_date"));
                var groupNumber = reader.GetString(reader.GetOrdinal("group_number"));

                result.Add(new Student(
                    firstName,
                    secondName,
                    patronymic,
                    birthday.ToString("dd.MM.yyyy"),
                    $"11-0{groupNumber}"));
            }
            
            await cmd.DisposeAsync();
            return result;
        }

        private static async Task<Result> AddStudentToDatabaseAsync(HttpListenerRequest request)
        {
            var studentInString = await StreamHelper.GetDataAsync(request);
            var (student, isValid) = Student.GetStudentFromStringAndIsValid(studentInString);

            if (!isValid) return StreamHelper.GetResultWhereBytesIsBool("false");

            await using var connection = new NpgsqlConnection(ConnectionString);
            await connection.OpenAsync();

            var maxPersonQuery = $"SELECT MAX(person_id) max_id FROM person;";
            var maxPersonId = await GetIntAggregatedColumnAsync(maxPersonQuery, "max_id", connection);
            
            var groupIdQuery = $"SELECT group_id FROM study_group WHERE group_number = '{student.GroupNumber[4..]}'";
            var groupId = await GetIntAggregatedColumnAsync(groupIdQuery, "group_id", connection);

            var updatePerson = GetInsertQuery("person", 
                new[]
                {
                    "first_name", "second_name", "patronymic", "birthday_date"
                }, 
                new[]
                {
                    $"'{student.FirstName}'", $"'{student.SecondName}'", $"'{student.Patronymic}'", $"'{student.DateBirth}'"
                });
            
            await ExecuteRequestAsync(updatePerson, connection);

            var updateStudent = GetInsertQuery("student",
                new[]
                {
                    "person_id", "group_id"
                },
                new[]
                {
                    (maxPersonId + 1).ToString(), 
                    groupId.ToString()
                });
            
            await ExecuteRequestAsync(updateStudent, connection);

            return StreamHelper.GetResultWhereBytesIsBool("true");
        }
        
        private static async Task<int> GetIntAggregatedColumnAsync(string query, string column, NpgsqlConnection connection)
        {
            await using var cmd = await new NpgsqlCommand(query, connection).ExecuteReaderAsync();
            await cmd.ReadAsync();
            var result = cmd.GetInt32(cmd.GetOrdinal(column));
            await cmd.CloseAsync();
            return result;
        }

        private static string GetInsertQuery(string table, string[] columns, string[] values)
        {
            if (columns.Length != values.Length)
                throw new ArgumentException();

            return $"INSERT INTO {table} ({string.Join(", ", columns)}) VALUES ({string.Join(", ", values)})";
        }

        private static async Task ExecuteRequestAsync(string query, NpgsqlConnection connection)
        {
            var cmd = new NpgsqlCommand(query, connection);
            await cmd.ExecuteNonQueryAsync();
        }
    }
}