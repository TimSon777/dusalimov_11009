﻿using System.Net;
using System.Threading.Tasks;
using HW10_async_addition.AdditionalClasses;

namespace HW10_async_addition.Applications
{
    public abstract class Application
    {
        protected readonly string Url;

        protected Application(string url)
        {
            Url = url;
        }
        
        public abstract Task<Result> DistributeByTasksAsync(HttpListenerRequest request);
    }
}