﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HW10_async_addition.AdditionalClasses;
using HW10_async_addition.Helpers;

namespace HW10_async_addition.Applications
{
    public sealed class ApplicationSecretPart : Application
    {
        public ApplicationSecretPart(string url) : base(url)
        { }
        
        public override async Task<Result> DistributeByTasksAsync(HttpListenerRequest request)
            => Url switch 
            {
                "get_secret_part_with_id" => await CheckRightsAndGiveResultAsync(request),
                "get_secret_part" => await CheckEnteredWordAndReturnResultAsync(request),
                _ => throw new NotImplementedException()
            };
        
        private static async Task<Result> CheckRightsAndGiveResultAsync(HttpListenerRequest request)
        {
            var id = await StreamHelper.GetDataAsync(request);
            return Session.Visitors.ContainsKey(id) &&
                   Session.Visitors[id].IsHaveAccessRightsSecretPart
                ? StreamHelper.GetResultWhereBytesIsBool("true")
                : StreamHelper.GetResultWhereBytesIsBool("false");
        }

        private static async Task<Result> CheckEnteredWordAndReturnResultAsync(HttpListenerRequest request)
        {
            var keyWord = await StreamHelper.GetDataAsync(request);
            if (Server.KeyWord != keyWord) return StreamHelper.GetResultWhereBytesIsBool("false");
            
            Session.UpdateAccessRightSecretPart(request);
            return StreamHelper.GetResultWhereBytesIsBool("true");
        }
    }
}