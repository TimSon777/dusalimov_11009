﻿using System;

namespace HW10_async_addition.Infrastructure
{
    public static class Directory
    {
        public static string FindDirectoryNet5()
        {
            var directoryInfo = System.IO.Directory.GetParent(Environment.CurrentDirectory)?.Parent;
            return directoryInfo?.Parent?.ToString();
        }
    }
}