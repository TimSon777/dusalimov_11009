﻿using System.Text.RegularExpressions;

namespace HW10_async_addition.Infrastructure
{
    public static class ValidationRegexes
    {
        private const string EnglishLetters = "a-zA-Z";
        private const string RussianLetters = "а-яА-ЯёЁ";
        public static readonly Regex Name = new($"^[{EnglishLetters}{RussianLetters}]+$");
        public static readonly Regex NumberTwoDigitsDashThreeDigits = new(@"\d{2}-\d{3}");
    }
}