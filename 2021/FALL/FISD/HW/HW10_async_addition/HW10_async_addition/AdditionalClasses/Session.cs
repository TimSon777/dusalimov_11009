﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HW10_async_addition.Extensions;
using HW10_async_addition.Helpers;

namespace HW10_async_addition.AdditionalClasses
{
    public class Session
    {
        private static readonly TimeSpan SessionLifetime = new(0, 3, 0);
        public bool IsHaveAccessRightsSecretPart { get; }
        private DateTime LastAction { get; set; }

        private static readonly Dictionary<string, Session> _visitors  = JsonHelper.ConvertJson<Dictionary<string,Session>>(JsonHelper.PathToSessionsJson);
        public static Dictionary<string, Session> Visitors { get { lock (LockVisitors) return _visitors; } }
        private static readonly object LockVisitors = new();
        
        private Session(bool isHaveAccessRightsSecretPart, DateTime lastAction)
        {
            IsHaveAccessRightsSecretPart = isHaveAccessRightsSecretPart;
            LastAction = lastAction;
        }

        private static bool IsOver(string visitorId)
        {
            return (DateTime.Now - Visitors[visitorId].LastAction).CompareTo(SessionLifetime) >= 0;
        }
        
        public static async void RemoveSessionsNoActiveUsersAsync(int timeInMs)
        {
            await Task.Delay(timeInMs);
            Visitors
                .AsParallel()
                .Select(pair => pair.Key)
                .Where(IsOver)
                .ToList()
                .ForEach(id => Visitors.Remove(id));
        }
        
        public static void AssignIdIfNoneOrUpdateSession(HttpListenerRequest request, HttpListenerResponse response)
        {
            var visitorId = request.Cookies["Id"];
            
            if (visitorId == null || !Visitors.ContainsKey(visitorId.Value))
            {
                var id = 20.GetRandomStringWithEnglishLettersAndDigitsLengthBetween(30);
                response.AppendHeader("Set-Cookie", $"Id={id}; path=/; Max-Age={SessionLifetime.TotalMilliseconds}");
                Visitors.Add(id, new Session(false, DateTime.Now));
                return;
            }
            
            Visitors[visitorId.Value].LastAction = DateTime.Now;
            response.AppendHeader("Set-Cookie", $"Id={visitorId.Value}; path=/; Max-Age={SessionLifetime.TotalMilliseconds}");
        }

        public static void UpdateAccessRightSecretPart(HttpListenerRequest request)
        {
            var visitorId = request.Cookies["Id"];
            if (visitorId == null || !Visitors.ContainsKey(visitorId.Value)) return;
            Visitors[visitorId.Value] = new Session(true, DateTime.Now);
        }
    }
}