﻿namespace HW10_async_addition.AdditionalClasses
{
    public record Result(int StatusCode, byte[] Bytes);
}