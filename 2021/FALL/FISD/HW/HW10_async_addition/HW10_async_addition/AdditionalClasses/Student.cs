﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.Json.Serialization;
using HW10_async_addition.Infrastructure;

namespace HW10_async_addition.AdditionalClasses
{
    public class Student
    {
        [JsonPropertyName("First Name")]
        public string FirstName { get; }
        
        [JsonPropertyName("Second Name")]
        public string SecondName { get; }
        
        [JsonPropertyName("Patronymic")]
        public string Patronymic { get; }
        
        [JsonPropertyName("Date birth")]
        public string DateBirth { get; }
        
        [JsonPropertyName("Group number")]
        public string GroupNumber { get; }
        
        public Student(string firstName, 
            string secondName, 
            string patronymic, 
            string dateBirth, 
            string groupNumber)
        {
            FirstName = firstName;
            SecondName = secondName;
            Patronymic = patronymic;
            DateBirth = dateBirth;
            GroupNumber = groupNumber;
        }

        private Student()
        { }

        private static Dictionary<string, string> GetPairsStudentPropertyAndValue(string strParameters)
        {
            var parameters = strParameters
                .Replace("+", " ")
                .Split('&');
            
            return parameters
                .AsParallel()
                .Select(param => param.Split('='))
                .ToDictionary(pair => pair[0], pair => pair[1]);
        }
        
        public static (Student, bool) GetStudentFromStringAndIsValid(string parameters)
        {
            var postParams = GetPairsStudentPropertyAndValue(parameters);

            var student =  new Student(
                postParams["First Name"],
                postParams["Second Name"],
                postParams["Patronymic"],
                postParams["Date birth"],
                postParams["Group number"]
            );

            return IsValid(student) 
                ? (LeadToGeneralView(student), true) 
                : (new Student(), false);
        }

        private static bool IsValid(Student student)
        {
            return ValidationRegexes.Name.IsMatch(student.FirstName)
                   && ValidationRegexes.Name.IsMatch(student.SecondName)
                   && (ValidationRegexes.Name.IsMatch(student.Patronymic) || string.IsNullOrEmpty(student.Patronymic))
                   && DateTime.TryParseExact(student.DateBirth, "dd.MM.yyyy", 
                       CultureInfo.InvariantCulture, DateTimeStyles.None, out _)
                   && ValidationRegexes.NumberTwoDigitsDashThreeDigits.IsMatch(student.GroupNumber);
        }

        private static Student LeadToGeneralView(Student student)
        {
            return new Student(LeadToName(student.FirstName),
                LeadToName(student.SecondName),
                string.IsNullOrEmpty(student.Patronymic) ? "" : LeadToName(student.Patronymic),
                student.DateBirth,
                student.GroupNumber
            );
        }

        private static string LeadToName(string str)
        {
            if (string.IsNullOrEmpty(str)) throw new ArgumentException();
            return char.ToUpper(str[0]) + str[1..str.Length];
        }
    }
}