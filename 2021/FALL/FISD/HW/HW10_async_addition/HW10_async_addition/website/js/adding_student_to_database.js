﻿function leadToCorrectName(str) {
    return str[0].toUpperCase() + str.slice(1).toLowerCase()
}

function leadToCorrectNameAndWriteToInput(input) {
    let result = leadToCorrectName(input.val())
    input.val(result)
}

function tryAddStudentToDatabase(params) {
    if (isValidStudentAndDisplayErrors()) {
        
        //LEAD INPUT VALUES TO CORRECT VIEW
        let patronymic = $('#input_patronymic')
        if (patronymic.val() !== null && patronymic.val() !== "")
            leadToCorrectNameAndWriteToInput(patronymic)

        let firstName = $('#input_first_name')
        leadToCorrectNameAndWriteToInput(firstName)

        let secondName = $('#input_second_name')
        leadToCorrectNameAndWriteToInput(secondName)

        //ADD NEW STUDENT TO SITE
        let student = [
            secondName.val(),
            firstName.val(),
            patronymic.val(),
            $('#input_date_birth').val(),
            $('#input_group_number').val()
        ]
        let result = createRowTable(student)

        $.ajax({
            url: 'Database!add_new_student',
            type: 'post',
            data: decodeURI($(params).serialize().replace("%40", "@")),
            success: function (data) {
                if (data === "false") alert("Вы нас не переиграете!")
                else $('#classmates_table').append(result)
            }
        })
    }
}