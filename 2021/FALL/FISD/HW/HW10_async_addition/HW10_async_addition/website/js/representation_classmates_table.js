﻿function representClassmatesTable(countInPage, countToOutput) {
    $.ajax({
        url: 'Database!get_students',
        method: 'post',
        data: `${countToOutput}_${countInPage}`,
        success: function (data) {
            let students = JSON.parse(data)
            $.each(students, function (i, student) {
                let result = createRowTable(
                    [
                        student['Second Name'],
                        student['First Name'],
                        student['Patronymic'],
                        student['Date birth'],
                        student['Group number']
                    ])

                $('#classmates_table').append(result)
            })

            numberOutputRecords += students.length
        }
    })
}

function checkInputAndRepresentPartOfClassmates() {
    let input = $('#count_in_page')
    let countInPage = input
    let numberRecordsAtTime = parseInt(countInPage.val() === "" ? "0" : countInPage.val())
    
    if (numberRecordsAtTime < 0 || numberRecordsAtTime > 2147483647) {
        countInPage.val('0')
        $('#error_value').text('Введите адекватное число')
        input.css('background-color', wrongColorRGB)
    }
    else representClassmatesTable(numberRecordsAtTime, numberOutputRecords)
}