﻿let numberOutputRecords = 0
const maxTimeLifeCookieMs = 2147483647
const wrongColorRGB = 'rgb(255, 0, 0)'
const whiteColor16 = '#ffffff'

$(document).ready(function() {
    
    // LOAD THE MAIN PAGE
    $("#content").load('html/menu.html')
    getCountUniqueUsers()
    
    // ESTABLISH THEME
    let theme = getCookie('Theme')
    if (theme === null) {
        setCookie('Theme', 'White', maxTimeLifeCookieMs)
        theme = 'White'
    }
    establishTheme(theme)
    
    // CHANGE THEME WHEN CLICKED
    $('#theme_btn').click(function () {
        let theme = getCookie('Theme')
        changeTheme(theme) 
    })
    
    // DOWNLOAD OTHER PAGES, WHEN CLICKED IN MENU
    $('.links_content').click(function() {
        
        numberOutputRecords = 0
        
        let reference = $(this).attr('href')
        $("#content").load(reference,function ()
        {
            
            //PROCESS OF WRITING COUNT OF UNIQUE USERS AT MAIN PAGE
            if (reference === "html/menu.html") getCountUniqueUsers()

            $('#count_in_page').on('focus', function () {
                $('#error_value').text('')
                $('#count_in_page').css('background-color', '#ffffff')
            })

            //PROCESSING SECRET PART
            if (reference === "html/secret_part.html" && getCookie('Id') !== null) {
                tryGetAccessToSecretPartWithId()
            }
            
            $("#code_word").on('submit', function () {
                tryGetAccessToSecretPartWithoutId()
                return false
            })
            
            //FILTERING PROCESS
            let filters = $('.filter_input')
            let table = $("#classmates_table")

            filters.on('focus', function () {
                sortTableWhenFilter(table, filters, $(this))
            })
            
            filters.on('input', filters, function() {
                filterTableAtSite(table, filters, [0,1,2])
            })
            
            //SORTING PROCESSING
            $('#classmates_table th').click(function () {
                sortTable($(this), false)
            })

            //ADDING STUDENT
            $(".input_student").on('focus', function () {
                if ($(this).css('background-color') === wrongColorRGB) $(this).val('')
                $(this).css('background-color', whiteColor16)
            }) 
            
            $("#adding_student_form").on("submit", function() {
                tryAddStudentToDatabase($(this))
                return false
            })

            //REPRESENTATION CLASSMATES
            $('#show_more_btn').click(checkInputAndRepresentPartOfClassmates)
        })
    
        return false
    })
})



