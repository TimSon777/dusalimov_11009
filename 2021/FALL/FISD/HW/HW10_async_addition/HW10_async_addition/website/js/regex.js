﻿const regex_name = new RegExp("^[a-zA-Zа-яА-ЯёЁ]+$")
const regex_patronymic = new RegExp("^[a-zA-Zа-яА-ЯёЁ]*$")
const regex_group_number = new RegExp("^[0-9]{2}-[0-9]{3}$")
const regex_date_birth = new RegExp("^(0[1-9]|[12][0-9]|(30|31)).(0[1-9]|10|11|12).[0-9]{4}$")

function isMatch(regex, value) {
    return !!regex.exec(value)
}