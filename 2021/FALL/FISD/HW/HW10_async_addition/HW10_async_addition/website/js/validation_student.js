﻿function displayErrors(input, message) {
    input.val(message)
    input.css('background-color', wrongColorRGB)
}

function isValidStudentAndDisplayErrors() {
    
    let input_first_name = $('#input_first_name')
    let input_second_name = $('#input_second_name')
    let input_patronymic = $('#input_patronymic')
    let input_group_number = $('#input_group_number')
    let input_date_birth = $('#input_date_birth')

    let isCorrectStudent = true
    
    if (!isMatch(regex_name, input_first_name.val())) {
        displayErrors(input_first_name, 'Введите имя')
        isCorrectStudent = false
    }
    
    if (!isMatch(regex_name, input_second_name.val())) {
        displayErrors(input_second_name, 'Введите фамилию')
        isCorrectStudent = false
    }

    if (!isMatch(regex_patronymic, input_patronymic.val())) {
        displayErrors(input_patronymic, 'Введите отчество или сотавьте пустым, если его нет')
        isCorrectStudent = false
    }

    if (!isMatch(regex_group_number, input_group_number.val())) {
        displayErrors(input_group_number, 'Формат (числовой): **-***')
        isCorrectStudent = false
    }

    if (!isMatch(regex_date_birth, input_date_birth.val())) {
        displayErrors(input_date_birth, 'Формат: dd.mm.yyyy')
        isCorrectStudent = false
    }

    return isCorrectStudent
}