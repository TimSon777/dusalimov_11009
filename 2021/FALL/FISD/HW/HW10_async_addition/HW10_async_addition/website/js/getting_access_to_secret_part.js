﻿function checkAndGrantAccessIfTrue(flag) {
    if (flag === "true") {
        $("#content").load("html/super_secret_part.html")
        $('#error_text').text('')
        $('#code_word_text').on('focus', function () {
            $('#code_word_text').val('')
        })
    }
}

function tryGetAccessToSecretPartWithId() {
    let id = getCookie('Id')
    $.ajax({
        url: 'Secret!get_secret_part_with_id',
        type: 'post',
        data: id,
        success: function (flag) {
            checkAndGrantAccessIfTrue(flag)
        }
    })
}

function tryGetAccessToSecretPartWithoutId() {
    $.ajax({
        url: 'Secret!get_secret_part',
        method: 'post',
        data: $("#code_word_text").val(),
        success: function (flag) {
            checkAndGrantAccessIfTrue(flag)
            $('#error_text').text('Почти...')
        }
    })
}