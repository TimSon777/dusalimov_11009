﻿function establishTheme(theme) {
    $('#theme_href').attr('href', `css/theme_${theme}.css`)
}

function changeTheme(theme) {
    if (theme === null) theme = 'White'
    let isWhite = theme === 'White' ? 'Black' : 'White'
    setCookie('Theme', isWhite, maxTimeLifeCookieMs)
    establishTheme(isWhite)
}