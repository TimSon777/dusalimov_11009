function getValue(row, index) {
    return $(row).children('td').eq(index).text()
}

function comparer(index) {
    return function(a, b)
    {
        let x = getValue(a, index)
        let y = getValue(b, index)
        
        return $.isNumeric(x) && $.isNumeric(y) 
            ? x - y 
            : x.toString().localeCompare(y)
    }
}

function sortTable(th, isAlwaysAsc) {
    const table = $(th).parents('table').first()
    let rows = table
        .find('tr:gt(0)')
        .toArray()
        .sort(comparer($(th).index()))
    
    this.asc = !this.asc
    if (!isAlwaysAsc && !this.asc) rows = rows.reverse()
    
    for (let i = 0; i < rows.length; i++) table.append(rows[i])
}

function sortTableWhenFilter(table, filters, currentInput) {
    let rowsTh = table.find('th').toArray()
    let filtersId = $.map(filters, function (filter) {
        return $(filter).attr('id')
    })
    
    let index = $.inArray($(currentInput).attr('id'), filtersId)
    let currentTh = rowsTh[index]
    sortTable(currentTh, true)
}

