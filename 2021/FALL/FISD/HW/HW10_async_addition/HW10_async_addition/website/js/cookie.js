﻿function setCookie(name, value, ms, path) {
    document.cookie = `${name}=${value || ""}; Max-Age=${ms || "Session"}; path=${path || "/"}`
}

function getCookie(name) {
    const nameAndEqual = `${name}=`
    const cookiesWithSpace = document.cookie.split(';')
    const cookies = cookiesWithSpace.join("").split(' ')
    
    for (let i = 0; i < cookies.length; i++) {
        if (cookies[i].indexOf(nameAndEqual) === 0)
            return cookies[i].substring(nameAndEqual.length)
    }
    return null
}
