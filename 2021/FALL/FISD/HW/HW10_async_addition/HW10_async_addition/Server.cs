﻿using System.Net;
using System.Threading.Tasks;
using HW10_async_addition.AdditionalClasses;
using HW10_async_addition.Applications;
using HW10_async_addition.Helpers;

namespace HW10_async_addition
{
    public static class Server
    {
        public const string KeyWord = "нарот";

        public static async Task StartAsync()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();
            
            RunTasks();

            while (true)
            {
                var ctx = await listener.GetContextAsync();
                await Task.Run(async () =>
                {
                    var request = ctx.Request;
                    var response = ctx.Response;

                    Configurator.ConfigureRequest(request, response);

                    var result = await Distributor.DistributeAndReturnResult(request);
                    StreamHelper.SendBytesToSite(result, response);
                });
            }
        }

        private static void RunTasks()
        {
            Task.Run(() => JsonHelper.UploadSthToJsonAsync(600000, 
                JsonHelper.PathToSessionsJson, 
                Session.Visitors));
            
            Task.Run(() => JsonHelper.UploadSthToJsonAsync(600000, 
                ApplicationUniqueUsers.PathToUniqueUsers, 
                ApplicationUniqueUsers.UniqueUsers));
            
            Task.Run(() => Session.RemoveSessionsNoActiveUsersAsync(300000));
            Task.Run(() => ApplicationUniqueUsers.ClearHistoryUniqueUsersAsync(86400000));
        }
    }
}