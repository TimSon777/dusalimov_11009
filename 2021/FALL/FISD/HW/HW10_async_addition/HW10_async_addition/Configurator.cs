﻿using System.Net;
using HW10_async_addition.AdditionalClasses;
using HW10_async_addition.Applications;

namespace HW10_async_addition
{
    public static class Configurator
    {
        public static void ConfigureRequest(HttpListenerRequest request, HttpListenerResponse response)
        {
            Session.AssignIdIfNoneOrUpdateSession(request, response);
            ApplicationUniqueUsers.UpdateUniqueUsersOrQuit(request);
        }
    }
}