﻿using System;
using System.Net;
using System.Threading.Tasks;
using HW10_async_addition.AdditionalClasses;
using HW10_async_addition.Applications;
using HW10_async_addition.Infrastructure;

namespace HW10_async_addition
{
    public static class Distributor
    {
        public static readonly string CurrentDirectory = Directory.FindDirectoryNet5();

        public static async Task<Result> DistributeAndReturnResult(HttpListenerRequest request)
        {
            var path = request.Url?.AbsolutePath;
            
            if (path is null or "/") path = "index.html";
            if (!char.IsUpper(path[1])) path = $"/File!{path}";
                
            var splitUrl = path.Split(new []{"/", "!"},2, StringSplitOptions.RemoveEmptyEntries);

            var application = splitUrl[0];
            var action = splitUrl[1];
            
            return application switch 
            {
                "Database" => await new ApplicationDatabase(action).DistributeByTasksAsync(request),
                "Secret" => await new ApplicationSecretPart(action).DistributeByTasksAsync(request),
                "Unique" => await new ApplicationUniqueUsers(action).DistributeByTasksAsync(request),
                _ => await new ApplicationFileReturn(action).DistributeByTasksAsync(request),
            };
        }
    }
}

    