﻿using System;
using System.Text;

namespace HW10_async_addition.Extensions
{
    public static class ArrayExtensions
    {
        private static readonly Random Rnd = new();

        public static string GetRandomWord(this char[] symbols, int length)
        {
            if (length < 0) throw new ArgumentException();
            
            var strBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var rndIndex = Rnd.Next(0, symbols.Length - 1);
                strBuilder.Append(symbols[rndIndex]);
            }

            return strBuilder.ToString();
        }
    }
}