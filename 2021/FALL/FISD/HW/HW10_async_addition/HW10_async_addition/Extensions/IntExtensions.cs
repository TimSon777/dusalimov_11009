﻿using System;

namespace HW10_async_addition.Extensions
{
    public static class IntExtensions
    {
        private static readonly char[] EnglishLettersAndDigits = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
        private static readonly Random Rnd = new();
        
        public static string GetRandomStringWithEnglishLettersAndDigitsLengthBetween(this int minLength, int maxLength)
        {
            if (minLength < 0 || minLength > maxLength) throw new ArgumentException();
            var length = Rnd.Next(minLength, maxLength);
            return EnglishLettersAndDigits.GetRandomWord(length);
        }
    }
}