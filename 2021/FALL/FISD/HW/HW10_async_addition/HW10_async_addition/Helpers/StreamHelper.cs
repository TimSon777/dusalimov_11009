﻿using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HW10_async_addition.AdditionalClasses;

namespace HW10_async_addition.Helpers
{
    public static class StreamHelper
    {
        public static void SendBytesToSite(Result result, HttpListenerResponse response)
        {
            response.StatusCode = result.StatusCode;
            var sw = response.OutputStream;
            response.ContentLength64 = result.Bytes.Length;
            sw.Write(result.Bytes, 0, result.Bytes.Length);
            sw.Close();
        }
        
        public static async Task<string> GetDataAsync(HttpListenerRequest request)
        {
            var inputStream = request.InputStream;
            var encoding = request.ContentEncoding;
            var streamReader = new StreamReader(inputStream, encoding);
            return await streamReader.ReadToEndAsync();
        }
        
        
        public static Result GetResultWhereBytesIsBool(string boolean)
        {
            var bytes = Encoding.Default.GetBytes(boolean);
            return new Result(200, bytes);
        }
    }
}