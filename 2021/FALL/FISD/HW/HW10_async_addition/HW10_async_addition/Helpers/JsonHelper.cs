﻿using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace HW10_async_addition.Helpers
{
    public static class JsonHelper
    {
        public static readonly string PathToSessionsJson = Path.Combine(Distributor.CurrentDirectory, "website", "json/sessions.json");
        
        private static readonly JsonSerializerOptions Options = new()
        {
            WriteIndented = true
        };

        public static async void UploadSthToJsonAsync<T>(int timeMs, string path, T obj)
        {
            while (true)
            {
                await Task.Delay(timeMs);
                var jsonInString = JsonSerializer.Serialize(obj, Options);
                await File.WriteAllTextAsync(path, jsonInString);
            }
        }

        public static T ConvertJson<T>(string path) where T: new()
        {
            var json = File.ReadAllText(path);
            try
            {
                return JsonSerializer.Deserialize<T>(json);
            }
            catch (Exception)
            {
                return new T();
            }
        }
    }
}