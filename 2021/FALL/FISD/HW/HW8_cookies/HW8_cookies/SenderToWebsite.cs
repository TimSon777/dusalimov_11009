﻿using System;
using System.Net;

namespace HW8_cookies
{
    public static class SenderToWebsite
    {
        public static void SendBytesToSite(int statusCode, byte[] bytes, HttpListenerResponse response)
        {
            response.StatusCode = statusCode;
            var sw = response.OutputStream;
            response.ContentLength64 = bytes.Length;
            sw.Write(bytes, 0, bytes.Length);
            sw.Close();
        }

        public static void SendBytesToSiteWithStatusCode404(HttpListenerResponse response)
        {
            SendBytesToSite(404, Array.Empty<byte>(), response);
        }
    }
}