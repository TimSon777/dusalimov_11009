﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace HW8_cookies
{
    public static class ApplicationJson
    {
        private static readonly string PathToClassmatesJson = $"{Server.CurrentDirectory}/website/json/table_classmates.json";
        public static readonly string PathToSessionsJson = $"{Server.CurrentDirectory}/website/json/sessions.json";
        
        private static readonly List<Student> Students;

        static ApplicationJson()
        {
            var json = File.ReadAllText(PathToClassmatesJson);
            
            try
            {
                Students = JsonSerializer.Deserialize<List<Student>>(json);
            }
            catch (Exception)
            {
                Students = new List<Student>();
            }
        }

        private static readonly JsonSerializerOptions Options = new()
        {
            WriteIndented = true
        };
        
        public static void WriteNewStudentToJson(HttpListenerRequest request, HttpListenerResponse response)
        {
            var inputStream = request.InputStream;
            var encoding = request.ContentEncoding;
            var streamReader = new StreamReader(inputStream, encoding);
            var studentInString = streamReader.ReadToEnd();
            var student = Student.ConvertStringToStudent(studentInString);
            Students.Add(student);

            var jsonInString = JsonSerializer.Serialize(Students, Options);
            File.WriteAllText(PathToClassmatesJson, jsonInString);
        }

        public static async void UploadSessionsToJson(int timeMs)
        {
            while (true)
            {
                await Task.Delay(timeMs);
                var jsonInString = JsonSerializer.Serialize(ApplicationCookie.Visitors, Options);
                await File.WriteAllTextAsync(PathToSessionsJson, jsonInString);
            }
        }
    }
}