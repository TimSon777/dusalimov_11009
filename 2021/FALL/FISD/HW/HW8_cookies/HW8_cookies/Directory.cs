﻿using System;
using System.IO;

namespace HW8_cookies
{
    public static class DirectoryExtension
    {
        public static string FindDirectoryNet5()
        {
            var directoryInfo = Directory.GetParent(Environment.CurrentDirectory)?.Parent;
            return directoryInfo?.Parent?.ToString();
        }
    }
}