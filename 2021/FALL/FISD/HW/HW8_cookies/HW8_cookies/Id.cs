﻿using System;

namespace HW8_cookies
{
    public static class Id
    {
        private static readonly char[] IdSymbols = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
        private static readonly Random Rnd = new();
        
        public static string GetRandomStringId()
        {
            var length = Rnd.Next(20, 30);
            return IdSymbols.GetRandomWord(length);
        }
    }
}