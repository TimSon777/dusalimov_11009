﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace HW8_cookies
{
    public static class Server
    {
        public static readonly string CurrentDirectory = DirectoryExtension.FindDirectoryNet5();
        
        public static int CountVisiting = 0;
        public static void DistributeByApplication()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();

            Task.Run(() => ApplicationJson.UploadSessionsToJson(600000));

            var json = File.ReadAllText(ApplicationJson.PathToSessionsJson);
            try
            {
                ApplicationCookie.Visitors =
                    JsonSerializer.Deserialize<Dictionary<string, Session>>(json);
            }
            catch (Exception)
            {
                ApplicationCookie.Visitors = new Dictionary<string, Session>();
            }
            
            while (true)
            {
                var ctx = listener.GetContext();
                var request = ctx.Request;
                var response = ctx.Response;

                var path = request.Url?.AbsolutePath;

                ApplicationCookie.AssignIdIfNoneOrUpdateSession(request, response);

                if (path == "/getting_access_to_secret_part_correctly")
                {
                    ApplicationCookie.UpdateAccessRightSecretPart(request, response);
                    SenderToWebsite.SendBytesToSite(200, Array.Empty<byte>(), response);
                }
                else if (path == "/json_update.table_classmates")
                {
                    ApplicationJson.WriteNewStudentToJson(request, response);
                    SenderToWebsite.SendBytesToSite(200, Array.Empty<byte>(), response);
                }
                else if (path == "/get_counter")
                {
                    var bytes = Encoding.Default.GetBytes(CountVisiting.ToString());
                    SenderToWebsite.SendBytesToSite(200, bytes, response);
                }
                else
                {
                    ApplicationVisualization.SendResponseToWebsite(response, path);
                }
            }
        }
    }
}