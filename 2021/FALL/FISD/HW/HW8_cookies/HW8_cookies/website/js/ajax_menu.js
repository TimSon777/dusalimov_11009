﻿function leadToCorrectName(str) {
    return str[0].toUpperCase() + str.slice(1).toLowerCase()
}

function leadToCorrectNameAndWriteToInput(input) {
    let result = leadToCorrectName(input.val())
    input.val(result)
}

function establishTheme(theme) {
    $('#theme_href').attr('href', `css/theme_${theme}.css`)
}

function changeTheme(theme) {
    if (theme === null) theme = 'White'
    
    setCookie('Theme', theme === 'White' ? 'Black' : 'White')
    establishTheme(theme === 'White' ? 'Black' : 'White')
}

$(document).ready(function() {
    let theme = getCookie('Theme');
    if (theme === null) {
        setCookie('Theme', 'White', 1)
        theme = 'White'
    }
    establishTheme(theme)
    
    $('#theme_btn').click(function () {
        let theme = getCookie('Theme');
        changeTheme(theme) 
    })
    
    $('.links_content').click(function() 
    {
        let reference = $(this).attr('href')
        $("#content").load(reference,function ()
        {
            if (reference === "html/menu.html") {
                $.ajax({
                    url: 'get_counter',
                    method: 'post',
                    success: function(data) {
                        $("#counter").html(data)
                    }
                })
            }

            if (reference === "html/secret_part.html" && getCookie("IsHaveAccessRightsSecretPart") === "true") {
                $("#content").load("html/super_secret_part.html")
            }
            else {
                $("#code_word").on('submit', function (){
                    if ($("#code_word_text").val() !== "нарот")
                        $("#error_text").html("Вы ввели фигню")
                    else {
                        $("#content").load("html/super_secret_part.html")
                        $.ajax({
                            url: 'getting_access_to_secret_part_correctly'
                        })
                    }

                    return false
                })
            }
            
            let filters = $('.filter_input')
            filters.on('input', filters, function() {
                filterTableAtSite($("#classmates_table"), filters, [1,2,3])
            });
            
            $('#classmates_table th').click(sortTableAtSite)
            
            $("#adding_student_form").on("submit", function() {
                if (validateStudentAndReturnResult()) {
                    let patronymic = $('#input_patronymic')
                    if (patronymic.val() !== null && patronymic.val() !== "")
                        leadToCorrectNameAndWriteToInput(patronymic)
                    
                    let firstName = $('#input_first_name')
                    leadToCorrectNameAndWriteToInput(firstName)

                    let secondName = $('#input_second_name')
                    leadToCorrectNameAndWriteToInput(secondName)
                    
                    let email = $('#input_email')
                    email.val(email.val().toLowerCase())
                    
                    let result = createRowTable(
                        [
                            $('#input_student_id_number').val(),
                            secondName.val(),
                            firstName.val(),
                            patronymic.val(),
                            $('#input_date_birth').val(),
                            $('#input_group_number').val(),
                            email.val().toLowerCase(),
                            $('#input_additional_information').val()
                        ])
                    
                    $.ajax({
                        url: 'json_update.table_classmates',
                        type: 'post',
                        dataType: 'html',
                        data: decodeURI($(this).serialize().replace("%40", "@")),
                        success: function () {
                            $('#classmates_table').append(result)}
                    });
                }
                
                return false;
            });
        })

        if (reference === "html/classmates.html") representClassmatesTable()
        
        return false;
    });


});



