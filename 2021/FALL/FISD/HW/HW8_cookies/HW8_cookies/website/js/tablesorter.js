function getValue(row, index)
{
    return $(row).children('td').eq(index).text()
}

function comparer(index)
{
    return function(a, b)
    {
        let x = getValue(a, index)
        let  y = getValue(b, index)
        
        return $.isNumeric(x) && $.isNumeric(y) 
            ? x - y 
            : x.toString().localeCompare(y)
    }
}

function sortTableAtSite()
{
    const table = $(this).parents('table').eq(0);
    let rows = table
        .find('tr:gt(0)')
        .toArray()
        .sort(comparer($(this).index()));
    
    this.asc = !this.asc
    if (!this.asc) rows = rows.reverse()
    
    for (let i = 0; i < rows.length; i++)
        table.append(rows[i])
}

