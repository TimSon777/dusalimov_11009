﻿function representClassmatesTable() {
    $.getJSON("../json/table_classmates.json", function (data) {
        $.each(data, function (i, student) {
            let result = createRowTable(
                [
                    student['Student ID number'],
                    student['Second Name'],
                    student['First Name'],
                    student['Patronymic'],
                    student['Date birth'],
                    student['Group number'],
                    student['E-mail'],
                    student['Additional information']
                ])

            $('#classmates_table').append(result)
        })
    })
}