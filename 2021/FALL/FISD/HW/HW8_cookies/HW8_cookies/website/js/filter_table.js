﻿function filterTableAtSite($table, $filters, columnIndexes) {
    const $rows = $table.find('tr');
    const filterValues = $filters.map(function() {
        return $(this).val().toLowerCase()
    });

    $rows.each(function() {
        let flag = true;
        
        $(this).find('td').each(function(colIndex) {
            let valueInRow = $(this).html().toLowerCase();
            
            if (columnIndexes.indexOf(colIndex) !== -1 && flag && valueInRow.indexOf(filterValues[colIndex - 1]) !== 0)
                flag = false;
        });
        
        $(this).css('display', flag ? '' : 'none')
    });
}