﻿using System;

namespace HW8_cookies
{
    public class Session
    {
        public bool IsHaveAccessRightsSecretPart { get; set; }
        public DateTime LastAction { get; set; }
        public static readonly TimeSpan SessionLifetime;

        static Session()
        {
            SessionLifetime = new TimeSpan(0, 3, 0);
        }

        public Session(bool isHaveAccessRightsSecretPart, DateTime lastAction)
        {
            IsHaveAccessRightsSecretPart = isHaveAccessRightsSecretPart;
            LastAction = lastAction;
        }
    }
}