﻿using System.IO;
using System.Net;

namespace HW8_cookies
{
    public static class ApplicationVisualization
    {
        public static void SendResponseToWebsite(HttpListenerResponse response, string path)
        {
            if (path is null or "/") path = "index.html";

            var pathToFile = $"{Server.CurrentDirectory}\\website\\{path}";
            
            if (File.Exists(pathToFile))
            {
                var bytes = File.ReadAllBytes(pathToFile);
                SenderToWebsite.SendBytesToSite(200, bytes, response);
            }
            else
            {
                SenderToWebsite.SendBytesToSiteWithStatusCode404(response);
            }
        }
    }
}