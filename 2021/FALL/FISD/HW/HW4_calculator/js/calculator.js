document.getElementById("btn_add_0").onclick = addDigit;
document.getElementById("btn_add_1").onclick = addDigit;
document.getElementById("btn_add_2").onclick = addDigit;
document.getElementById("btn_add_3").onclick = addDigit;
document.getElementById("btn_add_4").onclick = addDigit;
document.getElementById("btn_add_5").onclick = addDigit;
document.getElementById("btn_add_6").onclick = addDigit;
document.getElementById("btn_add_7").onclick = addDigit;
document.getElementById("btn_add_8").onclick = addDigit;
document.getElementById("btn_add_9").onclick = addDigit;

document.getElementById("btn_multiplication").onclick = calculate;
document.getElementById("btn_addition").onclick = calculate;
document.getElementById("btn_subtraction").onclick = calculate;
document.getElementById("btn_division").onclick = calculate;


document.getElementById("btn_mr").onclick = memoryRead;
document.getElementById("btn_ms").onclick = memorySave;
document.getElementById("btn_mc").onclick = memoryClear;
document.getElementById("btn_m+").onclick = memoryAdd;
document.getElementById("btn_m-").onclick = memorySubtract;

document.getElementById("btn_square").onclick = calculateSquare;
document.getElementById("btn_inverse").onclick = calculateInverse;
document.getElementById("btn_root").onclick = calculateRoot;
document.getElementById("btn_take_percentage").onclick = calculateTakePercentage;
document.getElementById("btn_ce").onclick = calculateCleanEntry;
document.getElementById("btn_c").onclick = calculateClean;
document.getElementById("btn_make_opposite").onclick = calculateMakeOpposite;
document.getElementById("btn_backspace").onclick = makeBackspace;

document.getElementById("btn_dot").onclick = addDot;
document.getElementById("btn_equal").onclick = getResult;

let inputNumber = document.getElementById("input_number")
let historyLabel= document.getElementById("history")

let operation = {
    multiplication: "*",
    division: "/",
    addition: "+",
    subtraction: "-"
}

let previous_number = 0
let previous_operation = null

function addDigit() {
    if (inputNumber.value === "0")
        inputNumber.value = this.value;
    else
        inputNumber.value = inputNumber.value + this.value;
}

function calculate() {
    calculateKeyboard(this.value)
}

function calculateKeyboard(op) {
    historyLabel.textContent = "";
    if (inputNumber.value !== "") {
        previous_number = parseFloat(inputNumber.value);
        historyLabel.textContent = previous_number + " " + op;
    }

    inputNumber.value = 0;
    previous_operation = op
}

function getResult() {
    if (inputNumber.value === "")
        inputNumber.value = previous_number;

    switch (previous_operation) {
        case operation.multiplication:
            inputNumber.value = previous_number * parseFloat(inputNumber.value);
            break;
        case operation.division:
            inputNumber.value = previous_number / parseFloat(inputNumber.value);
            break;
        case operation.addition:
            inputNumber.value = previous_number + parseFloat(inputNumber.value);
            break;
        case operation.subtraction:
            inputNumber.value = previous_number - parseFloat(inputNumber.value);
            break;
    }
    previous_number = inputNumber.value;
    historyLabel.textContent = "";
}

let validKeyboardOperations = new Set(["-", "+", "*", "/"])
let otherValidKeyboardKeys = new Set(["F5", "ArrowLeft", "ArrowRight"])

inputNumber.onkeydown = function (e) {
    if (e.key === "Delete" || e.key === "Backspace") {
        if (inputNumber.value.length === 1) {
            inputNumber.value = 0;
            return false;
        }
        return true;
    }

    if (e.key >= 0 && e.key <= 9) {
        if (inputNumber.value === "0")
            inputNumber.value = "";
        return true;
    }

    if (otherValidKeyboardKeys.has(e.key)) return true;
    if (validKeyboardOperations.has(e.key)) calculateKeyboard(e.key);
    if (e.key === "=" || e.key === "Enter") getResult();
    if (e.key === "." && !IsContainsOtherDots(inputNumber.value))
        inputNumber.value += ".";

    return false;
}

function IsContainsOtherDots(text) {
    return new Set(text).has(".")
}

let memoryValue = 0;

function memoryRead() {
    inputNumber.value = memoryValue;
}

function memorySave() {
    memoryValue = parseFloat(inputNumber.value);
}

function memoryClear() {
    memoryValue = 0;
}

function memorySubtract() {
    memoryValue = parseFloat(memoryValue) - parseFloat(inputNumber.value);
}

function memoryAdd() {
    memoryValue = parseFloat(memoryValue) + parseFloat(inputNumber.value);
}

function calculateSquare() {
    inputNumber.value = inputNumber.value * inputNumber.value;
}

function calculateInverse() {
    inputNumber.value = 1 / inputNumber.value;
}

function calculateRoot() {
    inputNumber.value = Math.sqrt(inputNumber.value);
}

function calculateTakePercentage() {
    inputNumber.value = previous_number * inputNumber.value / 100;
}

function calculateCleanEntry() {
    inputNumber.value = 0;
}

function calculateClean() {
    inputNumber.value = 0;
    previous_number = 0;
    previous_operation = null;
    historyLabel.textContent = "";
}

function calculateMakeOpposite() {
    inputNumber.value = inputNumber.value * (-1);
}

function addDot() {
    if (!IsContainsOtherDots(inputNumber.value))
        inputNumber.value += ".";
}

function makeBackspace() {
    if (inputNumber.value !== "0") {
        if (inputNumber.value.length === 1) {
            inputNumber.value = 0;
        } else {
            inputNumber.value = inputNumber.value.slice(0, -1);
        }
    }
}