﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HW9_db
{
    public static class Server
    {
        public static readonly string CurrentDirectory = DirectoryExtension.FindDirectoryNet5();
        public const string KeyWord = "нарот";
        public static int CountVisiting = 0;
        public static void DistributeByApplication()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();

            Task.Run(() => ApplicationJson.UploadSessionsToJson(600000));

            var json = File.ReadAllText(ApplicationJson.PathToSessionsJson);
            try
            {
                ApplicationCookie.Visitors =
                    JsonSerializer.Deserialize<Dictionary<string, Session>>(json);
            }
            catch (Exception)
            {
                ApplicationCookie.Visitors = new Dictionary<string, Session>();
            }
            
            while (true)
            {
                var ctx = listener.GetContext();
                var request = ctx.Request;
                var response = ctx.Response;

                var path = request.Url?.AbsolutePath;

                ApplicationCookie.AssignIdIfNoneOrUpdateSession(request, response);

                if (path == "/getting_access_to_secret_part_with_id")
                {
                    var id = GetData(request);
                    if (ApplicationCookie.Visitors.ContainsKey(id) &&
                        ApplicationCookie.Visitors[id].IsHaveAccessRightsSecretPart)
                    {
                        var bytes = Encoding.Default.GetBytes("true");
                        SenderToWebsite.SendBytesToSite(200, bytes, response);
                    }
                    else
                    {
                        var bytes = Encoding.Default.GetBytes("false");
                        SenderToWebsite.SendBytesToSite(200, bytes, response);
                    }
                }
                else if (path == "/getting_access_to_secret_part")
                {
                    var keyWord = GetData(request);
                    if (KeyWord == keyWord)
                    {
                        ApplicationCookie.UpdateAccessRightSecretPart(request, response);
                        var bytes = Encoding.Default.GetBytes("true");
                        SenderToWebsite.SendBytesToSite(200, bytes, response);
                    }
                    else
                    {
                        var bytes = Encoding.Default.GetBytes("false");
                        SenderToWebsite.SendBytesToSite(200, bytes, response);
                    }
                }
                else if (path == "/json_update.table_classmates")
                {
                    ApplicationDatabase.AddStudentToDatabase(request);
                    SenderToWebsite.SendBytesToSite(200, Array.Empty<byte>(), response);
                }
                else if (path == "/get_counter")
                {
                    var bytes = Encoding.Default.GetBytes(CountVisiting.ToString());
                    SenderToWebsite.SendBytesToSite(200, bytes, response);
                }
                else if (db.IsMatch(path ?? string.Empty))
                {
                    var split = path.Split('_');
                    var countInPage = int.Parse(split[3]);
                    var countPage = int.Parse(split[2]);
                    var jsonInString = JsonSerializer.Serialize(ApplicationDatabase.GetStudentsFromDatabase(countPage, countInPage));
                    var bytes = Encoding.UTF8.GetBytes(jsonInString);
                    SenderToWebsite.SendBytesToSite(200, bytes, response);
                }
                else
                {
                    ApplicationVisualization.SendResponseToWebsite(response, path);
                }
            }
        }

        private static string GetData(HttpListenerRequest request)
        {
            var inputStream = request.InputStream;
            var encoding = request.ContentEncoding;
            var streamReader = new StreamReader(inputStream, encoding);
            return streamReader.ReadToEnd();
        }

        private static Regex db = new(@"/getting_table_[\d]+_[\d]+");
    }
}