﻿using System;
using System.Collections.Generic;
using System.Net;

namespace HW9_db
{
    public static class ApplicationCookie
    {
        public static Dictionary<string, Session> Visitors;
        
        public static void AssignIdIfNoneOrUpdateSession(HttpListenerRequest request, HttpListenerResponse response)
        {
            var visitorId = request.Cookies["Id"];

            if (visitorId == null || !Visitors.ContainsKey(visitorId.Value))
            {
                var id = Id.GetRandomStringId();
                var cookie = new Cookie("Id", id, "/");
                response.SetCookie(cookie);
                
                Visitors.Add(id, new Session(false, DateTime.Now));
                Server.CountVisiting++;
            }
            else if ((DateTime.Now - Visitors[visitorId.Value].LastAction).CompareTo(Session.SessionLifetime) >= 0)
            {
                Visitors[visitorId.Value] = new Session(false, DateTime.Now);
                response.SetCookie(new Cookie("IsHaveAccessRightsSecretPart", "false", "/"));
            }
            else
            {
                Visitors[visitorId.Value].LastAction = DateTime.Now;
            }
        }

        public static void UpdateAccessRightSecretPart(HttpListenerRequest request, HttpListenerResponse response)
        {
            var visitorId = request.Cookies["Id"];
            if (visitorId == null || !Visitors.ContainsKey(visitorId.Value)) return;
            
            Visitors[visitorId.Value].IsHaveAccessRightsSecretPart = true;
            Visitors[visitorId.Value].LastAction = DateTime.Now;
            
            var cookie = response.Cookies["IsHaveAccessRightsSecretPart"];

            var newCookie = new Cookie("IsHaveAccessRightsSecretPart", "true", "/");
            
            if (cookie is null) response.SetCookie(newCookie);
            else
            {
                cookie.Value = "true";
                cookie.Path = "/";
            }
        }
    }
}