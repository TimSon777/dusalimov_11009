﻿using System;

namespace HW9_db
{
    public class Session
    {
        public bool IsHaveAccessRightsSecretPart { get; set; }
        public DateTime LastAction { get; set; }
        public static readonly TimeSpan SessionLifetime;

        static Session()
        {
            SessionLifetime = new TimeSpan(0, 0, 3);
        }

        public Session(bool isHaveAccessRightsSecretPart, DateTime lastAction)
        {
            IsHaveAccessRightsSecretPart = isHaveAccessRightsSecretPart;
            LastAction = lastAction;
        }
    }
}