﻿function validateStudentAndReturnResult() {
    
    let input_first_name = $('#input_first_name')
    let input_second_name = $('#input_second_name')
    let input_patronymic = $('#input_patronymic')
    let input_group_number = $('#input_group_number')
    let input_date_birth = $('#input_date_birth')

    let isCorrectStudent = true
    
    if (!isMatch(regex_name, input_first_name.val())) {
        input_first_name.val('Введите имя')
        isCorrectStudent = false
    }
    
    if (!isMatch(regex_name, input_second_name.val())) {
        input_second_name.val('Введите фамилию')
        isCorrectStudent = false
    }

    if (!isMatch(regex_patronymic, input_patronymic.val())) {
        input_patronymic.val('Введите отчество или сотавьте пустым, если его нет')
        isCorrectStudent = false
    }

    if (!isMatch(regex_group_number, input_group_number.val())) {
        input_group_number.val('Введите номер группы в формате: **-***')
        isCorrectStudent = false
    }

    if (!isMatch(regex_date_birth, input_date_birth.val())) {
        input_date_birth.val('Введите корректную дату рождения в формате: dd.mm.yyyy')
        isCorrectStudent = false
    }

    return isCorrectStudent
}