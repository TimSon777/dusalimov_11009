﻿function representClassmatesTable(countInPage, countvivod) {
    $.ajax({
        url: `getting_table_${countvivod}_${countInPage}`,
        success: function (data) {
            let students = JSON.parse(data)
            $.each(students, function (i, student) {
                let result = createRowTable(
                    [
                        student['Second Name'],
                        student['First Name'],
                        student['Patronymic'],
                        student['Date birth'],
                        student['Group number']
                    ])

                $('#classmates_table').append(result)
            })
        }
    })
}