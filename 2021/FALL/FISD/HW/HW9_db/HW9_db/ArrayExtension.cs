﻿using System;
using System.Text;

namespace HW9_db
{
    public static class ArrayExtension
    {
        private static readonly Random Rnd = new();

        public static string GetRandomWord(this char[] symbols, int length)
        {
            var strBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var rndIndex = Rnd.Next(0, symbols.Length - 1);
                strBuilder.Append(symbols[rndIndex]);
            }

            return strBuilder.ToString();
        }
    }
}