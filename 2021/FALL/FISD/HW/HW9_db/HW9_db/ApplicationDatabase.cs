﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using Npgsql;

namespace HW9_db
{
    public static class ApplicationDatabase
    {
        private const string ConnectionString = 
            "Host=localhost;Username=postgres;Password=postgres;Database=Point_Rating_System";
        
        public static IEnumerable<Student> GetStudentsFromDatabase(int propusk, int countvivod)
        {
            var st = new List<Student>();
            using var connection = new NpgsqlConnection(ConnectionString);
            connection.Open();
            
            var cmd = new NpgsqlCommand($"select first_name, second_name, COALESCE(patronymic, '') patronymic, birthday_date, group_number  from person natural join student natural join study_group OFFSET {propusk} LIMIT {countvivod}", connection);

            using var reader = cmd.ExecuteReader();
            
            while (reader.Read())
            {
                var firstName = reader.GetString(reader.GetOrdinal("first_name"));
                var secondName = reader.GetString(reader.GetOrdinal("second_name"));
                
                var patronymic = reader.GetString(reader.GetOrdinal("patronymic"));
                
                var birthday = reader.GetDateTime(reader.GetOrdinal("birthday_date"));
                var groupNumber = reader.GetString(reader.GetOrdinal("group_number"));
                
                st.Add(new Student(
                    firstName,
                    secondName,
                    patronymic,
                    birthday.ToString("dd.mm.yyyy"),
                    $"11-0{groupNumber}"
                ));
            }

            return st;
        }

        public static void AddStudentToDatabase(HttpListenerRequest request)
        {
            var inputStream = request.InputStream;
            var encoding = request.ContentEncoding;
            var streamReader = new StreamReader(inputStream, encoding);
            var studentInString = streamReader.ReadToEnd();
            var student = Student.ConvertStringToStudent(studentInString);
            
            using var connection = new NpgsqlConnection(ConnectionString);
            connection.Open();

            var maxPersonIdStr = $"SELECT MAX(person_id)  m FROM person;";
            using var maxPersonIdReader = new NpgsqlCommand(maxPersonIdStr, connection).ExecuteReader();
            maxPersonIdReader.Read();
            var maxPersonId = maxPersonIdReader.GetInt32(maxPersonIdReader.GetOrdinal("m"));
            maxPersonIdReader.Close();
            
            var groupIdStr = $"SELECT group_id FROM study_group WHERE group_number = '{student.GroupNumber[4..]}'";
            using var groupIdReader = new NpgsqlCommand(groupIdStr, connection).ExecuteReader();
            groupIdReader.Read();
            var groupId = groupIdReader.GetInt32(groupIdReader.GetOrdinal("group_id"));
            groupIdReader.Close();
            
            var updatePerson = $"INSERT INTO person (first_name, second_name, patronymic, birthday_date) " +
                               $"VALUES ('{student.FirstName}', '{student.SecondName}', '{student.Patronymic}', '{student.DateBirth}');";
            
            var cmd = new NpgsqlCommand(updatePerson, connection);
            cmd.ExecuteNonQuery();
            
            var updateStudent = $"INSERT INTO student (person_id, group_id)" +
                                $"VALUES ({maxPersonId + 1}, '{groupId}')";
            
            var cmd1 = new NpgsqlCommand(updateStudent, connection);
            cmd1.ExecuteNonQuery();
        }
    }
}