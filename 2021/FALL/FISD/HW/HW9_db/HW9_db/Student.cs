﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace HW9_db
{
    public class Student
    {
        [JsonPropertyName("First Name")]
        public string FirstName { get; set; }
        
        [JsonPropertyName("Second Name")]
        public string SecondName { get; set; }
        
        [JsonPropertyName("Patronymic")]
        public string Patronymic { get; set; }
        
        [JsonPropertyName("Date birth")]
        public string DateBirth { get; set; }
        
        [JsonPropertyName("Group number")]
        public string GroupNumber { get; set; }


        public Student(string firstName, 
            string secondName, 
            string patronymic, 
            string dateBirth, 
            string groupNumber)
        {
            FirstName = firstName;
            SecondName = secondName;
            Patronymic = patronymic;
            DateBirth = dateBirth;
            GroupNumber = groupNumber;
        }

        public static Dictionary<string, string> GetPairsStudentPropertyAndValue(string strParameters)
        {
            var parameters = strParameters
                .Replace("+", " ")
                .Split('&');
            
            return parameters
                .Select(param => param.Split('='))
                .ToDictionary(pair => pair[0], pair => pair[1]);
        }
        
        public static Student ConvertStringToStudent(string parameters)
        {
            var postParams = GetPairsStudentPropertyAndValue(parameters);

            return new Student(
                postParams["First Name"],
                postParams["Second Name"],
                postParams["Patronymic"],
                postParams["Date birth"],
                postParams["Group number"]
            );
        }
    }
}