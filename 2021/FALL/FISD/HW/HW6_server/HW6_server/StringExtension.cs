﻿using System;

namespace HW6_server
{
    public static class StringExtension
    {
        private static string RemoveOneElement(this string str, int position)
        {
            if (position < 0 || position >= str.Length)
                throw new ArgumentException($@"Invalid position: {position}");
            
            if (string.IsNullOrEmpty(str))
                throw new ArgumentException("Input string was null or empty");
            
            return str.Remove(position, 1);
        }
        
        public static string RemoveFirst(this string str)
        {
            return str.RemoveOneElement(0);
        }
    }
}