﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

namespace HW6_server
{
    public static class Program
    {
        public static void Main()
        {
            var regexImage = new Regex(@"images/\[\d\]");
            var directoryInfo = Directory.GetParent(Environment.CurrentDirectory)?.Parent;
            if (directoryInfo is null) throw new Exception("Error with finding the root of the project");
            var currentDirectory = directoryInfo.Parent;
            
            var listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();
            Console.WriteLine("Сервер начал прослушивание порта 8080");
            while (true)
            {
                var context = listener.GetContext();
                var request = context.Request;
                var response = context.Response;
                
                var fileName = request.Url.AbsolutePath.RemoveFirst();
                
                var directoryName = fileName
                    .Split(".", StringSplitOptions.RemoveEmptyEntries)
                    .First();
                
                if (regexImage.IsMatch(directoryName)) directoryName = "";
                
                List<byte> temp;
                try
                {
                    temp = File.ReadAllBytes($"{currentDirectory}\\website\\{directoryName}\\{fileName}").ToList();
                    
                }
                catch (Exception)
                {
                    temp = File.ReadAllBytes($"{currentDirectory}\\website\\index\\index.html").ToList();
                }

                var bytes = temp.ToArray();
                response.ContentLength64 = bytes.Length;
                var sw = response.OutputStream;
                sw.Write(bytes, 0, bytes.Length);
                sw.Close();
            }
            //listener.Stop();
            //Console.WriteLine("Сервер закончил прослушивание порта 8080");
        }
    }
}