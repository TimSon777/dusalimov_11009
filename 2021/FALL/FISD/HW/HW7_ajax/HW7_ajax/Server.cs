﻿using System.Net;

namespace HW7_ajax
{
    public static class Server
    {
        public static readonly string CurrentDirectory = DirectoryExtension.FindDirectoryNet5();

        public static void DistributeByApplication()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();
            
            while (true)
            {
                var ctx = listener.GetContext();
                var request = ctx.Request;
                var response = ctx.Response;

                var path = request.Url?.AbsolutePath;

                if (path == "/json_update.table_classmates") ApplicationJson.WriteNewStudentToJson(request, response);
                else ApplicationVisualization.SendResponseToWebsite(response, path);
            }
        }
    }
}