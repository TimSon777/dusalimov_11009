﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.Json;

namespace HW7_ajax
{
    public static class ApplicationJson
    {
        private static readonly string PathToClassmatesJson = $"{Server.CurrentDirectory}/website/json/table_classmates.json";
        private static readonly List<Student> Students;

        static ApplicationJson()
        {
            var json = File.ReadAllText(PathToClassmatesJson);
            
            try
            {
                Students = JsonSerializer.Deserialize<List<Student>>(json);
            }
            catch (Exception)
            {
                Students = new List<Student>();
            }
        }
        
        public static void WriteNewStudentToJson(HttpListenerRequest request, HttpListenerResponse response)
        {
            var inputStream = request.InputStream;
            var encoding = request.ContentEncoding;
            var streamReader = new StreamReader(inputStream, encoding);
            var studentInString = streamReader.ReadToEnd();

            var student = Student.ConvertStringToStudent(studentInString);
            Students.Add(student);
            
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            
            var jsonInString = JsonSerializer.Serialize(Students, options);
            File.WriteAllText(PathToClassmatesJson, jsonInString);
            SenderToWebsite.SendBytesToSite(200, Array.Empty<byte>(), response);
        }
    }
}