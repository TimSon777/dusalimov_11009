﻿using System;
using System.IO;
using System.Linq;
using System.Net;

namespace HW7_ajax
{
    public static class ApplicationVisualization
    {
        public static void SendResponseToWebsite(HttpListenerResponse response, string path)
        {
            if (path is null or "/") path = "index.html"; 
            
            var args  = path
                .Split(new[] {".", "/"}, StringSplitOptions.RemoveEmptyEntries)
                .ToArray();

            if (args.Length == 3 && args[0] == args[2])
                args = new[] { args[1], args[0] };
                
            if (args.Length != 2)
            {
                SenderToWebsite.SendBytesToSiteWithStatusCode404(response);
                return;
            }
                
            var fileName = $"{args[0]}.{args[1]}";
            var directoryName = args[1];
                
            var pathToFile = $"{Server.CurrentDirectory}\\website\\{directoryName}\\{fileName}";
            
            if (File.Exists(pathToFile))
            {
                var bytes = File.ReadAllBytes(pathToFile);
                SenderToWebsite.SendBytesToSite(200, bytes, response);
            }
            else
            {
                SenderToWebsite.SendBytesToSiteWithStatusCode404(response);
            }
        }
        
        
    }
}