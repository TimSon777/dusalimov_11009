﻿function leadToCorrectName(str) {
    return str[0].toUpperCase() + str.slice(1).toLowerCase()
}

function leadToCorrectNameAndWriteToInput(input) {
    let result = leadToCorrectName(input.val())
    input.val(result)
}

$(document).ready(function() {
    $('.links_content').click(function() 
    {
        let reference = $(this).attr('href')
        $("#content").load(reference,function ()
        {
            let filters = $('.filter_input')
            filters.on('input', filters, function() {
                filterTableAtSite($("#classmates_table"), filters, [1,2,3])
            });
            
            $('#classmates_table th').click(sortTableAtSite)
            
            $("#adding_student_form").on("submit", function() {
                if (validateStudentAndReturnResult()) {
                    let patronymic = $('#input_patronymic')
                    if (patronymic.val() !== null && patronymic.val() !== "")
                        leadToCorrectNameAndWriteToInput(patronymic)
                    
                    let firstName = $('#input_first_name')
                    leadToCorrectNameAndWriteToInput(firstName)

                    let secondName = $('#input_second_name')
                    leadToCorrectNameAndWriteToInput(secondName)
                    
                    let email = $('#input_email')
                    email.val(email.val().toLowerCase())
                    
                    let result = createRowTable(
                        [
                            $('#input_student_id_number').val(),
                            secondName.val(),
                            firstName.val(),
                            patronymic.val(),
                            $('#input_date_birth').val(),
                            $('#input_group_number').val(),
                            email.val().toLowerCase(),
                            $('#input_additional_information').val()
                        ])
                    
                    $.ajax({
                        url: 'json_update.table_classmates',
                        type: 'post',
                        dataType: 'html',
                        data: decodeURI($(this).serialize().replace("%40", "@")),
                        success: function () {
                            $('#classmates_table').append(result)}
                    });
                }
                
                return false;
            });
        })

        if (reference === "classmates.html") representClassmatesTable()
        
        return false;
    });
});



