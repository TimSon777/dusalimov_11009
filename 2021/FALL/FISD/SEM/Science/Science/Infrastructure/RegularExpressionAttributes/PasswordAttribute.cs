﻿using System.ComponentModel.DataAnnotations;

namespace Science.Infrastructure.RegularExpressionAttributes
{
    public class PasswordAttribute : RegularExpressionAttribute
    {
        public PasswordAttribute() : base( @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!?._]).{8,40}$")
        { }
        
        public override string FormatErrorMessage(string name)
            => "Пароль должен содержать минимум одну строчную, одну заглавную английскую букву, одну цифру, одни знак (!, ?, _ или .) и иметь длину в пределах от 8 до 40";
    }
}