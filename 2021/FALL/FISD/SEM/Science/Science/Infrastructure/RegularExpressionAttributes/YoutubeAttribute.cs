﻿using System.ComponentModel.DataAnnotations;

namespace Science.Infrastructure.RegularExpressionAttributes
{
    public class YoutubeAttribute : RegularExpressionAttribute
    {
        public YoutubeAttribute() : base(@"https://www\.youtube\.com/embed/[\w-]*")
        { }

        public override string FormatErrorMessage(string name)
            => "Формат ссылки: https://www.youtube.com/embed/...";
    }
}