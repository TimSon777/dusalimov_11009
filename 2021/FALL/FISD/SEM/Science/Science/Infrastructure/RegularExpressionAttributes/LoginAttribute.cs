﻿using System.ComponentModel.DataAnnotations;

namespace Science.Infrastructure.RegularExpressionAttributes
{
    public class LoginAttribute : RegularExpressionAttribute
    {
        public LoginAttribute() : base(@"^(\w|-)*$")
        { }

        public override string FormatErrorMessage(string name)
            => "Допустимо использовать только цифры, английские буквы и знаки _ или -";
    }
}