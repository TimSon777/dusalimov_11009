﻿using System.ComponentModel.DataAnnotations;

namespace Science.Infrastructure.RegularExpressionAttributes
{
    public class SpecificLanguageAttribute : RegularExpressionAttribute
    {
        public SpecificLanguageAttribute() : base("^([a-zA-Z]*|[а-яёА-ЯЁ]*)$") 
        { }

        public override string FormatErrorMessage(string name)
            => "Используйте только один язык";
    }
}