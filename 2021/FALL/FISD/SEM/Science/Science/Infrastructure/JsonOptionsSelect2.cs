﻿using System.Text.Json.Serialization;

namespace Science.Infrastructure
{
    public class JsonOptionsSelect2
    {
        public JsonOptionsSelect2(int id, string value)
        {
            Id = id;
            Value = value;
        }

        public JsonOptionsSelect2()
        { }

        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [JsonPropertyName("text")]
        public string Value { get; set; }
    }
}