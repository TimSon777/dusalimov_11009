﻿namespace Science.Infrastructure.HashedPassword
{
    public record HashedPasswordAndSalt(string HashedPassword, string Salt);
}