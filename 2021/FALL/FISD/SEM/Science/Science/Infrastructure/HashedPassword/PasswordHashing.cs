﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Science.Infrastructure.HashedPassword
{
    public class PasswordHashing : IPasswordHashing
    {
        private static void ThrowExceptionsOrQuit(string str, string name)
        {
            if (str is null) throw new ArgumentException($"The {name} cannot be null");
        }
        
        public HashedPasswordAndSalt Hash(string password, int length = 32)
        {
            ThrowExceptionsOrQuit(password, "str");
            if (length < 0) throw new ArgumentException("The length of salt must be greater than 0");

            var salt = GenerateSalt(length);
            var pepper = GetPepper();
            var hashedPassword = HashWithMd5(pepper + password + salt);

            return new HashedPasswordAndSalt(hashedPassword, salt);
        }

        public string Hash(string password, string salt)
        {
            ThrowExceptionsOrQuit(password, "password");
            ThrowExceptionsOrQuit(salt, "salt");
            
            var pepper = GetPepper();
            return HashWithMd5(pepper + password + salt);
        }

        private static string HashWithMd5(string str)
        {
            var bytes = Encoding.ASCII.GetBytes(str);
            using var md5 = MD5.Create();
            return string.Join("", md5.ComputeHash(bytes).Select(x => x.ToString("X2")));
        }

        private static string GenerateSalt(int length)
        {
            var array = new byte[length / 2];
            using var provider = new RNGCryptoServiceProvider();
            provider.GetBytes(array);
            return string.Join("", array.Select(x => x.ToString("X2")));
        }

        private static string GetPepper() => "SciencePepper777";
    }
}