﻿namespace Science.Infrastructure.HashedPassword
{
    public interface IPasswordHashing
    {
        HashedPasswordAndSalt Hash(string password, int length = 32);
        string Hash(string password, string salt);
    }
}