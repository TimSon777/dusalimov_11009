﻿using System;

namespace Science.Infrastructure.Search
{
    public static class EntityExtensions
    {
        public static string ToRussianLanguage(this Entity entity)
        {
            return entity switch
            {
                Entity.Article => "Статья",
                Entity.Scientist => "Ученый",
                Entity.Video => "Видео",
                _ => throw new ArgumentOutOfRangeException(nameof(entity), entity, null)
            };
        }
    }
}