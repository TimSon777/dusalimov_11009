﻿using System;

namespace Science.Infrastructure.Search
{
    public class SearchResult
    {
        public string Title { get; init; }
        public Entity Entity { get; init; }
        public DateTime Date { get; init; }
        public string Link { get; init; }

        public string FullLink => Entity switch
            {
                Entity.Article => $"Article/{Link}",
                Entity.Scientist => $"Scientist/{Link}",
                _ => Link
            };
    }
}