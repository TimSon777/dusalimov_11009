﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Science.Infrastructure.Search
{
    public static class SearchOrderByExtensions
    {
        public static string ToRussianLanguage(this SearchOrderBy search)
        {
            return search switch
            {
                SearchOrderBy.Date => "Дате",
                SearchOrderBy.Title => "Названию / ФИО",
                _ => throw new ArgumentOutOfRangeException(nameof(search), search, null)
            };
        }

        public static IEnumerable<SearchResult> OrderBy(
            this IEnumerable<SearchResult> searchResults,
            SearchOrderBy searchOrderBy)
        {
                return searchOrderBy switch
                {
                    SearchOrderBy.Date => searchResults.OrderBy(a => a.Date),
                    SearchOrderBy.Title => searchResults.OrderBy(a => a.Title),
                    _ => throw new ArgumentOutOfRangeException(nameof(searchOrderBy), searchOrderBy, null)
                };
        }
    }
}