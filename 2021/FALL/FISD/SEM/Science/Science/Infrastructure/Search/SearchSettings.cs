﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Science.Infrastructure.Search
{
    public class SearchSettings
    {
        [DisplayName("Название")]
        [MaxLength(100)]
        public string Title { get; set; }
        
        [DisplayName("Отсортировать по")]
        public SearchOrderBy? SearchOrderBy { get; set; }

        [DisplayName("От")]
        public DateTime? Start { get; set; }
        
        [DisplayName("До")]
        public DateTime? End { get; set; }

        [Required]
        [DisplayName("Сущности")]
        public IEnumerable<Entity> Entities { get; set; }
    }
}