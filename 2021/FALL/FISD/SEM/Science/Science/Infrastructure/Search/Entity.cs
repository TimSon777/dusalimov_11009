﻿namespace Science.Infrastructure.Search
{
    public enum Entity
    {
        Scientist,
        Video,
        Article
    }
}