﻿namespace Science.Infrastructure.Search
{
    public enum SearchOrderBy
    {
        Date,
        Title,
    }
}