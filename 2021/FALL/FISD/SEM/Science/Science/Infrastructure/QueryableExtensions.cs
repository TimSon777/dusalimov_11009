﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Science.Infrastructure
{
    public static class QueryableExtensions
    {
        public static async Task<int> CountSupportsNullAsync<TEntity>(
            this IQueryable<TEntity> queryable, 
            Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate == null) return await queryable.CountAsync();
            return await queryable.CountAsync(predicate);
        }
    }
}