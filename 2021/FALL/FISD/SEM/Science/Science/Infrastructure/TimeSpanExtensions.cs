﻿namespace Science.Infrastructure
{
    public static class TimeSpan
    {
        public static System.TimeSpan OneDay => new(1, 0, 0, 0);
    }
}