﻿using System.Threading.Tasks;

namespace Science.Infrastructure.Image
{
    public interface IImageHelper
    {
        public void CreateDirectory(params string[] paths);
        public Task InsertPictureAsync(SixLabors.ImageSharp.Image image, params string[] paths);
        public ImageStatus IsValid(SixLabors.ImageSharp.Image image, int maxSizeInMb);
    }
}