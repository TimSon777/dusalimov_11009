﻿namespace Science.Infrastructure.Image
{
    public enum ImageStatus
    {
        Valid, 
        WrongSize,
        NotImage,
        NotSquare
    }
}