﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using SixLabors.ImageSharp;

namespace Science.Infrastructure.Image
{
    public class ImageHelper : IImageHelper
    {
        private readonly IWebHostEnvironment _environment;

        public ImageHelper(IWebHostEnvironment environment)
            => _environment = environment;

        public void CreateDirectory(params string[] paths)
        {
            var path = Path.Combine(_environment.WebRootPath, Path.Combine(paths));
            Directory.CreateDirectory(path);
        }

        public async Task InsertPictureAsync(SixLabors.ImageSharp.Image image, params string[] paths)
        {
            var path = Path.Combine(_environment.WebRootPath, Path.Combine(paths));
            await image.SaveAsync(path);
        }

        public ImageStatus IsValid(SixLabors.ImageSharp.Image image, int maxSizeInMb)
            => !image.IsSquare()
                ? ImageStatus.NotSquare
                : !image.IsValidSizeInMb(maxSizeInMb)
                    ? ImageStatus.WrongSize
                    : ImageStatus.Valid;
    }
}