﻿using SixLabors.ImageSharp;

namespace Science.Infrastructure.Image
{
    public static class ImageExtensions
    {
        public static bool IsSquare(this IImageInfo image) => image.Height == image.Width;

        public static bool IsValidSizeInMb(this IImageInfo image, int maxSizeInMb)
        {
            var size = image.GetBits() / (double) 8 / 1024 / 1024;
            return size <= maxSizeInMb && size != 0;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public static long GetBits(this IImageInfo image)
            => image.Height * image.Width * image.PixelType.BitsPerPixel;
    }
}