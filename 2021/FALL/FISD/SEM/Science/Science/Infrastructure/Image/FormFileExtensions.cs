﻿using System;
using Microsoft.AspNetCore.Http;

namespace Science.Infrastructure.Image
{
    public static class FormFileExtensions
    {
        public static bool TryParseToImage(this IFormFile file, out SixLabors.ImageSharp.Image image)
        {
            try
            {
                image = SixLabors.ImageSharp.Image.Load(file.OpenReadStream());
                return true;
            }
            catch (Exception)
            {
                image = default;
                return false;
            }
        }
    }
}