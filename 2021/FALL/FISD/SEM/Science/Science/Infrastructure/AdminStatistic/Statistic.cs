﻿namespace Science.Infrastructure.AdminStatistic
{
    public class Statistic
    {
        public int Key { get; init; }
        public int Value { get; init; }
    }
}