﻿using System.Threading.Tasks;
using Science.Models;
using Science.Services.Database.AdminDb;

namespace Science.Infrastructure.AdminStatistic
{
    public class StatisticUsersStatus
    {
        public int Active { get; set; }
        public int Blocked { get; set; }
        public int Removed { get; set; }

        public static async Task<StatisticUsersStatus> CreateAsync(
            IAdminRepository adminRepository)
        {
            return new StatisticUsersStatus
            {
                Active = await adminRepository.GetCountAsync<User>(x => x.Status == Status.Active),
                Blocked = await adminRepository.GetCountAsync<User>(x => x.Status == Status.Blocked),
                Removed = await adminRepository.GetCountAsync<User>(x => x.Status == Status.Removed),
            };
        }
    }
}