﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Science.Services.Database.AdminDb;

namespace Science.Infrastructure.AdminStatistic
{
    public static class AdminStatistics
    {
        public static async Task<Statistic> Create<TEntity>(
            IAdminRepository adminRepository, 
            Expression<Func<TEntity, bool>> predicate)
            where TEntity : class
        {
            return new Statistic
            {
                Key = await adminRepository.GetCountAsync<TEntity>(null),
                Value = await adminRepository.GetCountAsync(predicate)
            };
        }
    }
}