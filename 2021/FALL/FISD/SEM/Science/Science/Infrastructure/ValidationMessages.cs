﻿using System;
using Science.Infrastructure.Image;

namespace Science.Infrastructure
{
    public static class ValidationMessages
    {
        public const string Required = "Это поле обязательно для заполнения!";
        public const string Access = "У вас недостаточно прав для добавления";
        public const string ErrorMessagesInvalidData = "Неверные логин или пароль";
        public const string SomeError = "Что-то пошло не так. Повторите попытку позже";
        public const string LoginAlreadyExists = "Логин уже существует";
        
        public static string GetErrorMessages(this ImageStatus status)
            => status switch
            {
                ImageStatus.NotImage => "Вы загрузили не картинку",
                ImageStatus.WrongSize => "Вы загрузили пустой файл или силшком большое изображение",
                ImageStatus.Valid => "Все отлично",
                ImageStatus.NotSquare => "Ваша картинка не квадратная",
                _ => throw new NotImplementedException()
            };
    }
}