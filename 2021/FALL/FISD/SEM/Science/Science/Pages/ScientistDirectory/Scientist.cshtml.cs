﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Science.Infrastructure;
using Science.Infrastructure.Image;
using Science.Models;
using Science.Services;
using Science.Services.Database.AreaScienceDb;
using Science.Services.Database.ScientificWorkDb;
using Science.Services.Database.ScientistDb;
using Science.ViewModels;
using JsonResult = Microsoft.AspNetCore.Mvc.JsonResult;

namespace Science.Pages.ScientistDirectory
{
    public class Scientist : CommonPage
    {
        private readonly IScientistRepository _scientistRepository;
        private readonly IScientificWorkRepository _scientificWorkRepository;
        private readonly IAreaScienceRepository _areaScienceRepository;
        private readonly IImageHelper _imageHelper;

        public List<SelectListItem> Options { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public int PageId { get; set; }

        [BindProperty]
        public ScientificWorkViewModel ScientificWorkVm { get; set; }

        [BindProperty] 
        [DisplayName("Научные труды")]
        public int[] ScientificWorksId { get; set; }

        public bool IsAccessToEdit { get; set; }
        
        public Scientist(IScientistRepository scientistRepository, 
            IScientificWorkRepository scientificWorkRepository, IAreaScienceRepository areaScienceRepository, IImageHelper imageHelper)
        {
            _scientistRepository = scientistRepository;
            _scientificWorkRepository = scientificWorkRepository;
            _areaScienceRepository = areaScienceRepository;
            _imageHelper = imageHelper;
        }

        public Models.Scientist ScientistModel { get; set; }

        private async Task UpdateScientistAsync()
        {
            await _scientistRepository.UpdateAndSaveAsync(ScientistVm, ScientistModel);
        }

        [BindProperty]
        public int DeleteFromOne { get; set; }
        
        [BindProperty]
        public int DeleteFromAll { get; set; }
        
        [BindProperty]
        public int Block { get; set; }

        public async Task<IActionResult> OnPostDeleteWorkFromOne()
        {
            await AssignAsync();
            if (DeleteFromOne <= 0) return BadRequest();
            var sw = ScientistModel.ScientificWorks.FirstOrDefault(a => a.Id == DeleteFromOne);
            if (sw == null) return BadRequest();
            await _scientificWorkRepository.DeleteFromOneAndSaveAsync(sw, ScientistModel);
            return Redirect($"~/Scientist/{ScientistModel.Id}");
        }
        
        public async Task<IActionResult> OnPostDeleteWorkFromAll()
        {
            await AssignAsync();
            if (DeleteFromAll <= 0) return BadRequest();
            var sw = ScientistModel.ScientificWorks.FirstOrDefault(a => a.Id == DeleteFromAll);
            if (sw == null) return BadRequest();
            if (sw.Status != Status.Active) return BadRequest();
            await _scientificWorkRepository.DeleteFromAllAndSaveAsync(sw);
            return Redirect($"~/Scientist/{ScientistModel.Id}");
        }

        public async Task<IActionResult> OnPostWorkBlock()
        {
            await AssignAsync();
            if (Block <= 0) return BadRequest();
            var sw = ScientistModel.ScientificWorks.FirstOrDefault(a => a.Id == Block);
            if (sw == null) return BadRequest();
            if (sw.Status != Status.Active) return BadRequest();
            await _scientificWorkRepository.BlockAndSaveAsync(sw);
            return Redirect($"~/Scientist/{ScientistModel.Id}");
        }

        public async Task<IActionResult> OnPostRestore()
        {
            await AssignAsync();
            if (ScientistModel.Status != Status.Removed) return BadRequest();
            await _scientistRepository.MakeActiveAndSaveAsync(ScientistModel);
            return Redirect($"/Scientist/{ScientistModel.Id}");
        }
        
        public async Task<IActionResult> OnPostBlock()
        {
            await AssignAsync();
            if (ScientistModel is null || ScientistModel.Status != Status.Active) return RedirectToPage("~/Scientists");
            ScientistModel.Status = Status.Blocked;
            await _scientistRepository.UpdateAndSaveAsync(ScientistModel);
            return Redirect("~/Scientists");
        }
        
        [BindProperty]
        public ScientistViewModel ScientistVm { get; set; }

        public async Task<IActionResult> OnPostDelete()
        {
            await AssignAsync();
            AssignAccessToEdit();
            await _scientistRepository.DeleteAndSaveAsync(ScientistModel);
            return Redirect("~/Scientists");
        }

        public async Task OnPostEdit()
        {
            await AssignAsync();
            AssignAccessToEdit();
            
            if (ModelState.ErrorCount is 2 or 3)
            {
                foreach (var value in ModelState.Values)
                {
                    value.Errors.Clear();
                }

                if (ScientistVm.Photo is null)
                {
                    await UpdateScientistAsync();
                    return;
                }

                if (!ScientistVm.Photo.TryParseToImage(out var image))
                {
                    ModelState.AddModelError("ScientistVm.Photo", ImageStatus.NotImage.GetErrorMessages());
                    return;
                }

                var status = _imageHelper.IsValid(image, 10);
                if (status != ImageStatus.Valid)
                {
                    ModelState.AddModelError("ScientistVm.Photo", status.GetErrorMessages());
                    return;
                }

                var t1 = _imageHelper.InsertPictureAsync(image, "images", "scientist", ScientistModel.Id.ToString(),
                    ScientistVm.Photo.FileName);
                var t2 = UpdateScientistAsync();
                
                await Task.WhenAll(t1, t2);
            }
        }

        private async Task AssignAsync()
        {
            ScientistModel = await _scientistRepository.FindById(PageId);
        }

        private void AssignAccessToEdit()
        {
            IsAccessToEdit = ScientistModel.Publisher == CurrentUser;
        }
        
        public async Task<IActionResult> OnGet()
        {
            await AssignAsync();
            if (ScientistModel is null || ScientistModel.Status != Status.Active) return NotFound();
            AssignAccessToEdit();
            Options = _areaScienceRepository.GetAreaSciencesSelectListItem().ToList();
            foreach (var option in Options
                .Where(option => ScientistModel.AreaSciences
                    .Select(a => a.Id)
                    .Contains(int.Parse(option.Value))))
            {
                option.Selected = true;
            }

            ScientistVm = new ScientistViewModel
            {
                FirstName = ScientistModel.FirstName,
                SecondName = ScientistModel.SecondName,
                Patronymic = ScientistModel.Patronymic,
                Biography = ScientistModel.Biography,
                Birth = ScientistModel.Birth,
                Death = ScientistModel.Death
            };
            
            return Page();
        }

        public async Task<IActionResult> OnPostAddScientificWork()
        {
            ScientistModel = await _scientistRepository.FindById(PageId);
            await _scientificWorkRepository.AddAndSaveAsync(ScientificWorkVm, CurrentUser, ScientistModel);
            return Redirect($"~/Scientist/{ScientistModel.Id}");
        }

        public async Task<IActionResult> OnGetScientificWorks(string term)
        {
            ScientistModel = await _scientistRepository.FindById(PageId);
            term ??= "";
            
            var scientificWorks = _scientificWorkRepository
                .GetScientificWorks(term, 20, ScientistModel)
                .Select(sw => new JsonOptionsSelect2(sw.Id, sw.Title)).ToList();
            
            var result = new JsonResult(scientificWorks)
            {
                ContentType = "json",
                StatusCode = 200
            };
            
            return new JsonResult(result);
        }

        public async Task OnPostUpdatingScientificWork()
        {
            ScientistModel = await _scientistRepository.FindById(PageId);
            await _scientificWorkRepository.UpdateAndSaveAsync(ScientificWorksId, ScientistModel);
        }
    }
}