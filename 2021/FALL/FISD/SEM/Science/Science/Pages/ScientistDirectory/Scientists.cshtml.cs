﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Science.Infrastructure;
using Science.Infrastructure.Image;
using Science.Models;
using Science.Services;
using Science.Services.Database.AreaScienceDb;
using Science.Services.Database.ScientificWorkDb;
using Science.Services.Database.ScientistDb;
using Science.ViewModels;

namespace Science.Pages.ScientistDirectory
{
    public class Scientists : CommonPage
    {
        private readonly IScientistRepository _scientistRepository;
        private readonly IAreaScienceRepository _areaScienceRepository;
        private readonly IScientificWorkRepository _scientificWorkRepository;
        private readonly IImageHelper _imageHelper;

        public List<SelectListItem> Options { get; set; }

        public Scientists(
            IScientistRepository scientistRepository,
            IAreaScienceRepository areaScienceRepository,
            IImageHelper imageHelper, 
            IScientificWorkRepository scientificWorkRepository)
        {
            _areaScienceRepository = areaScienceRepository;
            _imageHelper = imageHelper;
            _scientificWorkRepository = scientificWorkRepository;
            _scientistRepository = scientistRepository;
        }

        public IEnumerable<Models.Scientist> ScientistsModel { get; set; }

        [BindProperty(SupportsGet = true)] 
        public int CurrentPage { get; set; } = 1;

        [BindProperty] 
        public ScientistViewModel ScientistVm { get; set; }
        
        public Pagination Pagination { get; set; }

        private async Task AssignAsync()
        {
            var count = await _scientistRepository.GetCountRecords();
            Pagination = new Pagination(CurrentPage, count, nameof(Scientists), 4);
            ScientistsModel = await _scientistRepository.FindResultPagination(Pagination.CurrentPage, Pagination.PageSize);
            Options = _areaScienceRepository.GetAreaSciencesSelectListItem().ToList();
        }

        public async Task<IActionResult> OnGetAsync()
        {
            if (CurrentPage <= 0) return NotFound();
            await AssignAsync();
            return Page();
        }
        
        [BindProperty(SupportsGet = true)]
        public int Restore { get; set; }
        
        public async Task<IActionResult> OnPostWorkRestore()
        {
            await AssignAsync();
            if (Restore <= 0) return BadRequest();
            var sw = await _scientificWorkRepository.FindById(Restore);
            if (sw == null) return BadRequest();
            if (sw.Status != Status.Removed) return BadRequest();
            await _scientificWorkRepository.RestoreAndSaveAsync(sw);
            return Redirect("~/Scientists");
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await AssignAsync();

            if (!ModelState.IsValid) return Page();
            
            var isImage = ScientistVm.Photo.TryParseToImage(out var image);
            if (!isImage)
            {
                ModelState.AddModelError("ScientistVm.Photo", "Ожидалась картинка");
                return Page();
            }

            var status = _imageHelper.IsValid(image, 10);
            if (status != ImageStatus.Valid)
            {
                ModelState.AddModelError("ScientistVm.Photo", status.GetErrorMessages());
                return Page();
            }
                
            await _scientistRepository.AddAndSave(ScientistVm, CurrentUser);
            var scientist = await _scientistRepository.FindByFullNameAndBirthAsync(ScientistVm.FirstName,
                ScientistVm.SecondName, ScientistVm.Patronymic, ScientistVm.Birth);
                
            _imageHelper.CreateDirectory("images", "scientist", scientist.Id.ToString());

            await _imageHelper.InsertPictureAsync(image, "images", "scientist", scientist.Id.ToString(),
                ScientistVm.Photo.FileName);

            return Redirect($"~/Scientist/{scientist.Id}");
        }
    }
}