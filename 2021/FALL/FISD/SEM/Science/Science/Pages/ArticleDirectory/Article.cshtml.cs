﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Science.Infrastructure;
using Science.Infrastructure.Image;
using Science.Models;
using Science.Services;
using Science.Services.Database.ArticleDb;
using Science.Services.Database.ArticleReviewDb;
using Science.ViewModels;

namespace Science.Pages.ArticleDirectory
{
    public class Article : CommonPage
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IArticleReviewRepository _articleReviewRepository;
        private readonly IImageHelper _imageHelper;

        public Article(IArticleRepository articleRepository, IArticleReviewRepository articleReviewRepository, IImageHelper imageHelper)
        {
            _articleRepository = articleRepository;
            _articleReviewRepository = articleReviewRepository;
            _imageHelper = imageHelper;
        }

        [BindProperty]
        public ArticleViewModel ArticleVm { get; set; }

        public Models.Article Art { get; set; }

        [BindProperty(SupportsGet = true)] 
        public int PageId { get; set; }

        private async Task SetAsync()
        {
            Art = await _articleRepository.FindByIdAsync(PageId);
        }
        
        private void AssignAccessToEdit()
        {
            IsAccessToEdit = Art.Publisher == CurrentUser;
        }

        public bool IsAccessToEdit { get; set; }
        
        private async Task UpdateAsync()
        {
            Art.Title = ArticleVm.Title;
            Art.Description = ArticleVm.Description;
            Art.Text = ArticleVm.Text;
            if (ArticleVm.Image is not null)
                Art.ImageName = ArticleVm.Image.FileName;
            
            await _articleRepository.UpdateAndSaveAsync(Art);
        }

        public async Task OnPostEdit()
        {
            await SetAsync();
            AssignAccessToEdit();
            
            if (ModelState.IsValid)
            {
                if (ArticleVm.Image is null)
                {
                    await UpdateAsync();
                    return;
                }

                if (!ArticleVm.Image.TryParseToImage(out var image))
                {
                    ModelState.AddModelError("ArticleVm.Image", ImageStatus.NotImage.GetErrorMessages());
                    return;
                }

                var status = _imageHelper.IsValid(image, 10);
                if (status != ImageStatus.Valid)
                {
                    ModelState.AddModelError("ArticleVm.Image", status.GetErrorMessages());
                    return;
                }

                var t1 = UpdateAsync();
                var t2 = _imageHelper.InsertPictureAsync(image, "images", "article", Art.Id.ToString(),
                    ArticleVm.Image.FileName);

                await Task.WhenAll(t1, t2);
            }
        } 
        
        public async Task<IActionResult> OnPostBlock()
        {
            await SetAsync();
            if (Art is null || Art.Status != Status.Active) return BadRequest();
            await _articleRepository.BlockAndSaveAsync(Art);
            return Redirect("~/Articles");
        }
        
        public async Task<IActionResult> OnPostDelete()
        {
            await SetAsync();
            if (Art is null || Art.Status != Status.Active) return BadRequest();
            await _articleRepository.DeleteAndSaveAsync(Art);
            return Redirect("~/Articles");
        }
        
        public async Task<IActionResult> OnPostRestore()
        {
            await SetAsync();
            if (Art is null || Art.Status != Status.Removed) return BadRequest();
            await _articleRepository.MakeActiveAndSaveAsync(Art);
            return Redirect($"~/Article/{Art.Id}");
        }
        
        public async Task<IActionResult> OnPostCommentAsync(string comment)
        {
            await SetAsync();

            if (comment is null
                || comment.Length > 300
                || comment.Contains("<script>"))
            {
                return new BadRequestResult();
            }

            var resultComment = new CommentViewModel(comment);
            var commentModel = await _articleReviewRepository.AddAndSaveAsync(Art, CurrentUser, resultComment);
            
            return ViewComponent("Comment", new { comment = commentModel });
        }

        public async Task<IActionResult> OnGet()
        {
            await SetAsync();
            if (Art is null || Art.Status != Status.Active) return NotFound();
            AssignAccessToEdit();
            if (IsAccessToEdit)
            {
                ArticleVm = new ArticleViewModel(Art);
            }
            return Page();
        }

        public async Task<IActionResult> OnGetShowMoreAsync(int skip, int take)
        {
            await SetAsync();
            return ViewComponent("Comment", new {skip, take, article = Art});
        }

        public async Task<IActionResult> OnPostBlockComment(int blockedCommentId)
        {
            await SetAsync();
            AssignAccessToEdit();
            var comment = await _articleReviewRepository.FindById(blockedCommentId);
            if (comment is null || !IsAdmin && CurrentUser.Role != Role.Moderator) return BadRequest();
            await _articleReviewRepository.BlockAndSaveAsync(comment);
            return Page();
        }
    }
}