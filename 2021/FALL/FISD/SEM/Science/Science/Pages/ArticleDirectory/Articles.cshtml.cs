﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Science.Infrastructure;
using Science.Infrastructure.Image;
using Science.Models;
using Science.Services;
using Science.Services.Database.ArticleDb;
using Science.ViewModels;
using SixLabors.ImageSharp;

namespace Science.Pages.ArticleDirectory
{
    public class Articles : CommonPage
    {
        [BindProperty(SupportsGet = true)] 
        public int CurrentPage { get; set; } = 1;

        public Pagination Pagination { get; set; }

        [BindProperty]
        public ArticleViewModel ArticleVm { get; set; }

        private readonly IArticleRepository _articleRepository;
        private readonly IImageHelper _imageHelper;
        public IEnumerable<Models.Article> ArticlesModel { get; set; }

        public Articles(IArticleRepository articleRepository, IImageHelper imageHelper)
        {
            _articleRepository = articleRepository;
            _imageHelper = imageHelper;
        }

        private async Task AssignAsync()
        {
            var countRecords = await _articleRepository.GetCountRecords();
            Pagination = new Pagination(CurrentPage, countRecords, nameof(Articles), 5);
            ArticlesModel = await _articleRepository.FindResultPagination(Pagination.CurrentPage, Pagination.PageSize);
        }
        
        public async Task<IActionResult> OnGet()
        {
            if (CurrentPage <= 0) return NotFound();
            await AssignAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostAddingAsync()
        {
            await AssignAsync();
            if (!ModelState.IsValid) return Page();

            Image image = null;
            
            if (ArticleVm.Image is not null)
            {
                var isImage = ArticleVm.Image.TryParseToImage(out image);
            
                if (!isImage)
                {
                    ModelState.AddModelError("ArticleVm.Image", ImageStatus.NotImage.GetErrorMessages());
                    return Page();   
                }
            
                var imageStatus = _imageHelper.IsValid(image, 10);
                if (imageStatus != ImageStatus.Valid)
                {
                    ModelState.AddModelError("ArticleVm.Image", imageStatus.GetErrorMessages());
                    return Page();   
                }
            }
            
            if (CurrentUser is null
                || CurrentUser.Role == Role.CommonUser
                || CurrentUser.Status != Status.Active)
            {
                ModelState.AddModelError("", "У вас нет прав");
                return Page();   
            }

            var article = await _articleRepository.AddAndSaveAsync(ArticleVm, CurrentUser);
            var pathToDirectory = Path.Combine("images", "article", article.Id.ToString());
            _imageHelper.CreateDirectory(pathToDirectory);

            if (ArticleVm.Image is not null)
            {
                await _imageHelper.InsertPictureAsync(image, pathToDirectory, ArticleVm.Image.FileName);
                return Redirect($"/Article/{article.Id}");
            }
            
            return Redirect($"/Article/{article.Id}");
        }
    }
}