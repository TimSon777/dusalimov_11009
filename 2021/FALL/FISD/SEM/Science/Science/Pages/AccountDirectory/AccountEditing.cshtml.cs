﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Science.Infrastructure;
using Science.Infrastructure.Image;
using Science.Models;
using Science.Services;
using Science.Services.Database.UserDb;
using Science.ViewModels;

namespace Science.Pages.AccountDirectory
{
    public class AccountEditing : CommonPage
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IUserRepository _userRepository;
        private readonly IImageHelper _imageHelper;

        public AccountEditing(IWebHostEnvironment environment, 
            IUserRepository userRepository, 
            IImageHelper imageHelper)
        {
            _environment = environment;
            _userRepository = userRepository;
            _imageHelper = imageHelper;
        }
        
        [BindProperty]
        public AccountEditingViewModel AccountEditingVm { get; set; }
        
        public void OnGet()
            => AccountEditingVm = new AccountEditingViewModel(CurrentUser);
        
        public IActionResult OnPostBack() 
            => Redirect($"/AccountDirectory/Account/{CurrentUser.Id}");
        
        public async Task OnPostAsync()
        {
            if (!ModelState.IsValid) return;

            if (AccountEditingVm is null)
            {
                ModelState.AddModelError("", ValidationMessages.SomeError);
                return;
            }

            var extension = "";
            var uploadImage = AccountEditingVm?.ProfilePicture?.FileName is not null;
            if (uploadImage)
            {
                var isImage = AccountEditingVm.ProfilePicture.TryParseToImage(out var image);
                if (!isImage)
                {
                    ModelState.AddModelError("AccountEditingVm.ProfilePicture", ImageStatus.NotImage.GetErrorMessages());
                    return;
                }

                var status = _imageHelper.IsValid(image, 10);
                if (status != ImageStatus.Valid)
                {
                    ModelState.AddModelError("AccountEditingVm.ProfilePicture", status.GetErrorMessages());
                    return;
                }
                
                extension = AccountEditingVm.ProfilePicture.FileName.Split(".").Last();
                
                var path = Path.Combine(_environment.WebRootPath, "images", "profile", CurrentUser.Id.ToString());
                System.IO.File.Delete(Path.Combine(path, $"profile.{CurrentUser.ProfilePictureExtension}"));

                await _imageHelper.InsertPictureAsync(image, Path.Combine(path, $"profile.{extension}"));
            }

            var userUp = new User(AccountEditingVm, uploadImage ? extension : null);
            await _userRepository.UpdateUserAsync(userUp, CurrentUser.Login);
        }
    }
}