﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Science.Models;
using Science.Services;
using Science.Services.Database.SessionDb;
using Science.Services.Database.UserDb;

namespace Science.Pages.AccountDirectory
{
    public class Account : CommonPage
    {
        private readonly ISessionRepository _sessionRepository;
        private readonly IUserRepository _userRepository;

        public Account(
            ISessionRepository sessionRepository, 
            IUserRepository userRepository)
        {
            _sessionRepository = sessionRepository;
            _userRepository = userRepository;
        }

        [BindProperty(SupportsGet = true)] 
        public int UserId { get; set; }
        
        [BindProperty] 
        public Status Status { get; set; }
        
        [BindProperty]
        public Role Role { get; set; }
        
        public User AccountUser { get; set; }
        public bool IsHostAccount { get; set; }

        public async Task<IActionResult> OnGet()
        {
            await AssignUser();
            if (AccountUser is null) return NotFound();
            Role = AccountUser.Role;
            Status = AccountUser.Status;
            IsHostAccount = AccountUser == CurrentUser;
            return Page();
        }
        
        public async Task OnPostStatus()
        {
            await AssignUser();
            if (Status == 0) return;
            await _userRepository.ChangeStatusAsync(AccountUser, Status);
        }

        public async Task OnPostRole()
        {
            await AssignUser();
            if (Role == 0) return;
            await _userRepository.ChangeRoleAsync(AccountUser, Role);
        }

        public async Task<IActionResult> OnPostDeleteAccount()
        {
            await AssignUser();
            var t1 = _userRepository.ChangeStatusAsync(AccountUser, Status.Removed);
            var t2 = DeleteInformationAuthorization();
            await Task.WhenAll(t1, t2);
            return Redirect("~/Index");
        }
        
        public async Task<IActionResult> OnGetExitAsync()
        {
            await DeleteInformationAuthorization();
            return Redirect("~/Index");
        }
        
        private async Task DeleteInformationAuthorization()
        {
            var token = Request.Cookies["token"];
            if (token is not null) await _sessionRepository.DeleteSessionByTokenAsync(token);
            
            Response.Cookies.Delete("token");
            HttpContext.Session.Clear();
        }
        
        private async Task AssignUser() 
            => AccountUser = await _userRepository.FindById(UserId);
    }
}