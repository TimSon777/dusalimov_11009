﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Science.Infrastructure;
using Science.Infrastructure.Image;
using Science.Models;
using Science.Services;
using Science.Services.Database.AreaScienceDb;
using Science.ViewModels;

namespace Science.Pages.AreaDirectory
{
    public class Areas : CommonPage
    {
        private readonly IAreaScienceRepository _areaScienceRepository;
        private readonly IImageHelper _imageHelper;

        [BindProperty] public AreaScienceViewModel AreaScienceVm { get; init; }
        
        public Areas(IAreaScienceRepository areaScienceRepository, IWebHostEnvironment environment,
            IImageHelper imageHelper)
        {
            _areaScienceRepository = areaScienceRepository;
            _imageHelper = imageHelper;
        }
        
        public void OnGet() { }
        public IActionResult OnGetMore(int skip, int take) => ViewComponent("Area", new {skip, take});
        
        public async Task<IActionResult> OnPost()
        {
            if (CurrentUser is null 
                || CurrentUser.Role == Role.CommonUser 
                || CurrentUser.Status != Status.Active)
                ModelState.AddModelError("", ValidationMessages.Access);

            if (!ModelState.IsValid) return Page();
            
            var isImage = AreaScienceVm.Image.TryParseToImage(out var image);
            if (!isImage)
            {
                ModelState.AddModelError("AreaScienceVm.Image", ImageStatus.NotImage.GetErrorMessages());
                return Page();
            }

            var status = _imageHelper.IsValid(image, 10);
            if (status != ImageStatus.Valid)
            {
                ModelState.AddModelError("AreaScienceVm.Image", status.GetErrorMessages());
                return Page();
            }
            
            var area = new AreaScience
            {
                Title = AreaScienceVm.Title,
                Description = AreaScienceVm.Description,
                Publisher = CurrentUser,
                ImageName = AreaScienceVm.Image.FileName,
                Status = Status.Active,
                Text = AreaScienceVm.Text
            };

            await _areaScienceRepository.AddAndSaveAsync(area);
            area = await _areaScienceRepository.FindByTitle(area.Title);
            
            _imageHelper.CreateDirectory("images", "area", area.Id.ToString());
            await _imageHelper.InsertPictureAsync(image, 
                "images", "area", area.Id.ToString(), AreaScienceVm.Image.FileName);
            return Redirect("~/Areas");
        }
    }
}