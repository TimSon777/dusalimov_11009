﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Science.Infrastructure;
using Science.Infrastructure.Image;
using Science.Models;
using Science.Services;
using Science.Services.Database.AreaScienceDb;
using Science.ViewModels;

namespace Science.Pages.AreaDirectory
{
    public class Area : CommonPage
    {
        [BindProperty(SupportsGet = true)]
        public int PageId { get; set; }

        [BindProperty]
        public AreaScienceViewModel AreaVm { get; set; }

        public AreaScience AreaScience { get; private set; }

        private readonly IAreaScienceRepository _areaScienceRepository;
        private readonly IImageHelper _imageHelper;

        public Area(IAreaScienceRepository areaScienceRepository, IImageHelper imageHelper)
        {
            _areaScienceRepository = areaScienceRepository;
            _imageHelper = imageHelper;
        }

        public bool IsAccessToEdit { get; set; }

        private async Task AssignAsync()
        {
            AreaScience = await _areaScienceRepository.FindByIdAsync(PageId);
        }

        private void AssignAccess()
        {
            IsAccessToEdit = AreaScience.Publisher == CurrentUser;
        }

        public async Task<IActionResult> OnPostRestore()
        {
            await AssignAsync();
            if (AreaScience is null || AreaScience.Status != Status.Removed) return Redirect("~/Areas");
            AreaScience.Status = Status.Active;
            await _areaScienceRepository.UpdateAndSaveAsync(AreaScience);
            return Redirect($"~/Area/{AreaScience.Id}");
        }

        public async Task<IActionResult> OnPostBlock()
        {
            await AssignAsync();
            if (AreaScience is null || AreaScience.Status != Status.Active) return Redirect("~/Areas");
            AreaScience.Status = Status.Blocked;
            await _areaScienceRepository.UpdateAndSaveAsync(AreaScience);
            return Redirect("~/Areas");
        }

        public async Task<IActionResult> OnGet()
        {
            await AssignAsync();
            if (AreaScience is null || AreaScience.Status != Status.Active) return NotFound();
            AssignAccess();

            if (IsAccessToEdit)
            {
                AreaVm = new AreaScienceViewModel
                {
                    Title = AreaScience.Title,
                    Description = AreaScience.Description,
                    Text = AreaScience.Text
                };
            }
            
            return Page();
        }

        private async Task UpdateAsync()
        {
            await _areaScienceRepository.UpdateAndSaveAsync(AreaVm, AreaScience.Id);
        }

        public async Task<IActionResult> OnPostDelete()
        {
            await AssignAsync();
            AssignAccess();
            AreaScience.Status = Status.Removed;
            await _areaScienceRepository.UpdateAndSaveAsync(AreaScience);
            return Redirect("~/Areas");
        }
        
        public async Task OnPostEdit()
        {
            await AssignAsync();
            AssignAccess();
            
            var countErrors = ModelState
                .Where(aa => aa.Key != "AreaVm.Image")
                .Sum(keyValuePair => keyValuePair.Value.Errors.Count);

            if (countErrors == 0)
            {
                if (AreaVm.Image is null)
                {
                    await UpdateAsync();
                    return;
                }

                if (AreaVm.Image.TryParseToImage(out var image))
                {
                    var status = _imageHelper.IsValid(image, 10);
                    if (status != ImageStatus.Valid)
                    {
                        ModelState.AddModelError("AreaVm.Image", status.GetErrorMessages());
                        return;
                    }

                    await UpdateAsync();
                    await _imageHelper.InsertPictureAsync(image, "images", "area", 
                        AreaScience.Id.ToString(), AreaVm.Image.FileName);
                    
                    return;
                }
                
                ModelState.AddModelError("AreaVm.Image", ImageStatus.NotImage.GetErrorMessages());
            }
        }
    }
}