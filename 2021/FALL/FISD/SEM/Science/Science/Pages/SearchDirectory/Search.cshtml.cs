﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Science.Infrastructure.Search;
using Science.Models;
using Science.Services.Database.SearchDb;

namespace Science.Pages.SearchDirectory
{
    public class Search : PageModel
    {
        public List<SelectListItem> SearchOrderByOptions => new()
        {
            new SelectListItem
            {
                Value = ((int) SearchOrderBy.Title).ToString(),
                Text = SearchOrderBy.Title.ToRussianLanguage()
            },
            
            new SelectListItem
            {
                Value = ((int) SearchOrderBy.Date).ToString(),
                Text = SearchOrderBy.Date.ToRussianLanguage()
            },
        };
        public List<SelectListItem> EntitiesOptions => new()
        {
            new SelectListItem
            {
                Value = ((int) Entity.Article).ToString(),
                Text = Entity.Article.ToRussianLanguage()
            },
            
            new SelectListItem
            {
                Value = ((int) Entity.Video).ToString(),
                Text = Entity.Video.ToRussianLanguage()
            },
            
            new SelectListItem
            {
                Value = ((int) Entity.Scientist).ToString(),
                Text = Entity.Scientist.ToRussianLanguage()
            }
        };
        
        [BindProperty(SupportsGet = true)]
        public SearchSettings SearchSettings { get; set; }

        public List<SearchResult> SearchResults { get; set; }
        
        private readonly ISearchRepository _searchRepository;

        public Search(ISearchRepository searchRepository)
        {
            _searchRepository = searchRepository;
        }
        
        public void OnGet()
        { }

        public async Task<IActionResult> OnGetSearch()
        {
            if (!ModelState.IsValid) return Page();

            SearchResults = new List<SearchResult>();
            
            foreach (var entity in SearchSettings.Entities)
            {
                IEnumerable<SearchResult> collection;
                switch (entity)
                {
                    case Entity.Article:
                    {
                        Expression<Func<Article, bool>> predicate = art =>
                            art.Title.Contains(SearchSettings.Title ?? "")
                            && art.Publication > (SearchSettings.Start ?? DateTime.MinValue)
                            && art.Publication < (SearchSettings.End ?? DateTime.MaxValue)
                            && art.Status == Status.Active;
                        
                        var articles = await _searchRepository.GetCollection(predicate);
                        
                        collection = articles.Select(art => new SearchResult
                        {
                            Date = art.Publication,
                            Title = art.Title,
                            Entity = Entity.Article,
                            Link = art.Id.ToString()
                        });
                        SearchResults.AddRange(collection);
                        
                        break;
                    }
                    case Entity.Scientist:
                    {
                        Expression<Func<Scientist, bool>> predicate = sc =>
                            (sc.FirstName.Contains(SearchSettings.Title ?? "")
                             || sc.SecondName.Contains(SearchSettings.Title ?? ""))
                            && sc.Birth > (SearchSettings.Start ?? DateTime.MinValue)
                            && sc.Birth < (SearchSettings.End ?? DateTime.MaxValue)
                            && sc.Status == Status.Active;
                        
                        var scientists = await _searchRepository.GetCollection(predicate);
                        
                        collection = scientists.Select(sc => new SearchResult
                        {
                            Date = sc.Birth,
                            Title = sc.SecondName + " " + sc.FirstName,
                            Entity = Entity.Scientist,
                            Link = sc.Id.ToString()
                        });
                        SearchResults.AddRange(collection);
                        break;
                    }
                    case Entity.Video:
                    {
                        Expression<Func<Models.Video, bool>> predicate = v =>
                            v.Title.Contains(SearchSettings.Title ?? "")
                            && v.Publication > (SearchSettings.Start ?? DateTime.MinValue)
                            && v.Publication < (SearchSettings.End ?? DateTime.MaxValue)
                            && v.Status == Status.Active;
                        
                        var videos = await _searchRepository.GetCollection(predicate);
                        
                        collection = videos.Select(v => new SearchResult
                        {
                            Date = v.Publication,
                            Title = v.Title,
                            Entity = Entity.Video,
                            Link = v.Link
                        });
                        SearchResults.AddRange(collection);
                        break;
                    }

                    default: throw new ArgumentOutOfRangeException();
                }
            }
            
            SearchResults = SearchResults
                .OrderBy(SearchSettings.SearchOrderBy ?? SearchOrderBy.Title)
                .ToList();
            
            return Page();
        }
    }
}