﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Science.Infrastructure;
using Science.Models;
using Science.Services;
using Science.Services.Database.VideoDb;
using Science.ViewModels;

namespace Science.Pages
{
    public class ScientificVideo : CommonPage
    {
        private readonly IVideoRepository _videoRepository;
        
        public ScientificVideo(IVideoRepository videoRepository) 
            => _videoRepository = videoRepository;

        [BindProperty]
        public VideoViewModel VideoVm { get; init; }
        
        [BindProperty(SupportsGet = true)] 
        public int CurrentPage { get; set; } = 1;
        
        public Pagination Pagination { get; private set; }
        
        public IEnumerable<Models.Video> Videos { get; private set; }

        public async Task<IActionResult> OnGetAsync()
        {
            if (CurrentPage <= 0) return NotFound();
            await SetVideosAsync();
            if (Pagination.TotalPages == 0) return Page();

            if (CurrentPage > Pagination.TotalPages)
                return Redirect(Pagination.TotalPages.ToString());

            return Page();
        }

        [BindProperty]
        public int VideoDeletedId { get; set; }
        
        [BindProperty]
        public int VideoBlockedId { get; set; }
        
        [BindProperty]
        public int VideoRestoredId { get; set; }
        
        public async Task<IActionResult> OnPostBlock()
        {
            await SetVideosAsync();
            var video = Videos.FirstOrDefault(a => a.Id == VideoBlockedId);
            if (!IsAdmin || video is null || video.Status != Status.Active) return BadRequest();

            await _videoRepository.BlockAndSaveAsync(video);
            return Redirect("~/ScientificVideo");
        }
        
        public async Task<IActionResult> OnPostDelete()
        {
            await SetVideosAsync();
            var video = Videos.FirstOrDefault(a => a.Id == VideoDeletedId);
            if (video is null || video.Status != Status.Active || video.Publisher != CurrentUser) return BadRequest();
            await _videoRepository.DeleteAndSaveAsync(video);
            return Redirect("~/ScientificVideo");
        }
        
        public async Task<IActionResult> OnPostRestore()
        {
            await SetVideosAsync();
            var video = await _videoRepository.FindById(VideoRestoredId);
            
            if (video is null || video.Status != Status.Removed || video.Publisher != CurrentUser) return BadRequest();
            
            await _videoRepository.MakeActiveAndSaveAsync(video);
            return Redirect($"~/ScientificVideo");
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await SetVideosAsync();
            
            if (CurrentUser is null || CurrentUser.Role == Role.CommonUser)
            {
                ModelState.AddModelError("", ValidationMessages.Access);
                return Page();
            }
            
            if (ModelState.IsValid)
            {
                var video = new Models.Video
                {
                    Publisher = CurrentUser,
                    Publication = DateTime.Now,
                    Title = VideoVm.Title,
                    Description = VideoVm.Description,
                    Link = VideoVm.Link,
                    Status = Status.Active
                };
                
                await _videoRepository.AddVideoAsync(video);
            }

            return Redirect($"~/ScientificVideo/{CurrentPage}");
        }
        
        private async Task SetVideosAsync()
        {
            var countRecords = await _videoRepository.GetCountRecords();
            Pagination = new Pagination(CurrentPage, countRecords, nameof(ScientificVideo), 5);
            Videos = await _videoRepository.FindResultPagination(Pagination.CurrentPage, Pagination.PageSize);
        }
    }
}