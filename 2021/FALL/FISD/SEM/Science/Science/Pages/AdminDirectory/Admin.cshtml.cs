﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Science.Infrastructure.AdminStatistic;
using Science.Models;
using Science.Pages.AccountDirectory;
using Science.Services;
using Science.Services.Database.AdminDb;
using TimeSpan = Science.Infrastructure.TimeSpan;

namespace Science.Pages.AdminDirectory
{
    public class Admin : CommonPage
    {
        private readonly IAdminRepository _adminRepository;

        public Admin(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }

        public Statistic Registrations { get; private set; }
        public Statistic Articles { get; private set; }
        public Statistic Videos { get; private set; }

        public StatisticUsersStatus StatisticUsersStatus { get; private set; }
        
        public async Task<IActionResult> OnGet()
        {
            if (CurrentUser is null)
            {
                return Redirect(nameof(Authorization));
            }
            
            if (CurrentUser.Role != Role.Administrator)
            {
                return Redirect($"~/{nameof(Account)}/{CurrentUser.Id}");
            }

            Registrations = await AdminStatistics
                .Create<User>(_adminRepository, a => DateTime.Now - a.Registration < TimeSpan.OneDay);
            
            Articles = await AdminStatistics
                .Create<Article>(_adminRepository, a => DateTime.Now - a.Publication < TimeSpan.OneDay);
            
            Videos = await AdminStatistics
                .Create<Models.Video>(_adminRepository, a => DateTime.Now - a.Publication < TimeSpan.OneDay);

            StatisticUsersStatus = await StatisticUsersStatus.CreateAsync(_adminRepository);
            
            return Page();
        }

        public async Task<IActionResult> OnGetUsersShowMore(int skip, int take)
        {
            if (skip < 0 || take < 0) return BadRequest();
            var users = await _adminRepository.GetCollectionAsync<User>(skip, take);
            return Partial("Partials/AdminGettingUsers", users);
        }
    }
}