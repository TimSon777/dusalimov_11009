﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Science.Infrastructure;
using Science.Infrastructure.HashedPassword;
using Science.Infrastructure.Image;
using Science.Models;
using Science.Services;
using Science.Services.Database.UserDb;
using Science.ViewModels;

namespace Science.Pages
{
    public class Registration : CommonPage
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHashing _passwordHashing;
        private readonly IImageHelper _imageHelper;

        public Registration(IUserRepository userRepository, 
            IWebHostEnvironment environment,
            IPasswordHashing passwordHashing, IImageHelper imageHelper)
        {
            _userRepository = userRepository;
            _passwordHashing = passwordHashing;
            _imageHelper = imageHelper;
        }

        [BindProperty]
        public RegistrationViewModel RegistrationViewModel { get; set; }

        public IActionResult OnGet() 
            => HttpContext.Items.ContainsKey("user")
                ? Redirect($"AccountDirectory/Account/{CurrentUser.Id}")
                : Page();

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var isFreeLogin = await _userRepository.FindUserByLoginAsync(RegistrationViewModel.Login) == null;
            if (!isFreeLogin)
            {
                ModelState.AddModelError("RegistrationViewModel.Login", ValidationMessages.LoginAlreadyExists);
                return Page();
            }
            
            var (password, salt) = _passwordHashing.Hash(RegistrationViewModel.Password);
            var user = new User
            {
                FirstName = RegistrationViewModel.FirstName,
                SecondName = RegistrationViewModel.SecondName,
                HashedPassword = password,
                Salt = salt,
                Login = RegistrationViewModel.Login,
                Status = Status.Active,
                Role = Role.CommonUser,
                Registration = DateTime.Now
            };
                
            await _userRepository.AddUserAsync(user);
            var thisUser =  await _userRepository.FindUserByLoginAsync(RegistrationViewModel.Login);
            _imageHelper.CreateDirectory("images", "profile", thisUser.Id.ToString());

            return RedirectToPage("Authorization");
        }
    }
}