﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Science.Infrastructure;
using Science.Infrastructure.HashedPassword;
using Science.Models;
using Science.Services;
using Science.Services.Database.SessionDb;
using Science.Services.Database.UserDb;
using Science.ViewModels;

namespace Science.Pages
{
    public class Authorization : CommonPage
    {
        private readonly IPasswordHashing _passwordHashing;
        private readonly IUserRepository _userRepository;
        private readonly ISessionRepository _sessionRepository;

        public Authorization(IPasswordHashing passwordHashing, 
            IUserRepository userRepository, 
            ISessionRepository sessionRepository)
        {
            _passwordHashing = passwordHashing;
            _userRepository = userRepository;
            _sessionRepository = sessionRepository;
        }
        
        [BindProperty]
        public AuthorizationViewModel AuthorizationViewModel { get; set; }

        public async Task<IActionResult> OnGet()
        {
            var token = Request.Cookies["token"];
            if (token is not null)
            {
                var session = await _sessionRepository.FindSessionByTokenAsync(token);
                if (session is not null && session.User != null && session.User.Status == Status.Active)
                {
                    return Redirect($"AccountDirectory/Account/{CurrentUser?.Id}");
                }
            }

            var login = HttpContext.Session.GetString("login");

            if (login is not null)
            {
                var user = await _userRepository.FindUserByLoginAsync(login);
                if (user is not null && user.Status == Status.Active) return Redirect($"AccountDirectory/Account/{CurrentUser?.Id}");
            }

            return Page();
        }


        public async Task<IActionResult> OnPostRecovery()
        {
            var (isValid, user) = await TryGetAccessOrAddModelError();
            if (!isValid) return Page();
            if (user.Status != Status.Removed)
            {
                ModelState.AddModelError("", 
                    $"Ваш аккаунт не удален. Он находится в статусе: {user.Status.ToRussianLanguage()}");
                return Page();
            }

            user.Status = Status.Active;
            await _userRepository.UpdateUserAsync(user);
            return Page();
        }

        private async Task<IActionResult> Login(User user)
        {
            if (AuthorizationViewModel.IsRememberedMe)
            {
                Response.Cookies.Append("token", HttpContext.Session.Id, CookieOptions);
                var session = new Session
                {
                    Token = HttpContext.Session.Id,
                    User = user
                };
                
                await _sessionRepository.AddSessionAsync(session);
            }
            else
            {
                HttpContext.Session.SetString("login", user.Login);
            }

            return Redirect($"AccountDirectory/Account/{user.Id}");
        }
        
        public async Task<IActionResult> OnPostAuthorization()
        {
            var (isValid, user) = await TryGetAccessOrAddModelError();
            if (!isValid) return Page();

            if (user.Status == Status.Blocked)
            {
                ModelState.AddModelError("", "Ваш аккаунт заблокирован");
                return Page();
            }

            if (user.Status == Status.Removed)
            {
                ModelState.AddModelError("", "Ваш аккаунт удален, введите свои данные и восстановите аккаунт");
                return Page();
            }

            return await Login(user);
        }
        
        
        private async Task<(bool, User)> TryGetAccessOrAddModelError()
        {
            if (!ModelState.IsValid) return (false, null);
            
            var user = await _userRepository.FindUserByLoginAsync(AuthorizationViewModel.Login);
            if (user == null)
            {
                AddError();
                return (false, null);
            }
            
            var hashedPassword = _passwordHashing.Hash(AuthorizationViewModel.Password, user.Salt);
            if (user.HashedPassword != hashedPassword)
            {
                AddError();
                return (false, null);
            }

            return (true, user);
        }
        
        private static readonly CookieOptions CookieOptions = new()
        {
            Expires = DateTimeOffset.Now.AddDays(365)
        };

        private void AddError() 
            => ModelState.AddModelError("", ValidationMessages.ErrorMessagesInvalidData);
    }
}