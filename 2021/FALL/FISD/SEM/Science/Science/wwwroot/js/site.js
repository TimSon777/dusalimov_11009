﻿function showOrHidePassword(iconClicked, input) {
    $(iconClicked).on("click", function () {
        let isHidden = $(this).attr("class").indexOf("far fa-eye-slash") !== -1
        $(input).attr('type', isHidden ? 'text' : 'password')
        $(this).attr("class", `far fa-eye${isHidden ? '' : '-slash'}`)
    })
}

function runShowingOrHidingPassword() {
    showOrHidePassword($('#show-hide-password'), $('#RegistrationViewModel_Password'))
    showOrHidePassword($('#show-hide-confirmed-password'), $('#RegistrationViewModel_ConfirmedPassword'))
    showOrHidePassword($('#authorization-show-hide-password'), $('#AuthorizationViewModel_Password'))
}

function showNewImage(input) {
    $(input).change(function () {
        let files = $(input).prop('files')
        if (files && files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => $('#img').attr('src', e.target.result);
            reader.readAsDataURL(files[0]);
        }
    })
}

let skipArea = 0
let takeArea = 8

function onGetAreas() {
    $('#form-area-show-more').on('submit', function () {
        if (skipArea < 0 || takeArea < 0) {
            alert("Введите неотрицательное число")
        } else {
            $.ajax({
                type: "get",
                url: `Areas?handler=More&skip=${skipArea}&take=${takeArea}`,
                success: function (html) {
                    $('#area-content').append(html)
                }
            })

            skipArea += takeArea
        }

        return false
    })
}

let skipArticleComments = 0
let takeArticleComments = 5

function onGetArticleComments() {
    $('#form-article-comments-show-more').on('submit', function () {
        if (skipArticleComments < 0 || takeArticleComments < 0) {
            alert("Не пытайтесь сломать сайт!")
        } else {
            let href = window.location.href.split('?')[0]
            $.ajax({
                type: "get",
                url: `${href}?handler=ShowMore&skip=${skipArticleComments}&take=${takeArticleComments}`,
                success: function (html) {
                    $('#comments-content').append(html)
                }
            })

            skipArticleComments += takeArticleComments
        }

        return false
    })
}

function isContainsScript(text) {
    return text.indexOf("<script>") !== -1;
}

let maxLengthComment = 300

function isValidCommentOrAddErrorMessage(comment, validationTag) {
    if (comment.length === 0) {
        $(validationTag).text("Это поле обязательно для заполнения")
        return false
    }

    if (comment.length > maxLengthComment) {
        $(validationTag).text("Максимальная длина - 300 символов")
        return false
    }

    if (isContainsScript(comment)) {
        $(validationTag).text("Не пытайтесь вставить скрипт")
        return false
    }

    return true
}

function isValidComment(comment) {
    return comment.length !== 0
        && comment.length <= maxLengthComment
        && !isContainsScript(comment)
}

function validateArticleComment() {
    let span = $('#validation-adding-comment')
    let comment = $('#input-article-comment').val()
    return isValidCommentOrAddErrorMessage(comment, span);
}

function onPostComment() {
    $('#form-adding-article-comment').submit(function () {
        let span = $('#validation-adding-comment')
        let comment = $('#input-article-comment').val()
        if (!isValidCommentOrAddErrorMessage(comment, span)) {
            return false
        }

        let href = window.location.pathname
        let token = $('input[name="__RequestVerificationToken"]').val()

        $.ajax({
            type: "post",
            url: `${href}?handler=Comment&comment=${comment}`,
            headers: {
                "RequestVerificationToken": token
            },
            success: function (html) {
                $('#comments-content').prepend(html)
            },
            error: function () {
                alert("Что-то пошло не так. Перезагрузите страницу и попробуйте еше раз!")
            }
        })

        return false
    })
}

function onGetOptionsScientificWorks() {
    $('#ScientificWorksId').select2({
        ajax: {
            type: 'get',
            url: `?handler=ScientificWorks`,
            dataType: 'json',
            delay: 2000,
            processResults: function (json) {
                return {
                    results: json.value
                }
            }
        }
    })
}

let skipUsersAdmin = 0
let takeUsersAdmin = 8

function onGetUsersAdmin() {
    $('#form-admin-users-show-more').submit(function () {
        if (skipUsersAdmin < 0 || takeUsersAdmin < 0) {
            alert("Не пытайтесь сломать сайт!")
        } else {
            let href = window.location.href.split('?')[0]
            $.ajax({
                type: "get",
                url: `${href}?handler=UsersShowMore&skip=${skipUsersAdmin}&take=${takeUsersAdmin}`,
                success: function (html) {
                    $('#users-content').append(html)
                }
            })

            skipUsersAdmin += takeUsersAdmin
        }

        return false
    })
}

function alertIfImageIsNotValid() {
    document.body
        .querySelector('input[type="file"]')
        .addEventListener('change', function () {
            if (!this.files || !this.files[0]) return
            if (!this.files[0].type.includes('image')) {
                $(this).val(null)
                alert("Не пытайтесь вставить не картинку. Ничего не выйдет")
            } else if(this.files[0].size === 0 || this.files[0].size > 8 * 1024 * 1024 * 10) {
                $(this).val(null)
                alert("Слишком большая картинка")
            }
        })
}

$(document).ready(function () {
    runShowingOrHidingPassword()
    showNewImage($('#AccountEditingVm_ProfilePicture'))
    showNewImage($('#AreaVm_Image'))
    showNewImage($('#ScientistVm_Photo'))
    showNewImage($('#ArticleVm_Image'))
    onGetAreas()
    onGetOptionsScientificWorks()
    onGetArticleComments()
    onPostComment()
    onGetUsersAdmin()

    let inputArticleComment = $('#input-article-comment')
    inputArticleComment.keyup(validateArticleComment)

    inputArticleComment.keydown(() => {
        if (isValidComment(inputArticleComment.val()))
            $('#validation-adding-comment').text("")
    })

    $('#ScientistVm_AreaSciencesId').picker()
    $('#SearchSettings_Entities').picker()
    alertIfImageIsNotValid()
})