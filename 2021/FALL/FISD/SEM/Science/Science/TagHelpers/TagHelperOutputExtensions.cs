﻿using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Science.TagHelpers
{
    public static class TagHelperOutputExtensions
    {
        public static TagHelperOutput Class(this TagHelperOutput tagHelperOutput, string cls)
        {
            tagHelperOutput.AddClass(cls, HtmlEncoder.Default);
            return tagHelperOutput;
        }
        
        public static TagHelperOutput Attribute(
            this TagHelperOutput tagHelperOutput, 
            string attr, 
            object value)
        {
            tagHelperOutput.Attributes.Add(attr, value);
            return tagHelperOutput;
        }
    }
}