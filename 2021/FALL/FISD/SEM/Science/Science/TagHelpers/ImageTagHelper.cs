﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Science.TagHelpers
{
    [HtmlTargetElement(Attributes = "path")]
    public class ImageTagHelper : TagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var path = context.AllAttributes["path"];
            var cls = context.AllAttributes["class"]?.Value.ToString();
            
            output.Attributes.Clear();
            output.TagName = "img";
            
            output
                .Class(cls)
                .Class("w-100")
                .Class("h-100")
                .Class("green-frame")
                .Class("of-c")
                .Attribute("src", path.Value)
                .Attribute("alt", path.Value);
        }
    }
}