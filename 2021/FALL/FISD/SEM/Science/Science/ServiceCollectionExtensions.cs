﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Science.Services.Database;
using Science.Services.Database.AdminDb;
using Science.Services.Database.AreaScienceDb;
using Science.Services.Database.ArticleDb;
using Science.Services.Database.ArticleReviewDb;
using Science.Services.Database.ScientificWorkDb;
using Science.Services.Database.ScientistDb;
using Science.Services.Database.SearchDb;
using Science.Services.Database.SessionDb;
using Science.Services.Database.UserDb;
using Science.Services.Database.VideoDb;

namespace Science
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
            => services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseNpgsql(configuration.GetConnectionString("ConnectionDbScience")))
                .AddScoped<ISearchRepository, SearchRepository>()
                .AddScoped<IAdminRepository, AdminRepository>()
                .AddScoped<IArticleRepository, ArticleRepository>()
                .AddScoped<IScientistRepository, ScientistRepository>()
                .AddScoped<IUserRepository, UserRepository>()
                .AddScoped<IVideoRepository, VideoRepository>()
                .AddScoped<IAreaScienceRepository, AreaScienceRepository>()
                .AddScoped<ISessionRepository, SessionRepository>()
                .AddScoped<IScientificWorkRepository, ScientificWorkRepository>()
                .AddScoped<IArticleReviewRepository, ArticleReviewRepository>();
    }
}