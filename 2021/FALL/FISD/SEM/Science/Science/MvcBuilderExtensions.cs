﻿using Microsoft.Extensions.DependencyInjection;

namespace Science
{
    public static class MvcBuilderExtensions
    {
        public static IMvcBuilder ConfigureRouting(this IMvcBuilder services)
            => services.AddRazorPagesOptions(options =>
                options.Conventions
                    .AddPageRoute("/AreaDirectory/Areas",                     "/Areas")
                    .AddPageRoute("/AreaDirectory/Area",                      "/Area/{PageId}")
                    .AddPageRoute("/ScientistDirectory/Scientists",           "/Scientists/{CurrentPage:int?}")
                    .AddPageRoute("/ScientistDirectory/Scientist",            "/Scientist/{PageId:int?}")
                    .AddPageRoute("/ArticleDirectory/Articles",               "/Articles/{CurrentPage:int?}")
                    .AddPageRoute("/ArticleDirectory/Article",                "/Article/{PageId=1}")
                    .AddPageRoute("/AccountDirectory/Account",                "/Account/{UserId:int?}")
                    .AddPageRoute("/AdminDirectory/Admin",                    "/Admin")
                    .AddPageRoute("/SearchDirectory/Search", "Search"));
    }
}