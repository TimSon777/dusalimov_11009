using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Science.Infrastructure.HashedPassword;
using Science.Infrastructure.Image;
using Science.Middlewares.TokenMiddleware;

namespace Science
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services) => services
            .AddSession(options => options.IdleTimeout = TimeSpan.FromDays(365))
            .AddSingleton<IPasswordHashing, PasswordHashing>()
            .AddSingleton<IImageHelper, ImageHelper>()
            .ConfigureDatabase(Configuration)
            .AddRazorPages()
            .ConfigureRouting();

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else 
                app.UseExceptionHandler("/Error")
                    .UseHsts();
            
            app.UseHttpsRedirection()
                .UseStaticFiles()
                .UseRouting()
                .UseSession()
                .UseToken()
                .UseEndpoints(endpoints => endpoints.MapRazorPages());
        }
    }
}
