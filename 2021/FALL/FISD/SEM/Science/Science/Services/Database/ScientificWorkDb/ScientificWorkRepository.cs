﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.ScientificWorkDb
{
    public class ScientificWorkRepository : Repository, IScientificWorkRepository
    {
        public ScientificWorkRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        { }

        public async Task AddAndSaveAsync(ScientificWorkViewModel model, User publisher, Scientist scientist)
        {
            var work = new ScientificWork
            {
                Link = model.Link,
                Title = model.Title,
                Publisher = publisher,
                Scientists = new List<Scientist> {scientist},
                Status = Status.Active
            };

            await ApplicationDbContext.ScientificWorks.AddAsync(work);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task UpdateAndSaveAsync(int[] scientificWorksId, Scientist scientist)
        {
            foreach (var id in scientificWorksId)
            {
                var work = ApplicationDbContext.ScientificWorks
                    .Include(sw => sw.Scientists)
                    .FirstOrDefault(sw => sw.Id == id);
                
                work?.Scientists.Add(scientist);
                if (work != null) ApplicationDbContext.ScientificWorks.Update(work);
            }
            
            await ApplicationDbContext.SaveChangesAsync();
        }

        public IEnumerable<ScientificWork> GetScientificWorks(string startTitle, int take, Scientist scientist)
            => ApplicationDbContext.ScientificWorks
                .AsParallel()
                .Where(sw => sw.Title.IndexOf(startTitle, StringComparison.OrdinalIgnoreCase) == 0)
                .Where(sw => !scientist.ScientificWorks.Contains(sw))
                .Where(sw => sw.Status == Status.Active)
                .Take(take)
                .OrderBy(sw => sw.Title)
                .ToList();

        public async Task DeleteFromAllAndSaveAsync(ScientificWork work)
        {
            await DeleteAsync(work);
        }

        public async Task DeleteFromOneAndSaveAsync(ScientificWork work, Scientist scientist)
        {
            scientist.ScientificWorks.Remove(work);
            ApplicationDbContext.Update(scientist);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task RestoreAndSaveAsync(ScientificWork work)
        {
            work.Status = Status.Active;
            ApplicationDbContext.Update(work);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task BlockAndSaveAsync(ScientificWork work)
        {
            await BlockAsync(work);
        }

        public async Task<ScientificWork> FindById(int id)
        {
            return await ApplicationDbContext.ScientificWorks.FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}