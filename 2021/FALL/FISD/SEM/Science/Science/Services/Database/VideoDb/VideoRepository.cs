﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.VideoDb
{
    public class VideoRepository : Repository, IVideoRepository
    {
        public VideoRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        { }

        public async Task<IEnumerable<Video>> FindResultPagination(int currentPage, int pageSize = 10)
            => await ApplicationDbContext.Videos
                .Include(video => video.Publisher)
                .Where(video => video.Status == Status.Active)
                .OrderByDescending(a => a.Publication)
                .Skip((currentPage - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        
        public async Task<int> GetCountRecords()
            => await ApplicationDbContext.Videos
                .Where(video => video.Status == Status.Active)
                .CountAsync();

        public Task AddAndSave(VideoViewModel video, User user)
        {
            throw new System.NotImplementedException();
        }

        public async Task AddVideoAsync(Video video)
        {
            await ApplicationDbContext.Videos.AddAsync(video);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task<Video> FindById(int id)
        {
            return await ApplicationDbContext.Videos
                .Include(a => a.Publisher)
                .FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task DeleteAndSaveAsync(Video entity)
        {
            await DeleteAsync(entity);
        }

        public async Task MakeActiveAndSaveAsync(Video entity)
        {
            await MakeActiveAsync(entity);
        }

        public async Task BlockAndSaveAsync(Video entity)
        {
            await BlockAsync(entity);
        }
    }
}