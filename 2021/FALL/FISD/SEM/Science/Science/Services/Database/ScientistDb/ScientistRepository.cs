﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.ScientistDb
{
    public class ScientistRepository : Repository, IScientistRepository
    {
        public ScientistRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        { }

        public async Task<IEnumerable<Scientist>> FindResultPagination(int currentPage, int pageSize = 10)
            => await ApplicationDbContext.Scientists
                .Where(a => a.Status == Status.Active)
                .Include(a => a.AreaSciences.Where(b => b.Status == Status.Active))
                .Skip((currentPage - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

        public async Task<int> GetCountRecords()
            => await ApplicationDbContext.Scientists.CountAsync(a => a.Status == Status.Active);

        public async Task AddAndSave(ScientistViewModel model, User user)
        {
            var areaSciences = ApplicationDbContext.AreaSciences
                .AsParallel()
                .Where(a => model.AreaSciencesId.Contains(a.Id))
                .ToList();
            
            var scientist = new Scientist
            {
                FirstName       = model.FirstName,
                SecondName      = model.SecondName,
                Patronymic      = model.Patronymic,
                Birth           = model.Birth,
                Death           = model.Death,
                PhotoName       = model.Photo.FileName,
                Status          = Status.Active,
                Publisher       = user,
                AreaSciences    = areaSciences,
                Biography       = model.Biography
            };
            
            await ApplicationDbContext.AddAsync(scientist);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task<Scientist> FindByFullNameAndBirthAsync(string firstName, 
            string secondName, string patronymic, DateTime birth)
            => await ApplicationDbContext.Scientists.FirstOrDefaultAsync(scientist =>
                scientist.FirstName == firstName
                && scientist.SecondName == secondName
                && (patronymic == null || scientist.Patronymic == patronymic)
                && scientist.Birth == birth);

        public async Task<Scientist> FindById(int id)
            => await ApplicationDbContext.Scientists
                .Include(scientist => scientist.Publisher)
                .Include(scientist => scientist.AreaSciences)
                .Include(scientist => scientist.ScientificWorks.Where(a => a.Status == Status.Active))
                .ThenInclude(work => work.Scientists)
                .FirstOrDefaultAsync(scientist => scientist.Id == id);

        public async Task<Scientist> UpdateAndSaveAsync(ScientistViewModel vm, Scientist scientist)
        {
            scientist.FirstName = vm.FirstName;
            scientist.SecondName = vm.SecondName;
            scientist.Patronymic = vm.Patronymic;
            scientist.Biography = vm.Biography;
            scientist.Birth = vm.Birth;
            scientist.Death = vm.Death;

            if (vm.Photo is not null)
            {
                scientist.PhotoName = vm.Photo.FileName;
            }

            var r = ApplicationDbContext.Update(scientist);
            await ApplicationDbContext.SaveChangesAsync();
            return r.Entity;
        }

        public async Task UpdateAndSaveAsync(Scientist scientist)
            => await UpdateAsync(scientist);

        public async Task DeleteAndSaveAsync(Scientist scientist) 
            => await DeleteAsync(scientist);

        public Task MakeActiveAndSaveAsync(Scientist scientist)
            => MakeActiveAsync(scientist);
    }
}