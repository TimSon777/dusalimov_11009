﻿using System.Threading.Tasks;
using Science.Models;

namespace Science.Services.Database.SessionDb
{
    public interface ISessionRepository
    { 
        Task<Session> FindSessionByTokenAsync(string token);
        Task DeleteSessionByTokenAsync(string token);
        Task AddSessionAsync(Session session);
    }
}