﻿using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.ArticleDb
{
    public interface IArticleRepository : IRepository<Article, ArticleViewModel>, 
        IPagination<Article, ArticleViewModel>,
        ICrudRepository<Article>
    {
        
    }
}