﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Science.Models;

namespace Science.Services.Database.AdminDb
{
    public interface IAdminRepository
    {
        Task<int> GetCountAsync<TEntity>(
            Expression<Func<TEntity, bool>> predicate)
            where TEntity : class;

        Task<IEnumerable<TEntity>> GetCollectionAsync<TEntity>(int skip, int take)
            where TEntity : class;
    }
}