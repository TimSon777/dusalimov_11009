﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.AreaScienceDb
{
    public interface IAreaScienceRepository : IShowingMore<AreaScience>
    {
        Task AddAndSaveAsync(AreaScience area);
        Task<AreaScience> FindByTitle(string title);
        Task<AreaScience> FindByIdAsync(int id);
        IEnumerable<SelectListItem> GetAreaSciencesSelectListItem();
        Task<AreaScience> UpdateAndSaveAsync(AreaScienceViewModel vm, int id);
        Task<AreaScience> UpdateAndSaveAsync(AreaScience area);
    }
}