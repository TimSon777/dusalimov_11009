﻿using System.Threading.Tasks;

namespace Science.Services.Database
{
    public interface ICrudRepository<in TEntity>
    {
        Task DeleteAndSaveAsync(TEntity entity);
        Task MakeActiveAndSaveAsync(TEntity entity);
        Task BlockAndSaveAsync(TEntity entity);
        Task UpdateAndSaveAsync(TEntity entity);
    }
}