﻿using System.Threading.Tasks;
using Science.Models;

namespace Science.Services.Database.UserDb
{
    public interface IUserRepository : IStatusChanger<User>
    {
        Task AddUserAsync(User user);
        Task<User> FindUserByLoginAsync(string login);
        Task UpdateUserAsync(User user, string login = null);
        Task<User> FindById(int id);
        Task ChangeRoleAsync(User user, Role role);
    }
}