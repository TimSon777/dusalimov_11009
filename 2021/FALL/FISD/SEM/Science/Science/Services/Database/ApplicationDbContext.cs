﻿using Microsoft.EntityFrameworkCore;
using Science.Models;

namespace Science.Services.Database
{
    public sealed class ApplicationDbContext : DbContext
    {
        public DbSet<AreaScience> AreaSciences { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<CommentArticle> CommentArticles { get; set; }
        public DbSet<ScientificWork> ScientificWorks { get; set; }
        public DbSet<Scientist> Scientists { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Session> Sessions { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        { }
    }
}