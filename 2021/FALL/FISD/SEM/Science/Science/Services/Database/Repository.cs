﻿using System.Threading.Tasks;
using Science.Models;

namespace Science.Services.Database
{
    public class Repository
    {
        protected readonly ApplicationDbContext ApplicationDbContext;

        protected Repository(ApplicationDbContext applicationDbContext) 
            => ApplicationDbContext = applicationDbContext;

        protected async Task<T> AddAndSaveAsync<T>(T entity)
        {
            var result = await ApplicationDbContext.AddAsync(entity);
            await ApplicationDbContext.SaveChangesAsync();
            return (T) result.Entity;
        }

        protected async Task UpdateAsync<T>(T entity)
        {
            ApplicationDbContext.Update(entity);
            await ApplicationDbContext.SaveChangesAsync();
        }

        protected async Task DeleteAsync(dynamic entity)
        {
            entity.Status = Status.Removed;
            ApplicationDbContext.Update(entity);
            await ApplicationDbContext.SaveChangesAsync();
        }

        protected async Task MakeActiveAsync(dynamic entity)
        {
            entity.Status = Status.Active;
            ApplicationDbContext.Update(entity);
            await ApplicationDbContext.SaveChangesAsync();
        }

        protected async Task BlockAsync(dynamic entity)
        {
            entity.Status = Status.Blocked;
            ApplicationDbContext.Update(entity);
            await ApplicationDbContext.SaveChangesAsync();
        }
    }
}