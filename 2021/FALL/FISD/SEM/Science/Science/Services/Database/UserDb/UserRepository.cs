﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Science.Models;

namespace Science.Services.Database.UserDb
{
    public class UserRepository : Repository, IUserRepository
    {
        public UserRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) 
        { }
        
        public async Task AddUserAsync(User user)
        {
            await ApplicationDbContext.Users.AddAsync(user);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task<User> FindUserByLoginAsync(string login)
            => await ApplicationDbContext.Users
                .FirstOrDefaultAsync(a => a.Login == login);

        public async Task UpdateUserAsync(User user, string login = null)
        {
            login ??= user.Login;
            
            var userUp = await FindUserByLoginAsync(login);

            userUp.Birth                    = user.Birth;
            userUp.Email                    = user.Email;
            userUp.Patronymic               = user.Patronymic;
            userUp.AdditionalInformation    = user.AdditionalInformation;
            userUp.PhoneNumber              = user.PhoneNumber;
            userUp.Patronymic               = user.Patronymic;
            
            if (user.ProfilePictureExtension is not null)
                userUp.ProfilePictureExtension  = user.ProfilePictureExtension;
            
            ApplicationDbContext.Users.Update(userUp);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task<User> FindById(int id)
            => await ApplicationDbContext.Users
                .Include(x => x.ScientificWorks)
                .Include(x => x.Videos)
                .Include(x => x.Articles)
                .Include(x => x.Scientists)
                .Include(x => x.AreaSciences)
                .FirstOrDefaultAsync(x => x.Id == id);

        public async Task ChangeRoleAsync(User user, Role role)
        {
            user.Role = role;
            await UpdateUserAsync(user);
        }

        public async Task ChangeStatusAsync(User entity, Status status)
        {
            entity.Status = status;
            await UpdateUserAsync(entity);
        }
    }
}