﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Science.Models;

namespace Science.Services.Database.SessionDb
{
    public class SessionRepository : Repository, ISessionRepository
    {
        public SessionRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) 
        { }
        
        public async Task<Session> FindSessionByTokenAsync(string token) 
            => await ApplicationDbContext.Sessions
                .Include(a => a.User)
                .FirstOrDefaultAsync(session => session.Token == token);

        public async Task DeleteSessionByTokenAsync(string token)
        {
            var session = await ApplicationDbContext.Sessions.FirstOrDefaultAsync(session => session.Token == token);
            
            if (session is null) return;

            ApplicationDbContext.Sessions.Remove(session);
        }

        public async Task AddSessionAsync(Session session)
        {
            await ApplicationDbContext.Sessions.AddAsync(session);
            await ApplicationDbContext.SaveChangesAsync();
        }
    }
}