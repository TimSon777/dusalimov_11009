﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Science.Services.Database.SearchDb
{
    public class SearchRepository : Repository, ISearchRepository
    {
        public SearchRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        { }

        public async Task<List<TEntity>> GetCollection<TEntity>(
            Expression<Func<TEntity, bool>> predicate, int take = 10)
        where TEntity : class
            => await ApplicationDbContext
                .Set<TEntity>()
                .Where(predicate)
                .Take(take)
                .ToListAsync();
    }
}