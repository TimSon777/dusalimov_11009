﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.ArticleReviewDb
{
    public class ArticleReviewRepository : Repository, IArticleReviewRepository
    {
        public ArticleReviewRepository(ApplicationDbContext applicationDbContext) 
            : base(applicationDbContext)
        { }
        
        public async Task<CommentArticle> AddAndSaveAsync(Article article, User sender, CommentViewModel commentVm)
        {
            var reviewArticle = new CommentArticle
            {
                Article = article,
                Publication = DateTime.Now,
                Status = Status.Active,
                Sender = sender,
                Text = commentVm.Text
            };

            return await AddAndSaveAsync(reviewArticle);
        }

        public async Task<IEnumerable<CommentArticle>> GetCollection(int skip, int take, Article article)
        {
            return await ApplicationDbContext.CommentArticles
                .Include(x => x.Article)
                .Include(x => x.Sender)
                .Where(x => x.Article == article)
                .Where(x => x.Status == Status.Active)
                .OrderByDescending(x => x.Publication)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }

        public async Task BlockAndSaveAsync(CommentArticle comment)
        {
            await BlockAsync(comment);
        }

        public async Task<CommentArticle> FindById(int id)
        {
            return await ApplicationDbContext.CommentArticles
                .Include(a => a.Sender)
                .FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}