﻿using System;
using System.Threading.Tasks;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.ScientistDb
{
    public interface IScientistRepository : IPagination<Scientist, ScientistViewModel>
    {
        Task<Scientist> FindByFullNameAndBirthAsync(string firstName, string secondName, string patronymic, DateTime birth);
        Task<Scientist> FindById(int id);
        Task<Scientist> UpdateAndSaveAsync(ScientistViewModel vm, Scientist scientist);
        Task UpdateAndSaveAsync(Scientist scientist);
        Task DeleteAndSaveAsync(Scientist scientist);
        Task MakeActiveAndSaveAsync(Scientist scientist);
    }
}