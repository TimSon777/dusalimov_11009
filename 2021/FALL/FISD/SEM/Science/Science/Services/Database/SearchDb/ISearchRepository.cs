﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Science.Services.Database.SearchDb
{
    public interface ISearchRepository
    {
        Task<List<TEntity>> GetCollection<TEntity>(
            Expression<Func<TEntity, bool>> predicate, int take = 10)
            where TEntity : class;
    }
}