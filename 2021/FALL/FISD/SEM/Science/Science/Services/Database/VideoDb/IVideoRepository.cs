﻿using System.Threading.Tasks;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.VideoDb
{
    public interface IVideoRepository : IPagination<Video, VideoViewModel>
    {
        Task AddVideoAsync(Video video);
        Task<Video> FindById(int id);
        Task DeleteAndSaveAsync(Video entity);
        Task MakeActiveAndSaveAsync(Video entity);
        Task BlockAndSaveAsync(Video entity);
    }
}