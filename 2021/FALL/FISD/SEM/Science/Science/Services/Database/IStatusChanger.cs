﻿using System.Threading.Tasks;
using Science.Models;

namespace Science.Services.Database
{
    public interface IStatusChanger<in TEntity>
    {
        Task ChangeStatusAsync(TEntity entity, Status status);
    }
}