﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.AreaScienceDb
{
    public class AreaScienceRepository : Repository, IAreaScienceRepository
    {
        public AreaScienceRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        { }
        
        public async Task<IEnumerable<AreaScience>> GetCollection(int skip, int take)
            => await ApplicationDbContext.AreaSciences
                .Where(a => a.Status == Status.Active)
                .Skip(skip)
                .Take(take)
                .ToListAsync();

        public async Task AddAndSaveAsync(AreaScience area)
        {
            await ApplicationDbContext.AreaSciences.AddAsync(area);
            await ApplicationDbContext.SaveChangesAsync();
        }

        public async Task<AreaScience> FindByTitle(string title)
            => await ApplicationDbContext.AreaSciences.FirstOrDefaultAsync(e => e.Title == title);

        public async Task<AreaScience> FindByIdAsync(int id)
            => await ApplicationDbContext.AreaSciences
                .Include(area => area.Publisher)
                .FirstOrDefaultAsync(area => area.Id == id);

        public IEnumerable<SelectListItem> GetAreaSciencesSelectListItem()
            => ApplicationDbContext.AreaSciences
                .AsParallel()
                .Where(a => a.Status == Status.Active)
                .Select(a => new SelectListItem(a.Title, a.Id.ToString()));

        public async Task<AreaScience> UpdateAndSaveAsync(AreaScienceViewModel vm, int id)
        {
            var area = await FindByIdAsync(id);

            area.Title = vm.Title;
            area.Description = vm.Description;
            area.Text = vm.Text;
            
            if (vm.Image is not null)
            {
                area.ImageName = vm.Image.FileName;
            }

            await UpdateAndSaveAsync(area);
            return area;
        }

        public async Task<AreaScience> UpdateAndSaveAsync(AreaScience area)
        {
            var result = ApplicationDbContext.AreaSciences.Update(area);
            await ApplicationDbContext.SaveChangesAsync();
            return result.Entity;
        }
    }
}