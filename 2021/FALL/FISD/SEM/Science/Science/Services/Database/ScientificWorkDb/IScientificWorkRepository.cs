﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.ScientificWorkDb
{
    public interface IScientificWorkRepository
    {
        Task AddAndSaveAsync(ScientificWorkViewModel model, User publisher, Scientist scientist);
        Task UpdateAndSaveAsync(int[] scientificWorksId, Scientist scientist);
        IEnumerable<ScientificWork> GetScientificWorks(string startTitle, int take, Scientist scientist);
        Task DeleteFromAllAndSaveAsync(ScientificWork work);
        Task DeleteFromOneAndSaveAsync(ScientificWork work, Scientist scientist);
        Task RestoreAndSaveAsync(ScientificWork work);
        Task BlockAndSaveAsync(ScientificWork work);
        Task<ScientificWork> FindById(int id);
    }
}