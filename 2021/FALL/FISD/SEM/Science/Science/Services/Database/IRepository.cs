﻿using System.Threading.Tasks;
using Science.Models;

namespace Science.Services.Database
{
    public interface IRepository<TModel, in TViewModel>
    {
        Task<TModel> AddAndSaveAsync(TViewModel vm, User user);
        Task<TModel> FindByIdAsync(int id);
    }
}