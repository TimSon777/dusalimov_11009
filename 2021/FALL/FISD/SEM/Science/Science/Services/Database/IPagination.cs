﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Science.Models;

namespace Science.Services.Database
{
    public interface IPagination<TModel, in TViewModel>
    {
        Task<IEnumerable<TModel>> FindResultPagination(int currentPage, int pageSize = 10);
        Task<int> GetCountRecords();
        Task AddAndSave(TViewModel model, User user);
    }
}