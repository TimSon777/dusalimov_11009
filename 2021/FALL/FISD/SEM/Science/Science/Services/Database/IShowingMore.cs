﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Science.Services.Database
{
    public interface IShowingMore<T>
    {
        Task<IEnumerable<T>> GetCollection(int skip, int take);
    }
}