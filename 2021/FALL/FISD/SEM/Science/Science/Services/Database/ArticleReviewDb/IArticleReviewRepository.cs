﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.ArticleReviewDb
{
    public interface IArticleReviewRepository
    {
        Task<CommentArticle> AddAndSaveAsync(Article article, User sender, CommentViewModel commentVm);
        Task<IEnumerable<CommentArticle>> GetCollection(int skip, int take, Article article);
        Task BlockAndSaveAsync(CommentArticle comment);
        Task<CommentArticle> FindById(int id);
    }
}