﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Science.Infrastructure;
using Science.Models;

namespace Science.Services.Database.AdminDb
{
    public class AdminRepository : Repository, IAdminRepository
    {
        public AdminRepository(ApplicationDbContext applicationDbContext) 
            : base(applicationDbContext)
        { }
        
        public async Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> predicate) 
            where TEntity : class
        {
            return await ApplicationDbContext
                .Set<TEntity>()
                .CountSupportsNullAsync(predicate);
        }

        public async Task<IEnumerable<TEntity>> GetCollectionAsync<TEntity>(int skip, int take) where TEntity : class
        {
            return await ApplicationDbContext
                .Set<TEntity>()
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }
    }
}