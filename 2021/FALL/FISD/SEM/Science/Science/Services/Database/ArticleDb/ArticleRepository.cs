﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Science.Models;
using Science.ViewModels;

namespace Science.Services.Database.ArticleDb
{
    public class ArticleRepository : Repository, IArticleRepository
    {
        public ArticleRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        { }
        
        public async Task<Article> AddAndSaveAsync(ArticleViewModel vm, User user)
        {
            var article = new Article
            {
                Publisher = user,
                Description = vm.Description,
                Text = vm.Text,
                Publication = DateTime.Now,
                Status = Status.Active,
                Title = vm.Title,
                ImageName = vm.Image?.FileName
            };

            return await AddAndSaveAsync(article);
        }

        public async Task<IEnumerable<Article>> FindResultPagination(int currentPage, int pageSize = 10)
            => await ApplicationDbContext.Articles
                .Include(ar => ar.Publisher)
                .Where(ar => ar.Status == Status.Active)
                .OrderByDescending(ar => ar.Publication)
                .Skip((currentPage - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

        public async Task<int> GetCountRecords()
            => await ApplicationDbContext.Articles
                .CountAsync(a => a.Status == Status.Active);

        public Task AddAndSave(ArticleViewModel model, User user)
        {
            throw new NotImplementedException();
        }

        public async Task<Article> FindByIdAsync(int id)
            => await ApplicationDbContext.Articles
                .Include(a => a.Publisher)
                .FirstOrDefaultAsync(a => a.Id == id);

        public async Task DeleteAndSaveAsync(Article entity)
        {
            await DeleteAsync(entity);
        }

        public async Task MakeActiveAndSaveAsync(Article entity)
        {
            await MakeActiveAsync(entity);
        }

        public async Task BlockAndSaveAsync(Article entity)
        {
            await BlockAsync(entity);
        }

        public async Task UpdateAndSaveAsync(Article entity)
        {
            await UpdateAsync(entity);
        }
    }
}