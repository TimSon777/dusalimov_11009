﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Science.Models;

namespace Science.Services
{
    public class CommonPage : PageModel
    {
        public User CurrentUser => HttpContext.Items["user"] as User;
        public bool IsAdmin => CurrentUser is not null && CurrentUser.Role == Role.Administrator;
    }
}