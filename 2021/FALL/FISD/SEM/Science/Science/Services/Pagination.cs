﻿using System;

namespace Science.Services
{
    public class Pagination
    {
        private readonly int _currentPage;
        public int CurrentPage
        {
            get => TotalPages == 0 ? 1 : _currentPage > TotalPages ? TotalPages : _currentPage;
            private init => _currentPage = value;
        }

        public readonly string PageName;
        public readonly int Count;
        public readonly int PageSize;

        public Pagination(int currentPage, int count, string pageName, int pageSize = 10)
        {
            PageSize = pageSize;
            CurrentPage = currentPage;
            Count = count;
            PageName = pageName;
        }

        public int TotalPages     => (int) Math.Ceiling(Count / (double) PageSize);
        public bool ShowPrevious  => CurrentPage > 1;
        public bool ShowNext      => CurrentPage < TotalPages;
        public bool ShowFirst     => CurrentPage != 1;
        public bool ShowLast      => CurrentPage < TotalPages;
    }
}