﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Science.Models
{
    public class AreaScience
    {
        public int Id { get; set; }

        [Required]
        public User Publisher { get; set; }
        
        [Required]
        [Column(TypeName = "varchar(40)")]
        public string Title { get; set; }
        
        [Column(TypeName = "varchar(500)")]
        public string Description { get; set; }

        [Required]
        [Column(TypeName = "varchar(3000)")]
        public string Text { get; set; }
        
        [Column(TypeName = "varchar(100)")]
        public string ImageName { get; set; }

        [Required]
        public Status Status { get; set; }

        [Required]
        public ICollection<Scientist> Scientists { get; set; }
    }
}