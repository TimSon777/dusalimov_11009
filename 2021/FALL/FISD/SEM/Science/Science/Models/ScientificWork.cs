﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Science.Models
{
    public class ScientificWork
    {
        public int Id { get; set; }

        [Required]
        public User Publisher { get; set; }

        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Link { get; set; }

        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Title { get; set; }

        [Required]
        public Status Status { get; set; }
        
        [Required]
        public ICollection<Scientist> Scientists { get; set; }
    }
}