﻿using System.Collections.Generic;
using Science.ViewModels;

namespace Science.Models
{
    public partial class User
    {
        public ICollection<AreaScience> AreaSciences { get; set; }
        public ICollection<Article> Articles { get; set; }
        public ICollection<Video> Videos { get; set; }
        public ICollection<Scientist> Scientists { get; set; }
        public ICollection<ScientificWork> ScientificWorks { get; set; }
        
        public static bool operator ==(User user1, User user2) => user1?.Id == user2?.Id;
        public static bool operator !=(User user1, User user2) => !(user1 == user2);

        public User()
        { }

        public User(AccountEditingViewModel vm, string extension)
        {
            Email = vm.Email;
            AdditionalInformation = vm.AdditionalInformation;
            Birth = vm.Birth;
            Patronymic = vm.Patronymic;
            PhoneNumber = vm.PhoneNumber;
            ProfilePictureExtension = extension;
        }
    }
}