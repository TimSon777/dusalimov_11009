﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Science.Models
{
    public class CommentArticle
    {
        public int Id { get; set; }

        [Required]
        public virtual User Sender { get; set; }

        [Required]
        public Article Article { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string Text { get; set; }

        [Required]
        public Status Status { get; set; }

        [Required]
        public DateTime Publication { get; set; }
    }
}