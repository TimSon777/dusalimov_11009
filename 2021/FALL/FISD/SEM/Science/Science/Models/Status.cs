﻿using System;

namespace Science.Models
{
    public enum Status : byte
    {
        Active = 1,
        Blocked = 2,
        Removed = 3
    }

    public static class StatusExtensions
    {
        public static string ToRussianLanguage(this Status status)
            => status switch
            {
                Status.Active              => "Активный",
                Status.Blocked             => "Заблокированный",
                Status.Removed             => "Удаленный",
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
            };
    }
}