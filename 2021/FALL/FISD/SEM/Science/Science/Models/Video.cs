﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Science.Models
{
    public class Video
    {
        public int Id { get; set; }

        [Required]
        public User Publisher { get; set; }
        
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Title { get; set; }
        
        [Column(TypeName = "varchar(300)")]
        public string Description { get; set; }
        
        [Required]
        [Column(TypeName = "varchar(100)")]
        public string Link { get; set; }

        [Required]
        public Status Status { get; set; }
        
        [Required]
        public DateTime Publication { get; set; }
    }
}