﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Science.Models
{
    public partial class User
    {
        public int Id { get; set; }
       
        [Required]
        [Column(TypeName = "varchar(30)")]
        [DisplayName("Имя")]
        public string FirstName { get; set; }
        
        [Required]
        [Column(TypeName = "varchar(50)")]
        [DisplayName("Фамилия")]
        public string SecondName { get; set; }
        
        [Column(TypeName = "varchar(40)")]
        public string Patronymic { get; set; }
        
        [Required]
        [Column(TypeName = "varchar(30)")]
        [DisplayName("Никнейм")]
        public string Login { get; set; }

        public DateTime? Birth { get; set; }
        
        [Required]
        [Column(TypeName = "varchar(255)")]
        public string HashedPassword { get; set; }
        
        [Required]
        [Column(TypeName = "varchar(40)")]
        public string Salt { get; set; }
        
        [Column(TypeName = "varchar(200)")]
        public string AdditionalInformation { get; set; }

        [Required]
        public Status Status { get; set; }
        
        [Required]
        public Role Role { get; set; }
        
        [EmailAddress]
        [Column(TypeName = "varchar(256)")]
        public string Email { get; set; }
        
        [Phone]
        [Column(TypeName = "varchar(25)")]
        public string PhoneNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime Registration { get; set; }

        [Column(TypeName = "varchar(10)")]
        public string ProfilePictureExtension { get; set; }
    }
}