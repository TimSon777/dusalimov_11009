﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Science.Models
{
    public class Scientist
    {
        public int Id { get; set; }

        [Required]
        public User Publisher { get; set; }

        [Required]
        [Column(TypeName = "varchar(30)")]
        public string FirstName { get; set; }

        [Required]
        [Column(TypeName = "varchar(50)")]
        public string SecondName { get; set; }
        
        [Column(TypeName = "varchar(40)")]
        public string Patronymic { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime Birth { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Death { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string PhotoName { get; set; }

        [Required]
        public Status Status { get; set; }
        
        [Required]
        [Column(TypeName = "varchar(1000)")]
        public string Biography { get; set; }
        
        public ICollection<ScientificWork> ScientificWorks { get; set; }

        [Required]
        public ICollection<AreaScience> AreaSciences { get; set; }
    }
}