﻿using System;

namespace Science.Models
{
    public enum Role : byte
    {
        Administrator = 1,
        Moderator = 2,
        Publisher = 3,
        CommonUser = 4
    }
    
    public static class RoleExtensions
    {
        public static string ToRussianLanguage(this Role role) 
            => role switch 
            {
                Role.Administrator => "Администратор",
                Role.Moderator     => "Модератор",
                Role.Publisher     => "Публицист",
                Role.CommonUser    => "Пользователь",
                _ => throw new ArgumentOutOfRangeException(nameof(role), role, null)
            };
    }
}