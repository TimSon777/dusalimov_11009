﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Science.Infrastructure;
using Science.Infrastructure.RegularExpressionAttributes;

namespace Science.ViewModels
{
    public class LoginPassword
    {
        [Required(ErrorMessage = ValidationMessages.Required)]
        [Login]
        [DisplayName("Никнейм")]
        public string Login { get; set; }
        
        [Required(ErrorMessage = ValidationMessages.Required)]
        [Password]
        [DisplayName("Пароль")]
        public string Password { get; set; }
    }
}