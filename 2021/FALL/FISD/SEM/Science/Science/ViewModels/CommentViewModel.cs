﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Science.ViewModels
{
    public class CommentViewModel
    {
        public CommentViewModel(string text)
        {
            Text = text;
        }

        [Required]
        [MaxLength(300)]
        [DisplayName("Оставить комментарий")]
        public string Text { get; set; }
    }
}