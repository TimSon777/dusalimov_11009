﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Science.ViewModels
{
    public class ScientificWorkViewModel
    {
        [Required]
        [MaxLength(100)]
        [DisplayName("Ссылка")]
        public string Link { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Название")]
        public string Title { get; set; }
    }
}