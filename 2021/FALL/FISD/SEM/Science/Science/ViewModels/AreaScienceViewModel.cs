﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Science.ViewModels
{
    public class AreaScienceViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(40)]
        [DisplayName("Название")]
        public string Title { get; set; }
        
        [MaxLength(300)]
        [DisplayName("Описание")]
        public string Description { get; set; }

        [Required]
        [MaxLength(3000)]
        [DisplayName("Текст")]
        public string Text { get; set; }
        
        [Required]
        [DisplayName("Изображение")]
        public IFormFile Image { get; set; }
    }
}