﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Science.Models;

namespace Science.ViewModels
{
    public class ArticleViewModel
    {
        [Required]
        [MaxLength(50)]
        [DisplayName("Название")]
        public string Title { get; set; }

        [MaxLength(300)]
        [DisplayName("Описание")]
        public string Description { get; set; }

        [Required]
        [MaxLength(3000)]
        [DisplayName("Содержание")]
        public string Text { get; set; }
        
        [DisplayName("Изображение")]
        public IFormFile Image { get; set; }

        public ArticleViewModel()
        { }

        public ArticleViewModel(Article article)
        {
            Title = article.Title;
            Description = article.Description;
            Text = article.Text;
        }
    }
}