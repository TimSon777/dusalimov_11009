﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Science.Infrastructure;
using Science.Infrastructure.RegularExpressionAttributes;
using Science.Services;

namespace Science.ViewModels
{
    public class RegistrationViewModel : LoginPassword
    {
        [Required(ErrorMessage = ValidationMessages.Required)]
        [SpecificLanguage]
        [MinLength(3, ErrorMessage = "Имя должно содержать минимум 3 символа!")]
        [MaxLength(30, ErrorMessage = "Имя не может содержать более 30 символов!")]
        [DisplayName("Имя")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = ValidationMessages.Required)]
        [SpecificLanguage]
        [MinLength(3, ErrorMessage = "Фамилия должна содержать минимум 3 символа!")]
        [MaxLength(30, ErrorMessage = "Фамилия не может содержать более 30 символов!")]
        [DisplayName("Фамилия")]
        public string SecondName { get; set; }
        
        [Required(ErrorMessage = ValidationMessages.Required)]
        [DisplayName("Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают!")]
        public string ConfirmedPassword { get; set; }
    }
}