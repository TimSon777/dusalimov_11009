﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Science.Infrastructure.RegularExpressionAttributes;

namespace Science.ViewModels
{
    public class VideoViewModel
    {
        [Required]
        [MaxLength(100)]
        [DisplayName("Название видео")]
        public string Title { get; set; }
        
        [MaxLength(300)]
        [DisplayName("Описание видео")]
        public string Description { get; set; }

        [Required]
        [MaxLength(100)]
        [Youtube]
        [DisplayName("Ссылка на видео")]
        public string Link { get; set; }
    }
}