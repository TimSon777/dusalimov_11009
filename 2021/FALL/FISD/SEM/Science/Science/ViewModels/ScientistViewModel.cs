﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Science.ViewModels
{
    public class ScientistViewModel
    {
        [Required]
        [MaxLength(30)]
        [DisplayName("Имя")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        [DisplayName("Фамилия")]
        public string SecondName { get; set; }
        
        [MaxLength(40)]
        [DisplayName("Отчество")]
        public string Patronymic { get; set; }

        [Required]
        [DisplayName("Дата рождения")]
        public DateTime Birth { get; set; }

        [DisplayName("Дата смерти")]
        public DateTime? Death { get; set; }

        [Required]
        [DisplayName("Фото")]
        public IFormFile Photo { get; set; }

        [Required]
        [MaxLength(1000)]
        [DisplayName("Биография")]
        public string Biography { get; set; }

        [Required] 
        [DisplayName("Научные области")]
        public int[] AreaSciencesId { get; set; }
    }
}