﻿using System.ComponentModel;

namespace Science.ViewModels
{
    public class AuthorizationViewModel : LoginPassword
    {
        [DisplayName("Запомнить меня")]
        public bool IsRememberedMe { get; set; }
    }
}