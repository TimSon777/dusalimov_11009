﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Science.Models;

namespace Science.ViewModels
{
    public class AccountEditingViewModel
    {
        [DisplayName("Аватарка")]
        public IFormFile ProfilePicture { get; set; }

        [EmailAddress]
        [MaxLength(256)]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Phone]
        [MaxLength(25)]
        [DisplayName("Номер телефона")]
        public string PhoneNumber { get; set; }

        [DisplayName("Дата рождения")]
        public DateTime? Birth { get; set; }
        
        [MaxLength(200)]
        [DisplayName("Дополнительная информация")]
        public string AdditionalInformation { get; set; }
        
        [MaxLength(40)]
        public string Patronymic { get; set; }

        public AccountEditingViewModel()
        { }
        
        public AccountEditingViewModel(User user)
        {
            PhoneNumber = user?.PhoneNumber;
            Email = user?.Email;
            AdditionalInformation = user?.AdditionalInformation;
            Birth = user?.Birth;
            Patronymic = user?.Patronymic;
        }
    }
}