﻿using Microsoft.AspNetCore.Http;

namespace Science.Middlewares
{
    public abstract class Middleware
    {
        protected readonly RequestDelegate Next;
        protected Middleware(RequestDelegate next) => Next = next;
    }
}