﻿using Microsoft.AspNetCore.Builder;

namespace Science.Middlewares.TokenMiddleware
{
    public static class TokenMiddlewareExtensions
    {
        public static IApplicationBuilder UseToken(this IApplicationBuilder builder)
            => builder.UseMiddleware<TokenMiddleware>();
    }
}