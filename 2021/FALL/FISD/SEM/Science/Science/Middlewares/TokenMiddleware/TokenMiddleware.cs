﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Science.Models;
using Science.Services.Database.SessionDb;
using Science.Services.Database.UserDb;

namespace Science.Middlewares.TokenMiddleware
{
    public class TokenMiddleware : Middleware
    {
        public TokenMiddleware(RequestDelegate next) : base(next)
        { }

        public async Task InvokeAsync(HttpContext context, 
            IUserRepository userRepository, 
            ISessionRepository sessionRepository)
        {
            var token = context.Request.Cookies["token"];
            if (token is null)
            {
                var login = context.Session.GetString("login");
                if (login is not null)
                {
                    var user = await userRepository.FindUserByLoginAsync(login);
                    if (user is not null && IsValidUser(user)) context.Items.Add("user", user);
                }
            }
            else
            {
                var session = await sessionRepository.FindSessionByTokenAsync(token);
                if (session is not null && IsValidUser(session.User)) context.Items.Add("user", session.User);
            }
            
            await Next.Invoke(context);
        }

        private static bool IsValidUser(User user) => user.Status == Status.Active;
    }
}