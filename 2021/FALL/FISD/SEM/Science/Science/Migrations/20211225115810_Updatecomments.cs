﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Science.Migrations
{
    public partial class Updatecomments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReviewArticles_Articles_ArticleId",
                table: "ReviewArticles");

            migrationBuilder.DropForeignKey(
                name: "FK_ReviewArticles_Users_SenderId",
                table: "ReviewArticles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ReviewArticles",
                table: "ReviewArticles");

            migrationBuilder.RenameTable(
                name: "ReviewArticles",
                newName: "CommentArticles");

            migrationBuilder.RenameIndex(
                name: "IX_ReviewArticles_SenderId",
                table: "CommentArticles",
                newName: "IX_CommentArticles_SenderId");

            migrationBuilder.RenameIndex(
                name: "IX_ReviewArticles_ArticleId",
                table: "CommentArticles",
                newName: "IX_CommentArticles_ArticleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CommentArticles",
                table: "CommentArticles",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CommentArticles_Articles_ArticleId",
                table: "CommentArticles",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CommentArticles_Users_SenderId",
                table: "CommentArticles",
                column: "SenderId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentArticles_Articles_ArticleId",
                table: "CommentArticles");

            migrationBuilder.DropForeignKey(
                name: "FK_CommentArticles_Users_SenderId",
                table: "CommentArticles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CommentArticles",
                table: "CommentArticles");

            migrationBuilder.RenameTable(
                name: "CommentArticles",
                newName: "ReviewArticles");

            migrationBuilder.RenameIndex(
                name: "IX_CommentArticles_SenderId",
                table: "ReviewArticles",
                newName: "IX_ReviewArticles_SenderId");

            migrationBuilder.RenameIndex(
                name: "IX_CommentArticles_ArticleId",
                table: "ReviewArticles",
                newName: "IX_ReviewArticles_ArticleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReviewArticles",
                table: "ReviewArticles",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ReviewArticles_Articles_ArticleId",
                table: "ReviewArticles",
                column: "ArticleId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ReviewArticles_Users_SenderId",
                table: "ReviewArticles",
                column: "SenderId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
