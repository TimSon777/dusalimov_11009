﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Science.Migrations
{
    public partial class UpdateComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReviewArticles_Users_ModeratorId",
                table: "ReviewArticles");

            migrationBuilder.DropIndex(
                name: "IX_ReviewArticles_ModeratorId",
                table: "ReviewArticles");

            migrationBuilder.DropCheckConstraint(
                name: "CK_ReviewArticle_Rating",
                table: "ReviewArticles");

            migrationBuilder.DropColumn(
                name: "ModeratorId",
                table: "ReviewArticles");

            migrationBuilder.DropColumn(
                name: "Rating",
                table: "ReviewArticles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ModeratorId",
                table: "ReviewArticles",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<char>(
                name: "Rating",
                table: "ReviewArticles",
                type: "char(1)",
                nullable: false,
                defaultValue: ' ');

            migrationBuilder.CreateIndex(
                name: "IX_ReviewArticles_ModeratorId",
                table: "ReviewArticles",
                column: "ModeratorId");

            migrationBuilder.AddCheckConstraint(
                name: "CK_ReviewArticle_Rating",
                table: "ReviewArticles",
                sql: "\"Rating\" = '1' OR \"Rating\" = '2' OR \"Rating\" = '3' OR \"Rating\" = '4' OR \"Rating\" = '5'");

            migrationBuilder.AddForeignKey(
                name: "FK_ReviewArticles_Users_ModeratorId",
                table: "ReviewArticles",
                column: "ModeratorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
