﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Science.Migrations
{
    public partial class Creating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FirstName = table.Column<string>(type: "varchar(30)", nullable: false),
                    SecondName = table.Column<string>(type: "varchar(50)", nullable: false),
                    Patronymic = table.Column<string>(type: "varchar(40)", nullable: true),
                    Login = table.Column<string>(type: "varchar(30)", nullable: false),
                    Birth = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    HashedPassword = table.Column<string>(type: "varchar(255)", nullable: false),
                    Salt = table.Column<string>(type: "varchar(40)", nullable: false),
                    AdditionalInformation = table.Column<string>(type: "varchar(200)", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Role = table.Column<byte>(type: "smallint", nullable: false),
                    Email = table.Column<string>(type: "varchar(256)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "varchar(25)", nullable: true),
                    Registration = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AreaSciences",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PublisherId = table.Column<int>(type: "integer", nullable: false),
                    Title = table.Column<string>(type: "varchar(40)", nullable: false),
                    Description = table.Column<string>(type: "varchar(500)", nullable: true),
                    Text = table.Column<string>(type: "varchar(3000)", nullable: false),
                    ImageName = table.Column<string>(type: "varchar(100)", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaSciences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AreaSciences_Users_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PublisherId = table.Column<int>(type: "integer", nullable: false),
                    Title = table.Column<string>(type: "varchar(50)", nullable: false),
                    Description = table.Column<string>(type: "varchar(300)", nullable: true),
                    Text = table.Column<string>(type: "varchar(3000)", nullable: false),
                    ImageName = table.Column<string>(type: "varchar(100)", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Publication = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Articles_Users_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ScientificWorks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PublisherId = table.Column<int>(type: "integer", nullable: false),
                    Link = table.Column<string>(type: "varchar(100)", nullable: false),
                    Title = table.Column<string>(type: "varchar(100)", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScientificWorks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScientificWorks_Users_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Scientists",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PublisherId = table.Column<int>(type: "integer", nullable: true),
                    FirstName = table.Column<string>(type: "varchar(30)", nullable: false),
                    SecondName = table.Column<string>(type: "varchar(50)", nullable: false),
                    Patronymic = table.Column<string>(type: "varchar(40)", nullable: true),
                    Birth = table.Column<DateTime>(type: "date", nullable: false),
                    Death = table.Column<DateTime>(type: "date", nullable: false),
                    PhotoName = table.Column<string>(type: "varchar(100)", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Scientists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Scientists_Users_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Videos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PublisherId = table.Column<int>(type: "integer", nullable: true),
                    Title = table.Column<string>(type: "varchar(100)", nullable: false),
                    Description = table.Column<string>(type: "varchar(300)", nullable: true),
                    Link = table.Column<string>(type: "varchar(100)", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Publication = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Videos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Videos_Users_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReviewArticles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ModeratorId = table.Column<int>(type: "integer", nullable: false),
                    SenderId = table.Column<int>(type: "integer", nullable: false),
                    ArticleId = table.Column<int>(type: "integer", nullable: false),
                    Rating = table.Column<char>(type: "char(1)", nullable: false),
                    Text = table.Column<string>(type: "varchar(300)", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Publication = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewArticles", x => x.Id);
                    table.CheckConstraint("CK_ReviewArticle_Rating", "\"Rating\" = '1' OR \"Rating\" = '2' OR \"Rating\" = '3' OR \"Rating\" = '4' OR \"Rating\" = '5'");
                    table.ForeignKey(
                        name: "FK_ReviewArticles_Articles_ArticleId",
                        column: x => x.ArticleId,
                        principalTable: "Articles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReviewArticles_Users_ModeratorId",
                        column: x => x.ModeratorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReviewArticles_Users_SenderId",
                        column: x => x.SenderId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AreaScienceScientist",
                columns: table => new
                {
                    AreaSciencesId = table.Column<int>(type: "integer", nullable: false),
                    ScientistsId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaScienceScientist", x => new { x.AreaSciencesId, x.ScientistsId });
                    table.ForeignKey(
                        name: "FK_AreaScienceScientist_AreaSciences_AreaSciencesId",
                        column: x => x.AreaSciencesId,
                        principalTable: "AreaSciences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AreaScienceScientist_Scientists_ScientistsId",
                        column: x => x.ScientistsId,
                        principalTable: "Scientists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ScientificWorkScientist",
                columns: table => new
                {
                    ScientificWorksId = table.Column<int>(type: "integer", nullable: false),
                    ScientistsId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScientificWorkScientist", x => new { x.ScientificWorksId, x.ScientistsId });
                    table.ForeignKey(
                        name: "FK_ScientificWorkScientist_ScientificWorks_ScientificWorksId",
                        column: x => x.ScientificWorksId,
                        principalTable: "ScientificWorks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScientificWorkScientist_Scientists_ScientistsId",
                        column: x => x.ScientistsId,
                        principalTable: "Scientists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AreaSciences_PublisherId",
                table: "AreaSciences",
                column: "PublisherId");

            migrationBuilder.CreateIndex(
                name: "IX_AreaScienceScientist_ScientistsId",
                table: "AreaScienceScientist",
                column: "ScientistsId");

            migrationBuilder.CreateIndex(
                name: "IX_Articles_PublisherId",
                table: "Articles",
                column: "PublisherId");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewArticles_ArticleId",
                table: "ReviewArticles",
                column: "ArticleId");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewArticles_ModeratorId",
                table: "ReviewArticles",
                column: "ModeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_ReviewArticles_SenderId",
                table: "ReviewArticles",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_ScientificWorks_PublisherId",
                table: "ScientificWorks",
                column: "PublisherId");

            migrationBuilder.CreateIndex(
                name: "IX_ScientificWorkScientist_ScientistsId",
                table: "ScientificWorkScientist",
                column: "ScientistsId");

            migrationBuilder.CreateIndex(
                name: "IX_Scientists_PublisherId",
                table: "Scientists",
                column: "PublisherId");

            migrationBuilder.CreateIndex(
                name: "IX_Videos_PublisherId",
                table: "Videos",
                column: "PublisherId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AreaScienceScientist");

            migrationBuilder.DropTable(
                name: "ReviewArticles");

            migrationBuilder.DropTable(
                name: "ScientificWorkScientist");

            migrationBuilder.DropTable(
                name: "Videos");

            migrationBuilder.DropTable(
                name: "AreaSciences");

            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.DropTable(
                name: "ScientificWorks");

            migrationBuilder.DropTable(
                name: "Scientists");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
