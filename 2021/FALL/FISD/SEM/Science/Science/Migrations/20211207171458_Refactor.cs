﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Science.Migrations
{
    public partial class Refactor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProfilePictureName",
                table: "Users");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Publication",
                table: "Videos",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "ProfilePictureExtension",
                table: "Users",
                type: "varchar(10)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Publication",
                table: "Articles",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProfilePictureExtension",
                table: "Users");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Publication",
                table: "Videos",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AddColumn<string>(
                name: "ProfilePictureName",
                table: "Users",
                type: "varchar(100)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Publication",
                table: "Articles",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");
        }
    }
}
