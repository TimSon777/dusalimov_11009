﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Science.Migrations
{
    public partial class Refactornames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PathToImage",
                table: "Users",
                newName: "ProfilePictureName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProfilePictureName",
                table: "Users",
                newName: "PathToImage");
        }
    }
}
