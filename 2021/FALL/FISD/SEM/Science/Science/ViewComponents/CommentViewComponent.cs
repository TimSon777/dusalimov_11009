﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Science.Models;
using Science.Services.Database.ArticleReviewDb;

namespace Science.ViewComponents
{
    public class CommentViewComponent : ViewComponent
    {
        private readonly IArticleReviewRepository _articleReviewRepository;

        public CommentViewComponent(IArticleReviewRepository articleReviewRepository)
            => _articleReviewRepository = articleReviewRepository;

        public async Task<IViewComponentResult> InvokeAsync(CommentArticle comment, int skip, int take, Article article)
        {
            IEnumerable<CommentArticle> comments;
            
            if (comment is not null)
            {
                comments = new List<CommentArticle> { comment };
            }
            else
            {
                comments = await _articleReviewRepository.GetCollection(skip, take, article);
            }
            return View(comments);
        }
    }
}