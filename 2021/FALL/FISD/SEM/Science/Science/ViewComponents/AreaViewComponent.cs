﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Science.Services.Database.AreaScienceDb;

namespace Science.ViewComponents
{
    public class AreaViewComponent : ViewComponent
    {
        private readonly IAreaScienceRepository _areaScienceRepository;

        public AreaViewComponent(IAreaScienceRepository areaScienceRepository)
        {
            _areaScienceRepository = areaScienceRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync(int skip, int take)
        {
            var areas = await _areaScienceRepository.GetCollection(skip, take);
            return View(areas);
        }
    }
}