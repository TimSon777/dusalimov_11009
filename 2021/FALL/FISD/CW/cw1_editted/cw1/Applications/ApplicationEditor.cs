﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;

namespace cw1.Applications
{
    public class ApplicationEditor : Application
    {
        public ApplicationEditor(string url) : base(url)
        { }

        public override Result DistributeByTasks(HttpListenerRequest request)
        {
            var url = Url.Split("!", StringSplitOptions.RemoveEmptyEntries).Last();
            return url switch
            {
                "get_files_html" => GetResultFilesToEdit("html"),
                "get_files_css" => GetResultFilesToEdit("css"),
                "get_file" => GetFile(request),
                _ => WriteEditedFile(request, url)
            };
        }

        private static Result GetFile(HttpListenerRequest request)
        {
            var link = StreamHelper.GetDataFromRequest(request);
            var bytes = File.ReadAllBytes($"{Server.CurrentDirectory}/wwwroot/{link}");
            return new Result(bytes, 200);
        }

        private static Result GetResultFilesToEdit(string fileExtension)
        {
            var names = GetFilesName("files", fileExtension);
            var json = JsonSerializer.Serialize(names);
            var bytes = Encoding.Default.GetBytes(json);
            return new Result(bytes, 200);
        }

        private static Result WriteEditedFile(HttpListenerRequest request, string nameFile)
        {
            var text = StreamHelper.GetDataFromRequest(request);
            File.WriteAllText($"{Server.CurrentDirectory}/wwwroot/{nameFile}", text);
            return new Result(Array.Empty<byte>(), 200);
        }

        private static string[] GetFilesName(string nameDirectory, string pattern)
        {
            var files = Directory.EnumerateFiles($"{Server.CurrentDirectory}/wwwroot/{nameDirectory}", 
                $"*.{pattern}").ToArray();
            
            return files.Select(Path.GetFileName).ToArray();
        }
    }
}