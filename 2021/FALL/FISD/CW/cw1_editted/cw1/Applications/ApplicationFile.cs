﻿using System;
using System.IO;
using System.Net;

namespace cw1.Applications
{
    public class ApplicationFile : Application
    {
        public ApplicationFile(string url) : base(url)
        { }

        public override Result DistributeByTasks(HttpListenerRequest request) => GetFileResult();
        
        private Result GetFileResult()
        {
            var pathToFile = Path.Combine(Server.CurrentDirectory, "wwwroot", Url);

            if (!File.Exists(pathToFile)) return new Result(Array.Empty<byte>(), 404);
            
            var bytes = File.ReadAllBytes(pathToFile);
            return new Result(bytes, 200);
        }
    }
}