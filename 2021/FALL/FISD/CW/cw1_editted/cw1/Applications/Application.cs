﻿using System.Net;

namespace cw1.Applications
{
    public abstract class Application
    {
        protected readonly string Url;

        protected Application(string url)
        {
            Url = url;
        }

        public abstract Result DistributeByTasks(HttpListenerRequest request);
    }
}