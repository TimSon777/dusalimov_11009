﻿using System;
using System.Net;
using cw1.Applications;

namespace cw1
{
    public static class Distributor
    {
        public static Result Distribute(HttpListenerRequest request)
        {
            var url = request.Url?.AbsolutePath;
            if (string.IsNullOrEmpty(url) || url == "/") url = "/index.html";
            if (!char.IsUpper(url[1])) url = $"/File!{url}";
            
            var splitUrl = url.Split(new []{"/", "!"},2, StringSplitOptions.RemoveEmptyEntries);
            var application = splitUrl[0];
            var action = splitUrl[1];

            return application switch
            {
                "File" => new ApplicationFile(action).DistributeByTasks(request),
                "Editor" => new ApplicationEditor(action).DistributeByTasks(request),
                _ => throw new NotImplementedException()
            };
        }
    }
}