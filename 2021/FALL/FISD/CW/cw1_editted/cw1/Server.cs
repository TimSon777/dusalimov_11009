﻿using System;
using System.IO;
using System.Net;

namespace cw1
{
    public static class Server
    {
        public static void Start()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();

            while (true)
            {
                var ctx = listener.GetContext();
                var request = ctx.Request;
                var response = ctx.Response;

                var result = Distributor.Distribute(request);
                
                StreamHelper.SendResponse(result, response);
            }
            // ReSharper disable once FunctionNeverReturns
        }
        
        public static readonly string CurrentDirectory = FindDirectoryNet5();

        private static string FindDirectoryNet5()
        {
            var directoryInfo = Directory.GetParent(Environment.CurrentDirectory)?.Parent;
            return directoryInfo?.Parent?.ToString();
        }
    }
}