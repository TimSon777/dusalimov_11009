function establishLastFiles() {
    let linkOfLastActionCss = getCookie('lastCss')
    if (linkOfLastActionCss) {
        loadFileToEditor(linkOfLastActionCss)
        lastLinkCss = linkOfLastActionCss
    }
    
    let linkOfLastActionHtml = getCookie('lastHtml')
    if (linkOfLastActionHtml) {
        loadFileToEditor(linkOfLastActionHtml)
        lastLinkHtml = linkOfLastActionHtml
    }
}

$(document).ready(function () {
    establishLastFiles()
    
    $('#get_files_html').click(function () { getFiles('html') })
    $('#get_files_css').click(function () { getFiles('css') })
    $('#save_html').click(function () { save('html', lastLinkHtml) })
    $('#save_css').click(function () { save('css', lastLinkCss) })

    //HIDE SO THAT IT IS NOT POSSIBLE TO APPLY TAGS TO CSS
    $('.editor_css').click(function () { $('.sample-toolbar').hide() })
    $('.editor_html').click(function () { $('.sample-toolbar').show() })
})