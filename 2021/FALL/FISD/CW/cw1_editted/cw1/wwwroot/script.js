window.addEventListener('load', function() {
	document.getElementById('sampleeditor').setAttribute('contenteditable', 'true')
	document.getElementById('sampleeditor2').setAttribute('contenteditable', 'true')
})

function format(command, value) {
	document.execCommand(command, false, value)
}

function setUrl() {
	let url = document.getElementById('txtFormatUrl').value
	let sText = document.getSelection()
	document.execCommand('insertHTML', false, '<a href="' + url + '" target="_blank">' + sText + '</a>')
	document.getElementById('txtFormatUrl').value = ''
}

function loadFileToEditor(link) {
	let isHtml = link.indexOf('html') !== -1
	let extension = isHtml ? 'html' : 'css'

	if (isHtml) {
		lastLinkHtml = link
		setCookie('lastHtml', link, 180000, "/")
	}
	else {
		lastLinkCss = link
		setCookie('lastCss', link, 180000, "/")
	}

	$.ajax({
		url:'Editor!get_file',
		method: 'post',
		data: isHtml ? lastLinkHtml : lastLinkCss,
		success: function (data) {
			$(`.editor_${extension}`).html(data)
		}
	})

	return false
}

let lastLinkHtml = "";
let lastLinkCss = "";

//GET LIST OF FILE A CERTAIN TYPE
function getFiles(fileExtension) {
	$.ajax({
		url: `Editor!get_files_${fileExtension}`,
		success: function (data) {
			let links = JSON.parse(data);
			let result = ""
			
			for (let i = 0; i < links.length; i++)
				result += `<li><a href="files/${links[i]}" class="links">${links[i]}</a></li>`
			
			$(`#editor_list_files_${fileExtension}`).html(result)

			$('.links').click(function () {
				loadFileToEditor($(this).attr('href'))
				return false
			})
		}
	})
}

//REQUEST TO THE SERVER TO SAVE THE FILE
function save(fileExtension, link) {
	if (link === "") return
	$.ajax({
		url: `Editor!save!${link}`,
		method: 'post',
		data: fileExtension === 'html' ? $(`.editor_html`).html() : $(`.editor_css`).html(),
		datatype: fileExtension === fileExtension ? 'html' : 'css'
	})
}

