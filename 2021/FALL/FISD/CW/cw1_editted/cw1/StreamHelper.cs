﻿using System.IO;
using System.Net;

namespace cw1
{
    public static class StreamHelper
    {
        public static void SendResponse(Result result, HttpListenerResponse response)
        {
            response.StatusCode = result.StatusCode;
            var sw = response.OutputStream;
            response.ContentLength64 = result.Bytes.Length;
            sw.Write(result.Bytes, 0, result.Bytes.Length);
            sw.Close();
        }
        
        public static string GetDataFromRequest(HttpListenerRequest request)
        {
            var inputStream = request.InputStream;
            var encoding = request.ContentEncoding;
            var streamReader = new StreamReader(inputStream, encoding);
            return streamReader.ReadToEnd();
        }
    }
}