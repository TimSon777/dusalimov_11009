﻿namespace cw1
{
    public record Result(byte[] Bytes, int StatusCode);
}