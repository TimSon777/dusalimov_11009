window.addEventListener('load', function(){
			document.getElementById('sampleeditor').setAttribute('contenteditable', 'true');
			document.getElementById('sampleeditor2').setAttribute('contenteditable', 'true');
        });

		function format(command, value) {
			document.execCommand(command, false, value);
		}

		function setUrl() {
			var url = document.getElementById('txtFormatUrl').value;
			var sText = document.getSelection();
			document.execCommand('insertHTML', false, '<a href="' + url + '" target="_blank">' + sText + '</a>');
			document.getElementById('txtFormatUrl').value = '';
		}

		var lastLink = ""
$(document).ready(function () {

	
	$('#get_files').click(function () {
		$.ajax({
			url: 'get_files',
			success: function (data) {
				let a = JSON.parse(data);
				for (let i = 0; i < a.length; i++)
					$('#table').append('<tr><td>'+ `<a href="files/${a[i]}" class="links">${a[i]}</a>`  + '</td></tr>')

				$('.links').click(function () {
					let link = $(this).attr('href')
					lastLink = $(this).attr('href')
					$('#container').load(link, function () {

					})

					return false
				})
			}
		});
	})
	
	$('#save').click(function () {
		$.ajax({
			url: `save!${lastLink}`,
			method: 'post'
		})
	})
})