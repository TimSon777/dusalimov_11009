﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace cw1
{
    class Program
    {
        public string LastFile { get; set; }
        static void Main(string[] args)
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();

            while (true)
            {
                var ctx = listener.GetContext();
                var request = ctx.Request;
                var response = ctx.Response;
                
                var path = request.Url?.AbsolutePath;
            
                if (path is null or "/") path = "/index.html";

                var pathToFile = Path + "/kr" + path;
                
                if (File.Exists(pathToFile))
                {
                    response.StatusCode = 200;
                    var sw = response.OutputStream;
                    var bytes = File.ReadAllBytes(pathToFile);
                    response.ContentLength64 = bytes.Length;
                    sw.Write(bytes, 0, bytes.Length);
                    sw.Close();
                }
                else if (path == "/get_files")
                {
                    var files = Directory.EnumerateFiles(Path + "/kr" + "/files").ToArray();
                    var names = files.Select(System.IO.Path.GetFileName).ToArray();
                    var json = JsonSerializer.Serialize(names);
                    var bytes = Encoding.Default.GetBytes(json);
                    response.StatusCode = 200;
                    var sw = response.OutputStream;
                    response.ContentLength64 = bytes.Length;
                    sw.Write(bytes, 0, bytes.Length);
                    sw.Close();
                    
                }
                else if (path[..5] == "/save")
                {
                    //немного осталось считать текст с сайта
                    var link = path.Split("!")[1];
                    var inputStream = request.InputStream;
                    var encoding = request.ContentEncoding;
                    var streamReader = new StreamReader(inputStream, encoding);
                    var text = streamReader.ReadToEnd().Split("ul")[1];
                    File.WriteAllText(link, text);
                }
                else
                {
                    response.StatusCode = 404;
                    var sw = response.OutputStream;
                    response.ContentLength64 = 0;
                    sw.Write(Array.Empty<byte>(), 0, 0);
                    sw.Close();
                }
            }
        }

        private static string Path { get; } = FindDirectoryNet5();
        
        public static string FindDirectoryNet5()
        {
            var directoryInfo = Directory.GetParent(Environment.CurrentDirectory)?.Parent;
            return directoryInfo?.Parent?.ToString();
        }
    }
}