﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Sem2
{
    public class OrthogonalList : IEnumerable<Vertex>
    {
        public OrthogonalList()
        {
            Head = null;
            CountVertices = 0;
            CountEdges = 0;
        }

        public Dictionary<int, int> IndexesKeys;
        public int[,] ShortestPaths;

        public bool IsEmpty => CountVertices == 0;
        private Vertex Head { get; set; }
        private Vertex Tail { get; set; }
        public int CountVertices { get; private set; }
        public int CountEdges { get; private set; }
        
        /// <summary>Finds a vertex with a given key. If there is no such vertex, it returns null</summary>
        public Vertex SearchNode(int key) 
            => this.FirstOrDefault(current => current.Key == key);

        /// <summary>Adds a vertex with a given key to the graph </summary>
        public void AddVertex(int key)
        {
            ShortestPaths = null;
            if (IsEmpty) Head = Tail = new Vertex(key);
            else
            {
                var vertex = new Vertex(key);
                Tail.Next = vertex;
                Tail = vertex;
            }
            CountVertices++;
        }
        
        /// <summary>Adds an edge from the first vertex to the second with weight</summary>
        /// <exception cref="ArgumentException">Vertex does not exist</exception>
        /// <exception cref="ArgumentException">Edge already exists</exception>
        public void AddEdge(int firstKey, int secondKey, int weight)
        {
            var firstVertex = SearchNode(firstKey);
            if (firstVertex == null) throw new ArgumentException("First vertex does not exist");
            var secondVertex = SearchNode(secondKey);
            if (secondVertex == null) throw new ArgumentException("Second vertex does not exist");
            
            if (firstVertex.Any(edge => edge.Key == secondKey)) throw new ArgumentException("Such an edge already exists");
            
            if (firstVertex.FirstEdge == null)
            {
                firstVertex.FirstEdge = firstVertex.Tail = new Edge(secondKey, weight);
            }
            else
            {
                var edge = new Edge(secondKey, weight);
                firstVertex.Tail.Next = edge;
                firstVertex.Tail = edge;
            }

            firstVertex.CountEdges++;
            CountEdges++;
        }
        
        /// <summary>Removes an edge between two vertices</summary>
        /// <exception cref="ArgumentException">Vertex does not exist</exception>
        /// <exception cref="ArgumentException">No such edge exists</exception>
        public void RemoveEdge(int firstKey, int secondKey)
        {
            var firstVertex = SearchNode(firstKey);
            if (firstVertex == null) throw new ArgumentException("First vertex does not exist");
            var current = firstVertex.FirstEdge;
            if (current == null) throw new ArgumentException("No such edge exists");

            if (current.Key == secondKey)
            {
                firstVertex.CountEdges--;
                CountEdges--;
                firstVertex.FirstEdge = current.Next;
                return;
            }
            
            while (current.Next.Key != secondKey)
            {
                current = current.Next;
                if (current == null) throw new ArgumentException("No such edge exists");
            }

            firstVertex.CountEdges--;
            CountEdges--;
            current.Next = current.Next.Next;
        }
        
        /// <summary>Removes a vertex and all edges containing that vertex</summary>
        /// <exception cref="ArgumentException">Vertex does not exist</exception>
        public void RemoveVertex(int key)
        {
            RemoveVertexPrivate(key);
            foreach (var vertex in this)
                if (vertex.CountEdges != 0)
                    RemoveEdgePrivate(vertex, key);
        }
        
        private void RemoveVertexPrivate(int key)
        {
            var current = Head;
            if (current == null) throw new ArgumentException("Vertex does not exist");
            if (current.Key == key && CountVertices == 1)
            {
                Head = null;
                CountEdges = 0;
                CountVertices = 0;
                return;
            }

            if (current.Key == key)
            {
                CountEdges -= current.CountEdges;
                CountVertices--;
                Head = current.Next;
                return;
            }

            foreach (var vertex in this)
            {
                if (vertex.Next.Key != key) continue;
                CountVertices--;
                CountEdges -= vertex.Next.CountEdges;
                vertex.Next = vertex.Next.Next;
                break;
            }

            ShortestPaths = null;
        }
        
        private void RemoveEdgePrivate(Vertex vertex, int edgeKey)
        {
            var current = vertex?.FirstEdge;
            if (current == null) return;

            if (current.Key == edgeKey)
            {
                vertex.CountEdges--;
                CountEdges--;
                vertex.FirstEdge = current.Next;
                return;
            }
            
            while (current.Next.Key != edgeKey)
            {
                current = current.Next;
                if (current == null) return;
            }

            vertex.CountEdges--;
            CountEdges--;
            current.Next = current.Next.Next;
        }

        /// <summary>
        /// Takes as input a sequence of vertices and a sequence of edges: first vertex, second vertex and weight between them
        /// </summary>
        /// <exception cref="ArgumentException">Trying to create a list when there are already vertices in it</exception>
        public void MakeGraph(IEnumerable<int> keys, IEnumerable<Tuple<int, int, int>> edges)
        {
            if (!IsEmpty) throw new ArgumentException("An orthogonal list can be made if there are no vertices in it yet");
            IndexesKeys = new Dictionary<int, int>();
            foreach (var key in keys)
                AddVertex(key);
            
            foreach (var (firstKey, secondKey, weight) in edges)
                AddEdge(firstKey, secondKey, weight);
        }

        public IEnumerator<Vertex> GetEnumerator()
        {
            var current = Head;
            while (current != null)
            {
                yield return current;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        
        public Vertex this[int index]
        {
            get
            {
                if (index < 0 || index >= CountVertices) throw new IndexOutOfRangeException();
                if (index == CountVertices - 1) return Tail;
                var current = Head;
                for (var i = 0; i < index; i++) current = current.Next;
                return current;
            }
        }
    }
}