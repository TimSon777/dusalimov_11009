﻿using System.Collections;
using System.Collections.Generic;

namespace Sem2
{
    public class Vertex : IEnumerable<Edge>
    {
        public Vertex(int key)
        {
            Key = key;
            Next = null;
        }
        public int Key { get; }
        public Edge Tail { get; set; }
        public Vertex Next { get; set; }
        public Edge FirstEdge { get; set; }
        public int CountEdges { get; set; }
        public IEnumerator<Edge> GetEnumerator()
        {
            var current = FirstEdge;
            while (current != null)
            {
                yield return current;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public override int GetHashCode() => unchecked(Key * 1001 ^ Key);
    }
}