﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

namespace Sem2
{
    public class GenerationInputData
    {
        
        /// <summary>Generate input</summary>
        public GenerationInputData(int countVertices, int countEdges, int maxWeight)
        {
            this.countVertices = countVertices;
            this.maxWeight = maxWeight;
            this.countEdges = countEdges;
        }
        
        private Random random; 
        private readonly FileInfo file = new FileInfo("InputData.txt");
        private readonly int countVertices;
        private readonly int maxWeight;
        private readonly int countEdges;
        private void GenerateInputData()
        {
            random = new Random();
            var edges = new List<Tuple<int, int, int>>();
            using var streamWriter = file.CreateText();
            for (var i = 0; i < countVertices; i++)
                streamWriter.WriteLine(i);
            for (var i = 0; i < countVertices; i++)
            {
                for (var j = 0; j < countVertices; j++)
                {
                    if (i == j) continue;
                    edges.Add(Tuple.Create(i, j, random.Next(0, maxWeight)));
                }
            }

            var edgesNew = ShuffleArrayElements(edges.ToArray());
            foreach (var (firstVertex, secondVertex, weight) in edgesNew)
            {
                streamWriter.WriteLine(firstVertex);
                streamWriter.WriteLine(secondVertex);
                streamWriter.WriteLine(weight);
            }
        }

        /// <summary>Randomly shuffle the array</summary>
        private IList<T> ShuffleArrayElements<T>(IList<T> array)
        {
            random = new Random();
            for (var i = 0; i < array.Count; i++)
            {
                var j = random.Next(0, array.Count - 1);
                var temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }

            return array;
        }
        
        /// <summary>Reads input</summary>
        public (int[], Tuple<int, int, int>[]) ReadInputData()
        {
            GenerateInputData();
            using var streamReader = new StreamReader("InputData.txt");
            var vertices = new int[countVertices];
            var edges = new Tuple<int, int, int>[countEdges];
            var firstEdges = new int[countEdges];
            var secondEdges = new int[countEdges];
            var weights = new int[countEdges];
            for (var i = 0; i < countVertices; i++)
                vertices[i] = int.Parse(streamReader.ReadLine() ?? string.Empty);
            for (var i = 0; i < countEdges; i++)
            {
                firstEdges[i] = int.Parse(streamReader.ReadLine() ?? string.Empty);
                secondEdges[i] = int.Parse(streamReader.ReadLine() ?? string.Empty);
                weights[i] = int.Parse(streamReader.ReadLine() ?? string.Empty);
                edges[i] = Tuple.Create(firstEdges[i], secondEdges[i], weights[i]);
            }

            return (vertices, edges);
        }
    }
}