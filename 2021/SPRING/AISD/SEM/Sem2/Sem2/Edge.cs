﻿namespace Sem2
{
    public class Edge
    {
        public Edge(int key, int weight)
        {
            Key = key;
            Weight = weight;
        }
        public int Key { get; }
        public int Weight { get; set; }
        public Edge Next { get; set; }

        public override int GetHashCode() => unchecked(Key * 73 ^ Key);
        
    }
}