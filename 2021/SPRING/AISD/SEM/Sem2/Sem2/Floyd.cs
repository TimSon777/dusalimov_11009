﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Sem2
{
    public static class Floyd
    {
        /// <summary>Returns matrices of shortest distances and shortest paths</summary>
        /// <exception cref="ArgumentException">Matrix must be square</exception>
        /// <exception cref="ArgumentException">Wrong matrix fed: nonzero numbers found on the main diagonal</exception>
        public static int[,] Algorithm(int[,] matrix)
        {
            
            if (matrix.GetLength(0) != matrix.GetLength(1)) throw new ArgumentException("Matrix must be square");
            for (var i = 0; i < matrix.GetLength(0); i++)
                if (matrix[i, i] != 0)
                    throw new ArgumentException("There must be zeros on the main diagonal");
            
            var size = matrix.GetLength(0);
            var shortestPaths = new int[size, size];
            
            for (var i = 0; i < size; i++)
            for (var j = 0; j < size; j++)
            for (var k = 0; k < size; k++)
            {
                if (matrix[i, j] * matrix[j, k] == 0) continue;
                var currentDistance = matrix[i, j] + matrix[j, k];
                if (i != k && k != j && i != j && matrix[i, k] == 0)
                {
                    matrix[i, k] = currentDistance;
                    shortestPaths[i, k] = j;
                    continue;
                }
                if (currentDistance >= matrix[i, k]) continue;
                matrix[i, k] = currentDistance;
                shortestPaths[i, k] = j;
            }

            return shortestPaths;
        }
        
        /// <summary>Returns the shortest path between two vertices, including themselves</summary>
        /// <exception cref="ArgumentException">Matrix must be square</exception>
        /// <exception cref="ArgumentException">Vertex does not exist</exception>
        /// <exception cref="ArgumentException">Wrong matrix fed</exception>
        public static IEnumerable<int> GetPath(int[,] matrix, int firstVertex, int secondVertex)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1)) throw new ArgumentException("Matrix must be square");
            for (var i = 0; i < matrix.GetLength(0); i++)
                if (matrix[i, i] != 0)
                    throw new ArgumentException("There must be zeros on the main diagonal");
            if (firstVertex < 0 || firstVertex >= matrix.GetLength(0)) throw new ArgumentException("No such vertex exists");
            if (secondVertex < 0 || secondVertex >= matrix.GetLength(0)) throw new ArgumentException("No such vertex exists");

            var result = new List<int>();
            var currentVertex = matrix[firstVertex, secondVertex];
            var countVerticesInPath = 2;
            while (currentVertex != 0)
            {
                result.Add(currentVertex);
                countVerticesInPath++;
                currentVertex = matrix[firstVertex, currentVertex];
                if (countVerticesInPath == matrix.GetLength(0) + 1) throw new ArgumentException("Wrong matrix fed");
            }

            result.Reverse();
            return result;
        }

        /// <summary>Returns the shortest path between two vertices, including themselves</summary>
        /// <exception cref="ArgumentException">Vertex does not exist</exception>
        /// <exception cref="ArgumentException">Wrong list fed</exception>
        public static List<int> GetPath(OrthogonalList list, int firstVertex, int secondVertex)
        {
            var firstIndex = list.IndexesKeys[firstVertex];
            var secondIndex = list.IndexesKeys[secondVertex];
            if (firstIndex < 0 || firstIndex >= list.CountVertices) throw new ArgumentException("No such vertex exists");
            if (secondIndex < 0 || secondIndex >= list.CountVertices) throw new ArgumentException("No such vertex exists");

            var result = new List<int>();
            var currentVertex = list.ShortestPaths[firstIndex, secondIndex];
            var countVerticesInPath = 2;
            while (currentVertex != -1)
            {
                result.Add(currentVertex);
                countVerticesInPath++;
                currentVertex = list.ShortestPaths[firstIndex, list.IndexesKeys[currentVertex]];
                if (countVerticesInPath == list.CountVertices + 1) throw new ArgumentException("Wrong list fed");
            }

            result.Reverse();
            return result;
        }

        /// <summary>Modifies the list, finding shortest paths and shortest distances between vertices</summary>
        /// <exception cref="ArgumentException">A graph cannot have as many edges</exception>
        public static void Algorithm(OrthogonalList list)
        {
            if (list.CountEdges > list.CountVertices * (list.CountVertices - 1))
                throw new ArgumentException("A graph cannot have as many edges");

            list.ShortestPaths = new int[list.CountVertices, list.CountVertices];
            for (var k = 0; k< list.CountVertices; k++)
            for (var j = 0; j < list.CountVertices; j++)
                list.ShortestPaths[k, j] = -1;
            var i = 0;

            foreach (var vertex in list)
                list.IndexesKeys[vertex.Key] = i++;

            foreach (var vertex1 in list)
            {
                foreach (var edge12 in vertex1)
                {
                    foreach (var vertex2 in list)
                    {
                        if (vertex1.Key == vertex2.Key || edge12.Key != vertex2.Key) continue;
                        foreach (var edge23 in vertex2)
                        {
                            if (edge23.Key == vertex1.Key) continue;
                            var isEdge13Exists = vertex1.Any(edge => edge.Key == edge23.Key);
                            if (isEdge13Exists)
                            {
                                var edge13 = vertex1.First(edge => edge.Key == edge23.Key);
                                if (edge13.Weight <= edge12.Weight + edge23.Weight) continue;
                                edge13.Weight = edge12.Weight + edge23.Weight;
                            }
                            else list.AddEdge(vertex1.Key, edge23.Key, edge12.Weight + edge23.Weight);
                            var firstIndex = list.IndexesKeys[vertex1.Key];
                            var secondIndex = list.IndexesKeys[edge23.Key];
                            list.ShortestPaths[firstIndex, secondIndex] = edge12.Key;
                        }
                    }
                }
            }
        }
    }
}