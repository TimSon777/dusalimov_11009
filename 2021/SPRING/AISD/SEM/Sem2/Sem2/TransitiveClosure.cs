﻿using System;
using System.Linq;

namespace Sem2
{
    public static class TransitiveClosure
    {
        /// <summary>Returns the transitive closure matrix</summary>
        /// <exception cref="ArgumentException">Matrix must be square</exception>
        public static bool[,] Algorithm(int[,] matrix)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1)) throw new ArgumentException("Matrix must be square");
            var size = matrix.GetLength(0);
            var result = new bool[size, size];
            for (var x = 0; x < size; x++)
            for (var y = 0; y < size; y++)
            {
                if (matrix[x, y] != 0) result[x, y] = true;

            }

            for (var i = 0; i < size; i++)
            for (var j = 0; j < size; j++)
            for (var k = 0; k < size; k++)
            {
                if (result[j, k] || !result[j, i] || !result[i, k]) continue;
                result[j, k] = true;
            }

            return result;
        }

        /// <summary>Modifies the list by applying the transitive closure algorithm to it</summary>
        /// <exception cref="ArgumentException">A graph cannot have as many edges</exception>
        public static void Algorithm(OrthogonalList list)
        {
            if (list.CountEdges > list.CountVertices * (list.CountVertices - 1))
                throw new ArgumentException("A graph cannot have as many edges");
            
            foreach (var vertex1 in list)
            {
                foreach (var edge12 in vertex1)
                {
                    foreach (var vertex2 in list)
                    {
                        if (vertex1.Key == vertex2.Key || edge12.Key != vertex2.Key) continue;
                        foreach (var edge23 in vertex2)
                        {
                            var isEdge13Exists = vertex1.Any(edge => edge.Key == edge23.Key);
                            if (isEdge13Exists) continue;
                            list.AddEdge(vertex1.Key, edge23.Key, 1);
                        }
                    }
                }
            }
        }
    }
}