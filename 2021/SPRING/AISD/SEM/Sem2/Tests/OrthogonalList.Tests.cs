﻿using System;
using NUnit.Framework;
using Sem2;

namespace Tests
{
    public class AddVertexTests
    {
        private Random random;
        [Test]
        public void AddThreeVertices()
        {
            var list = new OrthogonalList();
            list.AddVertex(0);
            list.AddVertex(1);
            list.AddVertex(2);
            for (var i = 0; i < 3; i++)
                Assert.AreEqual(i, list[i].Key);
        }

        [Test]
        public void AddFiveVertices()
        {
            var list = new OrthogonalList();
            list.AddVertex(100);
            list.AddVertex(200);
            list.AddVertex(300);
            list.AddVertex(400);
            list.AddVertex(500);
            for (var i = 1; i < 6; i++)
                Assert.AreEqual(i * 100, list[i - 1].Key);
        }

        [Test]
        public void AddOneHundredVerticesInAscendingOrder()
        {
            var list = new OrthogonalList();
            const int count = 100;
            for (var i = 0; i < count; i++)
                list.AddVertex(i);
            for (var i = 0; i < count; i++)
                Assert.AreEqual(i, list[i].Key);
        }

        [Test]
        public void AddOneThousandRandomVertices()
        {
            random = new Random();
            const int count = 1000;
            var list = new OrthogonalList();
            var checkArray = new int [count];
            for (var i = 0; i < count; i++)
            {
                var temp = random.Next();
                list.AddVertex(temp);
                checkArray[i] = temp;
            }
            
            for (var i = 0; i < count; i++)
                Assert.AreEqual(checkArray[i], list[i].Key);
        }
    }

    public class AddEdgesTests
    {
        private Random random;
        [Test]
        public void ConnectedGraphFiveVertices()
        {
            random = new Random();
            const int size = 5;
            var vertices = new int[size];
            for (var i = 0; i < size; i++)
                vertices[i] = random.Next(0, 10000000);
            var list = new OrthogonalList();
            for (var i = 0; i < size; i++)
                list.AddVertex(vertices[i]);
            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    if (i == j) continue;
                    list.AddEdge(vertices[i], vertices[j], random.Next(0, 100));
                }
            }

            foreach (var vertex in list)
                Assert.AreEqual(size - 1, vertex.CountEdges);
        }

        [Test]
        public void ThreeVerticesThreeEdges()
        {
            var graph = new OrthogonalList();
            graph.AddVertex(1);
            graph.AddVertex(2);
            graph.AddVertex(3);
            graph.AddEdge(1, 2, 100);
            graph.AddEdge(1, 3, 100);
            graph.AddEdge(3, 2, 100);
            Assert.AreEqual(2, graph[0].FirstEdge.Key);
            Assert.AreEqual(3, graph[0].FirstEdge.Next.Key);
            Assert.AreEqual(2, graph[2].FirstEdge.Key);
        }

        [Test]
        public void FiveVerticesOneEdge()
        {
            var graph = new OrthogonalList();
            for (var i = 0; i < 5; i++)
                graph.AddVertex(i);
            graph.AddEdge(3, 4, 777);
            Assert.AreEqual(4, graph[3].FirstEdge.Key);
            Assert.AreEqual(1, graph.CountEdges);
            Assert.AreEqual(1, graph[3].CountEdges);
            for (var i = 0; i < 5 && i != 3; i++)
                Assert.AreEqual(0, graph[i].CountEdges);
        }
    }

    public class MakeGraphTests
    {
        private Random random;
        [Test]
        public void ThreeVerticesFiveEdges()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{0, 1, 2},
                new []
                {
                    Tuple.Create(0, 2, 100),
                    Tuple.Create(0, 1, 100),
                    Tuple.Create(1, 2, 100),
                    Tuple.Create(1, 0, 100),
                    Tuple.Create(2, 1, 100),
                });
            Assert.AreEqual(3, graph.CountVertices);
            Assert.AreEqual(5, graph.CountEdges);
            Assert.AreEqual(2, graph[0].CountEdges);
            Assert.AreEqual(2, graph[1].CountEdges);
            Assert.AreEqual(1, graph[2].CountEdges);
        }
        
        [Test]
        public void FiveVerticesTwoEdges()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{0, 1, 2, 3, 4},
                new []
                {
                    Tuple.Create(0, 4, 747),
                    Tuple.Create(3, 2, 1337) 
                });
            Assert.AreEqual(2, graph.CountEdges);
            Assert.AreEqual(5, graph.CountVertices);
            Assert.AreEqual(747, graph[0].FirstEdge.Weight);
        }

        [Test]
        public void FiveRandomVerticesFourEdges()
        {
            random = new Random();
            var graph = new OrthogonalList();
            const int countVertices = 5;
            var vertices = new int[countVertices];
            for (var i = 0; i < countVertices; i++)
                vertices[i] = random.Next(0, 1000000);
            graph.MakeGraph(new []
            {
                vertices[0],
                vertices[1],
                vertices[2],
                vertices[3],
                vertices[4],
                
            }, new []
            {
                Tuple.Create(vertices[1], vertices[2], random.Next(0, 10000)),
                Tuple.Create(vertices[1], vertices[3], random.Next(0, 10000)),
                Tuple.Create(vertices[2], vertices[1], random.Next(0, 10000)),
                Tuple.Create(vertices[4], vertices[0], random.Next(0, 10000)),
            });
            
            Assert.AreEqual(5, graph.CountVertices);
            Assert.AreEqual(4, graph.CountEdges);
            Assert.AreEqual(2, graph[1].CountEdges);
            Assert.AreEqual(true, graph[1].FirstEdge.Weight <= 10000);
        }
    }

    public class RemoveEdgesTests
    {
        [Test]
        public void RemoveOneEdge()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{100, 200, 300}, new []
            {
                Tuple.Create(100, 200, 1000),
                Tuple.Create(300, 200, 100)
            });
            graph.RemoveEdge(100, 200);
            Assert.AreEqual(1, graph.CountEdges);
        }

        [Test]
        public void RemoveAllEdges()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{100, 200, 300}, new []
            {
                Tuple.Create(100, 200, 1000),
                Tuple.Create(100, 300, 1010),
                Tuple.Create(200, 100, 10220),
                Tuple.Create(200, 300, 1500),
                Tuple.Create(300, 100, 1600),
                Tuple.Create(300, 200, 1070)
            });
            graph.RemoveEdge(100, 200);
            graph.RemoveEdge(100, 300);
            graph.RemoveEdge(200, 100);
            graph.RemoveEdge(200, 300);
            graph.RemoveEdge(300, 100);
            graph.RemoveEdge(300, 200);
            Assert.AreEqual(0, graph.CountEdges);
            Assert.AreEqual(0, graph[0].CountEdges);
            Assert.AreEqual(0, graph[1].CountEdges);
            Assert.AreEqual(0, graph[2].CountEdges);
        }

        [Test]
        public void RemoveLastEdgeInVertex()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{100, 1500, 0}, new []
            {
                Tuple.Create(100, 1500, 1000),
                Tuple.Create(100, 0, 1010),
            });
            graph.RemoveEdge(100, 0);
            Assert.AreEqual(1, graph[0].CountEdges);
            Assert.AreEqual(1500, graph[0].FirstEdge.Key);
        }

        [Test]
        public void RemoveFirstEdgeInVertex()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{100, 1500, 0, 3}, new []
            {
                Tuple.Create(100, 1500, 1000),
                Tuple.Create(100, 0, 1010),
                Tuple.Create(100, 3, 90),
            });
            graph.RemoveEdge(100, 1500);
            Assert.AreEqual(2, graph[0].CountEdges);
            Assert.AreEqual(0, graph[0].FirstEdge.Key);
            Assert.AreEqual(3, graph[0].FirstEdge.Next.Key);
        }
    }

    public class RemoveVertices
    {
        [Test]
        public void RemoveOneVertex()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{100, 1500, 0, 3}, new []
            {
                Tuple.Create(100, 1500, 1000),
                Tuple.Create(100, 0, 1010),
                Tuple.Create(100, 3, 90),
            });
            graph.RemoveVertex(100);
            Assert.AreEqual(3, graph.CountVertices);
            Assert.AreEqual(0, graph.CountEdges);
        }
        
        [Test]
        public void RemoveVertexInGraphWhereOneVertex()
        {
            var graph = new OrthogonalList();
            graph.AddVertex(777);
            graph.RemoveVertex(777);
            Assert.AreEqual(0, graph.CountEdges);
            Assert.AreEqual(0, graph.CountVertices);
        }

        [Test]
        public void RemoveTwoVertices()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{1, 2, 3, 4}, new []
            {
                Tuple.Create(1, 2, 45),
                Tuple.Create(1, 3, 440),
                Tuple.Create(1, 4, 3),
                Tuple.Create(2, 1, 67),
                Tuple.Create(2, 3, 11),
                Tuple.Create(2, 4, 1),
                Tuple.Create(3, 1, 99),
                Tuple.Create(3, 2, 100),
                Tuple.Create(3, 4, 300),
                Tuple.Create(4, 1, 5),
                Tuple.Create(4, 2, 4),
                Tuple.Create(4, 3, 111)
            });
            graph.RemoveVertex(1);
            graph.RemoveVertex(2);
            Assert.AreEqual(1, graph[0].CountEdges);
            Assert.AreEqual(1, graph[1].CountEdges);
            Assert.AreEqual(2, graph.CountEdges);
            Assert.AreEqual(2, graph.CountVertices);
            Assert.AreEqual(4, graph[0].FirstEdge.Key);
            Assert.AreEqual(3, graph[1].FirstEdge.Key);
        }
    }
}