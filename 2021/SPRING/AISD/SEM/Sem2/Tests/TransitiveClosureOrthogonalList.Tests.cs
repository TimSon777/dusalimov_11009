﻿using System;
using System.Linq;
using NUnit.Framework;
using Sem2;

namespace Tests
{
    public class TransitiveClosureOrthogonalListTests
    {
        [Test]
        public void FourVerticesOrdinary()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new [] {0,1,2,3},
                new []
                {
                    Tuple.Create(0, 0, 1),
                    Tuple.Create(0, 1, 1),
                    Tuple.Create(0, 3, 1),
                    Tuple.Create(1, 0, 1),
                    Tuple.Create(1, 1, 1),
                    Tuple.Create(2, 0, 1),
                    Tuple.Create(2, 2, 1),
                    Tuple.Create(3, 0, 1)
                });
            
            TransitiveClosure.Algorithm(graph);
            Assert.AreEqual(3, graph[0].CountEdges);
            Assert.AreEqual(3, graph[1].CountEdges);
            Assert.AreEqual(4, graph[2].CountEdges);
            Assert.AreEqual(3, graph[3].CountEdges);
        }
        
        [Test]
        public void FiveVerticesWhereVertexIsConnectedToNext()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new [] {0,1,2,3,4}, 
                new []
            {
                Tuple.Create(0, 1, 1),
                Tuple.Create(1, 2, 1),
                Tuple.Create(2, 3, 1),
                Tuple.Create(3, 4, 1),
                Tuple.Create(4, 0, 1)
            });
            TransitiveClosure.Algorithm(graph);
            Assert.AreEqual(25, graph.CountEdges);
        }

        [Test]
        public void FourVerticesWhereAddOneEdge()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new [] {1,2,100,3}, 
                new []
                {
                    Tuple.Create(1, 2, 1),
                    Tuple.Create(2, 3, 1),
                    Tuple.Create(2, 100, 1),
                    Tuple.Create(3, 100, 1)
                });
            TransitiveClosure.Algorithm(graph);
            Assert.AreEqual(6, graph.CountEdges);
        }

        [Test]
        public void ThreeVerticesWhereGraphClosed()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new [] {1,2,100}, 
                new []
                {
                    Tuple.Create(1, 2, 1),
                    Tuple.Create(100, 2, 1)
                });
            TransitiveClosure.Algorithm(graph);
            Assert.AreEqual(2, graph.CountEdges);
        }

        [Test]
        public void FiveVerticesDisconnectedGraph()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new [] {0,1,2,3,4}, 
                new []
                {
                    Tuple.Create(0, 1, 1),
                    Tuple.Create(1, 2, 1),
                    Tuple.Create(3, 4, 1)
                });
            TransitiveClosure.Algorithm(graph);
            Assert.AreEqual(4, graph.CountEdges);
            Assert.AreEqual(2, graph[0].CountEdges);
        }
    }
}