using System.Drawing;
using System.Linq;
using NUnit.Framework;
using Sem2;

namespace Tests
{
    public class ShortestDistancesTestsFloyd
    {
        [Test]
        public void SizeThreeOrdinary()
        {
            var matrix = new[,]
            {
                    {0, 5, 20},
                    {0, 0, 1},
                    {0, 0, 0}
            };
            Floyd.Algorithm(matrix);
            Assert.AreEqual(6, matrix[0, 2]);
        }
        
        [Test]
        public void SizeFiveNoEdges()
        {
            const int size = 5;
            var matrix = new int[size, size];
            Floyd.Algorithm(matrix);
            var result = 0;
            for (var x = 0; x < size; x++)
            for (var y = 0; y < size; y++)
                result += matrix[x, y];
            Assert.AreEqual(0, result);
        }
        
        [Test]
        public void SizeSixTwoConnectivityComponents()
        {
            var matrix = new[,]
            {
                {0, 1, 10, 0, 0, 0},
                {0, 0, 5, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 7, 0},
                {0, 0, 0, 6, 0, 1},
                {0, 0, 0, 2, 0, 0}
            };
            Floyd.Algorithm(matrix);
            Assert.AreEqual(6, matrix[0, 2]);
            Assert.AreEqual(0, matrix[0, 5]);
            Assert.AreEqual(3, matrix[4, 3]);
        }
    }

    public class ShortestPathsTestsFloyd
    {
        [Test]
        public void SizeThreeOrdinary()
        {
            var matrix = new[,]
            {
                {0, 5, 20},
                {0, 0, 1},
                {0, 0, 0}
            };
            var shortestPaths = Floyd.Algorithm(matrix);
            var result = Floyd.GetPath(shortestPaths, 0, 2);
            Assert.AreEqual(1, result.First());
        }

        [Test]
        public void SizeFiveNoEdges()
        {
            const int size = 5;
            var matrix = new int[size, size];
            var shortestPaths = Floyd.Algorithm(matrix);
            var result = 0;
            for (var x = 0; x < size; x++)
            for (var y = 0; y < size; y++)
                result += shortestPaths[x, y];
            Assert.AreEqual(0, result);
        }

        [Test]
        public void SizeSixTwoConnectivityComponents()
        {
            var matrix = new[,]
            {
                {0, 1, 10, 0, 0, 0},
                {0, 0, 5, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 7, 0},
                {0, 0, 0, 6, 0, 1},
                {0, 0, 0, 2, 0, 0}
            };
            var shortestPaths = Floyd.Algorithm(matrix);
            var pathZeroTwo = Floyd.GetPath(shortestPaths, 0, 2).First();
            var pathFourThree = Floyd.GetPath(shortestPaths, 4, 3).First();
            var pathThreeFive = Floyd.GetPath(shortestPaths, 3, 5).First();
            var pathOneFiveCountVertices = Floyd.GetPath(shortestPaths, 1, 5).Count();
            Assert.AreEqual(1, pathZeroTwo);
            Assert.AreEqual(5, pathFourThree);
            Assert.AreEqual(4, pathThreeFive);
            Assert.AreEqual(0, pathOneFiveCountVertices);
        }
    }
}
