﻿using NUnit.Framework;
using Sem2;

namespace Tests
{
    public class TransitiveClosureMatrixTests
    {
        [Test]
        public void SizeFourOrdinary()
        {
            const int size = 4;
            var matrix = new[,]
            {
                {1, 1, 0, 1},
                {1, 1, 0, 0},
                {1, 0, 1, 0},
                {1, 0, 0, 0}
            };
            var result = TransitiveClosure.Algorithm(matrix);
            for (var i = 0; i < size; i++)
            for (var j = 0; j < size; j++)
                if (j == 2 && i != 2)
                    Assert.AreEqual(false, result[i, j]);
                else Assert.AreEqual(true, result[i, j]);
        }

        [Test]
        public void SizeFiveWhereVertexIsConnectedToNext()
        {
            const int size = 5;
            var matrix = new[,]
            {
                {0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 1, 0},
                {0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0}
            };
            var result = TransitiveClosure.Algorithm(matrix);
            for (var x = 0; x < size; x++)
            for (var y = 0; y < size; y++)
                Assert.AreEqual(true, result[x, y]);
        }

        [Test]
        public void SizeFourDisconnectedGraph()
        {
            var matrix = new[,]
            {
                {0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0}
            };

            var result = TransitiveClosure.Algorithm(matrix);
            Assert.AreEqual(true, result[0, 2]);
            Assert.AreEqual(false, result[4, 3]);
        }
    }
}
