﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Sem2;

namespace Tests
{
    public class ShortestDistancesTests
    {
        [Test]
        public void CompleteGraphThreeVertices()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph( new [] { 0,1,2 },
                new []
                {
                    Tuple.Create( 0, 2, 10 ),
                    Tuple.Create( 0, 1, 5 ),
                    Tuple.Create( 1, 2, 1 ),
                    Tuple.Create( 1, 0, 1 ),
                    Tuple.Create( 2, 1, 2 ),
                    Tuple.Create( 2, 0, 5 ),
                });
            Floyd.Algorithm(graph);
            Assert.AreEqual(6,graph[0].FirstEdge.Weight);
            Assert.AreEqual(5,graph[0].FirstEdge.Next.Weight);
            Assert.AreEqual(1,graph[1].FirstEdge.Weight);
            Assert.AreEqual(1,graph[1].FirstEdge.Next.Weight);
            Assert.AreEqual(2,graph[2].FirstEdge.Weight);
            Assert.AreEqual(3,graph[2].FirstEdge.Next.Weight);
            Assert.AreEqual(6, graph.CountEdges);
        }

        [Test]
        public void AddEdgeThreeVertices()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{0,1,2},new []
            {
                Tuple.Create(0, 1, 10),
                Tuple.Create(1, 2, 100)
            });
            Floyd.Algorithm(graph);
            Assert.AreEqual(3, graph.CountEdges);
        }

        [Test]
        public void DisconnectedGraphSixVertices()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{1, 200, 301, 7, 5, 6},new []
            {
                Tuple.Create(1, 200, 5),
                Tuple.Create(200, 301, 6),
                Tuple.Create(7, 5, 2),
                Tuple.Create(5, 6, 1),
                Tuple.Create(7, 6, 10)
            });
            Floyd.Algorithm(graph);
            Assert.AreEqual(11, graph[0].FirstEdge.Next.Weight);
            Assert.AreEqual(3, graph[3].FirstEdge.Next.Weight);
            Assert.AreEqual(6, graph.CountEdges);
        }
    }

    public class ShortestPathsTests
    {
        [Test]
        public void ThreeVerticesOrdinary()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{7, 5, 6},new []
            {
                Tuple.Create(7, 5, 2),
                Tuple.Create(5, 6, 1),
                Tuple.Create(7, 6, 10)
            });
            Floyd.Algorithm(graph);
            var path = Floyd.GetPath(graph, 7, 6).First();
            Assert.AreEqual(5, path);
        }

        [Test]
        public void FourVerticesWherePathConsistsTwoVertices()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{0, 5, 7, 100},new []
            {
                Tuple.Create(0, 5, 1),
                Tuple.Create(5, 7, 2),
                Tuple.Create(7, 100, 997)
            });
            Floyd.Algorithm(graph);
            var path = Floyd.GetPath(graph, 0, 100).ToArray();
            Assert.AreEqual(5, path[0]);
            Assert.AreEqual(7, path[1]);
        }

        [Test]
        public void TwoVerticesWhereNoPathBetweenThem()
        {
            var graph = new OrthogonalList();
            graph.MakeGraph(new []{0, int.MaxValue},new []
            {
                Tuple.Create(0, int.MaxValue, 1),
            });
            Floyd.Algorithm(graph);
            var path = Floyd.GetPath(graph, 0, int.MaxValue);
            Assert.AreEqual(0, path.Count);
        }
    }
}