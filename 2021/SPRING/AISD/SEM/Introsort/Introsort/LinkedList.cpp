#include "LinkedList.h"
#include <iostream>

LinkedListItem::LinkedListItem(int element) {
	Item = element;
	Next = nullptr;
}

LinkedList::LinkedList() {
	Head = Tail = nullptr;
	count = 0;
}

LinkedList::~LinkedList() {
	Clean();
}

LinkedListItem* LinkedList::GetHead() {
	return Head;
}

int LinkedList::Count()
{
	return count;
}

void LinkedList::Add(int element) {
	auto item = new LinkedListItem(element);
	if (Head == nullptr) Head = item;
	else Tail->Next = item;
	Tail = item;
	count++;
}

int& LinkedList::operator[](int index) {
	if (index < 0) throw std::invalid_argument("Negative index");
	if (index >= count) throw std::invalid_argument("Index out of bounds");
	auto current = Head;
	for (auto i = 0; i < index; i++) {
		current = current->Next;
	}
	return current->Item;
}

void LinkedList::Print() {
	auto current = Head;
	while (current != nullptr) {
		std::cout << current->Item << std::endl;
		current = current->Next;
	}
}

void LinkedList::Clean() {
	auto current = Head;
	auto deleteItem = Head;
	while (current != nullptr) {
		deleteItem = current;
		current = current->Next;
		delete deleteItem;
	}
}

