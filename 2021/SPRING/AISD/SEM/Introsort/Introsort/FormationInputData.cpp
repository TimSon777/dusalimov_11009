#include <iostream>
#include <fstream>
#include "LinkedList.h"
#include "FormationInputData.h"

void GenerateInputData() { // ������� ����� ���� � ���������� ���� 100 �������� �� 100 �� 10000 ��������� (��� 100) 
	std::ofstream file("Input.txt");
	srand(time(NULL));
	auto numberSets = 100;
	auto numberElements = 100;
	for (auto i = 0; i < numberSets; i++) {
		numberElements = 100 + 100 * i;
		file << numberElements;
		file << std::endl;
		for (auto j = 0; j < numberElements; j++)
			file << rand() % 10000 << " ";
		file << std::endl;
	}
}

void ReadDataArray(int* arr, int size) { // ���������� � ����� � ������ � ������ 
	std::ifstream file("Input.txt");
	auto length = 0;
	auto element = 0;
	if (file) {
		auto setNumber = size / 100;
		for (auto i = 0; i < setNumber - 1; i++) {
			file >> length;
			for (auto j = 0; j < length; j++) {
				file >> element;
			}
		}
		file >> length;
		for (auto i = 0; i < size; i++)
			file >> arr[i];
	}
	else std::cout << "���� �� ������!";
}

void ReadDataLinkedList(LinkedList& list, int count) { // ���������� � ����� � ������ � ������� ������
	std::ifstream file("Input.txt");
	auto length = 0;
	auto element = 0;
	if (file) {
		auto setNumber = count / 100;
		for (auto i = 0; i < setNumber - 1; i++) {
			file >> length;
			for (auto j = 0; j < length; j++) {
				file >> element;
			}
		}
		file >> length;
		for (auto i = 0; i < count; i++) {
			file >> element;
			list.Add(element);
		}
	}
	else std::cout << "���� �� ������!";
}