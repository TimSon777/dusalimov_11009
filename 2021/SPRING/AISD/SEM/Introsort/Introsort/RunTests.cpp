#include "RunTests.h"
#include "TestsIntrosort.h"
#include "TestsLinkedList.h"

void RunTests() { // ��������� ��� �����
	Test_LinkedList_IsEmpty_Introsort();
	Test_LinkedList_OneElement_Introsort();
	Test_LinkedList_TenElements_Introsort();
	Test_LinkedList_ThousandElements_Introsort();
	Test_LinkedList_SortedElements_Introsort();
	Test_LinkedList_ReverseSortedElements_Introsort();

	Test_Array_OneElement_Introsort();
	Test_Array_TenElements_Introsort();
	Test_Array_HundredElements_Introsort();
	Test_Array_ThousandElements_Introsort();
	Test_Array_SortedElements_Introsort();
	Test_Array_ReverseSortedElements_Introsort();

	Test_AddOneElement();
	Test_AddTwoElements();
	Test_AddTwentyElements();
	Test_Count();
	Test_LinkedList_IsEmpty_Introsort();
	Test_WhereHeadIsNotNull();
	Test_WhereHeadIsNull();

	Test_Array_ThousandElements_Insertionsort();
	Test_LinkedList_ThousandElements_Insertionsort();
	Test_Array_ThousandElements_Heapsort();
	Test_LinkedList_ThousandElements_Heapsort();
}
