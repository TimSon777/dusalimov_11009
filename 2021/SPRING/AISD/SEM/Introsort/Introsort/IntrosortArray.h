#pragma once

class IntrosortArray
{
public:
	void Introsort(int*, int);
	void Heapsort(int*, int);
	void Insertionsort(int*, int, int);
private:
	const int AmountElementsArray = 16;
	void sortCertainWay(int*, int, int, int);
	int getMedianThree(int*, int, int, int);
	int quickSort(int*, int, int, int);
	void generateHeap(int*, int, int);
	void swap(int*, int, int);
	bool arrayIsSorted(int*, int, int);
};

