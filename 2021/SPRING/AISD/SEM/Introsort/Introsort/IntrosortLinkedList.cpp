#include <iostream>
#include "LinkedList.h"
#include "IntrosortLinkedList.h"

void IntrosortLinkedList::Introsort(LinkedList& list, int count) {
	sortCertainWay(list, 0, count, 2 * floor(log(count) / log(2)));
}

void IntrosortLinkedList::sortCertainWay(LinkedList& list, int left, int right, int depth) {
	while (right - left > AmountElementsLinkedList) {
		if (depth == 0) {
			Heapsort(list, right - left);
			return;
		}
		else {
			if (listIsSorted(list, left, right - 1)) return;
			auto newBoundary = quickSort(list, left, right - 1, getMedianThree(list, left, left + 1 + (right - left) / 2, right - 1));
			sortCertainWay(list, newBoundary, right, --depth);
			right = newBoundary;
		}
	}
	InsertionSort(list, left, right);
}

int IntrosortLinkedList::getMedianThree(LinkedList& list, int leftIndex, int middleIndex, int rightIndex) {
	if (list[leftIndex] > list[middleIndex]) {
		if (list[rightIndex] > list[leftIndex])
			return list[leftIndex];
		return (list[middleIndex] > list[rightIndex]) ? list[middleIndex] : list[rightIndex];
	}
	if (list[rightIndex] > list[middleIndex])
		return list[middleIndex];
	return (list[leftIndex] > list[rightIndex]) ? list[leftIndex] : list[rightIndex];
}

int IntrosortLinkedList::quickSort(LinkedList& list, int left, int right, int pivot)
{
	while (left <= right) {
		while (list[left] < pivot) left++;
		while (list[right] > pivot) right--;
		if (left <= right) swap(list, left++, right--);
	}
	return left;
}

void IntrosortLinkedList::InsertionSort(LinkedList& list, int left, int right) {
	for (auto i = left + 1; i < right; i++) {
		auto j = 0, max = list[i];
		for (j = i; j >= 1 && list[j - 1] > max; j--)
			list[j] = list[j - 1];
		list[j] = max;
	}

}

void IntrosortLinkedList::Heapsort(LinkedList& list, int count)
{
	for (int i = (count / 2); i >= 0; i--)
		generateHeap(list, i, count - 1);
	for (int i = count - 1; i >= 1; i--)
	{
		swap(list, 0, i);
		generateHeap(list, 0, i - 1);
	}
}

void IntrosortLinkedList::generateHeap(LinkedList& list, int root, int bottom)
{
	auto maxIndexDescendant = 0;
	auto heapIsFormed = false;
	while (2 * root <= bottom && !heapIsFormed)
	{
		if (2 * root == bottom) maxIndexDescendant = 2 * root;
		else maxIndexDescendant = list[root * 2] > list[root * 2 + 1] ? root * 2 : root * 2 + 1;
		if (list[root] < list[maxIndexDescendant])
		{
			swap(list, root, maxIndexDescendant);
			root = maxIndexDescendant;
		}
		else heapIsFormed = true;
	}
}

void IntrosortLinkedList::swap(LinkedList& list, int first, int second) {
	auto temp = list[first];
	list[first] = list[second];
	list[second] = temp;
}

bool IntrosortLinkedList::listIsSorted(LinkedList& list, int left, int right) {
	for (int i = left; i < right - 1; i++)
		if (list[i] > list[i + 1])
			return false;
	return true;
}

