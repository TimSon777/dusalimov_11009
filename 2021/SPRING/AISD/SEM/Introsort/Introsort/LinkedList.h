#pragma once

struct LinkedListItem {
public:
	int Item;
	LinkedListItem* Next;
	LinkedListItem(int);
};

class LinkedList {
private:
	LinkedListItem* Head;
	LinkedListItem* Tail;
	int count;
public:
	LinkedList();
	~LinkedList();
	LinkedListItem* GetHead();
	int Count();
	void Add(int element);
	int& operator[](int);
	void Print();
	void Clean();
};

