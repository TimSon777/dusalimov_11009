#pragma once

class IntrosortLinkedList
{
public:
	void Introsort(LinkedList&, int);
	void Heapsort(LinkedList&, int);
	void InsertionSort(LinkedList&, int, int);
private:
	const int AmountElementsLinkedList = 16;
	void sortCertainWay(LinkedList&, int, int, int);
	int getMedianThree(LinkedList&, int, int, int);
	int quickSort(LinkedList&, int, int, int);
	void generateHeap(LinkedList&, int, int);
	void swap(LinkedList&, int, int);
	bool listIsSorted(LinkedList&, int, int);
};



