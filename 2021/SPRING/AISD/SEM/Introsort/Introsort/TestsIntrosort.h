#pragma once
#include "LinkedList.h"

void FillArraysRandomNumbers(int*, int*, int);
void FillLinkedListRandomNumbers(LinkedList&, LinkedList&, int);
int Comparer(const void*, const void*);
void Test_Array_OneElement_Introsort();
void Test_Array_TenElements_Introsort();
void Test_Array_HundredElements_Introsort();
void Test_Array_ThousandElements_Introsort();
void Test_Array_SortedElements_Introsort();
void Test_Array_ReverseSortedElements_Introsort();
void Test_LinkedList_IsEmpty_Introsort();
void Test_LinkedList_OneElement_Introsort();
void Test_LinkedList_TenElements_Introsort();
void Test_LinkedList_ThousandElements_Introsort();
void Test_LinkedList_SortedElements_Introsort();
void Test_LinkedList_ReverseSortedElements_Introsort();
void Test_Array_ThousandElements_Insertionsort();
void Test_LinkedList_ThousandElements_Insertionsort();
void Test_Array_ThousandElements_Heapsort();
void Test_LinkedList_ThousandElements_Heapsort();

