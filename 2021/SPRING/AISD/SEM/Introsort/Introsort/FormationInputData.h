#pragma once

void GenerateInputData();
void ReadDataArray(int*, int);
void ReadDataLinkedList(LinkedList&, int);
