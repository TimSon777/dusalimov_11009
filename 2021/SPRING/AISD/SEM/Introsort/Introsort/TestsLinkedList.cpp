#include <iostream>
#include <cassert>
#include "LinkedList.h"
#include "TestsLinkedList.h"

void Test_AddOneElement() {
	auto list = new LinkedList();
	list->Add(5);
	assert((*list)[0] == 5);
	std::cout << "Test_AddOneElement is ok" << std::endl;
	list->~LinkedList();
}

void Test_AddTwoElements() {
	auto list = new LinkedList();
	list->Add(5);
	list->Add(228);
	assert((*list)[0] == 5 && (*list)[1] == 228);
	std::cout << "Test_AddTwoElements is ok" << std::endl;
	list->~LinkedList();
}

void Test_AddTwentyElements() {
	auto list = new LinkedList();
	auto result = true;
	for (auto i = 0; i < 20; i++)
		list->Add(i);
	for (auto i = 0; i < 20; i++)
		if ((*list)[i] != i) result = false;
	assert(result);
	std::cout << "Test_AddTwentyElements is ok" << std::endl;
	list->~LinkedList();
}

void Test_Count() {
	auto list = new LinkedList();
	auto count = 15;
	for (auto i = 0; i < count; i++)
		list->Add(i);
	assert(list->Count() == count);
	std::cout << "Test_Count is ok" << std::endl;
	list->~LinkedList();
}

void Test_WhereHeadIsNotNull() {
	auto list = new LinkedList();
	list->Add(5);
	list->Add(7);
	auto head = list->GetHead()->Item;
	assert(head == 5);
	std::cout << "Test_WhereHeadIsNotNull is ok" << std::endl;
	list->~LinkedList();
}

void Test_WhereHeadIsNull() {
	auto list = new LinkedList();
	auto head = list->GetHead();
	assert(head == nullptr);
	std::cout << "Test_WhereHeadIsNull is ok" << std::endl;
	list->~LinkedList();
}
