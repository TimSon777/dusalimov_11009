#include <iostream>
#include "IntrosortArray.h"

void IntrosortArray::Introsort(int* arr, int size) {
	sortCertainWay(arr, 0, size, 2 * floor(log(size) / log(2)));
}
 
void IntrosortArray::sortCertainWay(int* arr, int left, int right, int depth) {
	while (right - left > AmountElementsArray) {
		if (depth == 0) {
			Heapsort(arr, right - left);
			return;
		}
		else {
			if (arrayIsSorted(arr, left, right - 1)) return;
			auto newBoundary = quickSort(arr, left, right - 1, getMedianThree(arr, left, left + 1 + (right - left) / 2, right - 1));
			sortCertainWay(arr, newBoundary, right, --depth);
			right = newBoundary;
		}
	}
	Insertionsort(arr, left, right);
}

int IntrosortArray::getMedianThree(int* arr, int leftIndex, int middleIndex, int rightIndex) {
	if (arr[leftIndex] > arr[middleIndex]) {
		if (arr[rightIndex] > arr[leftIndex])
			return arr[leftIndex];
		return (arr[middleIndex] > arr[rightIndex]) ? arr[middleIndex] : arr[rightIndex];
	}
	if (arr[rightIndex] > arr[middleIndex])
		return arr[middleIndex];
	return (arr[leftIndex] > arr[rightIndex]) ? arr[leftIndex] : arr[rightIndex];
}

int IntrosortArray::quickSort(int* arr, int left, int right, int pivot)
{
	while (left <= right) {
		while (arr[left] < pivot) left++;
		while (arr[right] > pivot) right--;
		if (left <= right) swap(arr, left++, right--);
	}
	return left;
}

void IntrosortArray::Insertionsort(int* arr, int left, int right) {
	for (auto i = left + 1; i < right; i++) {
		auto j = 0, max = arr[i];
		for (j = i; j >= 1 && arr[j - 1] > max; j--)
			arr[j] = arr[j - 1];
		arr[j] = max;
	}

}

void IntrosortArray::Heapsort(int* arr, int size)
{
	for (int i = (size / 2); i >= 0; i--)
		generateHeap(arr, i, size - 1);
	for (int i = size - 1; i >= 1; i--)
	{
		swap(arr, 0, i);
		generateHeap(arr, 0, i - 1);
	}
}

void IntrosortArray::generateHeap(int* arr, int root, int bottom)
{
	auto maxIndexDescendant = 0;
	auto heapIsFormed = false;
	while (2 * root <= bottom && !heapIsFormed)
	{
		if (2 * root == bottom) maxIndexDescendant = 2 * root;    
		else maxIndexDescendant = arr[root * 2] > arr[root * 2 + 1] ? root * 2 : root * 2 + 1;
		if (arr[root] < arr[maxIndexDescendant])
		{
			swap(arr, root, maxIndexDescendant);
			root = maxIndexDescendant;
		}
		else heapIsFormed = true;
	}
}

void IntrosortArray::swap(int* arr, int first, int second) {
	auto temp = arr[first];
	arr[first] = arr[second];
	arr[second] = temp;
}

bool IntrosortArray::arrayIsSorted(int* arr, int left, int right) {
	for (int i = left; i < right - 1; i++)
		if (arr[i] > arr[i + 1]) return false;
	return true;
}
