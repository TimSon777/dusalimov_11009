#include <iostream>
#include "LinkedList.h"
#include "MethodsForSequences.h"

void CreateRandomArray(int* arr, int length) {
	srand(time(NULL));
	for (auto i = 0; i < length; i++)
		arr[i] = rand() % 10000;
}

void CreateRandomLinkedList(LinkedList* list, int count) {
	srand(time(NULL));
	for (auto i = 0; i < count; i++)
		list->Add(rand() % 10000);
}

void Print(int* arr, int size) {
	for (auto i = 0; i < size; i++)
		std::cout << arr[i] << std::endl;
}

// дополнительные методы для работы с последовательностями