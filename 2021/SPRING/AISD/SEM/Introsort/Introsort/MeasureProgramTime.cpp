#include <iostream>
#include <algorithm>
#include <chrono>
#include "LinkedList.h"
#include "IntrosortArray.h"
#include "IntrosortLinkedList.h"
#include "MeasureProgramTime.h"
#include "MethodsForSequences.h"


double GetTimeProgramArraySec(int size) { // �������� ����� ������ ��������� ��� �������
	int* arr;
	arr = new int[size];
	CreateRandomArray(arr, size);
	auto sort = IntrosortArray();
	auto begin = std::chrono::steady_clock::now();
	sort.Introsort(arr, size);
	auto end = std::chrono::steady_clock::now();
	delete[] arr;
	return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
}

double GetTimeProgramLinkedListSec(int count) { // �������� ����� ������ ��������� ��� �������� ������
	auto list = new LinkedList();
	CreateRandomLinkedList(list, count);
	auto sort = IntrosortLinkedList();
	auto begin = std::chrono::steady_clock::now();
	sort.Introsort(*list, count);
	auto end = std::chrono::steady_clock::now();
	list->~LinkedList();
	return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
}

// ����� �������� ������������ ����� ��� ��������, � ������� ����������� � ����� �������� ����� � ��������������� ��
// ����� �������� ������������ ����� ��� �������� ������, � ������� ���������� � �������� ���������� ������ � ������� ������, �.�.
// ��� ����� �������� ���� ����� ���� ���������