#include <iostream>
#include <cassert>
#include "LinkedList.h"
#include "TestsIntrosort.h"
#include "IntrosortArray.h"
#include "IntrosortLinkedList.h"

void FillArraysRandomNumbers(int* arr, int* arr2, int size) {
	srand(time(NULL));
	for (auto i = 0; i < size; i++)
		arr[i] = arr2[i] = rand() % 300;
}

void FillLinkedListRandomNumbers(LinkedList& list1, LinkedList& list2, int count) {
	srand(time(NULL));
	for (auto i = 0; i < count; i++) {
		auto temp = rand() % 300;
		list1.Add(temp);
		list2.Add(temp);
	}
}

int Comparer(const void* x1, const void* x2) { // ���������� ��� ���������� ������� ����������
	return (*(int*)x1 - *(int*)x2);
}

void Test_Array_OneElement_Introsort() {
	int arr[1] = { 1337 };
	auto sort = IntrosortArray();
	sort.Introsort(arr, 1);
	assert(arr[0] == 1337);
	std::cout << "Test_Array_OneElement_Introsort is ok" << std::endl;
}

void Test_Array_TenElements_Introsort() {
	int arr1[10], arr2[10];
	FillArraysRandomNumbers(arr1, arr2, 10);
	auto result = true;
	auto sort = IntrosortArray();
	sort.Introsort(arr1, 10);
	std::qsort(arr2, 10, sizeof(int), Comparer);
	for (auto i = 0; i < 10; i++)
		if (arr1[i] != arr2[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_Array_TenElements_Introsort is ok" << std::endl;
}

void Test_Array_HundredElements_Introsort() {
	int arr[100], arr2[100];
	FillArraysRandomNumbers(arr, arr2, 100);
	auto result = true;
	auto sort = IntrosortArray();
	sort.Introsort(arr, 100);
	std::qsort(arr2, 100, sizeof(int), Comparer);
	for (auto i = 0; i < 100; i++)
		if (arr[i] != arr2[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_Array_HundredElements_Introsort is ok" << std::endl;
}

void Test_Array_ThousandElements_Introsort() {
	int arr[1000], arr2[1000];
	FillArraysRandomNumbers(arr, arr2, 1000);
	auto result = true;
	auto sort = IntrosortArray();
	sort.Introsort(arr, 1000);
	std::qsort(arr2, 1000, sizeof(int), Comparer);
	for (auto i = 0; i < 1000; i++)
		if (arr[i] != arr2[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_Array_ThousandElements_Introsort is ok" << std::endl;
}

void Test_Array_SortedElements_Introsort() {
	int arr[1000], arr2[1000];
	for (auto i = 0; i < 1000; i++)
		arr[i] = arr2[i] = i;
	auto result = true;
	auto sort = IntrosortArray();
	sort.Introsort(arr, 1000);
	for (auto i = 0; i < 1000; i++)
		if (arr[i] != arr2[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_Array_SortedElements_Introsort is ok" << std::endl;
}

void Test_Array_ReverseSortedElements_Introsort() {
	int arr[1000], arr2[1000];
	for (auto i = 0; i < 1000; i++) {
		arr[i] = 999 - i;
		arr2[i] = i;
	}
	auto result = true;
	auto sort = IntrosortArray();
	sort.Introsort(arr, 1000);
	for (auto i = 0; i < 1000; i++)
		if (arr[i] != arr2[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_Array_ReverseSortedElements_Introsort is ok" << std::endl;
}
void Test_LinkedList_IsEmpty_Introsort() {
	auto list = new LinkedList();
	auto sort = IntrosortLinkedList();
	sort.Introsort(*list, list->Count());
	assert(list->GetHead() == nullptr);
	std::cout << "Test_LinkedList_IsEmpty_Introsort is ok" << std::endl;
	list->~LinkedList();
}

void Test_LinkedList_OneElement_Introsort() {
	auto list = new LinkedList();
	list->Add(777);
	auto sort = IntrosortLinkedList();
	sort.Introsort(*list, list->Count());
	assert((*list)[0] == 777);
	std::cout << "Test_LinkedList_OneElement_Introsort is ok" << std::endl;
	list->~LinkedList();
}

void Test_LinkedList_TenElements_Introsort() {
	auto result = true;
	auto list = new LinkedList();
	list->Add(5);
	list->Add(10);
	list->Add(55);
	list->Add(7);
	list->Add(0);
	list->Add(1);
	list->Add(1);
	list->Add(3);
	list->Add(2);
	list->Add(2);
	auto sort = IntrosortLinkedList();
	sort.Introsort(*list, list->Count());
	int arr[10] = { 0, 1, 1, 2, 2, 3, 5, 7, 10, 55 };
	for (auto i = 0; i < 10; i++)
		if ((*list)[i] != arr[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_LinkedList_TenElements_Introsort is ok" << std::endl;
	list->~LinkedList();
}

void Test_LinkedList_ThousandElements_Introsort() {
	auto result = true;
	auto list1 = new LinkedList();
	auto list2 = new LinkedList();
	auto count = 1000;
	FillLinkedListRandomNumbers(*list1, *list2, count);
	for (auto i = 0; i < 1000; i++)
		if ((*list1)[i] != (*list2)[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_LinkedList_ThousandElements_Introsort is ok" << std::endl;
	list1->~LinkedList();
	list2->~LinkedList();
}

void Test_LinkedList_SortedElements_Introsort() {
	auto result = true;
	int arr[1000];
	auto list = new LinkedList();
	for (auto i = 0; i < 1000; i++) {
		list->Add(i);
		arr[i] = i;
	}
	auto sort = IntrosortLinkedList();
	sort.Introsort(*list, list->Count());
	for (auto i = 0; i < 1000; i++)
		if ((*list)[i] != arr[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_LinkedList_SortedElements_Introsort is ok" << std::endl;
	list->~LinkedList();
}

void Test_LinkedList_ReverseSortedElements_Introsort() {
	auto result = true;
	int arr[1000];
	auto list = new LinkedList();
	for (auto i = 0; i < 1000; i++) {
		list->Add(999 - i);
		arr[i] = i;
	}
	auto sort = IntrosortLinkedList();
	sort.Introsort(*list, list->Count());
	for (auto i = 0; i < 1000; i++)
		if ((*list)[i] != arr[i]) {
			result = false;
			break;
		}
	assert(result);
	std::cout << "Test_LinkedList_ReverseSortedElements_Introsort is ok" << std::endl;
	list->~LinkedList();
}

void Test_Array_ThousandElements_Insertionsort() {
	auto result = true;
	auto size = 1000;
	int* arr1;
	int* arr2;
	arr1 = new int[size];
	arr2 = new int[size];
	FillArraysRandomNumbers(arr1, arr2, size);
	auto sort = IntrosortArray();
	sort.Insertionsort(arr1, 0, size);
	std::qsort(arr2, size, sizeof(int), Comparer);
	for (auto i = 0; i < size; i++) {
		if (arr1[i] != arr2[i]) {
			result = false;
			break;
		}
	}
	assert(result);
	std::cout << "Test_Array_ThousandElements_Insertionsort is ok" << std::endl;
	delete[] arr1;
	delete[] arr2;
}

void Test_LinkedList_ThousandElements_Insertionsort() {
	auto result = true;
	auto list1 = new LinkedList();
	auto list2 = new LinkedList();
	auto count = 1000;
	FillLinkedListRandomNumbers(*list1, *list2, count);
	auto sort = IntrosortLinkedList();
	sort.Introsort(*list1, count);
	sort.InsertionSort(*list2, 0, count);
	for (auto i = 0; i < count; i++) {
		if ((*list1)[i] != (*list2)[i]) {
			result = false;
			break;
		}
	}
	assert(result);
	std::cout << "Test_LinkedList_ThousandElements_Insertionsort is ok" << std::endl;
	list1->~LinkedList();
	list2->~LinkedList();
}

void Test_Array_ThousandElements_Heapsort() {
	auto result = true;
	auto size = 1000;
	int* arr1;
	int* arr2;
	arr1 = new int[size];
	arr2 = new int[size];
	FillArraysRandomNumbers(arr1, arr2, size);
	auto sort = IntrosortArray();
	sort.Heapsort(arr1, size);
	std::qsort(arr2, size, sizeof(int), Comparer);
	for (auto i = 0; i < size; i++) {
		if (arr1[i] != arr2[i]) {
			result = false;
			break;
		}
	}
	assert(result);
	std::cout << "Test_Array_ThousandElements_Heapsort is ok" << std::endl;
	delete[] arr1;
	delete[] arr2;
}

void Test_LinkedList_ThousandElements_Heapsort() {
	auto result = true;
	auto list1 = new LinkedList();
	auto list2 = new LinkedList();
	auto count = 1000;
	FillLinkedListRandomNumbers(*list1, *list2, count);
	auto sort = IntrosortLinkedList();
	sort.Introsort(*list1, count);
	sort.Heapsort(*list2, count);
	for (auto i = 0; i < count; i++) {
		if ((*list1)[i] != (*list2)[i]) {
			result = false;
			break;
		}
	}
	assert(result);
	std::cout << "Test_LinkedList_ThousandElements_Heapsort is ok" << std::endl;
	list1->~LinkedList();
	list2->~LinkedList();
}

