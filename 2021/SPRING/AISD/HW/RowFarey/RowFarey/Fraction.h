#pragma once

struct Fraction
{
public:
	Fraction(int, int);
	int Numerator();
	int Denominator();
	Fraction* Next;
	Fraction();
private:
	int numerator;
	int denominator;
};
