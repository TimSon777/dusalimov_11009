﻿#include <iostream>
#include "Fraction.h"
#include "RowFarey.h"

int main()
{
auto result = GetRowFarey(5);
auto i = 0;
while (result[i].Numerator() + result[i].Denominator() != 0) {
	std::cout << result[i].Numerator() << "/" << result[i].Denominator() << std::endl;
	i++;
}
delete[] result;
}

// можно переопределить операторы << и >>, + постоянно увеличивать массив, когда его размер закончится
