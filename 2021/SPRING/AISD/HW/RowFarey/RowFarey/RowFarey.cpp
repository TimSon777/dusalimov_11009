#include <iostream>
#include "Fraction.h"
#include "RowFarey.h"

int count = 2;

void Add(Fraction* arr, Fraction* leftFraction, Fraction* middleFraction, Fraction* rightFraction) {
	arr[count++] = *middleFraction;
	leftFraction->Next = middleFraction;
	middleFraction->Next = rightFraction;
}
void CreateRowFarey(Fraction* arr, Fraction* leftFraction, Fraction* rightFraction, int maxDenominator) {
	auto denominator = leftFraction->Denominator() + rightFraction->Denominator();
	if (denominator <= maxDenominator) {
		auto numerator = leftFraction->Numerator() + rightFraction->Numerator();
		auto fraction = new Fraction(numerator, denominator);
		Add(arr, leftFraction, fraction, rightFraction);
		CreateRowFarey(arr, leftFraction, fraction, maxDenominator);
		CreateRowFarey(arr, fraction, rightFraction, maxDenominator);
	}
}

Fraction* GetRowFarey(int maxDenominator) {
	Fraction* arr = new Fraction[100000];
	auto leftFraction = new Fraction(0, 1);
	arr[0] = *leftFraction;
	auto rightFraction = new Fraction(1, 1);
	arr[1] = *rightFraction;
	CreateRowFarey(arr, leftFraction, rightFraction, maxDenominator);
	auto result = new Fraction[100000];
	auto current = leftFraction;
	for (int i = 0; i < 10000; i++) {
		result[i] = *current;
		current = current->Next;
		if (current == nullptr) break;
	}
	auto deleteItem = leftFraction;
	while (deleteItem != nullptr) {
		auto temp = deleteItem->Next;
		delete deleteItem;
		deleteItem = temp;
	}
	delete[] arr;
	return result;
}