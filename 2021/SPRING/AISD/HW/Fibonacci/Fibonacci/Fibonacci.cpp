﻿#include <iostream>

unsigned int GetValueFibonacciRecursion(unsigned int n);
unsigned int GetValueFibonacciLoop(unsigned int n);
unsigned int GetValueFibonacciMatrix(unsigned int n);
unsigned int FastExponentiationMatrixFibonacci(unsigned int n);
void MultiplyMatrixFibonacci(int* matrixA, int* matrixB);

int main() {
    int n = 0;
    std::cin >> n;
    std::cout << GetValueFibonacciLoop(n) << std::endl;
    std::cout << GetValueFibonacciMatrix(n) << std::endl;
    std::cout << GetValueFibonacciRecursion(n);

    return 0;
}

unsigned int GetValueFibonacciRecursion(unsigned int n) {
    return n < 2 ? n : GetValueFibonacciRecursion(n - 1) + GetValueFibonacciRecursion(n - 2);
}

unsigned int GetValueFibonacciLoop(unsigned int n) {
    if (n == 0) return 0;
    unsigned int previous = 1;
    unsigned int now = 1;
    for (auto i = 2; i < n; i++) {

        now += previous;
        previous = now - previous;
    }
    return now;
}

unsigned int GetValueFibonacciMatrix(unsigned int n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return FastExponentiationMatrixFibonacci(n);
}

unsigned int FastExponentiationMatrixFibonacci(unsigned int n) {
    int E[4] = { 1, 0, 0, 1 }, matrixFibonacci[4] = { 1, 1, 1, 0 };
    while (n != 0) {
        if (n % 2 == 1) {
            MultiplyMatrixFibonacci(E, matrixFibonacci);
            n--;
        }
        else {
            MultiplyMatrixFibonacci(matrixFibonacci, matrixFibonacci);
            n >>= 1;
        }
    }
    return E[1];
}

void MultiplyMatrixFibonacci(int* matrixA, int* matrixB) {
    int result[4] = { 0, 0, 0, 0 };
    result[0] = matrixA[0] * matrixB[0] + matrixA[1] * matrixB[2];
    result[1] = matrixA[0] * matrixB[1] + matrixA[1] * matrixB[3];
    result[2] = matrixA[2] * matrixB[0] + matrixA[3] * matrixB[2];
    result[3] = matrixA[2] * matrixB[1] + matrixA[3] * matrixB[3];
    memcpy(matrixA, result, sizeof(int) * 4);
}
// Время замерял с помощью переменной clock, определенной в библиотеке ctime
