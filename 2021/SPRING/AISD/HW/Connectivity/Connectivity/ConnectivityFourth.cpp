#include "ConnectivityFourth.h"
#include <iostream>

ConnectivityFourth::ConnectivityFourth(int size) {
	if (size <= 0) throw std::invalid_argument("Negative size");
	this->size = size;
	this->array = new int[size];
	this->arrayCountVertices = new int[size];
	for (auto i = 0; i < size; i++) {
		array[i] = i;
		arrayCountVertices[i] = 1;
	}
}

ConnectivityFourth::~ConnectivityFourth() {
	delete[] array;
	delete[] arrayCountVertices;
}

int ConnectivityFourth::Find(int vertex) { // ��������� O(log(log(log(..log(n))), ��� ��� ���� ��� ����� ��������� � ��� ����.
	if (vertex < 0) throw std::invalid_argument("Negative vertex"); // ��� m �������� ��� ���������� ���������, ��������� �����
	if (vertex >= size) throw std::invalid_argument("There is no such peak vertex"); // O(log(..log(n)), ��� ���������� log = m
	while (array[vertex] != vertex) {
		vertex = array[vertex];
		array[vertex] = array[array[vertex]];
	}
	return vertex;
}

void ConnectivityFourth::Union(int firstVertex, int secondVertex) { // ��������� O(1)
	if (firstVertex < 0 || firstVertex >= size) throw std::invalid_argument("The first vertex does not exist ");
	if (secondVertex < 0 || secondVertex >= size) throw std::invalid_argument("The second vertex does not exist ");
	if (arrayCountVertices[firstVertex] < arrayCountVertices[secondVertex])
	{
		array[firstVertex] = secondVertex;
		arrayCountVertices[secondVertex] += arrayCountVertices[firstVertex];
		return;
	}
	array[secondVertex] = firstVertex;
	arrayCountVertices[firstVertex] += arrayCountVertices[secondVertex];
}

void ConnectivityFourth::UnionIfNotConnected(int firstVertex, int secondVertex) {
	auto headFirst = Find(firstVertex);
	auto headSecond = Find(secondVertex);
	if (headFirst != headSecond) Union(headFirst, headSecond);
}

bool ConnectivityFourth::AreVerticesConnected(int firstVertex, int secondVertex) {
	if (Find(firstVertex) == Find(secondVertex)) return true;
	return false;
}

int* ConnectivityFourth::Array() {
	return array;
}

int ConnectivityFourth::Size() {
	return size;
}

