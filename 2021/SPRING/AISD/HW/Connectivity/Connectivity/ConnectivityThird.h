#pragma once
class ConnectivityThird
{
private:
	int size;
	int* array;
	int* arrayCountVertices;
public:
	ConnectivityThird(int);
	~ConnectivityThird();
	int Find(int);
	void Union(int, int);
	void UnionIfNotConnected(int, int);
	bool AreVerticesConnected(int, int);
	int* Array();
	int Size();
};

