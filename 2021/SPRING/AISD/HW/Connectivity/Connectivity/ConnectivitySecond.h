#pragma once
class ConnectivitySecond
{
private:
	int size;
	int* array;
public:
	ConnectivitySecond(int*, int);
	ConnectivitySecond(int);
	~ConnectivitySecond();
	int Find(int);
	void Union(int, int);
	void UnionIfNotConnected(int, int);
	bool AreVerticesMerged(int, int);
	int* Array();
	int Size();
};

