#include "ConnectivitySecond.h"
#include <iostream>

ConnectivitySecond::ConnectivitySecond(int size) { 
	if (size <= 0) throw std::invalid_argument("Negative size");
	this->size = size;
	this->array = new int[this->size];
	for (auto i = 0; i < this->size; i++)
		this->array[i] = i;
}

ConnectivitySecond::ConnectivitySecond(int* array, int size) { // ���� ��� ���� ���������
	if (size <= 0) throw std::invalid_argument("Negative size");
	this->size = size;
	this->array = new int[this->size];
	memcpy(this->array, array, sizeof(int) * this->size);
	for (auto i = 0; i < this->size; i++)
		if (this->array[i] < 0 || this->array[i] >= this->size)
			throw std::invalid_argument("There are impossible elements in the array ");
}

ConnectivitySecond::~ConnectivitySecond() {
	delete[] this->array;
}

int ConnectivitySecond::Find(int vertex) { // ��������� O(n)
	if (vertex < 0) throw std::invalid_argument("Negative vertex");
	if (vertex >= size) throw std::invalid_argument("There is no such peak vertex");
	while (array[vertex] != vertex)
		vertex = array[vertex];
	return vertex;
}

void ConnectivitySecond::Union(int firstVertex, int secondVertex) { // ��������� O(1)
	if (firstVertex < 0 || firstVertex >= size) throw std::invalid_argument("The first vertex does not exist ");
	if (secondVertex < 0 || secondVertex >= size) throw std::invalid_argument("The second vertex does not exist ");
	array[secondVertex] = firstVertex;
}

void ConnectivitySecond::UnionIfNotConnected(int firstVertex, int secondVertex) {
	auto headFirst = Find(firstVertex);
	auto headSecond = Find(secondVertex);
	if (headFirst != headSecond) Union(headFirst, headSecond);
}

bool ConnectivitySecond::AreVerticesMerged(int firstVertex, int secondVertex) {
	if (Find(firstVertex) == Find(secondVertex)) return true;
	return false;
}

int* ConnectivitySecond::Array() {
	return array;
}

int ConnectivitySecond::Size() {
	return size;
}
