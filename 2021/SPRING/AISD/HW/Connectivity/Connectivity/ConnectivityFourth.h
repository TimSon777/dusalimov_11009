#pragma once
class ConnectivityFourth
{
private:
	int size;
	int* array;
	int* arrayCountVertices;
public:
	ConnectivityFourth(int);
	~ConnectivityFourth();
	int Find(int);
	void Union(int, int);
	void UnionIfNotConnected(int, int);
	bool AreVerticesConnected(int, int);
	int* Array();
	int Size();
};

