﻿#include <iostream>
#include <chrono>
#include "ConnectivityFirst.h"
#include "ConnectivitySecond.h"
#include "ConnectivityThird.h"
#include "ConnectivityFourth.h"

// я без понятия, как строить графики по 3 причинам
// 1) пусть даны n вершин, если соединять веришины n раз, то на 2, 3, 4 способе будут часто соединяться маленькие деревья,
// и в итоге сложность операций union и find в совокупности будет давать O(1) на всех этапах, а на начальных этапах практически
// не будут соединяться вершины
// 2) допустим, я буду соединять не n раз, а m раз, где число m намного меньше n, но и в этом случае повторится верхний этап 
// на значениях, близких к n
// 3) если брать все время разное количество объединений, то тогда будет абсолютно необъективно сравнивать время выполнения 
// программы. Снизу я попытался, на первом случае получилось (по понятным причинам), на других нет

int main()
{
	/*auto count = 0;
	for (auto i = 0; i < 50; i++) {
		auto time = 0.0;
		count += 1000;
		for (auto j = 0; j < 200; j++) {
			auto connectivity = new ConnectivityFirst(count);
			auto begin = std::chrono::steady_clock::now();
			for (auto i = 0; i < 1000; i++) {
				connectivity->Union(rand() % (count - 1), rand() % (count - 1));
			}
			auto end = std::chrono::steady_clock::now();
			time += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.0;
			connectivity->~ConnectivityFirst();
		}
		std::cout << time / 200 << std::endl;
	}*/

	//auto count = 0;
	//for (auto i = 0; i < 50; i++) {
	//	auto time = 0.0;
	//	count += 100000;
	//	for (auto j = 0; j < 200; j++) {
	//		auto connectivity = new ConnectivitySecond(count);
	//		auto begin = std::chrono::steady_clock::now();
	//		for (auto i = 0; i < 1000; i++) {
	//			connectivity->UnionIfNotConnected(rand() % (count - 1), rand() % (count - 1));
	//		}
	//		auto end = std::chrono::steady_clock::now();
	//		time += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.0;
	//		connectivity->~ConnectivitySecond();
	//	}
	//	std::cout << time / 200 << std::endl;
	//}
}
