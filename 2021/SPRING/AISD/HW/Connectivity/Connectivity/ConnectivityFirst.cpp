#include <iostream>
#include "ConnectivityFirst.h"

ConnectivityFirst::ConnectivityFirst(int size) {
	if (size <= 0) throw std::invalid_argument("Negative size");
	this->size = size;
	this->array = new int[size];
	for (auto i = 0; i < size; i++)
		array[i] = i;
}

ConnectivityFirst::ConnectivityFirst(int* array, int size) { // ���� ��� ������ ������������ ���������
	if (size <= 0) throw std::invalid_argument("Negative size");
	this->size = size;
	this->array = new int[this->size];
	memcpy(this->array, array, sizeof(int) * this->size);
	for (auto i = 0; i < this->size; i++)
		if (this->array[i] < 0 || this->array[i] >= this->size) 
			throw std::invalid_argument("There are impossible elements in the array ");
}

ConnectivityFirst::~ConnectivityFirst() {
	delete[] array;
}

int ConnectivityFirst::Find(int index) { // ��������� O(1)
	if (index < 0) throw std::invalid_argument("Negative index");
	if (index >= size) throw std::invalid_argument("Index out of bounds");
	return array[index];
}

void ConnectivityFirst::Union(int first, int second) { // ��������� O(n) 
	if (first < 0 || first >= size || second < 0 || second >= size) throw std::invalid_argument("Index out of bounds");
	if (Find(first) == Find(second)) return;
	for (auto i = 0; i < size; i++)
		if (array[i] == second) array[i] = array[first];
}

bool ConnectivityFirst::AreVerticesMerged(int first, int second) {
	if (Find(first) == Find(second)) return true;
	return false;
}

int* ConnectivityFirst::Array() {
	return array;
}

int ConnectivityFirst::Size() {
	return size;
}