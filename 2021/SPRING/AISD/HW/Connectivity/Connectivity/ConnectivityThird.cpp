#include "ConnectivityThird.h"
#include <iostream>

ConnectivityThird::ConnectivityThird(int size) { 
	if (size <= 0) throw std::invalid_argument("Negative size");
	this->size = size;
	this->array = new int[size];
	this->arrayCountVertices = new int[size];
	for (auto i = 0; i < size; i++) {
		array[i] = i;
		arrayCountVertices[i] = 1;
	}
}

ConnectivityThird::~ConnectivityThird() {
	delete[] array;
	delete[] arrayCountVertices;
}

int ConnectivityThird::Find(int vertex) { // ��������� O(log(n)), ��� ��� ������ ������ �� ����������� log(n)
	if (vertex < 0) throw std::invalid_argument("Negative vertex");
	if (vertex >= size) throw std::invalid_argument("There is no such peak vertex");
	while (array[vertex] != vertex)
		vertex = array[vertex];
	return vertex;
}

void ConnectivityThird::Union(int firstVertex, int secondVertex) { // ��������� O(1)
	if (firstVertex < 0 || firstVertex >= size) throw std::invalid_argument("The first vertex does not exist ");
	if (secondVertex < 0 || secondVertex >= size) throw std::invalid_argument("The second vertex does not exist ");
	if (arrayCountVertices[firstVertex] < arrayCountVertices[secondVertex])
	{
		array[firstVertex] = secondVertex;
		arrayCountVertices[secondVertex] += arrayCountVertices[firstVertex];
		return;
	}
	array[secondVertex] = firstVertex;
	arrayCountVertices[firstVertex] += arrayCountVertices[secondVertex];
}

void ConnectivityThird::UnionIfNotConnected(int firstVertex, int secondVertex) {
	auto headFirst = Find(firstVertex);
	auto headSecond = Find(secondVertex);
	if (headFirst != headSecond) Union(headFirst, headSecond);
}

bool ConnectivityThird::AreVerticesConnected(int firstVertex, int secondVertex) {
	if (Find(firstVertex) == Find(secondVertex)) return true;
	return false;
}

int* ConnectivityThird::Array() {
	return array;
}
int ConnectivityThird::Size() {
	return size;
}