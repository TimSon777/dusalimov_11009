#pragma once

class ConnectivityFirst {
private:
	int size;
	int* array;
public:
	ConnectivityFirst(int*, int);
	ConnectivityFirst(int);
	~ConnectivityFirst();
	int Find(int);
	void Union(int, int);
	bool AreVerticesMerged(int, int);
	int* Array();
	int Size();
};


