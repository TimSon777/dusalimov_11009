#include "pch.h"
#include "CppUnitTest.h"
#include "..\Connectivity\ConnectivityThird.cpp"
#include "..\Connectivity\ConnectivityFourth.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestsConnectivity {
	TEST_CLASS(Tests_ConnectivityThird) {
	public:
		TEST_METHOD(Test_TwoVertices_Ordinary) {
			auto connectivity = new ConnectivityThird(2);
			connectivity->UnionIfNotConnected(0, 1);
			Assert::AreEqual(true, connectivity->AreVerticesConnected(0, 1));
			connectivity->~ConnectivityThird();
		}

		TEST_METHOD(Test_FourthVertices_AreConnectedRow) {
			auto connectivity = new ConnectivityThird(4);
			connectivity->UnionIfNotConnected(0, 1);
			connectivity->UnionIfNotConnected(1, 2);
			connectivity->UnionIfNotConnected(2, 3);
			Assert::AreEqual(true, connectivity->AreVerticesConnected(0, 3));
			connectivity->~ConnectivityThird();
		}

		TEST_METHOD(Test_FourthVertices_TwoPairsConnectedVerticesNotConnected) {
			auto connectivity = new ConnectivityThird(4);
			connectivity->UnionIfNotConnected(0, 1);
			connectivity->UnionIfNotConnected(2, 3);
			Assert::AreEqual(false, connectivity->AreVerticesConnected(1, 2));
			connectivity->~ConnectivityThird();
		}

		TEST_METHOD(Test_FiveVertices_Ordinary) {
			auto connectivity = new ConnectivityThird(5);
			connectivity->UnionIfNotConnected(0, 1);
			connectivity->UnionIfNotConnected(3, 4);
			connectivity->UnionIfNotConnected(1, 4);
			Assert::AreEqual(false, connectivity->AreVerticesConnected(2, 1));
			connectivity->~ConnectivityThird();
		}

		TEST_METHOD(Test_FiveVertices_AllVerticesConnected) {
			auto connectivity = new ConnectivityThird(5);
			connectivity->UnionIfNotConnected(0, 1);
			connectivity->UnionIfNotConnected(3, 4);
			connectivity->UnionIfNotConnected(1, 4);
			connectivity->UnionIfNotConnected(1, 2);
			for (auto i = 0; i < 5; i++)
			for (auto j = i; j < 5; j++)
				if (i != j) Assert::AreEqual(true, connectivity->AreVerticesConnected(i, j));
			connectivity->~ConnectivityThird();
		}
	};

	TEST_CLASS(Tests_ConnectivityFourth) {
	public:
		TEST_METHOD(Test_TwoVertices_Ordinary) {
			auto connectivity = new ConnectivityFourth(2);
			connectivity->UnionIfNotConnected(0, 1);
			Assert::AreEqual(true, connectivity->AreVerticesConnected(0, 1));
			connectivity->~ConnectivityFourth();
		}

		TEST_METHOD(Test_FourthVertices_AreConnectedRow) {
			auto connectivity = new ConnectivityFourth(4);
			connectivity->UnionIfNotConnected(0, 1);
			connectivity->UnionIfNotConnected(1, 2);
			connectivity->UnionIfNotConnected(2, 3);
			Assert::AreEqual(true, connectivity->AreVerticesConnected(0, 3));
			connectivity->~ConnectivityFourth();
		}

		TEST_METHOD(Test_FourthVertices_TwoPairsConnectedVerticesNotConnected) {
			auto connectivity = new ConnectivityFourth(4);
			connectivity->UnionIfNotConnected(0, 1);
			connectivity->UnionIfNotConnected(2, 3);
			Assert::AreEqual(false, connectivity->AreVerticesConnected(1, 2));
			connectivity->~ConnectivityFourth();
		}

		TEST_METHOD(Test_FiveVertices_Ordinary) {
			auto connectivity = new ConnectivityFourth(5);
			connectivity->UnionIfNotConnected(0, 1);
			connectivity->UnionIfNotConnected(3, 4);
			connectivity->UnionIfNotConnected(1, 4);
			Assert::AreEqual(false, connectivity->AreVerticesConnected(2, 1));
			connectivity->~ConnectivityFourth();
		}

		TEST_METHOD(Test_FiveVertices_AllVerticesConnected) {
			auto connectivity = new ConnectivityFourth(5);
			connectivity->UnionIfNotConnected(0, 1);
			connectivity->UnionIfNotConnected(3, 4);
			connectivity->UnionIfNotConnected(1, 4);
			connectivity->UnionIfNotConnected(1, 2);
			for (auto i = 0; i < 5; i++)
			for (auto j = i; j < 5; j++)
				if (i != j) Assert::AreEqual(true, connectivity->AreVerticesConnected(i, j));
			connectivity->~ConnectivityFourth();
		}
	};
}
