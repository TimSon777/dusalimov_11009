#include "pch.h"
#include "CppUnitTest.h"
#include "..\LinkedList\LinkedList.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestsLinkedList
{
	TEST_CLASS(Tests_Add)
	{
	public:
		TEST_METHOD(Test_AddEnd) {
			auto list = new LinkedList<int>();
			list->AddEnd(1);
			list->AddEnd(2);
			list->AddEnd(3);
			auto result = true;
			for (auto i = 0; i < 3; i++)
				if ((*list)[i] != i + 1) result = false;
			Assert::AreEqual(result, true);
		}

		TEST_METHOD(Test_AddBegin) {
			auto list = new LinkedList<int>();
			list->AddEnd(2);
			list->AddEnd(3);
			list->AddBegin(1);
			auto result = true;
			for (auto i = 0; i < 3; i++)
				if ((*list)[i] != i + 1) result = false;
			Assert::AreEqual(result, true);
		}

		TEST_METHOD(Test_Insert) {
			auto list = new LinkedList<int>();
			list->AddEnd(1);
			list->AddEnd(3);
			list->Insert(2, 1);
			auto result = true;
			for (auto i = 0; i < 3; i++)
				if ((*list)[i] != i + 1) result = false;
			Assert::AreEqual(result, true);
		}
	};

	TEST_CLASS(Tests_Remove)
	{
	public:
		TEST_METHOD(Test_RemoveFirst) {
			auto list = new LinkedList<int>();
			list->AddEnd(0);
			list->AddEnd(1);
			list->AddEnd(2);
			list->AddEnd(3);
			list->RemoveFirst();
			auto result = true;
			for (auto i = 0; i < 3; i++)
				if ((*list)[i] != i + 1) result = false;
			Assert::AreEqual(result, true);
		}

		TEST_METHOD(Test_RemoveLast) {
			auto list = new LinkedList<int>();
			list->AddEnd(1);
			list->AddEnd(2);
			list->AddEnd(3);
			list->AddEnd(4);
			list->RemoveLast();
			auto result = true;
			for (auto i = 0; i < 3; i++)
				if ((*list)[i] != i + 1) result = false;
			Assert::AreEqual(result, true);
		}

		TEST_METHOD(Test_Remove) {
			auto list = new LinkedList<int>();
			list->AddEnd(1);
			list->AddEnd(2);
			list->AddEnd(4);
			list->AddEnd(3);
			list->Remove(2);
			auto result = true;
			for (auto i = 0; i < 3; i++)
				if ((*list)[i] != i + 1) result = false;
			Assert::AreEqual(result, true);
		}
	};

	TEST_CLASS(Tests_CopyConstructor) 
	{
	public:
		TEST_METHOD(Test_Remove) {
			auto list = new LinkedList<int>();
			list->AddEnd(1);
			list->AddEnd(2);
			list->AddEnd(3);
			auto copyList = new LinkedList<int>(*list);
			list->~LinkedList();
			auto result = true;
			for (auto i = 0; i < 3; i++)
				if ((*copyList)[i] != i + 1) result = false;
			Assert::AreEqual(result, true);
		}
	};
}
