#include "pch.h"
#include "CppUnitTest.h"
#include "..\\LexicographicSort\LexicographicSort.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(Tests) {
	public:
		TEST_METHOD(Test_OneWord) {
			auto arr = new std::string[1]{ "helloworld" };
			auto result = Sort(arr, 1, 10);
			Assert::AreEqual(true, result[0] == "helloworld");
			delete[] arr, result;
		}

		TEST_METHOD(Test_ThreeWords) {
			auto arr = new std::string[3]{ "timur", "artem", "tagir" };
			auto result = Sort(arr, 3, 5);
			Assert::AreEqual(true, result[0] == "artem");
			Assert::AreEqual(true, result[1] == "tagir");
			Assert::AreEqual(true, result[2] == "timur");
			delete[] arr, result;
		}

		TEST_METHOD(Test_FiveWords) {
			auto arr = new std::string[5]{ "abc", "bca", "tgh", "fgt", "fff" };
			auto result = Sort(arr, 5, 3);
			Assert::AreEqual(true, result[0] == "abc");
			Assert::AreEqual(true, result[1] == "bca");
			Assert::AreEqual(true, result[2] == "fff");
			Assert::AreEqual(true, result[3] == "fgt");
			Assert::AreEqual(true, result[4] == "tgh");
			delete[] arr, result;
		}

		TEST_METHOD(Test_FiveWords_AllWordsAreSame) {
			auto arr = new std::string[5]{ "cley", "cley", "cley", "cley", "cley" };
			auto result = Sort(arr, 5, 4);
			for (auto i = 0; i < 4; i++)
				Assert::AreEqual(true, result[i] == "cley");
			delete[] arr, result;
		}

		TEST_METHOD(Test_FiveWords_WordsConsistOneLetter) {
			auto arr = new std::string[5]{ "f", "j", "a", "b", "a" };
			auto result = Sort(arr, 5, 1);
			Assert::AreEqual(true, result[0] == "a");
			Assert::AreEqual(true, result[1] == "a");
			Assert::AreEqual(true, result[2] == "b");
			Assert::AreEqual(true, result[3] == "f");
			Assert::AreEqual(true, result[4] == "j");
			delete[] arr, result;
		}
	};
}
