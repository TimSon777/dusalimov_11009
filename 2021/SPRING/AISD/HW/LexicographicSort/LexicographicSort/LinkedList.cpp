#include <iostream>

template<typename T>
struct LinkedListItem {
public:
	T Item;
	LinkedListItem<T>* Next;

	LinkedListItem(T element) {
		Item = element;
		Next = nullptr;
	}

	//LinkedListItem<T>& operator=(const LinkedListItem<T>& e) {
	//	if (this == &e) return *this;
	//	Item = e.Item;
	//	Next = e.Next;
	//	return *this;
	//}
};

template<class T>
class LinkedList {
private:
	LinkedListItem<T>* Head;
	LinkedListItem<T>* Tail;
	int count;
public:
	LinkedList() {
		Head = Tail = nullptr;
		count = 0;
	}

	~LinkedList() {
		Clean();
	}

	LinkedListItem<T>* GetHead() {
		if (Head == nullptr) throw std::invalid_argument("The linked list is empty");
		return Head;
	}

	LinkedListItem<T>* GetTail() {
		if (Tail == nullptr) throw std::invalid_argument("The linked list is empty");
		return Tail;
	}

	int Count() {
		return count;
	}

	void AddEnd(T element) {
		auto item = new LinkedListItem<T>(element);
		if (count == 0) Head = item;
		else Tail->Next = item;
		Tail = item;
		count++;
	}

	T& operator[](const int index) {
		if (index < 0) throw std::invalid_argument("Negative index");
		if (index >= count) throw std::invalid_argument("Index out of bounds");
		auto current = Head;
		for (auto i = 0; i < index; i++)
			current = current->Next;
		return current->Item;
	}

	void Print() {
		auto current = Head;
		while (current != nullptr) {
			std::cout << current->Item << std::endl;
			current = current->Next;
		}
	}

	void Clean() {
		auto current = Head;
		auto deleteItem = Head;
		while (current != nullptr) {
			deleteItem = current;
			current = current->Next;
			delete deleteItem;
		}
	}

	void AddBegin(T element) {
		auto temp = new LinkedListItem<T>(element);
		temp->Next = Head;
		Head = temp;
		count++;
	}

	void Insert(T element, int index)
	{
		if (index < 0 || index >= count) throw std::invalid_argument("Index out of bounds");
		if (index == 0) {
			AddBegin(element);
			return;
		}
		if (index == count) {
			AddEnd(element);
			return;
		}
		auto temp = new LinkedListItem<T>(element);
		auto current = Head;
		for (auto i = 0; i < index - 1; i++)
			current = current->Next;
		temp->Next = current->Next;
		current->Next = temp;
		count++;
	}

	void Remove(int index) {
		if (index < 0) throw std::invalid_argument("Negative index");
		if (index >= count) throw std::invalid_argument("Index out of bounds");
		if (index == count - 1) {
			RemoveLast();
			return;
		}
		auto current = Head;
		auto previous = Head;
		for (auto i = 0; i < index; i++) {
			previous = current;
			current = current->Next;
		}
		previous->Next = current->Next;
		delete current;
		count--;
	}

	void RemoveFirst() {
		if (count <= 0) throw std::invalid_argument("The linked list is empty");
		if (count == 1) delete Head;
		if (count >= 2) {
			auto temp = Head;
			Head = Head->Next;
			delete temp;
		}
		count--;
	}

	/*T Dequeue() {
		if (count <= 0) throw std::invalid_argument("The linked list is empty");
		if (count == 1) {
			auto result = Head->Item;
			delete Head;
			return result;
		}
		if (count >= 2) {
			auto temp = Head;
			auto result = Head->Item;
			Head = Head->Next;
			delete temp;
			return result;
		}
		count--;
	}*/ 

	void RemoveLast() {
		if (count <= 0) throw std::invalid_argument("The linked list is empty");
		if (count == 1) delete Head;
		if (count >= 2) {
			auto current = Head;
			while (current->Next->Next != nullptr)
				current = current->Next;
			delete Tail;
			current->Next = nullptr;
			Tail = current;
		}
		count--;
	}

	LinkedList(const LinkedList& other) {
		auto currentOther = other.Head;
		for (int i = 0; i < other.count; i++) {
			AddEnd(currentOther->Item);
			currentOther = currentOther->Next;
		}
	}
};



