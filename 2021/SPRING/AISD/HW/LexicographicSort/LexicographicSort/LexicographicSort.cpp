#include <iostream>
#include <string>
#include "LexicographicSort.h"
#include "LinkedList.cpp"
#include <map>

std::string* Sort(std::string* words, int count, int wordLength) {
    auto scoops = new std::map<char, LinkedList<std::string>>;
    auto list = new LinkedList<std::string>();
    for (auto i = 0; i < count; i++)
        list->AddEnd(words[i]);

    for (auto i = wordLength - 1; i >= 0; i--) {
        while (list->Count() != 0) {
            auto temp = list->GetHead()->Item;
            (*scoops)[temp[i]].AddEnd(temp);
            list->RemoveFirst();
        }

        for (auto j = 'a'; j <= 'z'; j++) {
            auto k = (*scoops)[j].Count();
            for (auto l = 0; l < k; l++) {
                list->AddEnd((*scoops)[j].GetHead()->Item);
                (*scoops)[j].RemoveFirst();
            }
        }
    }
    auto result = new std::string[list->Count()];
    auto current = list->GetHead();
    auto j = 0;
    for (auto current = list->GetHead(); current != nullptr; current = current->Next) {
        result[j++] = current->Item;
    }
    list->~LinkedList();
    // ��� ������� map � �� ����. ��� ���-�� � ����������� �������, � �� ���� �� ����, �� ���� � ���� ��� ��� ������
    // ������ ���� �� ����������, ��� ���-�� ��� �����, ������ ��� ��� ��������� � 24 �������
    return result;
}