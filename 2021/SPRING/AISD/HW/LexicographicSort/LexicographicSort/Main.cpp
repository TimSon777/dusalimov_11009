﻿#include <iostream>
#include <string>
#include "LexicographicSort.h"

int main()
{
    auto arr = new std::string[12]{ "timurb", "timura", "timurc", "aaaaaa", "aaaaab", "landys", "truuup", "prikol", "zrikol", "zachem", "crutot" };
    auto result = Sort(arr, 11, 6);
    for (auto i = 0; i < 11; i++)
        std::cout << result[i] << std::endl;
    delete[] arr, result;
}

