﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CW
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            // var seq = Task1(new int[] {2, 3, 34, 3, 10, 4});
            // foreach (var e in seq)
            // {
            //     Console.WriteLine(e);
            // }
            // var seq2 = new[] {6, 2, 3}.JoinWithPredict<int, string, string, int>(new string[] {"wegegr", "rge", "erg"},
            //     (a, b) => a == b.Length, (a, b) => Tuple.Create(a.ToString(), b.Length));
            // foreach (var e in seq2)
            // {
            //     Console.WriteLine(e);
            // }
        }
        
        private static IEnumerable<int> Task1(IEnumerable<int> seq)
        {
            return seq
                .Select((x, i) => x * (i + 1))
                .Where(x => x.ToString().Length != 2)
                .OrderByDescending(x => x);
        }
    }

    public static class LinqExtension
    {
        public static IEnumerable<Tuple<TResult1, TResult2>> JoinWithPredict<T1, T2, TResult1, TResult2>(this IEnumerable<T1> seq1,
            IEnumerable<T2> seq2, Func<T1, T2, bool> predict,  Func<T1, T2, Tuple<TResult1, TResult2>> selector)
        {
            return from e1 in seq1 
                from e2 in seq2 
                where predict(e1, e2) 
                select selector(e1, e2);
        }
    }
}