﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqControl
{
	class Product
	{
		public string ProductNumber { get; }	
		public string Category { get; }
		public string Country { get; }

		public Product(string productNumber, string category, string country)
		{
			ProductNumber = productNumber;
			Category = category;
			Country = country;
		}
	}
}
