﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqControl
{
	class Price
	{
		public string ProductNumber { get; }
		public string StoreName { get; }
		public double PriceValue { get; }

		public Price(string productNumber, string storeName, double priceValue)
		{
			ProductNumber = productNumber;
			StoreName = storeName;
			PriceValue = priceValue;
		}
	}
}
