﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqControl
{
	class Purchase
	{
		public int CustomerId { get; }
		public string ProductNumber { get; }
		public string StoreName { get; }

		public Purchase(int customerId, string productNumber, string storeName)
		{
			CustomerId = customerId;
			ProductNumber = productNumber;
			StoreName = storeName;
		}
	}
}
