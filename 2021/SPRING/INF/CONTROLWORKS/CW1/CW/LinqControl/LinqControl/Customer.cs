﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqControl
{
	class Customer
	{
		public int Id { get; }
		public int Year { get; }
		public string Street { get; }

		public Customer(int id, int year, string street)
		{
			Id = id;
			Year = year;
			Street = street;
		}
	}
}
