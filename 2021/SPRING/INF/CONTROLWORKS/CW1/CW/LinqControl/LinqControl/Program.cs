﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqControl
{
	class Program
	{
		static void Main(string[] args)
		{
			var customers = InitCustomers();
			var products = InitProducts();
			var prices = InitPrices();
			var discounts = InitDiscounts();
			var purchases = InitPurchases();
		}

		static IEnumerable<Customer> InitCustomers()
		{
			var cusomers = new List<Customer>();

			cusomers.Add(new Customer(1, 1990, "Lenina"));
			cusomers.Add(new Customer(2, 1995, "Lenina"));
			cusomers.Add(new Customer(3, 1991, "Ulyanova"));
			cusomers.Add(new Customer(4, 2000, "Zorge"));
			cusomers.Add(new Customer(5, 2000, "Pushkina"));
			cusomers.Add(new Customer(6, 2000, "Pushkina"));
			cusomers.Add(new Customer(7, 1997, "Zorge"));
			cusomers.Add(new Customer(8, 1990, "Lenina"));
			
			return cusomers;
		}

		static IEnumerable<Product> InitProducts()
		{
			var products = new List<Product>();

			products.Add(new Product("QW122-4378", "Электротехника", "Германия"));
			products.Add(new Product("AN123-1233", "Косметика и уход", "Россия"));
			products.Add(new Product("FA542-2338", "Электроинструмент", "Китай"));
			products.Add(new Product("QA585-7384", "Обувь", "Китай"));
			products.Add(new Product("QW123-2738", "Электроинструмент", "Германия"));
			products.Add(new Product("QF367-8428", "Обувь", "Китай"));
			products.Add(new Product("HF376-3283", "Автомобиль", "Германия"));
			products.Add(new Product("MG947-2487", "Электротехника", "Россия"));
			products.Add(new Product("DF137-3902", "Электротехника", "Россия"));

			return products;
		}

		static IEnumerable<Price> InitPrices()
		{
			var prices = new List<Price>();

			prices.Add(new Price("QA585-7384", "Магазин 1", 100));
			prices.Add(new Price("QA585-7384", "Магазин 2", 99.8));
			prices.Add(new Price("AN123-1233", "Магазин 3", 1245));
			prices.Add(new Price("AN123-1233", "Магазин 2", 3562));
			prices.Add(new Price("QW123-2738", "Магазин 2", 2342));
			prices.Add(new Price("MG947-2487", "Магазин 1", 123));
			prices.Add(new Price("DF137-3902", "Магазин 2", 3324));
			prices.Add(new Price("FA542-2338", "Магазин 3", 4323));
			prices.Add(new Price("FA542-2338", "Магазин 1", 3234));
			prices.Add(new Price("FA542-2338", "Магазин 2", 6767));
			prices.Add(new Price("DF137-3902", "Магазин 3", 4366));

			return prices;
		}

		static IEnumerable<DiscountForCustomer> InitDiscounts()
		{
			var discounts = new List<DiscountForCustomer>();

			discounts.Add(new DiscountForCustomer(1, "Магазин 2", 5));
			discounts.Add(new DiscountForCustomer(1, "Магазин 1", 15));
			discounts.Add(new DiscountForCustomer(2, "Магазин 2", 5));
			discounts.Add(new DiscountForCustomer(4, "Магазин 3", 7));
			discounts.Add(new DiscountForCustomer(6, "Магазин 3", 3));
			discounts.Add(new DiscountForCustomer(8, "Магазин 1", 3));
			discounts.Add(new DiscountForCustomer(8, "Магазин 2", 3));

			return discounts;
		}
		static IEnumerable<Purchase> InitPurchases()
		{
			var purchases = new List<Purchase>();

			purchases.Add(new Purchase(1, "FA542-2338", "Магазин 2"));
			purchases.Add(new Purchase(1, "FA542-2338", "Магазин 3"));
			purchases.Add(new Purchase(2, "FA542-2338", "Магазин 2"));
			purchases.Add(new Purchase(1, "DF137-3902", "Магазин 3"));
			purchases.Add(new Purchase(1, "QA585-7384", "Магазин 1"));
			purchases.Add(new Purchase(1, "MG947-2487", "Магазин 1"));

			return purchases;
		}
	}
}
