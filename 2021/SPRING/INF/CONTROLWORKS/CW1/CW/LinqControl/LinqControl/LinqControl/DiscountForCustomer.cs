﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqControl
{
	class DiscountForCustomer
	{
		public int CustomerId { get; }
		public string StoreName { get; }
		public int Discount { get; }

		public DiscountForCustomer(int customerId, string storeName, int discount)
		{
			CustomerId = customerId;
			StoreName = storeName;
			Discount = discount;
		}
	}
}
