﻿using System;
using System.Collections.Generic;
using System.Net;

namespace QueueWithTwoStacks
{
    internal class QueueWithTwoStacks<T>
    {
        private readonly Stack<T> stackFirst = new Stack<T>();
        private readonly Stack<T> stackSecond = new Stack<T>();

        public void Enqueue(T value)
        {
            stackFirst.Push(value);
        }

        public T Dequeue()
        {
            while (stackFirst.Count != 0)
            {
                stackSecond.Push(stackFirst.Pop());
            }

            return stackSecond.Pop();
        }
    }

    internal static class Program
    {
        public static void Main(string[] args)
        {
        }
    }
}
